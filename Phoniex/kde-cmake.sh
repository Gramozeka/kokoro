#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
mkdir -p build
cd build
  cmake \
    -DCMAKE_C_FLAGS:STRING="${BUILD64}" \
    -DCMAKE_CXX_FLAGS:STRING="${BUILD64}" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DSYSCONF_INSTALL_DIR=/etc \
    -DLIB_SUFFIX=64 \
    ..
    make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
