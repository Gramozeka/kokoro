#! /bin/bash
set -e
cd /phoniex/system/Slack-src/lzip/
sh ./lzip.SlackBuild
cd /phoniex/system/Slack-src/lzlib/
sh ./lzlib.SlackBuild
cd /phoniex/system/Slack-src/lzo/
sh ./lzo.SlackBuild
cd /phoniex/system/Slack-src/plzip/
sh ./plzip.SlackBuild
cd /phoniex/system/Slack-src/lrzip/
sh ./lrzip.SlackBuild

 
