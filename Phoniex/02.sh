#!/bin/bash
autoconf-2.13
# mozjs-60.8.0
mlocate-0.26
libtirpc-1.2.6
rpcbind-1.2.5
sharutils-4.15.2
db-5.3.28
cyrus-sasl-2.1.27
openldap-2.4.53
cyrus-sasl-2.1.27-LDAP
krb5-1.18.2
gnupg-2.2.23
cracklib-2.9.7
audit-userspace-2.8.5
rpcsvc-proto-1.4.2
libnsl-1.3.0
# # # libprelude-5.2.0 лютая хрень, не использовать никогда!
lynx2.8.9rel.1
Linux-PAM-1.4.0
libpwquality-1.4.2
shadow-4.8.1
libcap-2.44
stop
Pygments-2.7.2
gtk-doc-1.33.0
glib-2.66.2
cairo-1.17.2+f93fc72c03e
gobject-introspection-1.66.1
elogind-243.7
dbus-1.12.20
dbus-glib-0.110
json-c-0.15
json-glib-1.6.0
vala-0.50.1
pycairo-1.19.1-py3
pygobject-3.38.0
libical-3.0.8
bluez-5.55
curl-7.73.0
net-tools-Phoniex
bridge-utils-1.6
openssh-8.4p1
libssh2-1.9.0
rustc-1.47.0-src
mozjs78-78.4.1
tiff-4.1.0
shared-mime-info-2.0
jasper-2.0.14
gdk-pixbuf-2.42.0
gdk-pixbuf-xlib-3116b8ae
lua-5.4.1
highlight-3.58
harfbuzz-2.7.2
brotli-1.0.9
curl-7.73.0
freetype-2.10.4
fontconfig-2.13.1
cairo-1.17.2+f93fc72c03e
libxslt-1.1.34
libxml2-2.9.10
fribidi

# # appdirs==1.4.4            # via fs
# # attrs==19.3.0             # via -r requirements.in, cattrs, statmake, ufolib2
# # booleanoperations==0.9.0  # via ufo2ft
# # cattrs==1.0.0             # via statmake
# # cffsubr==0.2.7            # via ufo2ft
# # compreffor==0.5.0         # via ufo2ft
# # cu2qu==1.6.7              # via ufo2ft
# # fontmath==0.6.0           # via -r requirements.in
# # fonttools[lxml,ufo,unicode]==4.12.0  # via -r requirements.in, booleanoperations, cffsubr, compreffor, cu2qu, fontmath, psautohint, statmake, ufo2ft, ufolib2
# # fs==2.4.11                # via fonttools
# # lxml==4.5.1               # via fonttools
# # psautohint==2.0.1         # via -r requirements.in
# # pyclipper==1.1.0.post3    # via booleanoperations
# # pytz==2020.1              # via fs
# # six==1.15.0               # via fs
# # skia-pathops==0.4.1       # via -r requirements.in
# # statmake==0.3.0           # via -r requirements.in
# # ufo2ft[cffsubr]==2.15.0   # via -r requirements.in
# # ufolib2==0.8.0            # via -r requirements.in
# # unicodedata2==13.0.0.post2  # via fonttools

attrs-20.3.0
booleanOperations-0.9.0
Cython-0.29.21
pytest-cython-0.1.0
cython-setuptools-0.2.3
cython_compiler-1.0.1
# pyenv-0.0.1 # установить вручную
skia-pathops-0.5.1.post1
statmake-0.3.0
ufoLib2-0.8.0
unicodedata2-13.0.0.post2
cantarell-fonts
pango-1.48.0
rrdtool-1.7.2
lm-sensors-3-6-0
atk-2.36.0
at-spi2-core-2.38.0
at-spi2-atk-2.38.0
graphviz-2.44.1
libgusb-0.3.5
stop
# mc-4.8.22
libexif-0.6.22
LibRaw-0.20.0
enscript-1.6.6
# ImageMagick-6.9.10-93
ImageMagick-7.0.10-27
libgudev-234
apache-ant-1.10.9
fop-2.5
hicolor-icon-theme-0.17
gtk+-2.24.32
libxkbcommon-1.0.1
libcroco-0.6.13
iso-codes-4.5.0
dbus-python-1.2.16
polkit-0.118
elogind-243.7
dbus-1.12.20
colord-1.4.4
adwaita-icon-theme-3.38.0
gtk+-3.24.23
libgxps-0.3.1
libsecret-0.20.3
pinentry-1.1.0
libdazzle-3.38.0
sysprof-3.38.1
gjs-1.66.1
fcron-3.2.1
sysstat-12.4.0
librsvg-2.50.1
check-0.15.2
libglade-2.6.4
tidy-html5-5.6.0
libidn-1.36
libidn2-2.3.0
ghostscript-9.53.3
xorg-server-1.20.9
libgudev-234
xf86-input-wacom-0.39.0
libwacom-1.5
libva-2.9.0
# # # # Xorg-drivers!!! ###################
libevdev-1.9.1
libinput-1.16.1
libvdpau-1.4
vdpauinfo-1.0
xf86-input-evdev-2.10.6
xf86-input-libinput-0.30.0
xf86-input-synaptics-1.9.1
xf86-input-vmmouse-13.1.0
xf86-input-wacom-0.39.0
xf86-video-amdgpu-19.1.0
xf86-video-ati-19.1.0
xf86-video-fbdev-0.5.0
xf86-video-intel-20200817
xf86-video-nouveau-1.0.16
libva-2.9.0
intel-vaapi-driver-2.4.1
xf86-input-acecad-1.5.0
xf86-input-joystick-1.6.3
xf86-input-keyboard-1.9.0
xf86-input-mouse-1.9.3
xf86-input-penmount-1.5.0
xf86-input-void-1.4.1
libvdpau-va-gl-0.4.0
xf86-input-keyboard-1.9.0
mesa-20.2.2
libva-2.9.0
xinit-1.4.1
xterm-360
twm-1.0.11
libwebp-1.1.0
tiff-4.1.0
links-2.21
pyxdg-0.25
libid3tag-0.15.1b
imlib2-1.7.0
libemf-1.0.13
plotutils-2.6
tcl8.6.10
libpng-1.6.37
svgalib-1.9.25
libming-ming-0_4_8
pstoedit-3.75
openjpeg-2.3.1
# autotrace-0.31.1
# ImageMagick-6.9.10-97_X11
ImageMagick-7.0.10-27_X11
GraphicsMagick-1.3.35
autotrace-3.0.0
startup-notification-0.12
openbox-3.6.1
# # weston-master
