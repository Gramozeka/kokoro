#!/bin/bash
set -e
unpack () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar  xvjf $1  2> /dev/null   ;;
      *.tar.gz)    tar xvzf $1   2> /dev/null  ;;
      *.tar.xz)    tar xvJf $1   2> /dev/null  ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1   2> /dev/null   ;;
      *.tbz2)      tar xvjf $1  2> /dev/null   ;;
      *.tgz)       tar xvzf $1  2> /dev/null   ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *.lz)        tar xvf $1   2> /dev/null   ;;
      *)           echo "\`$1': Unknown method of file compression" ;;
    esac
  else
    echo "\`$1' no found"
  fi
}

BUILD=$1
read_list=$1
arg1=$2
arg2=$3
home=$(pwd)
if [ -n "$1" ]
then
scriptsdir="Config_scripts32"
else
echo "Не указан входящий файл!"
exit 1
fi
TEMPBUILD=/phoniex/system/Building
SOURCE=/phoniex/sources
PATCHSOURCE=/phoniex/Patches
destdir=/phoniex/Base-Pack
######################################################################################
USE_ARCH="32"
ARCH="i686"
SAVE_path=${PATH}
set +h
export PATH="${PATH32}:${PATH}"
export MACHTYPE=${CLFS_TARGET32}
export CLFS_TARGET=${CLFS_TARGET32}
export PKG_CONFIG_PATH="/i686/lib/pkgconfig:/i686/share/pkgconfig"
export GOARCH=386
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
# alias gcc='/i686/bin/gcc'
# alias g++='/i686/bin/g++'
# alias cc='/i686/bin/gcc'

# # #  Влючать опционально!
# export AR="/i686/bin/ar"
# export AS="/i686/bin/as"
# export RANLIB="/i686/bin/ranlib"
# export LD="/i686/bin/ld"
# export STRIP="/i686/bin/strip"

export LD_LIBRARY_PATH="/i686/lib" 
export LDFLAGS="-L/i686/lib" 
export CPPFLAGS="-I/i686/include"
export CFLAGS="-O2 -march=i686"
export CXXFLAGS="-O2 -march=i686"
export CLFS_TARGET=$CLFS_TARGET32
# export CC="gcc -isystem /i686/include -m32"
# export CXX="g++ -isystem /i686/include -m32"
export PYTHON=/i686/bin/python3
export PERL=/i686/bin/perl
# export LLVM_INSTALL_DIR=/i686
# export CLANG_INSTALL_DIR=/i686
# export JAVA_HOME=/opt/jdk32
export CC="/i686/bin/gcc"
export CXX="/i686/bin/g++"

# export PKG_CONFIG_LIBDIR="/i686/lib:/usr/lib32"

# set +h
######################################################################################
pack () {
pkg=${destdir}/$1
if [[ $(basename $(pwd)) == "meson-build" ]]
then
DESTDIR=${pkg} ninja install
else
make DESTDIR=${pkg} install
fi
rm -rf ${pkg}/{usr,etc,var,run,tmp,lib,bin,sbin,lib64}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
if [ -d ${pkg}/i686/share/man ]; then
echo n|find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/i686/share/info ]; then
echo n|find ${pkg}/i686/share/info -type f -exec gzip -9 {} \;
fi
find ${pkg} -depth -print0  -type f -name "*.la"  -exec rm -fv {} \;
find ${pkg} -depth -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
######################################################################################
######################################################################################
while read -r line; do
source /etc/profile &&
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
if [ -n "$2" ]
then
if [ -n "$3" ]
then
case $arg2 in
"one" )
if [ $line != $arg1 ]
then
continue
else

    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
    . ${home}/${scriptsdir}/${line}.sh

break

fi
;;
"more" )
if [ $line != $arg1 ]; then continue; else
arg2="0"
fi
;;
esac
else

if [ $line != $arg1 ]
then
continue
else
    file=$(echo $line | cut -d" " -f2)
echo "второй аргумент отсутствует, будет собран только $file"
    . ${home}/${scriptsdir}/${line}.sh

break
fi
fi
fi
    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
    . ${home}/${scriptsdir}/${line}.sh
    unset -f pack_local
    unset -f pack_local32
    unset -f pack_local64

done < $1.sh
export PATH=${SAVE_path}
unset CC CXX AR AS RANLIB LD STRIP LD_LIBRARY_PATH LDFLAGS CPPFLAGS CFLAGS CXXFLAGS LLVM_INSTALL_DIR GOARCH CLANG_INSTALL_DIR
# set -h
exit
