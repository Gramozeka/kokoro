#!/bin/bash
libsigc++-2.10.3
atk-2.36.0
at-spi2-core-2.38.0
at-spi2-atk-2.38.0
atkmm-2.28.0
libsecret-0.20.3
libgxps-0.3.1
libdazzle-3.38.0
libsoup-2.72.0
gjs-1.66.0
gsettings-desktop-schemas-3.38.0
gcr-3.38.0
libnotify-0.7.9
GConf-3.2.6
dconf-0.38.0
dconf-editor-3.38.0
gtksourceview-4.8.0
glade-3.38.0
libpeas-1.28.0
libmediaart-1.9.4
gvfs-1.46.0
gedit-3.38.0
gnome-autoar-0.2.4
tracker-2.3.6
tracker-master
tracker-miners-master
libgsf-1.14.47
totem-pl-parser-3.26.5
libcue-2.2.1
tracker-miners-2.3.5
gmime-3.2.7
gnome-desktop-3.38.0
nautilus-3.38.1
libgweather-3.36.1
gnome-terminal-3.38.1
gnome-settings-daemon-3.38.1
libnma-1.8.30
mm-common-1.0.1
# # # libdmapsharing-3.9.7
# # # totem-pl-parser-3.26.4
# # # gjs-1.64.0
# # # gnome-autoar-0.2.4
# # # gnome-desktop-3.36.0
polkit-gnome-0.105
gnome-menus-3.36.0
gnome-video-effects-0.5.0
gnome-online-accounts-3.38.0
grilo-0.3.13
yelp-3.38.1
gnome-backgrounds-3.38.0
gvfs-1.46.0
# # # gexiv2-0.12.0
# # # bubblewrap-0.4.0
# # # nautilus-3.36.0
zenity-3.32.0
gnome-bluetooth-3.34.1
# # # gnome-keyring-3.36.0
# # # gnome-settings-daemon-3.36.0
gsound-1.0.2
cogl-1.22.8
clutter-1.26.4
clutter-gst-3.0.27
clutter-gtk-1.8.4
libchamplain-0.12.20
# # # liboauth-1.0.3
uhttpmock-0.5.3
libgdata-0.17.13
libgee-0.20.3
libgtop-2.40.0
# # # libpeas-1.24.1
# # # accountsservice-0.6.55
libhandy-v0.0.13
libhandy-1.0.0
# # # network-manager-applet-1.8.24
cldr-emoji-annotation
ibus-1.5.22
gnome-color-manager-3.36.0
gnome-control-center-3.38.1
# # # graphene-1.10.0
# # # jack2-1.9.14
# # # pipewire-0.3.0
mutter-3.38.1
evolution-data-server-3.38.1
bogofilter-1.2.5
seahorse-3.36.2
evolution-3.38.1
gnome-shell-3.38.1
gnome-shell-extensions-3.38.1
gnome-session-3.38.0
gdm-3.38.0
gnome-tweaks-3.34.0
gfbgraph-0.2.4
gnome-user-docs-3.38.1
file-roller-3.38.0
gnome-nettool-3.8.1
evince-3.38.0
libgnomekbd-3.26.1
gnome-clocks-3.38.0
#gnome-panel.git
folks-0.14.0
gnome-maps-3.38.0
gnome-calculator-3.38.0
eog-3.38.0
gnome-font-viewer-3.34.0
gnome-screenshot-3.38.0
GPaste-3.36.4
gnome-system-monitor-3.38.0
gnome-weather-3.36.1
gnome-disk-utility-3.38.0
gnome-power-manager-3.32.0
libxmlb-0.2.1
libgphoto2-2.5.25
# gnome-software-master

