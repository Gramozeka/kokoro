# polkit-0.118
# glib-2.66.0
# dbus-1.12.20
# elogind-243.7
#  fuse-3.9.3
# dosfstools-4.1
# parted-3.3
# btrfs-progs-v5.7
udisks-2.9.1
# gtk+-2.24.32
# gtk-engines-2.20.2
# gtk+-3.24.23
#gtk-xfce-engine-2.10.1
#dbus-glib-0.110
#dbus-python-1.2.16
gcc-10.2.0
lz4-1.9.2_i686
zstd-1.4.5
