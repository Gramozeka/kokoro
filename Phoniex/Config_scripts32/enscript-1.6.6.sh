#!/bin/bash
###############################################################################
set -e
###############################################################################
# Enscript converts ASCII text files to PostScript, HTML, RTF, ANSI and overstrikes.
# This package is known to build and work properly using an LFS-9.0 platform.
# [Caution] Caution
# Enscript cannot convert UTF-8 encoded text to PostScript. The issue is discussed in detail in the Needed Encoding Not a Valid Option section of the Locale Related Issues page. The solution is to use paps-0.7.1, instead of Enscript, for converting UTF-8 encoded text to PostScript.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/enscript/enscript-1.6.6.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/enscript/enscript-1.6.6.tar.gz
# Download MD5 sum: 3acc242b829adacabcaf28533f049afd
# Download size: 1.3 MB
# Estimated disk space required: 14 MB
# Estimated build time: 0.1 SBU
# Enscript Dependencies
# Optional
# texlive-20190410 (or install-tl-unx)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
            --sysconfdir=/etc/enscript \
            --with-media=Letter \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
