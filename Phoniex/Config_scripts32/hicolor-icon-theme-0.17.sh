#!/bin/bash
###############################################################################
set -e
###############################################################################
# The hicolor-icon-theme package contains a default fallback theme for implementations of the icon theme specification.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://icon-theme.freedesktop.org/releases/hicolor-icon-theme-0.17.tar.xz
# Download MD5 sum: 84eec8d6f810240a069c731f1870b474
# Download size: 52 KB
# Estimated disk space required: 340 KB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
