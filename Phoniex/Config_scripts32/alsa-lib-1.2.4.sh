#!/bin/bash
###############################################################################
set -e
# The ALSA Library package contains the ALSA library used by programs (including ALSA Utilities) requiring access to the ALSA sound interface.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.alsa-project.org/files/pub/lib/alsa-lib-1.2.1.2.tar.bz2
# Download (FTP): ftp://ftp.alsa-project.org/pub/lib/alsa-lib-1.2.1.2.tar.bz2
# Download MD5 sum: 82ddd3698469beec147e4f4a67134ea0
# Download size: 981 KB
# Estimated disk space required: 35 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# ALSA Library Dependencies
# Optional
# Doxygen-1.8.17 and Python-2.7.17
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vfi
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
make doc
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
