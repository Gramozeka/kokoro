#!/bin/bash
###############################################################################
set -e
# libunistring is a library that provides functions for manipulating Unicode strings and for manipulating C strings according to the Unicode standard.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/libunistring/libunistring-0.9.10.tar.xz
# Download (FTP): ftp://ftp.gnu.org/gnu/libunistring/libunistring-0.9.10.tar.xz
# Download MD5 sum: db08bb384e81968957f997ec9808926e
# Download size: 2.0 MB
# Estimated disk space required: 49 MB (add 43 MB for tests)
# Estimated build time: 0.9 SBU (add 1.0 SBU for tests)
# libunistring Dependencies
# Optional
# texlive-20190410 (or install-tl-unx) (to rebuild the documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
LT_SYS_LIBRARY_PATH=/i686/lib \
PERL=/i686/bin/perl \
PYTHON=/i686/bin/python3 \
./configure --prefix=/i686 \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
