#!/bin/bash
###############################################################################
set -e
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-64//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
if (/i686/bin/pkg-config --exists libsasl2) 
then 
LDAP="--with-ldap"
fi
cd src &&
set -e
sed -e 's@\^u}@^u cols 300}@' \
    -i tests/dejagnu/config/default.exp &&

sed -e '/eq 0/{N;s/12 //}' \
    -i plugins/kdb/db2/libdb2/test/run.test &&

autoconf &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
            --localstatedir=/var/lib \
            --with-system-et         \
            --with-system-ss         \
            --with-system-verto=no   \
            --enable-dns-for-realm $LDAP \
--build=${CLFS_TARGET32} &&
make -j4
pkg=${destdir}/${packagedir}
make install  DESTDIR=${pkg} && echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
cd ${pkg}

for f in gssapi_krb5 gssrpc k5crypto kadm5clnt kadm5srv \
         kdb5 kdb_ldap krad krb5 krb5support verto ; do

    find ${pkg}/i686/lib -type f -name "lib$f*.so*" -exec chmod -v 755 {} \;    
done          &&

find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
