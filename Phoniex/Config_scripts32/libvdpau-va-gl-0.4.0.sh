#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=/i686 \
-DLIB_SUFFIX="" \
-DCMAKE_CXX_FLAGS="${BUILD32}"  \
-DCMAKE_C_FLAGS="${BUILD32}" \
.. &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
