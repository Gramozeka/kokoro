#!/bin/bash
###############################################################################
set -e
# The libusb package contains a library used by some applications for USB device access.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com//libusb/libusb/releases/download/v1.0.23/libusb-1.0.23.tar.bz2
# Download MD5 sum: 1e29700f6a134766d32b36b8d1d61a95
# Download size: 592 KB
# Estimated disk space required: 6.4 MB
# Estimated build time: less than 0.1 SBU
# libusb Dependencies
# Optional
# Doxygen-1.8.17
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "s/^PROJECT_LOGO/#&/" doc/doxygen.cfg.in &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
