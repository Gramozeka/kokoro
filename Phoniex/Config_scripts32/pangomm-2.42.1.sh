#!/bin/bash
###############################################################################
set -e
# The Pangomm package provides a C++ interface to Pango.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/pangomm/2.42/pangomm-2.42.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/pangomm/2.42/pangomm-2.42.0.tar.xz
# Download MD5 sum: 6cffedf2225c4e72645a7d757fb5b832
# Download size: 836 KB
# Estimated disk space required: 16 MB
# Estimated build time: 0.3 SBU
# Pangomm Dependencies
# Required
# Cairomm-1.12.2, GLibmm-2.62.0 and Pango-1.44.7
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/^libdocdir =/ s/$(book_name)/pangomm-2.42.1/' \
    -i docs/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
