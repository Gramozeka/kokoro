#!/bin/bash
###############################################################################
set -e
# libusb-compat: libusb-compat (Compatibility library for libusb-0.1 apps)
# libusb-compat:
# libusb-compat: A compatibility layer allowing applications written for libusb-0.1 to
# libusb-compat: work with libusb-1.0. libusb-compat-0.1 attempts to retain as much
# libusb-compat: ABI and API compatibility with libusb-0.1 as possible.
# libusb-compat:
# libusb-compat: Homepage: http://libusb.info
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
gcc -v
