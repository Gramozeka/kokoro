#!/bin/bash
###############################################################################
set -e
# The Doxygen package contains a documentation system for C++, C, Java, Objective-C, Corba IDL and to some
# extent PHP, C# and D. It is useful for generating HTML documentation and/or an off-line reference manual
# from a set of documented source files. There is also support for generating output in RTF, PostScript,
# hyperlinked PDF, compressed HTML, and Unix man pages. The documentation is extracted directly from the
# sources, which makes it much easier to keep the documentation consistent with the source code.
# You can also configure Doxygen to extract the code structure from undocumented source files. This is very
# useful to quickly find your way in large source distributions. Used along with Graphviz, you can also visualize
# the relations between the various elements by means of include dependency graphs, inheritance diagrams,
# and collaboration diagrams, which are all generated automatically.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://doxygen.nl/files/doxygen-1.8.17.src.tar.gz
# Download MD5 sum: 7997a15c73a8bd6d003eaba5c2ee2b47
# Download size: 4.9 MB
# Estimated disk space required: 159 MB (with tests)
# Estimated build time: 0.9 SBU (using parallelism=4; with tests)
# Doxygen Dependencies
# Required
# CMake-3.16.3 and git-2.25.0
# Optional
# Graphviz-2.42.3, ghostscript-9.50, libxml2-2.9.10 (required for the tests), LLVM-9.0.1 (with clang), Python-2.7.17, Qt-5.14.0 (for doxywizard), texlive-20190410 (or install-tl-unx), and xapian-1.4.14 (for
# doxyindexer)
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -v build &&
cd       build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -G "Unix Makefiles"         \
      -DCMAKE_BUILD_TYPE=Release  \
      -DCMAKE_INSTALL_PREFIX=/i686 \
      -Wno-dev .. &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
