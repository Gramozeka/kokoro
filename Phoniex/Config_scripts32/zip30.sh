#!/bin/bash
###############################################################################
# The Zip package contains Zip utilities. These are useful for compressing files into ZIP archives.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/infozip/zip30.tar.gz
# Download (FTP): ftp://ftp.info-zip.org/pub/infozip/src/zip30.tgz
# Download MD5 sum: 7b74551e63f8ee6aab6fbc86676c0d37
# Download size: 1.1 MB
# Estimated disk space required: 6.4 MB
# Estimated build time: 0.1 SBU
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's@CC=gcc CPP="gcc -E"@CC=/i686/bin/gcc CPP="i686/bin/gcc -E"c@g' unix/Makefile
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -f unix/Makefile generic_gcc

pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}/i686/share/man/man1
mkdir -pv ${pkg}/i686/bin
make prefix=${pkg} MANDIR=/i686/share/man/man1 -f unix/Makefile install && echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
mv -vf ${pkg}/bin/* ${pkg}/i686/bin/
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
