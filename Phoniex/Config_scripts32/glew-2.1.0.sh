#!/bin/bash
###############################################################################
set -e
# GLEW is the OpenGL Extension Wrangler Library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/glew/glew-2.1.0.tgz
# Download MD5 sum: b2ab12331033ddfaa50dc39345343980
# Download size: 747 KB
# Estimated disk space required: 16 MB
# Estimated build time: less than 0.1 SBU
# glew Dependencies
# Required
# Mesa-19.3.2
###############################################################################
###############################################################################
pack_local32 () {
pkg=${destdir}/$1
make install DESTDIR=${pkg}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
cd build
/i686/bin/cmake \
-DCMAKE_INSTALL_PREFIX=/i686 \
./cmake
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
CC="/i686/bin/gcc" CXX="/i686/bin/g++" \
setarch i386 make -j5
pack_local32 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
