#!/bin/bash
###############################################################################
set -e
###############################################################################

###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

make install-man  DESTDIR=${pkg}                                               &&
ln -v -sf openjade ${pkg}/i686/bin/jade                               &&
ln -v -sf libogrove.so ${pkg}/i686/lib/libgrove.so                    &&
ln -v -sf libospgrove.so ${pkg}/i686/lib/libspgrove.so                &&
ln -v -sf libostyle.so ${pkg}/i686/lib/libstyle.so                    &&
mkdir -pv ${pkg}/i686/share/sgml/${packagedir}
install -v -m644 dsssl/catalog ${pkg}/i686/share/sgml/${packagedir}/ &&

install -v -m644 dsssl/*.{dtd,dsl,sgm}              \
    ${pkg}/i686/share/sgml/${packagedir}  

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/openjade-1.3.2-upstream-1.patch
sed -i -e '/getopts/{N;s#&G#g#;s#do .getopts.pl.;##;}' \
       -e '/use POSIX/ause Getopt::Std;' msggen.pl
export CXXFLAGS="$CXXFLAGS -fno-lifetime-dse"            &&
./configure --prefix=/i686 \

 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
            --mandir=/i686/share/man                      \
            --enable-http                                \
            --enable-default-catalog=/etc/sgml/catalog   \
            --enable-default-search-path=/i686/share/sgml \
            --datadir=/i686/share/sgml/${packagedir}   &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
install-catalog --add /etc/sgml/openjade-1.3.2.cat  \
    /i686/share/sgml/openjade-1.3.2/catalog                     &&

install-catalog --add /etc/sgml/sgml-docbook.cat    \
    /etc/sgml/openjade-1.3.2.cat
echo "SYSTEM \"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd\" \
    \"/i686/share/xml/docbook/xml-dtd-4.5/docbookx.dtd\"" >> \
    /i686/share/sgml/openjade-1.3.2/catalog
    
