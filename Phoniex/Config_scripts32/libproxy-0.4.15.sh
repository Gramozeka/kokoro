
###############################################################################
set -e
###############################################################################
# https://github.com/libproxy/libproxy
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
	mkdir build
	cd    build
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
  cmake \
    -DCMAKE_INSTALL_PREFIX=/i686 \
    -DLIB_INSTALL_DIR=/i686/lib \
    -DPYTHON3_EXECUTABLE:FILEPATH=/i686/bin/python3 \
    -DPYTHON3_SITEPKG_DIR=/i686/lib/python3.9/site-packages \
    -DPERL_EXECUTABLE:FILEPATH=/i686/bin/perl \
    -DPERL_INCLUDE_PATH:PATH=/i686/lib/perl5/CORE \
    -DPERL_LIBRARY:FILEPATH=/i686/lib/perl5/CORE/libperl.so \
    -DWITH_GNOME3:BOOL=OFF \
    -DWITH_KDE:BOOL=OFF \
    -DGAC_DIR=/i686/lib/mono \
    -DWITH_VALA=yes \
    -DWITH_DOTNET=yes \
    -DPERL_VENDORINSTALL=yes \
    -DCMAKE_BUILD_TYPE=Release \
    ..
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
