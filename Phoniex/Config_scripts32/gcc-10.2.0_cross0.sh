#!/bin/bash
#######################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install-gcc install-target-libgcc
make
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_cross0*$//" <<< $(basename $line))
package="$packagedir.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${SOURCE}/gcc-10.2.0-specs-1_i686.diff
# #zcat ${SOURCE}/gcc-no_fixincludes.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1
# #patch -Np1 -i ${SOURCE}/gcc-graphite.diff
# set +h
# echo -en '\n#undef STANDARD_STARTFILE_PREFIX_1\n#define STANDARD_STARTFILE_PREFIX_1 "/i686/lib/"\n' >> gcc/config/linux.h
# echo -en '\n#undef STANDARD_STARTFILE_PREFIX_2\n#define STANDARD_STARTFILE_PREFIX_2 ""\n' >> gcc/config/linux.h
# touch /i686/include/limits.h
mkdir -v ../gcc-build
cd ../gcc-build
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/i686/lib/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/i686/lib/ \
 LDFLAGS="-L/i686/lib:/usr/lib" \
CC="gcc -m32" \
CXX="g++ -m32" \
AR=ar \
../${packagedir}/configure \
    --prefix=/i686 \
    --build=${CLFS_TARGET32} \
    --host=${CLFS_TARGET32} \
    --target=${CLFS_TARGET32} \
    --with-sysroot=/i686 \
    --with-local-prefix=/i686 \
    --disable-shared \
    --with-mpfr=/i686 \
    --with-gmp=/i686 \
    --with-mpc=/i686 \
    --with-isl=/i686 \
             LD=ld                    \
             --enable-languages=c,c++ \
             --disable-multilib       \
             --disable-bootstrap      \
             --with-system-zlib \
    --with-glibc-version=2.11
make -j4 all-gcc all-target-libgcc

echo "###########################**** COMPLITE!!! ****##################################"
set -h
pack
