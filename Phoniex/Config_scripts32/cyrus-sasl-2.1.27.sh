#!/bin/bash
###############################################################################
set -e
# The Cyrus SASL package contains a Simple Authentication and Security Layer, a method for adding authentication support to connection-based protocols. To use SASL, a protocol includes a command for identifying and authenticating a user to a server and for optionally negotiating protection of subsequent protocol interactions. If its use is negotiated, a security layer is inserted between the protocol and the connection.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/cyrusimap/cyrus-sasl/releases/download/cyrus-sasl-2.1.27/cyrus-sasl-2.1.27.tar.gz
# Download MD5 sum: a33820c66e0622222c5aefafa1581083
# Download size: 3.9 MB
# Estimated disk space required: 26 MB
# Estimated build time: 0.1 SBU
# Cyrus SASL Dependencies
# Recommended
# Berkeley DB-5.3.28
# Optional
# Linux-PAM-1.3.1, MIT Kerberos V5-1.17.1, MariaDB-10.4.11 or MySQL, OpenJDK-12.0.2, OpenLDAP-2.4.48, PostgreSQL-12.1, SQLite-3.31.1, krb4 and Dmalloc
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/i686/lib
make sasldir=/i686/lib/sasl2 DESTDIR=${pkg} install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoreconf -fi &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
            --sysconfdir=/etc    \
              --localstatedir=/var \
              --mandir=/i686/share/man \
              --disable-static \
            --enable-auth-sasldb \
            --with-dblib=gdbm \
            --enable-plain \
            --with-dbpath=/var/lib/sasl/sasldb2 \
            --with-saslauthd=/var/run/saslauthd \
            --enable-anon --enable-login \
            --with-openssl \
            --enable-cram --enable-digest --enable-otp  --host=${CLFS_TARGET}
make -j1 sasldir=/i686/lib/sasl2
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
ldconfig
