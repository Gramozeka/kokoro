#!/bin/bash
###############################################################################
set -e
# libsamplerate is a sample rate converter for audio.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.mega-nerd.com/SRC/libsamplerate-0.1.9.tar.gz
# Download MD5 sum: 2b78ae9fe63b36b9fbb6267fad93f259
# Download size: 4.1 MB
# Estimated disk space required: 17 MB
# Estimated build time: 0.1 SBU
# libsamplerate Dependencies
# Optional
# libsndfile-1.0.28 and fftw-3.3.8 (for tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
