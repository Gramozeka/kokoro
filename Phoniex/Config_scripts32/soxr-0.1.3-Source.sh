
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
###############################################################################
	unpack ${SOURCE}/${package}
	cd ${packagedir}
	mkdir build
	cd    build
	cmake    \
	-DCMAKE_C_FLAGS="$BUILD32"               \
	-DCMAKE_CXX_FLAGS="$BUILD32"          \
	-DCMAKE_INSTALL_PREFIX=/i686                \
	-DCMAKE_INSTALL_LIBDIR=/i686/lib    \
	-DLIB_INSTALL_DIR=/i686/lib                   \
	-DBUILD_SHARED_LIBS=ON                         \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX=""                                           \
	-DBUILD_TESTING=OFF                                  \
	-Wno-dev ..
	make -j4 || make || exit 1

	pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


















