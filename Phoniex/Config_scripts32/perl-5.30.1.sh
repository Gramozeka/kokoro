#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
# patch -Np1 -i ${SOURCE}/perl.configure.multilib.patch
# sed -i -e '/libc/s#/lib/#/lib/#' hints/linux.sh
echo 'installstyle="lib/perl5"' >> hints/linux.sh
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./Configure -des \
  -Dprefix=/i686 \
  -Darchlib="/i686/lib/perl5" \
  -Dvendorprefix=/i686 \
  -Dinstallprefix=/i686 \
  -Dusethreads -Duseithreads \
  -Duseshrplib \
 -Dcc="gcc ${BUILD64}" \
  -Ubincompat5005 \
  -Uversiononly \
                    -Dman1dir=/i686/share/man/man1 \
                  -Dman3dir=/i686/share/man/man3 \
  -Dpager='/i686/bin/less -isr' \
  -Darchname=i686-linux || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

#    -Dldflags="-L/lib:/i686/lib:/usr/lib" \

#   -Dlibpth="/i686/local/lib /i686/lib /lib" \
