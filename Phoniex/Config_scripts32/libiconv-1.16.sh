
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} LIBDIR="/i686/lib" install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
mv ${pkg}/i686/include/{iconv.h,libiconv.h}
  mv -vf ${pkg}/i686/bin/{iconv,libiconv}
  mv -vf ${pkg}/i686/share/man/man1/{,lib}iconv.1
  mv -vf ${pkg}/i686/share/man/man3/{,libiconv_}iconv.3
  mv -vf ${pkg}/i686/share/man/man3/{,libiconv_}iconvctl.3
  mv -vf ${pkg}/i686/share/man/man3/{,libiconv_}iconv_open.3
  mv -vf ${pkg}/i686/share/man/man3/{,libiconv_}iconv_close.3
  mv -vf ${pkg}/i686/share/man/man3/{,libiconv_}iconv_open_into.3
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed '/LD_RUN_PATH/d' -i Makefile.in
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET32} \
--docdir=/i686/share/doc/${packagedir}
cp -fv /i686/include/stdio.h srclib/stdio.in.h
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
