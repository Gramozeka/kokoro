#!/bin/bash
###############################################################################
set -e
# libtasn1 is a highly portable C library that encodes and decodes DER/BER data following an ASN.1 schema.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/libtasn1/libtasn1-4.15.0.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/libtasn1/libtasn1-4.15.0.tar.gz
# Download MD5 sum: 33e3fb5501bb2142184238c815b0beb8
# Download size: 1.7 MB
# Estimated disk space required: 11 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# libtasn1 Dependencies
# Optional
# GTK-Doc-1.32 and Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
