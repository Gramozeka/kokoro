#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
rm -f src/elf.h
# patch -Np1 -i ${PATCHSOURCE}/
if [ ! -r configure ]; then
  ./bootstrap.sh
fi
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--build=${CLFS_TARGET32} \
--docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
