#!/bin/bash
set -e
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
  if [ -d ${pkg}/i686/lib/X11/fonts ]; then
mkdir -p ${pkg}/i686/share
mv ${pkg}/i686/lib/X11/fonts ${pkg}/i686/share
fi
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}

###############################################################################
######################################################################################
while read -r line; do

    # Get the file name, ignoring comments and blank lines
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
    file=$(echo $line | cut -d" " -f2)

    temp_val=$(echo $file|sed 's|^.*/||')          # Remove directory
    packagedir=$(echo $temp_val|sed 's|\.tar.*||') # Package directory
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/Xorg/font/${file}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
 \

   --localstatedir=/var \
--disable-static ${CONF_INDIVIDUAL}
# \
# --build=${CLFS_TARGET} &&
make -j4

pack_local ${packagedir}
ldconfig
done < ${home}/${scriptsdir}/Xorg-font-7.sh

