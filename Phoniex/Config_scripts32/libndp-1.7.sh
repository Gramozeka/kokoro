
###############################################################################
set -e
###############################################################################
# The libndp package provides a wrapper for IPv6 Neighbor Discovery Protocol. It also provides a tool named ndptool for sending and receiving NDP messages.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://libndp.org/files/libndp-1.7.tar.gz
# Download MD5 sum: ea4a2a3351991c1d561623772364ae14
# Download size: 356 KB
# Estimated disk space required: 2.4 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
