set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/libmusicbrainz-5.1.0-cmake_fixes-1.patch
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake   \
-DCMAKE_INSTALL_PREFIX=/i686 \
 -DCMAKE_INSTALL_LIBDIR=/i686/lib \
-DCMAKE_LIB_INSTALL_DIR=/i686/lib \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev ..
make -j4 
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
