#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
# LINKER=$(readelf -l /tools/bin/bash | sed -n 's@.*interpret.*/tools\(.*\)]$@\1@p')
# sed -i "s|libs -o|libs -L/i686/lib -Wl,-dynamic-linker=${LINKER} -o|" \
#   scripts/test-installation.pl
# unset LINKER
apply_patches() {
  zcat ${SOURCE}/glibc.locale.no-archive.diff.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc.ru_RU.CP1251.diff.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc-c-utf8-locale.patch.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc-2.29.en_US.no.am.pm.date.format.diff.gz | patch -p1 --verbose || exit 1
}
patch -Np1 -i ${SOURCE}/glibc-2.32-fhs-1.patch
apply_patches
if [ ! $? = 0 ]; then
  exit 1
fi
mkdir -v ../glibc-build
cd ../glibc-build
# CC="gcc -m32" CXX="g++ -m32" \
../${packagedir}/configure \
    --prefix=/i686 \
    --enable-kernel=3.2.0   \
    --libexecdir=/i686/lib/glibc \
    -with-headers=/i686/include              \
    --enable-stack-protector=strong \
    --with-cpu=i686 \
    --enable-add-ons \
  --enable-obsolete-nsl \
  --enable-obsolete-rpc \
  --disable-werror      \
   libc_cv_slibdir=/i686/lib
make -j4
sed '/test-installation/s@$(PERL)@echo not running@' -i ../${packagedir}/Makefile
pack_local ${packagedir}
