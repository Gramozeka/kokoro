#!/bin/bash
###############################################################################
set -e
# The Apache Portable Runtime Utility Library provides a predictable and consistent interface to underlying client library interfaces. This application programming interface assures predictable if not identical behaviour regardless of which libraries are available on a given platform.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.apache.org/dist/apr/apr-util-1.6.1.tar.bz2
# Download (FTP): ftp://ftp.mirrorservice.org/sites/ftp.apache.org/apr/apr-util-1.6.1.tar.bz2
# Download MD5 sum: 8ff5dc36fa39a2a3db1df196d3ed6086
# Download size: 420 KB
# Estimated disk space required: 6.5 MB (add 1.4 MB for tests)
# Estimated build time: less than 0.1 SBU (add 0.3 SBU for tests)
# Apr Util Dependencies
# Required
# Apr-1.7.0
# Optional
# Berkeley DB-5.3.28, FreeTDS, MariaDB-10.4.11 or MySQL, OpenLDAP-2.4.48, PostgreSQL-12.1, SQLite-3.30.1 and unixODBC-2.3.7
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir} \
            --with-apr=/i686     \
            --with-gdbm=/i686    \
            --with-openssl=/i686 \
            --with-crypto \
             --with-ldap \
            --with-berkeley-db=/i686 &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
