#!/bin/bash
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install &&
make DESTDIR=${pkg} soinstall &&
mkdir -pv ${pkg}/i686/include/ghostscript
install -v -m644 base/*.h ${pkg}/i686/include/ghostscript 
ln -v -s ghostscript ${pkg}/i686/include/ps
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
du --all | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
rm -rf freetype lcms2 jpeg libpng
rm -rf zlib &&
# 
# patch -Np1 -i $PATCHSOURCE/ghostscript-9.27-upstream_fixes-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
            --disable-compile-inits \
            --enable-dynamic        \
            --with-system-libtiff   \
--build=${CLFS_TARGET} &&
make -j4
make so
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
