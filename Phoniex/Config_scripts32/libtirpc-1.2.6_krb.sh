#!/bin/bash
###############################################################################
set -e
# The libtirpc package contains libraries that support programs that use the Remote Procedure Call (RPC) API. It replaces the RPC, but not the NIS library entries that used to be in glibc.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/libtirpc/libtirpc-1.2.5.tar.bz2
# Download MD5 sum: 688787ddff7c6a92ef15ae3f5dc4dfa1
# Download size: 504 KB
# Estimated disk space required: 8.3 MB
# Estimated build time: 0.1 SBU
# libtirpc Dependencies
# Optional
# MIT Kerberos V5-1.17.1 for the GSSAPI
###############################################################################
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_krb*$//" <<< $(basename $line))
package="$packagedir.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
chmod +x ./autogen.sh
setarch i386 ./autogen.sh --prefix=/i686
# patch -Np1 -i ${PATCHSOURCE}/
setarch i386 ./configure --prefix=/i686 \
--libdir=/i686/lib \
--with-sysroot=/i686 \
--oldincludedir=/i686/include \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET32} \
--docdir=/i686/share/doc/${packagedir} \
GSSAPI_CFLAGS="-L/i686/lib -I/i686/include" \
GSSAPI_LIBS="-L/i686/lib -Wl,--enable-new-dtags -Wl,-rpath -Wl,/i686/lib -lgssapi_krb5 -lkrb5 -lk5crypto -lcom_err"
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
