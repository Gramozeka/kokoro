
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/i686/lib
./waf --destdir=${pkg} install 
  chown root:audio ${pkg}/i686/bin/*
  chmod 0750 ${pkg}/i686/bin/*
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
chown -R root:root .
find -L .  -perm /111 -a \! -perm 755 -a -exec chmod 755 {} \+ -o \
        \! -perm /111 -a \! -perm 644 -a -exec chmod 644 {} \+

# Minor source of incompatibility between jack 1.x and 2.x: some jack
# apps don't add -lpthread to their CFLAGS because they assume jack.pc
# will already include it (it does, in 1.x). So make 2 act like 1:
sed -i '/^Libs:/s,$, -lpthread,' jack.pc.in
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./waf configure \
  --prefix=/i686 \
  --libdir=/i686/lib \
  --classic \
  --dbus \
  --alsa 
./waf build
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
