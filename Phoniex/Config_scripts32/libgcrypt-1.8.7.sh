#!/bin/bash
###############################################################################
set -e
# The libgcrypt package contains a general purpose crypto library based on the code used in GnuPG. The library provides a high level interface to cryptographic building blocks using an extendable and flexible API.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-1.8.5.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/libgcrypt/libgcrypt-1.8.5.tar.bz2
# Download MD5 sum: 348cc4601ca34307fc6cd6c945467743
# Download size: 2.9 MB
# Estimated disk space required: 44 MB (with tests; add 8 MB for building docs)
# Estimated build time: 0.3 SBU (with docs; add 0.5 SBU for tests)
# libgcrypt Dependencies
# Required
# libgpg-error-1.36
# Optional
# Pth-2.0.7 and texlive-20190410 (or install-tl-unx)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
