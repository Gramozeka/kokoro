mkdir -pv /i686/X11R6
cd /i686
ln -sfv X11R6 X11
ln -sfv ../bin X11R6/bin
ln -sfv ../include X11R6/include
ln -sfv ../../i686/lib X11R6/lib
ln -sfv ../lib X11R6/lib
ln -sfv ../libexec X11R6/libexec
ln -sfv ../man X11R6/man
ln -sfv ../share X11R6/share
cd $home
