#!/bin/bash
###############################################################################


###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}
unpack ${SOURCE}/${package}
cd ${packagedir}
chown -R root:root .
find . -perm 666 -exec chmod 644 {} \;
find . -perm 664 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
find . -name "*.h" -exec chmod 644 {} \;
zcat ${PATCHSOURCE}/svgalib.prefix.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-kernel-2.6.26.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib.nohelper.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-round_gtf_gtfcalc_c.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-vga_getmodenumber.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-quickmath-h-redefinitions.patch.gz | patch -p1 --verbose || exit 1
sed -i 's@usr/include@i686/include@g' Makefile
sed -i 's@usr/lib@i686/lib@g' Makefile
sed -i 's@CC	= gcc@CC	= /i686/bin/gcc@g' Makefile.cfg
sed -i 's@AR	  = ar@AR	  = /i686/bin/ar@g' Makefile.cfg
echo 'NO_ASM = y' >> Makefile.cfg
export ARCH=i386
CC=/i686/bin/gcc \
make install includedir=/i686/include \
  prefix=/i686 \
  mandir=/i686/share/man \
  sharedlibdir=/i686/lib \
  ARCH=i386 \
NO_HELPER=y || exit 1
ARCH=i386 \
CC=/i686/bin/gcc \
make install \
 TOPDIR=${pkg} \
  prefix=${pkg}/i686 \
  mandir=${pkg}/i686/share/man \
  sharedlibdir=${pkg}/i686/lib \
  MANFORMAT=compressed \
  ARCH=i386 \
  NO_HELPER=y \
  || exit 1

# Build demos:

# make demoprogs || exit 1

# Install demos:

# cd $(basename $(pwd))/demos
# mkdir -p ${pkg}/i686/share/svgalib-demos
#   # this will produce a harmless error... hey, some of these demos might come back, right?rwpage bad
#   cp fun testgl speedtest mousetest vgatest scrolltest testlinear keytest testaccel accel forktest eventtest spin bg_test printftest joytest mjoytest bankspeed lineart linearspeed addmodetest svidtune linearfork cursor vgatweak buildcsr  \
#   linuxlogo.bitmap \
#   ${pkg}/i686/share/svgalib-demos
#   chmod 755 ${pkg}/i686/share/svgalib-demos/*
# cd ..

# Make sure the package contains all library symlinks:
cd ${pkg}/i686/lib
  ldconfig -l *

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home

echo "###########################**** COMPLITE!!! ****##################################"
