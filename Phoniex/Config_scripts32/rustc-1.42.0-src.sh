###############################################################################
set -e
###############################################################################
# The Rust programming language is designed to be a safe, concurrent, practical language.
# This package is updated on a six-weekly release cycle. Because it is such a large and slow package to build, and is at the moment only required by a few packages in this book, the BLFS editors take the view that it should only be updated when that is necessary (either to fix problems, or to allow a new version of firefox to build).
# As with many other programming languages, rustc (the rust compiler) needs a binary from which to bootstrap. It will download a stage0 binary and many cargo crates (these are actually .tar.gz source archives) at the start of the build, so you cannot compile it without an internet connection.
# These crates will then remain in various forms (cache, directories of extracted source), in ~/.cargo for ever more. It is common for large rust packages to use multiple versions of some crates. If you purge the files before updating this package, very few crates will need to be updated by the packages in this book which use it (and they will be downloaded as required). But if you retain an older version as a fallback i686ion and then use it (when not building in /i686), it is likely that it will then have to re-download some crates. For a full download (i.e. starting with an empty or missing ~/.cargo) downloading the external cargo files for this version only takes a minute or so on a fast network.
# [Note] Note
# Although BLFS usually installs in /i686, when you later upgrade to a newer version of rust the old libraries in /i686/lib/rustlib will remain, with various hashes in their names, but will not be usable and will waste space. The editors recommend placing the files in the /i686 directory. In particular, if you have reason to rebuild with a modified configuration (e.g. using the shipped LLVM after building with shared LLVM, but perhaps also the reverse situation) it it possible for the install to leave a broken cargo program. In such a situation, either remove the existing installation first, or use a different prefix such as /i686/rustc-1.42.0-build2.
# If you prefer, you can of course change the prefix to /i686 and omit the ldconfig and the actions to add rustc to the PATH.
# The current rustbuild build-system will use all available processors, although it does not scale well and often falls back to just using one core while waiting for a library to compile.
# At the moment Rust does not provide any guarantees of a stable ABI.
# [Note] Note
# Rustc defaults to building for ALL supported architectures, using a shipped copy of LLVM. In BLFS the build is only for the X86 architecture. Rustc still claims to require Python 2, but that is only really necessary when building some other architectures with the shipped LLVM. If you intend to develop rust crates, this build may not be good enough for your purposes.
# The build times of this version when repeated on the same machine are often reasonably consistent, but as with all compilations using rustc there can be some very slow outliers.
# Unusually, a DESTDIR-style method is being used to install this package. This is because running the install as root not only downloads all of the cargo files again (to /root/.cargo), it then spends a very long time recompiling. Using this method saves a lot of time, at the cost of extra disk space.
# This package is known to build and work properly using an LFS-9.0 platform.
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
###############################################################################
pkg=${destdir}/${packagedir}

mkdir -pv ${pkg}/i686/rustc-1.42.0
mkdir -pv /i686/rustc-1.42.0
ln -svfin rustc-1.42.0 ${pkg}/i686/rustc
ln -svfin rustc-1.42.0 /i686/rustc
cat << EOF > config.toml
# see config.toml.example for more possible i686ions
# See the 8.4 book for an example using shipped LLVM
# e.g. if not installing clang, or using a version before 8.0.
[llvm]
# by default, rust will build for a myriad of architectures
targets = "X86"

# When using system llvm prefer shared libraries
link-shared = true

[build]
# omit docs to save time and space (default is to build them)
docs = false

# install cargo as well as rust
extended = true

[install]
prefix = "/i686/rustc-1.42.0"
docdir = "share/doc/rustc-1.42.0"

[rust]
channel = "stable"
rpath = false

# BLFS does not install the FileCheck executable from llvm,
# so disable codegen tests
codegen-tests = false

[target.x86_64-unknown-linux-gnu]
# NB the output of llvm-config (i.e. help i686ions) may be
# dumped to the screen when config.toml is parsed.
llvm-config = "/i686/bin/llvm-config"

[target.i686-unknown-linux-gnu]
# NB the output of llvm-config (i.e. help i686ions) may be
# dumped to the screen when config.toml is parsed.
llvm-config = "/i686/bin/llvm-config"


EOF
USE_ARCH="32"
ARCH="i686"

# unset PKG_CONFIG_PATH
CLFS_TARGET=${CLFS_TARGET32}
AR="/i686/bin/ar"
AS="/i686/bin/as"
RANLIB="/i686/bin/ranlib"
LD="/i686/bin/ld"
STRIP="/i686/bin/strip"
PKG_CONFIG_PATH="/i686/lib/pkgconfig:/i686/share/pkgconfig"
LD_LIBRARY_PATH="/i686/lib" 
LDFLAGS="-L/i686/lib" 
CPPFLAGS="-I/i686/include" 
CFLAGS="-O2 -march=i686"
CXXFLAGS="-O2 -march=i686"
LLVM_INSTALL_DIR=/i686 \
CLANG_INSTALL_DIR=/i686 \
JAVA_HOME=/opt/jdk32 \
GOARCH=386 \
PKG_CONFIG_LIBDIR=/i686/lib \
LC_COLLATE=C \
LANG=C \
LC_ALL=POSIX \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS
export PATH="${PATH32}:$PATH"
export MACHTYPE=i686-Phoniex-linux
export RUSTFLAGS="$RUSTFLAGS -C link-args=-lffi" &&
CC="/i686/bin/gcc" \
CXX="/i686/bin/g++" \
/i686/bin/python3 ./x.py build --exclude src/tools/miri

export LIBSSH2_SYS_USE_PKG_CONFIG=1 &&
DESTDIR=${pkg} /i686/bin/python3 ./x.py install &&
unset LIBSSH2_SYS_USE_PKG_CONFIG

# chown -R root:root install &&
# cp -avr install/* ${pkg}
chown -R root:root ${pkg}
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
# find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

cd ${pkg} &&
mkdir -p i686/tree-info
du --all | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-$ARCH-$BUILD-tree
rm -rf ${pkg}/{usr,etc,var,run,tmp,lib,bin,sbin,lib64}
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home

###############################################################################
###############################################################################
echo "###########################**** COMPLITE!!! ****##################################"
