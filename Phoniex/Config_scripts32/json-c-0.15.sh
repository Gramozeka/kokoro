#!/bin/bash
###############################################################################
set -e
# The JSON-C implements a reference counting object model that allows you to easily construct JSON objects in C, output them as JSON formatted strings and parse JSON formatted strings back into the C representation of JSON objects.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://s3.amazonaws.com/json-c_releases/releases/json-c-0.13.1.tar.gz
# Download MD5 sum: 04969ad59cc37bddd83741a08b98f350
# Download size: 620 KB
# Estimated disk space required: 5.0 MB (add 6.0 MB for tests)
# Estimated build time: less than 0.1 SBU (add 1.7 SBU for tests)
###############################################################################
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake   \
-DCMAKE_C_FLAGS="$BUILD32" \
-DCMAKE_CXX_FLAGS="$BUILD32" \
-DCMAKE_INSTALL_PREFIX=/i686 \
-DCMAKE_INSTALL_LIBDIR=/i686/lib \
-DLIB_INSTALL_DIR=lib \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev ..
make -j4 || make || exit 1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
