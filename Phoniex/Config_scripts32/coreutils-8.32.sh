#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
USE_ARCH="32"
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${SOURCE}/coreutils-8.32-i18n-1.patch
sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk
autoreconf -fiv
FORCE_UNSAFE_CONFIGURE=1 \
setarch i386 ./configure \
    --prefix=/i686 \
    --enable-no-install-program=kill,uptime \
    --build=${CLFS_TARGET32} TIME_T_32_BIT_OK=yes

FORCE_UNSAFE_CONFIGURE=1 setarch i386 make -j4

pack ${packagedir}

###############################################################################
