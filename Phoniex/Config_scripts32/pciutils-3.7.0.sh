#!/bin/bash
###############################################################################
set -e
###############################################################################
# The PCI Utils package contains a set of programs for listing PCI devices, inspecting their status and setting their configuration registers.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/software/utils/pciutils/pciutils-3.6.2.tar.xz
# Download MD5 sum: 77963796d1be4f451b83e6da28ba4f82
# Download size: 340 KB
# Estimated disk space required: 3.5 MB
# Estimated build time: less than 0.1 SBU
# pciutils Dependencies
# Recommended
# cURL-7.68.0, Wget-1.20.3, or Lynx-2.8.9rel.1 (for the update-pciids script to function correctly).
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make install install-lib \
  PREFIX=/i686 \
  SBINDIR=/i686/sbin \
  LIBDIR=/i686/lib \
  MANDIR=/i686/man \
  IDSDIR=/i686/share/hwdata \
  PCI_IDS=pci.ids \
  PCI_COMPRESSED_IDS=0 \
  SHARED=yes \
  OPT="$BUILD32" \
  DESTDIR=${pkg} || exit 1
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's@CROSS_COMPILE=@CROSS_COMPILE=/i686/bin/@g' Makefile
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make PREFIX=/i686                \
     SHAREDIR=/i686/share/hwdata \
     SHARED=yes
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
