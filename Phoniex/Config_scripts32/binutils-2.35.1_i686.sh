#!/bin/bash
#######################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_i686*$//" <<< $(basename $line))
package="${packagedir}.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
mkdir -v ../binutils-build
cd ../binutils-build
ARCH="32"
../${packagedir}/configure \
    --prefix=/i686 \
    --enable-shared \
    --libdir=/i686/lib \
    --enable-gold=yes \
    --enable-plugins \
    --with-system-zlib \
    --enable-64-bit-bfd \
    --enable-threads \
     --enable-install-libiberty \
    --build=${CLFS_TARGET32}
make  -j4 tooldir=/i686
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
