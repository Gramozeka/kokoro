#!/bin/bash
set -e
###############################################################################
# The Xorg Server is the core of the X Window system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.x.org/pub/individual/xserver/xorg-server-1.20.7.tar.bz2
# Download (FTP): ftp://ftp.x.org/pub/individual/xserver/xorg-server-1.20.7.tar.bz2
# Download MD5 sum: d2e96355ad47244c675bce38db2b48a9
# Download size: 6.0 MB
# Estimated disk space required: 257 MB (with tests)
# Estimated build time: 0.8 SBU (using parallelism=4; with tests)
# Xorg Server Dependencies
# Required
# Pixman-0.38.4 and Xorg Fonts (only font-util), and at runtime: xkeyboard-config-2.28
# Recommended
# elogind-241.4, libepoxy-1.5.4 (needed for glamor and Xwayland), Polkit-0.116 (runtime), Wayland-1.17.0 (needed for Xwayland), and wayland-protocols-1.18
# Optional
# acpid-2.0.32 (runtime), Doxygen-1.8.17 (to build API documentation), fop-2.4 (to build documentation), Nettle-3.5.1, libgcrypt-1.8.5, xcb-util-keysyms-0.4.0, xcb-util-image-0.4.0, xcb-util-renderutil-0.3.9, xcb-util-wm-0.4.1 (all three to build Xephyr), xmlto-0.0.28 (to build documentation), libunwind, and xorg-sgml-doctools (to build documentation)
###############################################################################
DEF_FONTPATH="/usr/share/fonts/Speedo,/usr/share/fonts/TTF,/usr/share/fonts/OTF,/usr/share/fonts/Type1,/usr/share/fonts/terminus,/usr/share/fonts/misc,/usr/share/fonts/75dpi/:unscaled,/usr/share/fonts/100dpi/:unscaled,/usr/share/fonts/75dpi,/usr/share/fonts/100dpi,/usr/share/fonts/cyrillic,/usr/share/fonts/cantarell,/usr/share/fonts/ghostscript"
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}

BUILD_SERVERS="--enable-xorg \
  --enable-dmx \
  --enable-xvfb \
  --enable-xnest \
  --enable-glamor \
  --enable-kdrive \
  --enable-xephyr \
  --enable-xfbdev \
  --enable-config-udev \
  --enable-kdrive \
  --disable-config-hal \
   --enable-xf86bigfont"
#   --disable-systemd-logind"
###################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/Xorg/xorg-server/${package}
cd ${packagedir}
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure \
  --prefix=/i686 \
  --libdir=/i686/lib \
  --localstatedir=/var \
   --enable-suid-wrapper \
  --with-int10=x86emu \
  --infodir=/i686/info \
  --disable-static \
  --with-pic \
  --with-os-name="Phoniex" \
  --with-os-vendor="CLFS" \
  $BUILD_SERVERS --with-default-font-path="${DEF_FONTPATH}" \
  --with-xkb-output=/var/lib/xkb \
--build=${CLFS_TARGET} 
  sed -i -e 's#-ldl##' hw/xfree86/Makefile
  sed -i -e 's#-lm#-lm -ldl#' hw/xfree86/Makefile
make -j4
export PATH=$SAVE_PATH
pack_local64 ${packagedir}

