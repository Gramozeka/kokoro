#!/bin/bash
###############################################################################
set -e
###############################################################################
# The librsvg package contains a library and tools used to manipulate, convert and view Scalable Vector Graphic (SVG) images.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/librsvg/2.46/librsvg-2.46.4.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/librsvg/2.46/librsvg-2.46.4.tar.xz
# Download MD5 sum: aca4001b267de77924b786886a1ad64a
# Download size: 12 MB
# Estimated disk space required: 849 MB (add 375 MB for tests)
# Estimated build time: 1.7 SBU (add 1.2 SBU for tests)
# librsvg Dependencies
# Required
# gdk-pixbuf-2.40.0, libcroco-0.6.13, Cairo-1.17.2+f93fc72c03e, Pango-1.44.7, and rustc-1.37.0
# Recommended
# gobject-introspection-1.62.0 and Vala-0.46.5
# Optional
# GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# ./autogen.sh
# patch -Np1 -i ${PATCHSOURCE}/
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
  --enable-vala=yes \
  --enable-static=no \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# gdk-pixbuf-query-loaders --update-cache
