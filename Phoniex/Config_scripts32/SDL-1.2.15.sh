#!/bin/bash
###############################################################################
set -e
# The Simple DirectMedia Layer (SDL for short) is a cross-platform library designed to make it easy to write multimedia software, such as games and emulators.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.libsdl.org/release/SDL-1.2.15.tar.gz
# Download MD5 sum: 9d96df8417572a2afb781a7c4c811a85
# Download size: 3.8 MB
# Estimated disk space required: 40 MB
# Estimated build time: 0.6 SBU
# SDL Dependencies
# Optional
# AAlib-1.4rc5, ALSA-1.2.1, GLU-9.0.1, NASM-2.14.02, PulseAudio-13.0, Pth-2.0.7, X Window System, DirectFB, GGI, libcaca, PicoGUI, and SVGAlib
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/_XData32/s:register long:register _Xconst long:' \
    -i src/video/x11/SDL_x11sym.h &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
