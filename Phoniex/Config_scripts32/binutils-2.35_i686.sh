#!/bin/bash
#######################################################
set -e
###############################################################################
###############################################################################
pack_local () {
pkg=${destdir}/$1
make tooldir=/i686 DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
du --all | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_i686*$//" <<< $(basename $line))
package="${packagedir}.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
mkdir -v ../binutils-build
cd ../binutils-build
ARCH="32"
../${packagedir}/configure \
    --prefix=/i686 \
    --enable-shared \
    --libdir=/i686/lib \
    --enable-gold=yes \
    --enable-plugins \
    --with-system-zlib \
    --enable-threads \
     --enable-install-libiberty \
    --build=${CLFS_TARGET32}
make  -j4 tooldir=/i686
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
