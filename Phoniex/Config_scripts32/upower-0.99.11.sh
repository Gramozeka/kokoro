#!/bin/bash
###############################################################################
set -e
# The UPower package provides an interface to enumerating power devices, listening to device events and querying history and statistics. Any application or service on the system can access the org.freedesktop.UPower service via the system message bus.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gitlab.freedesktop.org/upower/upower/uploads/93cfe7c8d66ed486001c4f3f55399b7a/upower-0.99.11.tar.xz
# Download MD5 sum: abe6acb617f11f2e8dbd9846fcf86e24
# Download size: 424 KB
# Estimated disk space required: 11 MB (add 3MB for tssts)
# Estimated build time: 0.1 SBU (add 0.1 SBU for tests)
# UPower Dependencies
# Required
# dbus-glib-0.110, libgudev-233, libusb-1.0.23, and Polkit-0.116
# Optional (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# GTK-Doc-1.32, PyGObject-3.34.0, umockdev, and python-dbusmock (for part of the testsuite).
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--enable-deprecated  \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
