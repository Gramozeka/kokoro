#!/bin/bash
###############################################################################
set -e
# NASM (Netwide Assembler) is an 80x86 assembler designed for portability and modularity. It includes a disassembler as well.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.nasm.us/pub/nasm/releasebuilds/2.14.02/nasm-2.14.02.tar.xz
# Download MD5 sum: 6390bd67b07ff1df9fe628b6929c0353
# Download size: 812 KB
# Estimated disk space required: 32 MB
# Estimated build time: 0.2 SBU
# Additional Downloads
# Optional documentation: http://www.nasm.us/pub/nasm/releasebuilds/2.14.02/nasm-2.14.02-xdoc.tar.xz
# NASM Dependencies
# Optional (for generating documentation):
# asciidoc-8.6.9 and xmlto-0.0.28
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS="-O2" \
AR="/i686/bin/ar" \
AS="/i686/bin/as" \
RANLIB="/i686/bin/ranlib" \
LD="/i686/bin/ld" \
STRIP="/i686/bin/strip" \
CC="/i686/bin/gcc" \
PYTHON3=/i686/bin/python3 \
PERL=/i686/bin/perl \
setarch i386 ./configure --prefix=/i686
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
