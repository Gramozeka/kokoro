#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

rm -rf ${pkg}/{etc,i686,sbin,var}
rm -rf ${pkg}/usr/{include,sbin,share}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_libs*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
# LINKER=$(readelf -l /tools/bin/bash | sed -n 's@.*interpret.*/tools\(.*\)]$@\1@p')
# sed -i "s|libs -o|libs -L/usr/lib -Wl,-dynamic-linker=${LINKER} -o|" \
#   scripts/test-installation.pl
# unset LINKER
apply_patches() {
  zcat ${SOURCE}/glibc.locale.no-archive.diff.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc.ru_RU.CP1251.diff.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc-c-utf8-locale.patch.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc-2.29.en_US.no.am.pm.date.format.diff.gz | patch -p1 --verbose || exit 1
}
patch -Np1 -i ${SOURCE}/glibc-2.32-fhs-1.patch
apply_patches
if [ ! $? = 0 ]; then
  exit 1
fi
mkdir -v ../glibc-build
cd ../glibc-build
USE_ARCH="32"
PATH=$PATH32:$PATH \
CC="gcc ${BUILD32}" \
CXX="g++ ${BUILD32}" \
../${packagedir}/configure \
    --prefix=/usr \
    --bindir=/i686/bin \
    --sbindir=/i686/sbin \
    --enable-kernel=3.2.0                 \
    --libexecdir=/usr/lib/glibc \
    --host=${CLFS_TARGET32} \
    --enable-stack-protector=strong \
    --with-headers=/i686/include            \
         --enable-add-ons \
  --enable-obsolete-nsl \
  --enable-obsolete-rpc \
  --disable-werror                       \
   libc_cv_slibdir=/lib

make -j4
sed '/test-installation/s@$(PERL)@echo not running@' -i ../${packagedir}/Makefile
pack_local ${packagedir}_libsonly
