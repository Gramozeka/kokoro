#!/bin/bash
###############################################################################
set -e
# The Haveged package contains a daemon that generates an unpredictable stream of random numbers and feeds the /dev/random device.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/haveged/haveged-1.9.2.tar.gz
# Download MD5 sum: fb1d8b3dcbb9d06b30eccd8aa500fd31
# Download size: 484 KB
# Estimated disk space required: 20 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
