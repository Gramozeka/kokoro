#!/bin/bash
###############################################################################
set -e
# The Fontconfig package contains a library and support programs used for configuring and customizing font access.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/fontconfig/release/fontconfig-2.13.1.tar.bz2
# Download MD5 sum: 36cdea1058ef13cbbfdabe6cb019dc1c
# Download size: 1.6 MB
# Estimated disk space required: 16 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# Fontconfig Dependencies
# Required
# FreeType-2.10.1
# Optional
# DocBook-utils-0.6.14 and libxml2-2.9.10, texlive-20190410 (or install-tl-unx)
# [Note] Note
# If you have DocBook Utils installed and you remove the --disable-docs parameter from the configure command below, you must have SGMLSpm-1.1 and texlive-20190410 installed also, or the Fontconfig build will fail.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
rm -f src/fcobjshash.h
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--disable-docs \
--docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
