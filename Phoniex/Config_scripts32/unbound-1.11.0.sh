#!/bin/bash
###############################################################################
set -e
###############################################################################
# Unbound is a validating, recursive, and caching DNS resolver. It is designed as a set of modular components that incorporate modern features, such as enhanced security (DNSSEC) validation, Internet Protocol Version 6 (IPv6), and a client resolver library API as an integral part of the architecture.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.unbound.net/downloads/unbound-1.9.6.tar.gz
# Download MD5 sum: e6423d68e293ffec953477ef1adbbfb7
# Download size: 5.4 MB
# Estimated disk space required: 52 MB (with docs; add 9 MB for tests)
# Estimated build time: 0.4 SBU (Using parallelism=4; with docs; add 0.3 SBU for tests)
# Unbound Dependencies
# Optional
# libevent-2.1.11, Nettle-3.5.1, Python-2.7.17, SWIG-4.0.1 (for Python bindings), Doxygen-1.8.17 (for html documentation), dnstap, and Sphinx (for Python bindings documentation)
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/i686/bin
mv -v ${pkg}/i686/sbin/unbound-host ${pkg}/i686/bin/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET32}  \
--docdir=/i686/share/doc/${packagedir} \
--with-pidfile=/run/unbound.pid &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

