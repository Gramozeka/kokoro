#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's:\\\${:\\\$\\{:' intltool-update.in
./configure \
--libdir=/i686/lib \
    --prefix=/i686
make
pack ${packagedir}

echo "###########################**** COMPLITE!!! ****##################################"
