#!/bin/bash
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
./autogen.sh
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
sed -i '5043,5044 d' libtool 
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
