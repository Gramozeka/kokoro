#!/bin/bash
###############################################################################
set -e
# The LibTIFF package contains the TIFF libraries and associated utilities. The libraries are used by many programs for reading and writing TIFF files and the utilities are used for general work with TIFF files.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://download.osgeo.org/libtiff/tiff-4.1.0.tar.gz
# Download MD5 sum: 2165e7aba557463acc0664e71a3ed424
# Download size: 2.3 MB
# Estimated disk space required: 15 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# LibTIFF Dependencies
# Recommended
# CMake-3.16.3
# Optional
# Freeglut-3.2.1 (required for tiffgt), libjpeg-turbo-2.0.4, libwebp-1.1.0, JBIG-KIT, and Zstd
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} /i686/bin/ninja install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -p libtiff-build &&
cd       libtiff-build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/cmake -DCMAKE_INSTALL_DOCDIR=/i686/share/doc/${packagedir} \
      -DCMAKE_INSTALL_PREFIX=/i686 \
      -DLIB_SUFFIX="" \
      -G Ninja .. &&
 /i686/bin/ninja -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
