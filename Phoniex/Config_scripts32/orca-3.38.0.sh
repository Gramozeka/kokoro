
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
#unpack ${SOURCE}/${package}
git clone https://gitlab.gnome.org/GNOME/orca.git ${packagedir}
cd ${packagedir}
PYTHON=/i686/bin/python3.9 ./autogen.sh
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHON=/i686/bin/python3.9 ./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
