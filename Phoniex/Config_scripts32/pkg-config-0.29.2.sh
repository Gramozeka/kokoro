#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET32} \
            --with-internal-glib       \
            --disable-host-tool \
            PKG_CONFIG_PATH="/i686/lib/pkgconfig:/i686/share/pkgconfig" \
            LT_SYS_LIBRARY_PATH=/i686/lib:/usr/lib32 \
--docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
