#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install_doc_dir=/i686/share/doc/slang-2.3.2   \
     SLSH_DOC_DIR=/i686/share/doc/slang-2.3.2/slsh \
     install-all &&

chmod -v 755 ${pkg}/i686/lib/libslang.so.2.3.2 \
             ${pkg}/i686/lib/slang/v2/modules/*.so
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir} \
--with-readline=gnu 
make -j1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
