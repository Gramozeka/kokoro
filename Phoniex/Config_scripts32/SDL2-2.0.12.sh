#!/bin/bash
###############################################################################
set -e
# The Simple DirectMedia Layer Version 2 (SDL2 for short) is a cross-platform library designed to make it easy to write multimedia software, such as games and emulators.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.libsdl.org/release/SDL2-2.0.10.tar.gz
# Download MD5 sum: 5a2114f2a6f348bdab5bf52b994811db
# Download size: 5.3 MB
# Estimated disk space required: 169 MB (with docs)
# Estimated build time: 0.4 SBU (using parallelism=4; with docs)
# Additional Downloads
# Required patch (for i686 systems): http://www.linuxfromscratch.org/patches/blfs/svn/SDL2-2.0.10-opengl_include_fix-1.patch
# SDL2 Dependencies
# Optional
# ALSA-1.2.1, Doxygen-1.8.17 (to create documentation), ibus-1.5.21, NASM-2.14.02, PulseAudio-13.0, X Window System, DirectFB, and fcitx
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
