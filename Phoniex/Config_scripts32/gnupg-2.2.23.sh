#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GnuPG package is GNUs tool for secure communication and data storage. It can be used to encrypt data and to create digital signatures. It includes an advanced key management facility and is compliant with the proposed OpenPGP Internet standard as described in RFC2440 and the S/MIME standard as described by several RFCs. GnuPG 2 is the stable version of GnuPG integrating support for OpenPGP and S/MIME.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.19.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/gnupg/gnupg-2.2.19.tar.bz2
# Download MD5 sum: cb3b373d08ba078c325299945a7f2818
# Download size: 6.4 MB
# Estimated disk space required: 117 MB (with all tests; add 24 MB for docs)
# Estimated build time: 0.4 SBU (using parallelism=4; add 0.6 SBU for tests)
# GnuPG 2 Dependencies
# Required
# Libassuan-2.5.3, libgcrypt-1.8.5, Libksba-1.3.5, and npth-1.6
# Recommended
# pinentry-1.1.0 (Run-time requirement for most of the packages functionality)
# Optional
# cURL-7.68.0, Fuse-3.9.0, GnuTLS-3.6.11.1, ImageMagick-7.0.8-60 (for the convert utility, used for generating the documentation), libusb-1.0.23, an MTA, OpenLDAP-2.4.48, SQLite-3.31.0, texlive-20190410 (or install-tl-unx), fig2dev (for generating documentation), and GNU adns
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/noinst_SCRIPTS = gpg-zip/c sbin_SCRIPTS += gpg-zip' \
    -i tools/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PKG_CONFIG_PATH=/i686/lib/pkgconfig \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--enable-symcryptrun     \
--with-libgcrypt-prefix=/i686/ \
--with-libgpg-error-prefix=/i686 \
--with-libassuan-prefix=/i686/ \
--with-ksba-prefix=/i686/ \
 --with-npth-prefix=/i686/ \
--enable-g13 \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
