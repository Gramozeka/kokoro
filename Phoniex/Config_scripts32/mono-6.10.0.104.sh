#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS="-march=i686" \
CXXFLAGS="-march=i686" \
./configure --prefix=/i686 \
--with-sysroot=/i686 \
PYTHON=/i686/bin/python3 \
LT_SYS_LIBRARY_PATH=/i686/lib \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
