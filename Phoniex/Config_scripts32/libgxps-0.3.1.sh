#!/bin/bash
###############################################################################
set -e
# The libgxps package provides an interface to manipulate XPS documents.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libgxps/0.3/libgxps-0.3.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libgxps/0.3/libgxps-0.3.1.tar.xz
# Download MD5 sum: ade83c264b3af2551a0dff9144478df8
# Download size: 92 KB
# Estimated disk space required: 6.0 MB
# Estimated build time: 0.1 SBU
# Libgxps Dependencies
# Required
# GTK+-3.24.13, Little CMS-2.9, libarchive-3.4.1, libjpeg-turbo-2.0.4, LibTIFF-4.1.0, and libxslt-1.1.34
# Optional
# git-2.25.0 and GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
  export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
