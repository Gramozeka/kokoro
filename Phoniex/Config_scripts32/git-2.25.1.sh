#!/bin/bash
###############################################################################
set -e
###############################################################################
# Git is a free and open source, distributed version control system designed to handle everything from small to very large projects with speed and efficiency. Every Git clone is a full-fledged repository with complete history and full revision tracking capabilities, not dependent on network access or a central server. Branching and merging are fast and easy to do. Git is used for version control of files, much like tools such as Mercurial-5.2.2, Bazaar, Subversion-1.13.0, CVS, Perforce, and Team Foundation Server.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/software/scm/git/git-2.25.0.tar.xz
# Download MD5 sum: eab5cdee7c478c0804346d8835e314c9
# Download size: 5.6 MB
# Estimated disk space required: 315 MB (with downloaded documentation)
# Estimated build time: 0.3 SBU (with parallelism=4; add 1.9 SBU for tests with parallelism=4)
# Additional Downloads
# https://www.kernel.org/pub/software/scm/git/git-manpages-2.25.0.tar.xz (not needed if you've installed asciidoc-8.6.9, xmlto-0.0.28, and prefer to rebuild them)
# https://www.kernel.org/pub/software/scm/git/git-htmldocs-2.25.0.tar.xz and other docs (not needed if you've installed asciidoc-8.6.9 and want to rebuild the documentation).
# Git Dependencies
# Recommended
# cURL-7.68.0 (needed to use Git over http, https, ftp or ftps)
# Optional
# pcre2-10.34 (or the deprecated PCRE-8.43), in either case configured with --enable-jit, Python-2.7.17, Subversion-1.13.0 with Perl bindings (for git svn), Tk-8.6.10 (gitk, a simple Git repository viewer, uses Tk at runtime), and Valgrind-3.15.0
# Optional (to create the man pages, html docs and other docs)
# xmlto-0.0.28 and asciidoc-8.6.9 or AsciiDoctor, and also dblatex (for the PDF version of the user manual), and docbook2x to create info pages
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make DESTDIR=${pkg} install-man
make DESTDIR=${pkg} htmldir=/i686/share/doc/${packagedir} install-html
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--with-gitconfig=/etc/gitconfig \
--docdir=/i686/share/doc/${packagedir}
make -j4
make html
make man
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
