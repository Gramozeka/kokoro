
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $TEMPBUILD/$VERSION/source
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
VERSION=$(echo $packagedir | rev | cut -f 1- -d . | cut -f 1 -d - | rev)

unpack ${SOURCE}/${package}
cd $VERSION/source

cd glslang
for i in $(find . -name CMakeLists.txt); do
  sed -i "s|DESTINATION lib|DESTINATION \${CMAKE_INSTALL_LIBDIR}|" "$i"
done
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/i686 \
             -DCMAKE_INSTALL_LIBDIR=lib \
             -DCMAKE_BUILD_TYPE=Release         \
             ..
make -j4 || make || exit 1
pack_local64 glslang-$VERSION
echo "###########################**** COMPLITE!!! ****##################################"
cd SPIRV-Headers
for i in $(find . -name CMakeLists.txt); do
  sed -i "s|DESTINATION lib|DESTINATION \${CMAKE_INSTALL_LIBDIR}|" "$i"
done
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/i686 \
             -DCMAKE_INSTALL_LIBDIR=lib \
             -DCMAKE_BUILD_TYPE=Release         \
             ..
make -j4 || make || exit 1
pack_local64 SPIRV-Headers-$VERSION
echo "###########################**** COMPLITE!!! ****##################################"
cd Vulkan-Headers
# for i in $(find . -name CMakeLists.txt); do
#   sed -i "s|DESTINATION lib|DESTINATION \${CMAKE_INSTALL_LIBDIR}|" "$i"
# done
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/i686 \
             -DCMAKE_INSTALL_LIBDIR=lib \
             -DCMAKE_BUILD_TYPE=Release         \
             ..
make -j4 || make || exit 1
pack_local64 Vulkan-Headers-$VERSION
echo "###########################**** COMPLITE!!! ****##################################"
cd Vulkan-Loader
# for i in $(find . -name CMakeLists.txt); do
#   sed -i "s|DESTINATION lib|DESTINATION \${CMAKE_INSTALL_LIBDIR}|" "$i"
# done
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/i686 \
             -DCMAKE_INSTALL_LIBDIR=lib \
             -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_WSI_WAYLAND_SUPPORT=On \
    -DBUILD_WSI_MIR_SUPPORT=Off \
             ..
make -j4 || make || exit 1
pack_local64 Vulkan-Loader-$VERSION
echo "###########################**** COMPLITE!!! ****##################################"
cd Vulkan-ValidationLayers
# for i in $(find . -name CMakeLists.txt); do
#   sed -i "s|DESTINATION lib|DESTINATION \${CMAKE_INSTALL_LIBDIR}|" "$i"
# done
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/i686 \
             -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_DATADIR=/share \
    -DCMAKE_SKIP_RPATH=True \
    -DBUILD_TESTS=Off \
    -DBUILD_WSI_XLIB_SUPPORT=On \
    -DBUILD_WSI_XCB_SUPPORT=On \
    -DBUILD_WSI_WAYLAND_SUPPORT=On \
    -DBUILD_WSI_MIR_SUPPORT=Off \
    -DGLSLANG_INSTALL_DIR=/i686 \
    -DVULKAN_HEADERS_INSTALL_DIR=/i686/include/vulkan \
    -DSPIRV_HEADERS_INSTALL_DIR=/i686/include/spirv \
    -DSPIRV_TOOLS_INSTALL_DIR=/i686 \
             ..
make -j4 || make || exit 1
pack_local64 Vulkan-ValidationLayers-$VERSION
echo "###########################**** COMPLITE!!! ****##################################"
cd Vulkan-Tools
# for i in $(find . -name CMakeLists.txt); do
#   sed -i "s|DESTINATION lib|DESTINATION \${CMAKE_INSTALL_LIBDIR}|" "$i"
# done
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/i686 \
             -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_WSI_WAYLAND_SUPPORT=On \
    -DBUILD_WSI_MIR_SUPPORT=Off \
    -DVULKAN_HEADERS_INSTALL_DIR=/i686 \
    -DGLSLANG_INSTALL_DIR=/i686 \
    -DVULKAN_LOADER_INSTALL_DIR=/i686 \
             ..
make -j4 || make || exit 1
pack_local64 Vulkan-Tools-$VERSION
echo "###########################**** COMPLITE!!! ****##################################"
cd $home





















