#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Avahi package is a system which facilitates service discovery on a local network.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/lathiat/avahi/releases/download/v0.7/avahi-0.7.tar.gz
# Download MD5 sum: d76c59d0882ac6c256d70a2a585362a6
# Download size: 1.3 MB
# Estimated disk space required: 23 MB
# Estimated build time: 0.3 SBU
# Avahi Dependencies
# Required
# GLib-2.62.4
# Recommended
# gobject-introspection-1.62.0, GTK+-2.24.32, GTK+-3.24.13, libdaemon-0.14 and libglade-2.6.4
# Optional
# D-Bus Python-1.2.16, PyGTK-2.24.0, Doxygen-1.8.17 and xmltoman (for generating documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vfi
# patch -Np1 -i ${PATCHSOURCE}/
SAVE_PATH=${PATH}
export PATH=$PATH32
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--disable-mono       \
--disable-monodoc    \
            --disable-python     \
            --disable-qt3        \
            --disable-qt4        \
            --with-distro=none   \
            --with-systemdsystemunitdir=no \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir} --disable-qt5
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

