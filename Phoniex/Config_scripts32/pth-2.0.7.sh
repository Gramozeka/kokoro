#!/bin/bash
###############################################################################
set -e
# The Pth package contains a very portable POSIX/ANSI-C based library for Unix platforms which provides non-preemptive priority-based scheduling for multiple threads of execution (multithreading) inside event-driven applications. All threads run in the same address space of the server application, but each thread has its own individual program-counter, run-time stack, signal mask and errno variable.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/pth/pth-2.0.7.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/pth/pth-2.0.7.tar.gz
# Download MD5 sum: 9cb4a25331a4c4db866a31cbe507c793
# Download size: 652 KB
# Estimated disk space required: 5 MB
# Estimated build time: 0.2 SBU

###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's#$(LOBJS): Makefile#$(LOBJS): pth_p.h Makefile#' Makefile.in &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--mandir=/i686/share/man \
--build=${CLFS_TARGET}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
