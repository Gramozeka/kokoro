#!/bin/bash
###############################################################################
set -e
# The libarchive library provides a single interface for reading/writing various compression formats.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/libarchive/libarchive/releases/download/v3.4.1/libarchive-3.4.1.tar.xz
# Download MD5 sum: 3a541b90977029ad8c5aaa5a49631b15
# Download size: 4.6 MB
# Estimated disk space required: 46 MB (add 29 MB for tests)
# Estimated build time: 0.4 SBU (add 1.5 SBU for tests)
# libarchive Dependencies
# Optional
# libxml2-2.9.10, LZO-2.10, and Nettle-3.5.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
