#!/bin/bash
###############################################################################
set -e
# The libxslt package contains XSLT libraries used for extending libxml2 libraries to support XSLT files.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://xmlsoft.org/sources/libxslt-1.1.34.tar.gz
# Download (FTP): ftp://xmlsoft.org/libxslt/libxslt-1.1.34.tar.gz
# Download MD5 sum: db8765c8d076f1b6caafd9f2542a304a
# Download size: 3.4 MB
# Estimated disk space required: 36 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# libxslt Dependencies
# Required
# libxml2-2.9.10
# Recommended
# docbook-xml-4.5 and docbook-xsl-1.79.2
# [Note] Note
# Although it is not a direct dependency, many applications using libxslt will expect docbook-xml-4.5 and docbook-xsl-1.79.2 to be present.
# Optional
# libgcrypt-1.8.5, libxml2-2.9.10 (for Python2)
# [Note] Note
# The libxml2-2.9.10 (for Python2) dependency is only appropriate if the Python2 module for this package is needed. The libxslt Python2 module is not needed for any package in BLFS but various packages may install Python2 modules which reference it.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i s/3000/5000/ libxslt/transform.c doc/xsltproc.{1,xml} &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
