#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's@/desktop/ibus@/org/freedesktop/ibus@g' \
    data/dconf/org.freedesktop.ibus.gschema.xml
patch -Np1 -i ${PATCHSOURCE}/ibus-1.5.22-wayland_display-1.patch
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
