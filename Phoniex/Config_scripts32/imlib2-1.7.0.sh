#!/bin/bash
###############################################################################
set -e
# imlib2 is a graphics library for fast file loading, saving, rendering and manipulation.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/enlightenment/imlib2-1.6.1.tar.bz2
# Download MD5 sum: 7b3fbcb974b48822b32b326c6a47764b
# Download size: 876 KB
# Estimated disk space required: 12 MB
# Estimated build time: 0.2 SBU
# imlib2 Dependencies
# Required
# Xorg Libraries
# Optional
# libpng-1.6.37, libjpeg-turbo-2.0.4, LibTIFF-4.1.0, giflib-5.2.1, and libid3tag
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
