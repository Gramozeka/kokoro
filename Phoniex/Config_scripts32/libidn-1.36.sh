#!/bin/bash
###############################################################################
set -e
# libidn is a package designed for internationalized string handling based on the Stringprep, Punycode and IDNA specifications defined by the Internet Engineering Task Force (IETF) Internationalized Domain Names (IDN) working group, used for internationalized domain names. This is useful for converting data from the system's native representation into UTF-8, transforming Unicode strings into ASCII strings, allowing applications to use certain ASCII name labels (beginning with a special prefix) to represent non-ASCII name labels, and converting entire domain names to and from the ASCII Compatible Encoding (ACE) form.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/libidn/libidn-1.35.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/libidn/libidn-1.35.tar.gz
# Download MD5 sum: bef634141fe39326cb354b75e891fead
# Download size: 4.0 MB
# Estimated disk space required: 25 MB
# Estimated build time: 0.2 SBU (using parallelim=4)
# libidn Dependencies
# Optional
# Pth-2.0.7, Emacs-26.3, GTK-Doc-1.32, OpenJDK-12.0.2, Valgrind-3.15.0, and Mono'
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
# MONO_PATH=/i686 \
CFLAGS="-O2 -march=i586 -mtune=i686" \
LT_SYS_LIBRARY_PATH=/i686/lib \
./configure --prefix=/i686 \
--disable-static \
--disable-csharp \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
