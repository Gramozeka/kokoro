#!/bin/bash
###############################################################################
set -e
# The GLib package contains low-level libraries useful for providing data structure handling for C, portability wrappers and interfaces for such runtime functionality as an event loop, threads, dynamic loading and an object system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/glib/2.62/glib-2.62.4.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/glib/2.62/glib-2.62.4.tar.xz
# Download MD5 sum: d52234ecba128932bed90bbc3553bfe5
# Download size: 4.5 MB
# Estimated disk space required: 167 MB (add 3 MB for tests)
# Estimated build time: 0.5 SBU (add 0.6 SBU for tests; both using parallelism=4)
# Additional Downloads
# Optional patch: http://www.linuxfromscratch.org/patches/blfs/svn/glib-2.62.4-skip_warnings-1.patch
# GLib Dependencies
# Recommended
# libxslt-1.1.34 and PCRE-8.43 (built with Unicode properties)
# Optional
# dbus-1.12.16 and bindfs (both may be used in some tests), GDB-8.3.1 (for bindings), docbook-xml-4.5, docbook-xsl-1.79.2, and GTK-Doc-1.32 (to build API documentation)
# Additional Runtime Dependencies
# gobject-introspection-1.62.0 (should be installed before gtk+, atk, etc.)
# Quoted directly from the INSTALL file: “Some of the mimetype-related functionality in GIO requires the update-mime-database and update-desktop-database utilities”, which are part of shared-mime-info-1.15 and desktop-file-utils-0.24, respectively. These two utilities are also needed for some tests.
###############################################################################
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
 patch -Np1 -i ${PATCHSOURCE}/glib-2.66.2-skip_warnings-1.patch
#  patch -Np1 -i ${PATCHSOURCE}/glib-2.62.4-cve_2020_6750_fix-1.patch
sed -i 's/1\.32\.1/1.32/' docs/reference/meson.build
mkdir meson-build
cd meson-build
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dselinux=disabled \
  -Dman=false         \
  -Dgtk_doc=false \
  -Dinstalled_tests=false \
  ..
  /i686/bin/ninja -j5
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
