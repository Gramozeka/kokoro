#!/bin/bash
###############################################################################
set -e
###############################################################################
# Libssh2 package is a client-side C library implementing the SSH2 protocol.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.libssh2.org/download/libssh2-1.9.0.tar.gz
# Download MD5 sum: 1beefafe8963982adc84b408b2959927
# Download size: 868 KB
# Estimated disk space required: 13 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# libssh2 Dependencies
# Optional
# GnuPG-2.2.19, libgcrypt-1.8.5, and OpenSSH-8.1p1 (all three required for the testsuite)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake      -DCMAKE_C_FLAGS="$BUILD32" \
     -DCMAKE_CXX_FLAGS="$BUILD32" \
-DCMAKE_INSTALL_PREFIX=/i686 \
 -DCMAKE_INSTALL_LIBDIR=/i686/lib \
 -DLIB_INSTALL_DIR=lib \
 -DBUILD_SHARED_LIBS:BOOL=ON \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev ..
make -j4 || make || exit 1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
