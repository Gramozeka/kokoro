#!/bin/bash
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg}  install 
echo "$1 ----- $(date)" >> ${destdir}/loginstall
ln -sv bash ${pkg}/i686/bin/sh
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi

find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true  &&

cd ${pkg} &&
 
tar -cvpzf ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.tar.gz --one-file-system .
tar -xvpzf ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.tar.gz -C / --numeric-owner
rm -rf ${pkg}
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
USE_ARCH="64"
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${SOURCE}/bash-5.0-upstream_fixes-1.patch

CFLAGS_FOR_BUILD=$CFLAGS \
CPPFLAGS_FOR_BUILD=$CPPFLAGS \
LT_SYS_LIBRARY_PATH="/i686/lib" \
LDFLAGS="-L/i686/lib -lgmp" \
LDFLAGS_FOR_BUILD=$LDFLAGS \
CC="gcc -isystem /i686/include ${BUILD32}" \
CXX="gcc -isystem /i686/include ${BUILD32}" \
PKG_CONFIG_PATH=/i686/lib/pkgconfig \
./configure --prefix=/i686                       \
           --libdir=/i686/lib \
            --docdir=/i686/share/doc/bash-5.0 \
            --with-libiconv-prefix=/i686 \
            --with-libintl-prefix=/i686 \
            --without-bash-malloc               \
            --with-installed-readline \
            --build=${CLFS_TARGET32}
sed -i 's|SHOBJ_CFLAGS = -fPIC|SHOBJ_CFLAGS =|' examples/loadables/Makefile
make
pack_local ${packagedir}

cd ${home}
echo "###########################**** COMPLITE!!! ****##################################"
