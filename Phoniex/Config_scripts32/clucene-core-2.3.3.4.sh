#!/bin/bash
###############################################################################
# CLucene is a C++ version of Lucene, a high performance text search engine.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/clucene/clucene-core-2.3.3.4.tar.gz
# Download MD5 sum: 48d647fbd8ef8889e5a7f422c1bfda94
# Download size: 2.2 MB
# Estimated disk space required: 78 MB
# Estimated build time: 0.8 SBU
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/clucene-2.3.3.4-contribs_lib-1.patch
# CLucene Dependencies
# Required
# CMake-3.16.3
# Recommended
# Boost-1.72.0
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/clucene.pkgconfig.patch.gz | patch -p1 --verbose || exit 1
patch -Np1 -i ${PATCHSOURCE}/clucene-2.3.3.4-contribs_lib-1.patch &&

mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
  cmake \
    -DCMAKE_C_FLAGS:STRING="${BUILD32}" \
    -DCMAKE_C_FLAGS_RELEASE:STRING="${BUILD32}" \
    -DCMAKE_CXX_FLAGS:STRING="${BUILD32}" \
    -DCMAKE_CXX_FLAGS_RELEASE:STRING="${BUILD32}" \
    -DCMAKE_INSTALL_LIBDIR=/i686/lib \
    -DLIB_INSTALL_DIR=/i686/lib \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/i686 \
    -DBUILD_CONTRIBS_LIB:BOOL=ON \
    -DLIB_SUFFIX="" ..
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
