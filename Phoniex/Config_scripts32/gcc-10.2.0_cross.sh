#!/bin/bash
#######################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_cross*$//" <<< $(basename $line))
package="$packagedir.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${SOURCE}/gcc-10.2.0-specs-1_i686.diff
#patch -Np1 -i ${SOURCE}/gcc-graphite.diff
# zcat ${SOURCE}/gcc-no_fixincludes.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1
echo -en '\n#undef STANDARD_STARTFILE_PREFIX_1\n#define STANDARD_STARTFILE_PREFIX_1 "/i686/lib/"\n' >> gcc/config/linux.h
echo -en '\n#undef STANDARD_STARTFILE_PREFIX_2\n#define STANDARD_STARTFILE_PREFIX_2 ""\n' >> gcc/config/linux.h
mkdir -v ../gcc-build
cd ../gcc-build
AR=ar \
LDFLAGS="-Wl,-rpath,/i686/lib" \
../${packagedir}/configure \
    --prefix=/i686 \
    --build=${CLFS_HOST} \
    --target=${CLFS_TARGET} \
    --host=${CLFS_HOST} \
    --with-sysroot=${CLFS} \
    --with-local-prefix=/i686 \
    --with-native-system-header-dir=/i686/include \
    --disable-static \
    --enable-languages=c,c++ \
    --with-mpc=/i686 \
    --with-mpfr=/i686 \
    --with-gmp=/i686 \
    --with-isl=/i686
make -j4 AS_FOR_TARGET="${CLFS_TARGET}-as" \
    LD_FOR_TARGET="${CLFS_TARGET}-ld"
make install
echo "###########################**** COMPLITE!!! ****##################################"
