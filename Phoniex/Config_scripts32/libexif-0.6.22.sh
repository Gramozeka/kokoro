#!/bin/bash
###############################################################################
set -e
# The libexif package contains a library for parsing, editing, and saving EXIF data. Most digital cameras produce EXIF files, which are JPEG files with extra tags that contain information about the image. All EXIF tags described in EXIF standard 2.1 are supported.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/libexif/libexif-0.6.21.tar.bz2
# Download MD5 sum: 27339b89850f28c8f1c237f233e05b27
# Download size: 1.4 MB
# Estimated disk space required: 17 MB
# Estimated build time: 0.2 SBU
# libexif Dependencies
# Optional (to Build Documentation)
# Doxygen-1.8.17 and Graphviz-2.42.3
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/libexif-0.6.21-security_fix-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
