#!/bin/bash
###############################################################################
set -e
# The CMake package contains a modern toolset used for generating Makefiles. It is a successor of the auto-generated configure script and aims to be platform- and compiler-independent. A significant user of CMake is KDE since version 4.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://cmake.org/files/v3.16/cmake-3.16.3.tar.gz
# Download MD5 sum: 9e6fa59704d3a52812e279996b5b01c7
# Download size: 8.7 MB
# Estimated disk space required: 443 MB (add 541 MB for tests)
# Estimated build time: 2.8 SBU (add 3.8 SBU for tests, both using parallelism=4)
# CMake Dependencies
# Required
# libuv-1.34.2
# Recommended
# cURL-7.68.0 and libarchive-3.4.1
# Optional
# Qt-5.14.0 (for the Qt-based GUI), Subversion-1.13.0 (for testing), and Sphinx (for building documents)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./bootstrap  --prefix=/i686 \
            --system-libs        \
            --no-system-jsoncpp  \
            --no-system-librhash \
            --parallel="5" 
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
