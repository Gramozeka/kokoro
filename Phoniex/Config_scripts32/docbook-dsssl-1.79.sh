#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

install -v -d ${pkg}/i686/bin  &&
install -v -m755 bin/collateindex.pl ${pkg}/i686/bin                      &&
chown -R root:root . &&
install -v -d -m755 ${pkg}/i686/share/man/man1 &&
install -v -m644 bin/collateindex.pl.1 ${pkg}/i686/share/man/man1         &&
install -v -d -m755 ${pkg}/i686/share/sgml/docbook/dsssl-stylesheets-1.79 &&
cp -v -R * ${pkg}/i686/share/sgml/docbook/dsssl-stylesheets-1.79          &&

if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
tar -xf ${SOURCE}/docbook-dsssl-doc-1.79.tar.bz2 --strip-components=1
 
ARCH="noarch"

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
install-catalog --add /etc/sgml/dsssl-docbook-stylesheets.cat \
    /i686/share/sgml/docbook/dsssl-stylesheets-1.79/catalog         &&

install-catalog --add /etc/sgml/dsssl-docbook-stylesheets.cat \
    /i686/share/sgml/docbook/dsssl-stylesheets-1.79/common/catalog  &&

install-catalog --add /etc/sgml/sgml-docbook.cat              \
    /etc/sgml/dsssl-docbook-stylesheets.cat

