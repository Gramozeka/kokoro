#!/bin/bash
###############################################################################
set -e
# The libical package contains an implementation of the iCalendar protocols and data formats.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/libical/libical/releases/download/v3.0.7/libical-3.0.7.tar.gz
# Download MD5 sum: 8a5d07a7fba9e73a85e67f76258bf042
# Download size: 864 KB
# Estimated disk space required: 24 MB (with tests)
# Estimated build time: 0.2 SBU (Using parallelism=5; with tests)
# libical Dependencies
# Required
# CMake-3.16.3
# Recommended
# gobject-introspection-1.62.0 and Vala-0.46.5(both required for Gnome)
# Optional
# Berkeley DB-5.3.28, Doxygen-1.8.17 (for the API documentation), ICU-65.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_INSTALL_PREFIX=/i686  \
      -DCMAKE_BUILD_TYPE=Release   \
      -DSHARED_ONLY=yes            \
        -DLIB_SUFFIX="" \
      -DICAL_BUILD_DOCS=false      \
      -DGOBJECT_INTROSPECTION=true \
      -DICAL_GLIB_VAPI=true        \
      .. &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
