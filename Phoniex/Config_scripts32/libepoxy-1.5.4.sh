#!/bin/bash
###############################################################################
set -e
# libepoxy is a library for handling OpenGL function pointer management.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/anholt/libepoxy/releases/download/1.5.4/libepoxy-1.5.4.tar.xz
# Download MD5 sum: 00f47ad447321f9dc59f85bc1c9d0467
# Download size: 224 KB
# Estimated disk space required: 27 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# libepoxy Dependencies
# Required
# Mesa-19.3.2
# Optional
# Doxygen-1.8.17 (for documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --localstatedir=/var \
  --buildtype=release \
  ..
  /i686/bin/ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
