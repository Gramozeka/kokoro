#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/python.readline.set_pre_input_hook.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/python.no-static-library.diff.gz | patch -p1 --verbose || exit 1
# # zcat ${PATCHSOURCE}/python.x86_64.diff.gz | patch -p1 --verbose || exit 1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
CC="gcc -m32" CXX="g++ -m32" \
setarch i386 ./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --with-ensurepip=yes \
             --enable-ipv6 \
            --enable-unicode=ucs4 \
--build=${CLFS_TARGET32} \
--docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
