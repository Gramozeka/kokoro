#!/bin/bash
###############################################################################
set -e
###############################################################################
# The IJS package contains a library which implements a protocol for transmission of raster page images.
# This package is known to build and work properly using an LFS-8.4 platform.
# Package Information
# Download (HTTP): https://www.openprinting.org/download/ijs/download/ijs-0.35.tar.bz2
# Download MD5 sum: 896fdcb7a01c586ba6eb81398ea3f6e9
# Download size: 252 KB
# Estimated disk space required: 2.2 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
            --mandir=/i686/share/man \
            --enable-shared \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
