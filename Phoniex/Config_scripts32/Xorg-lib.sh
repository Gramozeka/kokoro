#!/bin/bash
set -e
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
if [ -d ${pkg}/i686/share/info ]; then
  find ${pkg}/i686/share/info -type f -exec rm -rf {} \;
fi
case ${packagedir} in
    libXaw3d-[0-9]* )
( cd ${pkg}/i686/lib
  ln -sf libXaw3d.so.8 libXaw3d.so.6
  ln -sf libXaw3d.so.8 libXaw3d.so.7
)
    ;;
    pixman-[0-9]* )
( cd ${pkg}/i686/include
  ln -sf pixman-1/pixman-version.h .
  ln -sf pixman-1/pixman.h .
  ln -sf pixman-1 pixman
)
;;
esac
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
######################################################################################
while read -r line; do

    # Get the file name, ignoring comments and blank lines
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
    file=$(echo $line | cut -d" " -f2)

    temp_val=$(echo $file|sed 's|^.*/||')          # Remove directory
    packagedir=$(echo $temp_val|sed 's|\.tar.*||') # Package directory
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/Xorg/lib/${file}.*
cd ${packagedir}
chown -R root:root *
CONF_INDIVIDUAL=""

case ${packagedir} in
    libxcb-[0-9]* )
sed -i "s/pthread-stubs//" configure
CONF_INDIVIDUAL="            --without-doxygen \
            --docdir=/i686/share/doc/libxcb-1.13.1"
    ;;
    xcb-proto-[0-9]* )
CONF_INDIVIDUAL="PYTHON=/i686/bin/python3"
patch -Np1 -i ${PATCHSOURCE}/xcb-proto-1.14-python3_9.patch
    ;;

    libICE* )
      CONF_INDIVIDUAL=" ICE_LIBS=-lpthread "
    ;;
    libX11-[0-9]* )
      CONF_INDIVIDUAL=" --enable-loadable-i18n "
    ;;
    libFS-[0-9]* )
#     CONF_INDIVIDUAL=
#     patch_line="patch -Np1 -i ${PATCHSOURCE}/FSErrDis.diff"
        ;;
    libXpm-[0-9]* )
#     CONF_INDIVIDUAL=
#     patch_line="patch -Np1 -i ${PATCHSOURCE}/parse.diff"
        ;;

    libXfont2-[0-9]* )
      CONF_INDIVIDUAL=" --disable-devel-docs --build=${CLFS_TARGET}"
#       patch_line="patch -Np1 -i ${PATCHSOURCE}/fontxlfd.diff"
    ;;

    libXt-[0-9]* )
      CONF_INDIVIDUAL=" \
                  --with-appdefaultdir=/etc/X11/app-defaults "
    ;;

    libXaw3dXft-[0-9]* )
     CONF_INDIVIDUAL="  --enable-multiplane-bitmaps \
  --enable-gray-stipples \
  --enable-arrow-scrollbars \
  --enable-internationalization "
    ;;
    font-util-[0-9]* )
      CONF_INDIVIDUAL=" --with-fontrootdir=/i686/share/fonts "
    ;;
       pixman-[0-9]* )
      zcat  ${PATCHSOURCE}/pixman.remove.tests.that.fail.to.compile.diff.gz | patch -p1 --backup --suffix=.orig
    ;;
esac
case ${packagedir} in
    libvdpau-[0-9]* )

mkdir meson-build
cd meson-build
PKG_CONFIG_PATH="/i686/lib/pkgconfig:/i686/share/pkgconfig" \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHON=/i686/bin/python3 \
setarch i386 /i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --localstatedir=/var \
  --buildtype=release \
  .. || exit 1
setarch i386   /i686/bin/ninja  -j5 || exit 1
pkg=${destdir}/${packagedir}
  DESTDIR=${pkg} /i686/bin/ninja install || exit 1
cd ..
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
ldconfig
continue
;;
    libdrm-[0-9]* )
PKG_CONFIG_PATH="/i686/lib/pkgconfig:/i686/share/pkgconfig" \
mkdir meson-build
cd meson-build
PYTHON=/i686/bin/python3 \
setarch i386 /i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release -Dudev=true \
  .. || exit 1
setarch i386   /i686/bin/ninja  -j5 || exit 1
pkg=${destdir}/${packagedir}
  DESTDIR=${pkg} /i686/bin/ninja install || exit 1
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
ldconfig
continue
;;
esac
$patch_line
patch_line=""
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHONPATH=/i686/lib/python3.9/site-packages \
setarch i386 ./configure --prefix=/i686 \
   --localstatedir=/var \
   PYTHON=/i686/bin/python3 \
--disable-static ${CONF_INDIVIDUAL}
# \
# --build=${CLFS_TARGET} &&
setarch i386 make -j4

pack_local ${packagedir}
ldconfig
done < ${home}/${scriptsdir}/Xorg-lib-7.sh
echo "Complite all !"
