#!/bin/bash
###############################################################################
set -e
###############################################################################
# The OpenSSH package contains ssh clients and the sshd daemon. This is useful for encrypting authentication and subsequent traffic over a network. The ssh and scp commands are secure implementations of telnet and rcp respectively.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-8.1p1.tar.gz
# Download MD5 sum: 513694343631a99841e815306806edf0
# Download size: 1.5 MB
# Estimated disk space required: 45 MB (add 12 MB for tests)
# Estimated build time: 0.4 SBU (running the tests takes 17+ minutes, irrespective of processor speed)
# OpenSSH Dependencies
# Optional
# GDB-8.3.1 (for tests), Linux-PAM-1.3.1, X Window System, MIT Kerberos V5-1.17.1, libedit, LibreSSL Portable, OpenSC, and libsectok
# Optional Runtime (Used only to gather entropy)
# OpenJDK-12.0.2, Net-tools-CVS_20101030, and Sysstat-12.3.1
##############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
rm -rf ${pkg}/{var,etc}
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
            --sysconfdir=/etc/ssh             \
            --with-md5-passwords              \
            --with-privsep-path=/var/lib/sshd \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir} --with-pam
make -j4
pack_local64 ${packagedir}

echo "###########################**** COMPLITE!!! ****##################################"

