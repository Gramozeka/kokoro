#!/bin/bash
###############################################################################
set -e
# The popt package contains the popt libraries which are used by some programs to parse command-line options.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://rpm5.org/files/popt/popt-1.16.tar.gz
# Download (FTP): ftp://anduin.linuxfromscratch.org/BLFS/popt/popt-1.16.tar.gz
# Download MD5 sum: 3743beefa3dd6247a73f8f7a32c14c33
# Download size: 702 kB
# Estimated disk space required: 8 MB (includes installing documentation)
# Estimated build time: 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
