#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
rm -rf qlo10k1 Makefile gitcompile sb16_csp ld10k1
sed -i -e "/#include/i #define __user" \
                             hdspconf/src/*.cxx  \
                             hdspmixer/src/*.cxx \
                             hdsploader/hdsploader.c &&
sed -i -e '39 s/uint32_t/__u32/' hdspmixer/src/HDSPMixerWindow.cxx &&
sed -i -e '40 s/uint64_t/__u64/' hdspmixer/src/HDSPMixerWindow.cxx

for tool in *
do
  case $tool in
    seq )
      tool_dir=seq/sbiload
      t_dir=seq-sbiload
    ;;
    * )
      tool_dir=$tool
      t_dir=$tool
    ;;
  esac
  pushd $tool_dir
 SAVE_PATH=${PATH}
export PATH=$PATH32 
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}-$t_dir
/sbin/ldconfig
  popd
done
unset tool tool_dir
 
echo "###########################**** COMPLITE!!! ****##################################"
