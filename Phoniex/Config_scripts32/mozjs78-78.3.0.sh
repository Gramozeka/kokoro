#!/bin/bash
###############################################################################
set -e
###############################################################################
# JS is Mozillas JavaScript engine written in C.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/teams/releng/tarballs-needing-help/mozjs/mozjs-60.8.0.tar.bz2
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/teams/releng/tarballs-needing-help/mozjs/mozjs-60.8.0.tar.bz2
# Download MD5 sum: 5eda38cc08a3594a3f2c3cc185d4f15b
# Download size: 31 MB
# Estimated disk space required: 640 MB
# Estimated build time: 6.6 SBU
# JS60 Dependencies
# Required
# Autoconf-2.13, ICU-65.1, Python-2.7.17, Which-2.21, and Zip-3.0
# Optional
# Doxygen-1.8.17
######################################################################################

set -e
cd $TEMPBUILD
rm -rf *
package="firefox-78.3.0.tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd firefox-78.3.0

mkdir obj &&
cd    obj &&
# export SHELL=/bin/bash
SAVE_PATH=${PATH}
export PATH=$PATH32
# PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../js/src/configure --prefix=/i686      \
--libdir=/i686/lib           \
                    --with-intl-api          \
                    --with-system-zlib       \
                    --with-system-icu        \
                    --disable-jemalloc       \
                    --disable-debug-symbols  \
                    --enable-readline        &&
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
rm -v /i686/lib/libjs_static.ajs &&
sed -i '/@NSPR_CFLAGS@/d' /i686/bin/js78-config
