#!/bin/bash
###############################################################################
set -e
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
###############################################################################
# /i686/bin/cpan -i Parse::Yapp
# /i686/bin/pip install PyCryptodome
# /i686/bin/pip3 install PyCryptodome
/i686/bin/cpan -i Parse::Yapp
/i686/bin/pip2 install PyCryptodome
/i686/bin/pip3 install PyCryptodome
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -r 's/nss_(setpw|endpw|setgr|endgr)ent/my_&/' \
    -i nsswitch/nsstest.c
echo "^samba4.rpc.echo.*on.*ncacn_np.*with.*object.*nt4_dc" >> selftest/knownfail
SAVE_PATH=${PATH}
export PATH=$PATH32:/usr/bin
CXXFLAGS=$CXXFLAGS \
CFLAGS="$CFLAGS -I/i686/include/tirpc"          \
LDFLAGS="$LDFLAGS -ltirpc"                      \
  ./configure                          \
    --prefix=/i686                      \
    --libdir=/i686/lib \
    --localstatedir=/var               \
    --with-piddir=/run/samba           \
    --with-pammodulesdir=/i686/lib/security \
    --enable-fhs                       \
    --without-ad-dc                    \
    --without-systemd                  \
    --enable-selftest                  &&
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

