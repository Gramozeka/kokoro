#!/bin/bash
###############################################################################
set -e
# neon is an HTTP and WebDAV client library, with a C interface.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://fossies.org/linux/www/neon-0.30.2.tar.gz
# Download MD5 sum: e28d77bf14032d7f5046b3930704ef41
# Download size: 911 KB
# Estimated disk space required: 8.5 MB (additional 18 MB for the tests)
# Estimated build time: 0.1 SBU (additional 0.3 SBU for the tests)
# neon Dependencies
# Optional
# GnuTLS-3.6.11.1, libxml2-2.9.10, MIT Kerberos V5-1.17.1, libproxy, and pakchois
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e 's/client_set/set/'  \
    -e 's/gnutls_retr/&2/'  \
    -e 's/type = t/cert_&/' \
    -i src/ne_gnutls.c
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
            --with-ssl=openssl \
            --enable-shared  \
            --disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
