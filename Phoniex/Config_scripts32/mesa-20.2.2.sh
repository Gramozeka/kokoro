#!/bin/bash
set -e
###############################################################################
# Mesa is an OpenGL compatible 3D graphics library.
# [Note] Note
# Mesa is updated relatively often. You may want to use the latest available 19.3.x mesa version.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://mesa.freedesktop.org/archive/mesa-19.3.3.tar.xz
# Download (FTP): ftp://ftp.freedesktop.org/pub/mesa/mesa-19.3.3.tar.xz
# Download MD5 sum: 00010e0bb8f6641276ff6cb3e9386114
# Download size: 11 MB
# Estimated disk space required: 397 MB (with demos and docs), add 204 MB for tests
# Estimated build time: 2.6 SBU (with parallelism=4, demos, and docs), add 0.4 SBU for tests
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/mesa-19.3.3-fix_svga_vmwgfx_segfaults-1.patch
# Recommended patch: http://www.linuxfromscratch.org/patches/blfs/svn/mesa-19.3.3-add_xdemos-1.patch (installs 2 demo programs for testing Mesa - not needed if you install the mesa-demos package)
# Mesa Dependencies
# Required
# Xorg Libraries, libdrm-2.4.100, and Mako-1.1.1
# Recommended
# libva-2.6.1 (to provide VA-API support for some gallium drivers, note that there is a circular dependency. You must build libva first without EGL and GLX support, install this package, and rebuild libva), libvdpau-1.3 (to build VDPAU drivers), LLVM-9.0.1 (required for Gallium3D, nouveau, r300, and radeonsi drivers and for swrast, the software rasterizer which is sometimes referred to as llvmpipe. See http://www.mesa3d.org/systems.html for more information), and wayland-protocols-1.18 (required for Plasma-5.17.3, GNOME, and recommended for GTK+-3.24.13)
# Optional
# libgcrypt-1.8.5, lm-sensors-3-6-0 , Nettle-3.5.1, Valgrind-3.15.0, mesa-demos (provides more than 300 extra demos to test Mesa; this includes the same programs added by the patch above), Bellagio OpenMAX Integration Layer (for mobile platforms), and libtizonia
# [Note] Note
# The instructions below assume that LLVM with the r600/amdgpu and host backends and run-time type information (RTTI - needed for nouveau) are installed. You will need to modify the instructions if you choose not to install all of these. For an explanation of Gallium3D see https://en.wikipedia.org/wiki/Gallium3D.
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
  DESTDIR=${pkg} /i686/bin/ninja install || exit 1
echo "$1 ----- $(date)" >> ${destdir}/loginstall
mkdir -p ${pkg}/i686/lib/pkgconfig

cat > ${pkg}/i686/lib/pkgconfig/glesv2.pc << "EOF"
prefix=/i686
libdir=${prefix}/lib
includedir=${prefix}/include

Name: glesv2
Description: Mesa OpenGL ES 2.0 library
Version: 20.2.2
Libs: -L${libdir} -lGLESv2
Libs.private: -lpthread -pthread -lm -ldl
Cflags: -I${includedir}
EOF

install -v -dm755 ${pkg}/i686/share/doc/${packagedir} &&
cp -rfv ../docs/* ${pkg}/i686/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/Xorg/mesa/${package}
cd ${packagedir}
# patch -Np1 -i ${SOURCE}/Xorg/mesa/mesa-add_xdemos-1.patch
sed '1s/python/&3/' -i bin/symbols-check.py
DRI_DRIVERS="i915,i965,r100,r200,nouveau"
GALLIUM_DRIVERS="nouveau,r300,r600,svga,radeonsi,swrast,virgl,iris,swr"
mkdir meson-build
cd meson-build
SAVE_PATH=${PATH}
export PATH=$PATH32
LLVM_INSTALL_DIR=/i686 \
CLANG_INSTALL_DIR=/i686 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
LLVM_LDFLAGS="-L/i686/lib" \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dplatforms=auto \
  -Dgallium-opencl=icd \
  -Dgallium-nine=true \
  -Dosmesa=gallium \
  -Ddri-drivers=$DRI_DRIVERS \
  -Dgallium-drivers=$GALLIUM_DRIVERS \
  -Dglvnd=true \
  -Dllvm=true \
  -Ddri3=true \
  -Dshared-llvm=true \
  -Dshared-glapi=true \
  -Dglx=dri           \
  -Degl=true \
  -Dgles1=true \
  -Dgles2=true \
  .. || exit 1
/i686/bin/ninja -j5 || exit 1
export PATH=$SAVE_PATH
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

