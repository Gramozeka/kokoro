#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure \
    --prefix=/i686 \
    --libdir=/i686/lib \
    --docdir=/i686/share/doc/gperf-3.1

make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
