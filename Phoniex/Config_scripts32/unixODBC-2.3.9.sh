#!/bin/bash
###############################################################################
set -e
# The unixODBC package is an Open Source ODBC (Open DataBase Connectivity) sub-system and an ODBC SDK for Linux, Mac OSX, and UNIX. ODBC is an open specification for providing application developers with a predictable API with which to access data sources. Data sources include optional SQL Servers and any data source with an ODBC Driver. unixODBC contains the following components used to assist with the manipulation of ODBC data sources: a driver manager, an installer library and command line tool, command line tools to help install a driver and work with SQL, drivers and driver setup libraries.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (FTP): ftp://ftp.unixodbc.org/pub/unixODBC/unixODBC-2.3.7.tar.gz
# Download MD5 sum: 274a711b0c77394e052db6493840c6f9
# Download size: 1.6 MB
# Estimated disk space required: 34 MB
# Estimated build time: 0.2 SBU (using parallelism=4)
# unixODBC Dependencies
# Optional
# Mini SQL and Pth-2.0.7
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
 --sysconfdir=/etc/unixODBC \
--libdir=/i686/lib \
--enable-drivers \
--enable-drivers-conf \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
