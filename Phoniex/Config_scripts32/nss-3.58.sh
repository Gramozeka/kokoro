###############################################################################
set -e
# The volume_key package provides a library for manipulating storage volume encryption keys and storing them separately from volumes to handle forgotten passphrases.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/felixonmars/volume_key/archive/volume_key-0.3.12.tar.gz
# Download MD5 sum: d1c76f24e08ddd8c1787687d0af5a814
# Download size: 196 KB
# Estimated disk space required: 11 MB
# Estimated build time: 0.2 SBU
# volume_key Dependencies
# Required
# cryptsetup-2.0.6, GLib-2.62.4, GPGME-1.13.1, and NSS-3.49.2
# Recommended
# SWIG-4.0.1
# Optional
# Python-2.7.17
###############################################################################
VERSION=${VERSION:-3.50}
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/i686/include/nss
mkdir -pv ${pkg}/i686/{bin,lib}
mkdir -pv ${pkg}/i686/lib/pkgconfig

cd ../dist                                                          &&

install -v -m755 Linux*/lib/*.so              ${pkg}/i686/lib              &&
install -v -m644 Linux*/lib/{*.chk,libcrmf.a} ${pkg}/i686/lib              &&

install -v -m755 -d                           ${pkg}/i686/include/nss      &&
cp -v -RL {public,private}/nss/*              ${pkg}/i686/include/nss      &&
chmod -v 644                                  ${pkg}/i686/include/nss/*    &&

install -v -m755 Linux*/bin/{certutil,nss-config,pk12util} ${pkg}/i686/bin &&

install -v -m644 Linux*/lib/pkgconfig/nss.pc  ${pkg}/i686/lib/pkgconfig

chmod -v 644  ${pkg}/i686/lib/pkgconfig/nss.pc
  ln -s nspr.pc ${pkg}/i686/lib/pkgconfig/mozilla-nspr.pc
  ln -s nss.pc ${pkg}/i686/lib/pkgconfig/mozilla-nss.pc

ln -sfv ./pkcs11/p11-kit-trust.so ${pkg}/i686/lib/libnssckbi.so
ln -sfv ./pkcs11/p11-kit-trust.so ${pkg}/i686/lib/libnssckbi.so
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/nss-3.58-standalone-1.patch
cd nss
setarch i386 make BUILD_OPT=1                      \
  NSPR_INCLUDE_DIR=/i686/include/nspr  \
  USE_SYSTEM_ZLIB=1                   \
  ZLIB_LIBS="-L/i686/lib -lz"        \
  NSS_ENABLE_WERROR=0                 \
  NSS_USE_SYSTEM_SQLITE=1 NSS_DISABLE_GTESTS=1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
