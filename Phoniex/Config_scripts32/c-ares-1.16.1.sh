#!/bin/bash
###############################################################################
set -e
# c-ares is a C library for asynchronous DNS requests.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://c-ares.haxx.se/download/c-ares-1.15.0.tar.gz
# Download MD5 sum: d2391da274653f7643270623e822dff7
# Download size: 1.3 MB
# Estimated disk space required: 8.5 MB
# Estimated build time: 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  --docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
