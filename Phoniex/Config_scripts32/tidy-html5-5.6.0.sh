#!/bin/bash
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg}  install || exit 1
install -v -m755 tab2space ${pkg}/i686/bin
echo "$1 ----- $(date)" >> ${destdir}/loginstall
rm -rf ${pkg}/{usr,etc,var,run,tmp,lib,bin,sbin,lib64}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}

cd build/cmake &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake   -DCMAKE_C_FLAGS="${BUILD32}" \
  -DCMAKE_CXX_FLAGS="${BUILD32}" \
  -DCMAKE_INSTALL_PREFIX=/i686    \
  -DCMAKE_INSTALL_LIB_SUFFIX="" \
      -DBUILD_TAB2SPACE=ON        \
      ../..    &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
