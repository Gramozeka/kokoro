#!/bin/bash
###############################################################################
set -e
###############################################################################
pack_local32 () {
pkg=${destdir}/$1
make prefix=/i686  libdir=/i686/lib install DESTDIR=${pkg}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
rm -rf ${pkg}/i686/share/man
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
USE_ARCH="32"
ARCH="i686"
SAVE_path=${PATH}
PATH="${PATH32}:${PATH}"
export AR="/i686/bin/ar"
export AS="/i686/bin/as"
export RANLIB="/i686/bin/ranlib"
export LD="/i686/bin/ld"
export STRIP="/i686/bin/strip"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH32}
export LD_LIBRARY_PATH="/i686/lib" 
export CPPFLAGS="-I/i686/include" 
CC="gcc ${BUILD32}" CXX="g++ ${BUILD32}" \
export CFLAGS="-O2 -m32 -march=i686 -mtune=i686"
export CXXFLAGS="-O2 -m32 -march=i686 -mtune=i686"
setarch i386 make  -j4 LDFLAGS="-L/i686/lib -lc -lz -lbz2 -lpthread -llzma" CXXFLAGS="-O2 -m32 -march=i686 -mtune=i686" CFLAGS="-O2 -m32 -march=i686 -mtune=i686"
pack_local32 ${packagedir}
export PATH=${SAVE_path}
unset AR AS RANLIB LD STRIP LD_LIBRARY_PATH LDFLAGS CPPFLAGS
source /etc/profile &&
echo "###########################**** COMPLITE!!! ****##################################"
