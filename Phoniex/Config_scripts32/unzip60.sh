#!/bin/bash
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/i686/share/man/man1
mkdir -pv ${pkg}/i686/bin
setarch i386 make prefix=${pkg}/i686 MANDIR=${pkg}/i686/share/man/man1 \
 -f unix/Makefile install

echo "$1 ----- $(date)" >> ${destdir}/loginstall

find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
tree > i686/tree-info/$1-$USE_ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/unzip-6.0-consolidated_fixes-1.patch
CFLAGS_BZ=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 make -f unix/Makefile generic CC="gcc -m32  -liconv"
pack_local64 ${packagedir}-64
echo "###########################**** COMPLITE!!! ****##################################"
