#!/bin/bash
###############################################################################
set -e
###############################################################################
# The libseccomp package provides an easy to use and platform independent interface to the Linux kernel's syscall filtering mechanism.'
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/seccomp/libseccomp/releases/download/v2.4.2/libseccomp-2.4.2.tar.gz
# Download MD5 sum: ee87fb68ae293883bde9e042c6bc324f
# Download size: 588 KB
# Estimated disk space required: 6.6 MB (additional 5 MB for tests)
# Estimated build time: less than 0.1 SBU (additional 2.8 SBU for tests)
# libseccomp Dependencies
# Optional
# Which-2.21 (needed for tests), Valgrind-3.15.0, Cython, and LCOV
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
