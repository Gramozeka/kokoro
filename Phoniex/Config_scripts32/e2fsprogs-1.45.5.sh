#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
mkdir -v build
cd       build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../configure --prefix=/i686           \
             --with-root-prefix="/i686"   \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck \
    --build=${CLFS_TARGET} \
    --docdir=/i686/share/doc/${packagedir}
make -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
