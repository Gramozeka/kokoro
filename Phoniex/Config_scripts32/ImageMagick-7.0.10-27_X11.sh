#!/bin/bash
###############################################################################
set -e
# ImageMagick is a collection of tools and libraries to read, write, and manipulate an image in various image formats. Image processing operations are available from the command line. Bindings for Perl and C++ are also available.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.imagemagick.org/download/releases/ImageMagick-7.0.8-60.tar.xz
# Download (FTP): ftp://ftp.imagemagick.org/pub/ImageMagick/releases/ImageMagick-7.0.8-60.tar.xz
# Download MD5 sum: 27ba8f1daef4404b457a5e7218f56fe8
# Download size: 9.1 MB
# Estimated disk space required: 209 MB, with typical dependencies (add 36MB for make check)
# Estimated build time: 0.9 SBU - typical build with parallelism=4 (add 1.8 SBU for make check and the validation suite)
# [Note] Note
# The ImageMagick source releases are updated frequently and the version shown above may no longer be available from the download locations. You can download a more recent version and use the existing BLFS instructions to install it. Chances are that it will work just fine, but this has not been tested by the BLFS team. If the package version shown above is not available from the locations shown above, or from the legacy/ directory at ftp.ImageMagick.org/pub/ImageMagick you can download it from the BLFS package server at Oregon State University: ftp://ftp.osuosl.org/pub/blfs/conglomeration/ImageMagick/.
# ImageMagick Dependencies
# Recommended
# Xorg Libraries
# The optional dependencies listed below should be installed if you need support for the specific format or the conversion tool the dependency provides. Many of the dependencies' capabilities and uses are described in the “MAGICK DELEGATES” section of the README.txt file located in the source tree. Additional information about the dependencies can be found in the Install-unix.txt file located in the source tree as well as issuing the ./configure --help command. A summary of this information, as well as some additional notes can be viewed on-line at http://www.imagemagick.org/script/advanced-unix-installation.php.
# Optional System Utilities
# Clang from LLVM-9.0.1, Cups-2.3.1, cURL-7.68.0, FFmpeg-4.2.2, fftw-3.3.8, p7zip-16.02 (LZMA), SANE-1.0.27, Wget-1.20.3, xdg-utils-1.1.3, xterm-352, Dmalloc, Electric Fence, PGP or GnuPG-2.2.19 (you'll have to do some hacking to use GnuPG), Profiles, and ufraw (for raw formats listed in http://www.imagemagick.org/www/formats.html)
# Optional Graphics Libraries
# JasPer-2.0.14, Little CMS-1.19 or Little CMS-2.9, libexif-0.6.21, libgxps-0.3.1, libjpeg-turbo-2.0.4, libpng-1.6.37, libraw-0.19.5 (RAW_R), librsvg-2.46.4, LibTIFF-4.1.0, libwebp-1.1.0, OpenJPEG-2.3.1, Pango-1.44.7, DjVuLibre, FlashPIX (libfpx), FLIF, JBIG-KIT, libheif, libraqm, Liquid Rescale, OpenEXR, and RALCGM (or ralcgm)
# Optional Graphics Utilities
# Dejavu fonts, ghostscript-9.50, Gimp-2.10.14, Graphviz-2.42.3, Inkscape-0.92.4, Blender, corefonts, GhostPCL, Gnuplot, POV-Ray, and Radiance
# Optional Conversion Tools
# Enscript-1.6.6, texlive-20190410 (or install-tl-unx) AutoTrace, GeoExpress Command Line Utilities, AKA MrSID Utilities (binary package), hp2xx, html2ps, libwmf, UniConvertor, and Utah Raster Toolkit (or URT-3.1b)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} DOCUMENTATION_PATH=/i686/share/doc/imagemagick-7.0.9 install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
# rm -f $PKG/i686/lib/*.la #### ! Он без la-файлов не работает!
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_X11*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vfi
# patch -Np1 -i ${PATCHSOURCE}/
# sed -e "s/ImfCRgbaFile.h/OpenEXR\/ImfCRgbaFile.h/g" -i coders/exr.c
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--with-sysroot=/i686 \
--with-perl=/i686/bin/perl \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
            --enable-hdri     \
            --with-modules    \
            --with-perl       \
            --with-gslib \
            --with-rsvg \
            --with-gvc \
            --with-autotrace \
            --with-libtiff \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack_local64 ${packagedir}

echo "###########################**** COMPLITE!!! ****##################################"
