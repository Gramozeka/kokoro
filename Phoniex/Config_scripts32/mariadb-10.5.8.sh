#!/bin/bash
###############################################################################
set -e
###############################################################################
# MariaDB is a community-developed fork and a drop-in replacement for the MySQL relational database management system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.mariadb.org/interstitial/mariadb-10.4.11/source/mariadb-10.4.11.tar.gz
# Download (FTP): ftp://mirrors.fe.up.pt/pub/mariadb/mariadb-10.4.11/source/mariadb-10.4.11.tar.gz
# Download MD5 sum: d0de881ab8ead46928cafb7d558535c1
# Download size: 75 MB
# Estimated disk space required: 1.5 GB
# Estimated build time: 13 SBU (with parallelism=4, add 0.4 SBU for tests)
# [Note] Note
# The installed size of MariaDB is 473 MB, but this can be reduced by about 200 MB, if desired, by removing the /i686/share/mysql/test directory after installation.
# MariaDB Dependencies
# Required
# CMake-3.16.3
# Recommended
# libevent-2.1.11
# Optional
# Boost-1.72.0, libaio-0.3.112, libxml2-2.9.10, Linux-PAM-1.3.1, MIT Kerberos V5-1.17.1, PCRE-8.43, Ruby-2.7.0, unixODBC-2.3.7, Valgrind-3.15.0, Groonga, KyTea, Judy, lz4, MeCab, MessagePack, mruby, Sphinx, TokuDB, and ZeroMQ
# For security reasons, running the server as an unprivileged user and group is strongly encouraged. Issue the following (as root) to create the user and group:
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
# cd ..
# install -m755 debian/additions/innotop/innotop ${pkg}/i686/bin/
# install -m755 debian/additions/mariadb-report ${pkg}/i686/bin/

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# sed -i "s@data/test@\${INSTALL_MYSQLTESTDIR}@g" sql/CMakeLists.txt &&
mkdir build &&
cd    build &&
# CXXFLAGS="-Wall -Wextra -Wunused -Wwrite-strings -Wno-uninitialized -Wno-strict-aliasing -Wno-unused-parameter -Wno-invalid-offsetof -Wnon-virtual-dtor -Wimplicit-fallthrough=2 -felide-constructors -fexceptions -mtune=i686 -O3 -fno-omit-frame-pointer"
# CFLAGS="-Wall -Wextra -Wunused -Wwrite-strings -Wno-uninitialized -Wno-strict-aliasing -Wimplicit-fallthrough=2 -mtune=i686 -O3 -fno-omit-frame-pointer"
# SAVE_PATH=${PATH}
# export PATH=$PATH32:/usr/bin
# PYTHON=/i686/bin/python3 \
# PKG_CONFIG_PATH="/i686/lib/pkgconfig:/i686/share/pkgconfig" \
# LD_LIBRARY_PATH="/i686/lib" \
# LDFLAGS="-L/i686/lib" \
# CPPFLAGS="-I/i686/include" \
cmake  \
        -DCMAKE_C_FLAGS="$CFLAGS" \
  -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
      -DCMAKE_BUILD_TYPE=Release                      \
      -DCMAKE_INSTALL_PREFIX=/i686                     \
      -DINSTALL_DOCDIR=share/doc/${packagedir}       \
      -DINSTALL_DOCREADMEDIR=share/doc/${packagedir} \
      -DINSTALL_MANDIR=share/man                      \
      -DINSTALL_MYSQLSHAREDIR=share/mysql             \
      -DINSTALL_MYSQLTESTDIR=share/mysql/test         \
      -DINSTALL_PLUGINDIR=lib/mysql/plugin            \
       -DINSTALL_LIBDIR=lib            \
      -DINSTALL_SBINDIR=sbin                          \
      -DINSTALL_SCRIPTDIR=bin                         \
      -DINSTALL_SQLBENCHDIR=share/mysql/bench         \
      -DINSTALL_SUPPORTFILESDIR=share/mysql           \
      -DMYSQL_DATADIR=/srv/mysql                      \
      -DMYSQL_UNIX_ADDR=/run/mysqld/mysqld.sock       \
      -DWITH_EXTRA_CHARSETS=complex                   \
      -DWITH_EMBEDDED_SERVER=ON                       \
      -DSKIP_TESTS=ON                                 \
      -DTOKUDB_OK=0                                   \
      .. &&
make -j5

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
