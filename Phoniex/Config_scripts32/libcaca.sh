#!/bin/bash
###############################################################################
set -e
# libcaca is a graphics library that outputs text instead of pixels, so that it can work on older video cards or text terminals. It is not unlike the famous ​AAlib library, with the following improvements:
# Unicode support
# 2048 available colours (some devices can only handle 16)
# dithering of colour images
# advanced text canvas operations (blitting, rotations)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/cacalabs/libcaca.git
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./bootstrap
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
  --program-prefix= \
  --program-suffix= \
  --disable-doc \
  --disable-static \
  --enable-slang \
  --enable-ncurses \
  --enable-x11
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
