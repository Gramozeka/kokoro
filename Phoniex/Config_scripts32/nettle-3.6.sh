#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
setarch i386 make DESTDIR=${pkg} install
chmod   -v   755 ${pkg}/i686/lib/lib{hogweed,nettle}.so 
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
 

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
LIBS="-L/i686/lib -lgmp" \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--build=${CLFS_TARGET32}  --docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
