#!/bin/bash
###############################################################################
set -e
###############################################################################

###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/i686/include/CL
cp -rv CL/* ${pkg}/i686/include/CL

echo "$1 ----- $(date)" >> ${destdir}/loginstall
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/KhronosGroup/OpenCL-Headers.git
cd ${packagedir}
ARCH="noarch"
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
