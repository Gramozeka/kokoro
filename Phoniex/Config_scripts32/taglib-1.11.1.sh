#!/bin/bash
###############################################################################
# Taglib is a library used for reading, writing and manipulating audio file tags and is used by applications such as Amarok and VLC.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://taglib.github.io/releases/taglib-1.11.1.tar.gz
# Download MD5 sum: cee7be0ccfc892fa433d6c837df9522a
# Download size: 1.2 MB
# Estimated disk space required: 9.4 MB
# Estimated build time: 0.4 SBU
# Taglib Dependencies
# Required
# CMake-3.16.3
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake      -DCMAKE_C_FLAGS="$BUILD32" \
     -DCMAKE_CXX_FLAGS="$BUILD32" \
-DCMAKE_INSTALL_PREFIX=/i686 \
 -DLIB_INSTALL_DIR=/i686/lib \
 -DBUILD_SHARED_LIBS=ON \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev ..
make -j4 || make || exit 1

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
