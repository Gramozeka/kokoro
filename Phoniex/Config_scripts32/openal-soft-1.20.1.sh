#!/bin/bash
###############################################################################
# OpenAL is a cross-platform 3D audio API appropriate for use with gaming
# applications and many other types of audio applications.
# https://www.openal-soft.org/openal-releases/openal-soft-1.20.1.tar.bz2
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
pkg=${destdir}/$1
make DESTDIR=${pkg} install/strip

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake      -DCMAKE_C_FLAGS="$BUILD32" \
     -DCMAKE_CXX_FLAGS="$BUILD32" \
-DCMAKE_INSTALL_PREFIX=/i686 \
 -DCMAKE_INSTALL_LIBDIR=/i686/lib \
 -DLIB_INSTALL_DIR=lib \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev
make -j4 || make || exit 1

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
