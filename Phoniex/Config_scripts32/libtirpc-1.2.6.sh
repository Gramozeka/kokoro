#!/bin/bash
###############################################################################
set -e
# The libtirpc package contains libraries that support programs that use the Remote Procedure Call (RPC) API. It replaces the RPC, but not the NIS library entries that used to be in glibc.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/libtirpc/libtirpc-1.2.5.tar.bz2
# Download MD5 sum: 688787ddff7c6a92ef15ae3f5dc4dfa1
# Download size: 504 KB
# Estimated disk space required: 8.3 MB
# Estimated build time: 0.1 SBU
# libtirpc Dependencies
# Optional
# MIT Kerberos V5-1.17.1 for the GSSAPI
###############################################################################
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
setarch i386 ./configure --prefix=/i686 \
--libdir=/i686/lib \
--with-sysroot=/i686 \
--disable-static \
--localstatedir=/var \
--without-krb5 \
--disable-gssapi \
--without-mit-krb5-gssapi \
--without-mit-krb5 \
--without-ndr_krb5pac \
--build=${CLFS_TARGET32} \
--docdir=/i686/share/doc/${packagedir}
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
