#!/bin/bash
###############################################################################
set -e
###############################################################################
# The libpsl package provides a library for accessing and resolving information from the Public Suffix List (PSL). The PSL is a set of domain names beyond the standard suffixes, such as .com.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/rockdaboot/libpsl/releases/download/libpsl-0.21.0/libpsl-0.21.0.tar.gz
# Download MD5 sum: 171e96d887709e36a57f4ee627bf82d2
# Download size: 8.8 MB
# Estimated disk space required: 54 MB
# Estimated build time: Less than 0.1 SBU (including tests)
# libpsl Dependencies
# Required
# libidn2-2.3.0
# Optional
# GTK-Doc-1.32 (for documentation) and Valgrind-3.15.0 (for tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/env python/&3/' src/psl-make-dafsa &&
SAVE_PATH=${PATH}
export PATH=$PATH32
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
