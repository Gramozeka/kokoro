#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} ninja install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
pip install sphinx_rtd_theme
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/Xorg/driver/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
        -Dudev-dir=/lib/udev  \
      -Ddebug-gui=false     \
      -Dtests=false \
  ..
  ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
