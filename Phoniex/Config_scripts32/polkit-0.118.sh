#!/bin/bash
###############################################################################
set -e
###############################################################################
# Polkit is a toolkit for defining and handling authorizations. It is used for allowing unprivileged processes to communicate with privileged processes.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/polkit/releases/polkit-0.116.tar.gz
# Download MD5 sum: 4b37258583393e83069a0e2e89c0162a
# Download size: 1.5 MB
# Estimated disk space required: 15 MB (with tests)
# Estimated build time: 0.4 SBU (with tests)
# Additional Downloads
# Recommended patch: http://www.linuxfromscratch.org/patches/blfs/svn/polkit-0.116-fix_elogind_detection-1.patch
# Polkit Dependencies
# Required
# GLib-2.62.4 and js60-60.8.0
# Recommended
# Linux-PAM-1.3.1 and elogind-241.4
# [Note] Note
# Since elogind uses PAM to register user sessions, it is a good idea to build Polkit with PAM support so elogind can track Polkit sessions.
# Optional (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# docbook-xml-4.5, docbook-xsl-1.79.2, GTK-Doc-1.32, and libxslt-1.1.34
# [Note] Note
# If libxslt-1.1.34 is installed, then docbook-xml-4.5 and docbook-xsl-1.79.2 are required. If you have installed libxslt-1.1.34, but you do not want to install any of the DocBook packages mentioned, you will need to use --disable-man-pages in the instructions below.
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

 chown root:root ${pkg}/i686/lib/polkit-1/polkit-agent-helper-1
chmod 4755  ${pkg}/i686/lib/polkit-1/polkit-agent-helper-1
 chown root:root ${pkg}/i686/bin/pkexec
chmod 4755   ${pkg}/i686/bin/pkexec

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/polkit-0.118-fix_elogind_detection-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoreconf -fiv
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--libexecdir=/i686/libexec \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--with-pam-module-dir=/i686/lib/security \
            --with-os-type=Phoniex   \
--disable-libsystemd-login \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
