#!/bin/bash
###############################################################################
set -e
# The p11-kit package provides a way to load and enumerate PKCS #11 (a Cryptographic Token Interface Standard) modules.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/p11-glue/p11-kit/releases/download/0.23.18.1/p11-kit-0.23.18.1.tar.gz
# Download MD5 sum: 79480c3a2c905a74f86e885966148537
# Download size: 1.2 MB
# Estimated disk space required: 46 MB (add 169 MB for tests)
# Estimated build time: 0.4 SBU (add 0.6 SBU for tests)
# p11-kit Dependencies
# Recommended
# libtasn1-4.15.0 and make-ca-1.5 (runtime)
# Optional
# GTK-Doc-1.32, libxslt-1.1.34, and NSS-3.49.1 (runtime)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
ln -sfv /i686/libexec/p11-kit/trust-extract-compat \
        ${pkg}/i686/bin/update-ca-certificates
ln -sfv ./pkcs11/p11-kit-trust.so ${pkg}/i686/lib/libnssckbi.so
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################

# --with-hash-impl=freebl \
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed '20,$ d' -i trust/trust-extract-compat.in &&
cat >> trust/trust-extract-compat.in << "EOF"
# Copy existing anchor modifications to /etc/ssl/local
/i686/libexec/make-ca/copy-trust-modifications

# Generate a new trust store
/i686/sbin/make-ca -f -g
EOF
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET32}  \
            --with-trust-paths=/etc/pki/anchors \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
