#!/bin/bash
###############################################################################
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
ARCH="i686"
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}
mkdir -pv ${pkg}/i686/lib/pkgconfig
cat > lua.pc << "EOF"
V=5.4
R=5.4.0

prefix=/i686
INSTALL_BIN=${prefix}/bin
INSTALL_INC=${prefix}/include
INSTALL_LIB=${prefix}/lib
INSTALL_MAN=${prefix}/share/man/man1
INSTALL_LMOD=${prefix}/share/lua/${V}
INSTALL_CMOD=${prefix}/lib/lua/${V}
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir=${prefix}/include

Name: Lua
Description: An Extensible Extension Language
Version: ${R}
Requires:
Libs: -L${libdir} -llua -lm -ldl
Cflags: -I${includedir}
EOF
patch -Np1 -i ${PATCHSOURCE}/lua-5.4.0-shared_library-1.patch
sed -i '/#define LUA_ROOT/s:/usr/local/:/i686/:' src/luaconf.h &&
sed -i 's@CC= gcc@CC= /i686/bin/gcc@g' src/Makefile
CC="/i686/bin/gcc" \
CXX="/i686/bin/g++" \
make MYCFLAGS="-DLUA_COMPAT_5_2 -DLUA_COMPAT_5_1" linux \
  CFLAGS="-O2 -march=i686 \$(MYCFLAGS)" \
  MYLDFLAGS=$LDFLAGS \
  SYSCFLAGS=$CPPFLAGS \
  INSTALL_TOP=/i686 \
  INSTALL_LIB=/i686/lib \
  INSTALL_LMOD=/i686/share/lua/5.4 \
  INSTALL_CMOD=/i686/lib/lua/5.4

make install \
  INSTALL_TOP=${pkg}/i686 \
  INSTALL_LIB=${pkg}/i686/lib \
  INSTALL_LMOD=${pkg}/i686/share/lua/5.4 \
  INSTALL_CMOD=${pkg}/i686/lib/lua/5.4  \
     INSTALL_DATA="cp -d"            \
     INSTALL_MAN=${pkg}/i686/share/man/man1 \
     TO_LIB="liblua.so liblua.so.5.4 liblua.so.5.4.0" \
     install
install -v -m644 -D lua.pc ${pkg}/i686/lib/pkgconfig/lua.pc
rm -rf ${pkg}/{usr,etc,var,run,tmp,lib,bin,sbin,lib64}
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
du --all | cut -f 2 | cut -c 2- > i686/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
