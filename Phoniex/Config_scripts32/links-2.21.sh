#!/bin/bash
###############################################################################
set -e
###############################################################################
# Links is a text and graphics mode WWW browser. It includes support for rendering tables and frames, features background downloads, can display colors and has many other features.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://links.twibright.com/download/links-2.20.2.tar.bz2
# Download MD5 sum: ee39e612249440d0497535d0dafc3c0e
# Download size: 6.2 MB
# Estimated disk space required: 36 MB
# Estimated build time: 0.3 SBU
# Links Dependencies
# Recommended
# libevent-2.1.11
# Optional
# Graphics mode requires at least one of GPM-1.20.7 (mouse support to be used with a framebuffer-based console), SVGAlib, DirectFB, and X Window System
# For decoding various image formats Links can utilize libpng-1.6.37, libjpeg-turbo-2.0.4, librsvg-2.46.4, and LibTIFF-4.1.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
if (/i686/bin/pkg-config --exists xorg-server) 
then 
EGL="--enable-graphics"
fi
 

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--build=${CLFS_TARGET} $EGL
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
