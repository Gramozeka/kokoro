#!/bin/bash
###############################################################################
pack_local () {
pkg=${destdir}/$1
make install install-uni install-ref install-psf DESTDIR=${pkg} &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
#patch -Np1 -i ${PATCHSOURCE}/terminus-font.Makefile.paths.diff
# make || exit 1
# make psf
./configure --prefix=/i686 --psfdir=/i686/share/consolefonts \
--x11dir=/i686/share/fonts/terminus
make
pack_local ${packagedir}
if [ -x /i686/bin/mkfontdir ]; then
  /i686/bin/mkfontscale /i686/share/fonts
  /i686/bin/mkfontdir /i686/share/fonts
fi
if [ -x /i686/bin/fc-cache ]; then
  /i686/bin/fc-cache -fv /i686/share/fonts
fi
echo "###########################**** COMPLITE!!! ****##################################"
