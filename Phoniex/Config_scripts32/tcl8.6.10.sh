#!/bin/bash
###############################################################################
set -e
# The Tcl package contains the Tool Command Language, a robust general-purpose scripting language.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/tcl/tcl8.6.10-src.tar.gz
# Download MD5 sum: 97c55573f8520bcab74e21bfd8d0aadc
# Download size: 9.7 MB
# Estimated disk space required: 67 MB (including html documentation)
# Estimated build time: 0.7 SBU (add 2.7 SBU for tests)
# Additional Downloads
# Optional Documentation
# Download (HTTP): https://downloads.sourceforge.net/tcl/tcl8.6.10-html.tar.gz
# Download MD5 sum: a012711241ba3a5bd4a04e833001d489
# Download size: 1.2 MB
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
setarch i386 make DESTDIR=${pkg} install
setarch i386 make install-private-headers DESTDIR=${pkg}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
ln -v -sf tclsh8.6 ${pkg}/i686/bin/tclsh &&
chmod -v 755 ${pkg}/i686/lib/libtcl8.6.so
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}

cd ${packagedir}
tar -xf ${SOURCE}/tcl8.6.10-html.tar.gz --strip-components=1
export SRCDIR=`pwd` &&
cd unix &&
CFLAGS="$CFLAGS -DHAVE_USLEEP=1" \
CXXFLAGS="$CXXFLAGS -DHAVE_USLEEP=1" \
./configure --prefix=/i686 \
--enable-shared \
--mandir=/i686/share/man \
--build=${CLFS_TARGET}
setarch i386 make -j4
 sed -e "s@^\(TCL_SRC_DIR='\).*@\1/i686/include'@" \
     -e "/TCL_B/s@='\(-L\)\?.*unix@='\1/i686/lib@" \
     -i tclConfig.sh                     &&
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
