#!/bin/bash
###############################################################################
set -e
# The Little CMS library is used by other programs to provide color management facilities.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/lcms/lcms-1.19.tar.gz
# Download MD5 sum: 8af94611baf20d9646c7c2c285859818
# Download size: 927 KB
# Estimated disk space required: 27 MB
# Estimated build time: 0.5 SBU
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/lcms-1.19-cve_2013_4276-1.patch
# Little CMS Dependencies
# Optional
# LibTIFF-4.1.0, libjpeg-turbo-2.0.4, and Python-2.7.17 (with SWIG-4.0.1 also)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/lcms-1.19-cve_2013_4276-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--build=${CLFS_TARGET}  --docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
