#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${SOURCE}/python3.readline.set_pre_input_hook.diff.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/python3.no-static-library.diff.gz | patch -p1 --verbose || exit 1
#   zcat ${SOURCE}/python3.x86_64.diff.gz | patch -p1 --verbose || exit 1
sed -i '1s|^#.*/i686/local/bin/python|#!/i686/bin/python3|' Lib/cgi.py
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
CXX=g++ \
./configure --prefix=/i686 \
  --enable-ipv6 \
  --enable-shared \
            --with-system-expat \
            --with-system-ffi   \
            --with-ensurepip=yes \
            --enable-loadable-sqlite-extensions \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
