#!/bin/bash
###############################################################################
set -e
# v4l-utils provides a series of utilities for media devices, allowing to handle the proprietary formats available at most webcams (libv4l), and providing tools to test V4L devices.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.linuxtv.org/downloads/v4l-utils/v4l-utils-1.18.0.tar.bz2
# Download MD5 sum: 18996bd5e9d83d47055c05de376708cd
# Download size: 1.9 MB
# Estimated disk space required: 97 MB
# Estimated build time: 0.5 SBU (using parallelism=4)
# v4l-utils Dependencies
# Required
# LLVM-9.0.1 (with target BPF)
# Recommended
# alsa-lib-1.2.1.2, GLU-9.0.1, libjpeg-turbo-2.0.4, and Qt-5.14.1
# Optional
# Doxygen-1.8.17 and SDL2-2.0.10
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--disable-qt5 \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
