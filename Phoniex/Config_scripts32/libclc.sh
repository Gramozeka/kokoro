#!/bin/bash
###############################################################################
set -e
# libclc is an open source, BSD licensed implementation of the library
# requirements of the OpenCL C programming language, as specified by the
# OpenCL 1.1 Specification. The following sections of the specification
# impose library requirements:
# 
#   * 6.1: Supported Data Types
#   * 6.2.3: Explicit Conversions
#   * 6.2.4.2: Reinterpreting Types Using as_type() and as_typen()
#   * 6.9: Preprocessor Directives and Macros
#   * 6.11: Built-in Functions
#   * 9.3: Double Precision Floating-Point
#   * 9.4: 64-bit Atomics
#   * 9.5: Writing to 3D image memory objects
#   * 9.6: Half Precision Floating-Point
# 
# libclc is intended to be used with the Clang compilers OpenCL frontend.
# 
# libclc is designed to be portable and extensible. To this end, it provides
# generic implementations of most library requirements, allowing the target
# to override the generic implementation at the granularity of individual
# functions.
# 
# libclc currently only supports the PTX target, but support for more
# targets is welcome.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/libclc-20181127_1ecb16d.tar.xz
git clone https://github.com/llvm-mirror/libclc.git
cd ${packagedir}
# mkdir meson-build
# cd meson-build
# cd libclc-20181127_1ecb16d
# patch -Np1 -i ${PATCHSOURCE}/
# CC=gcc CXX=g++ \
CFLAGS="$CFLAGS  -D__extern_always_inline=inline" \
./configure.py \
--with-llvm-config=/i686/bin/llvm-config \
--prefix=/i686 \
--libexecdir=lib/clc \
--pkgconfigdir=/i686/lib/pkgconfig/ \
--includedir=/i686/include/
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
