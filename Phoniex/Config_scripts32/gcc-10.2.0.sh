#!/bin/bash
###############################################################################

###############################################################################
set -e
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
# mkdir -pv ${pkg}/i686/share/gdb/auto-load/i686/lib
# mv -v ${pkg}/i686/lib/*gdb.py ${pkg}/i686/share/gdb/auto-load/i686/lib/
# ln -v -sf ../lib/gcc ${pkg}/i686/lib/gcc
# mkdir -pv ${pkg}/lib
# ln -v -sf ../i686/bin/cpp ${pkg}/lib/cpp
# ln -v -sf gcc ${pkg}/i686/bin/cc
# install -v -dm755 ${pkg}/i686/lib/bfd-plugins
# ln -sfv ../../libexec/gcc/${CLFS_TARGET}/9.3.0/liblto_plugin.so ${pkg}/i686/lib/bfd-plugins/liblto_plugin.so
# chown -v -R root:root \
#     ${pkg}/i686/lib/gcc/${CLFS_TARGET}/9.3.0/include{,-fixed}
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec rm -rf {} \;
fi
if [ -d ${pkg}/i686/share/info ]; then
  find ${pkg}/i686/share/info -type f -exec rm -rf {} \;
fi
( cd ${pkg}
  for file in $(find . -type f -name "*.la") ; do
    cat $file | sed -e 's%-L/gcc-[[:graph:]]* % %g' > $TEMPBUILD/tmp-la-file
    cat $TEMPBUILD/tmp-la-file > $file
  done
  rm $TEMPBUILD/tmp-la-file
)

# Don't ship .la files in /{,i686/}lib${LIBDIRSUFFIX}:
rm -f ${pkg}/{,i686/}lib/*.la

# Strip bloated binaries and libraries:
( cd ${pkg}
  find . -name "lib*so*" -exec strip --strip-unneeded "{}" \;
  find . -name "lib*so*" -exec patchelf --remove-rpath "{}" \;
  find . -name "lib*a" -exec strip -g "{}" \;
  strip --strip-unneeded i686/bin/* 2> /dev/null
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -v ../gcc-build
cd ../gcc-build
export CC="gcc -m32"
export CXX="g++ -m32"
LDFLAGS="-Wl,-rpath,/i686/lib:/usr/lib32" \
setarch i386 ../${packagedir}/configure \
    --prefix=/i686 \
    --build=${CLFS_TARGET32} \
    --disable-static \
    --disable-multilib \
    --enable-languages=c,c++
setarch i386 make -j5
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
