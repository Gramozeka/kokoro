#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/i686/lib/sasl2
make sasldir=/i686/lib/sasl2 DESTDIR=${pkg} install
libtool --mode=install install plugins/libldapdb.la ${pkg}/i686/lib/sasl2
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-LDAP*$//" <<< $(basename $line))
package="$packagedir.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoreconf -fi &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
            --sysconfdir=/etc    \
              --localstatedir=/var \
              --mandir=/i686/share/man \
              --disable-static \
            --enable-auth-sasldb \
            --with-dblib=gdbm \
            --enable-plain \
            --with-dbpath=/var/lib/sasl/sasldb2 \
            --with-saslauthd=/var/run/saslauthd \
            --enable-anon --enable-login \
            --with-openssl \
            --enable-cram --enable-digest --enable-otp  --host=${CLFS_TARGET} --with-ldap --enable-ldapdb
make -j1 sasldir=/i686/lib/sasl2
make -j1 -C include &&
make -j1 -C sasldb &&
make -j1 -C plugins
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
ldconfig
