#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
install -vdm755 ${pkg}/i686/share/xml/docbook/schema/{dtd,rng,sch,xsd}/5.0 &&
install -vm644  dtd/* ${pkg}/i686/share/xml/docbook/schema/dtd/5.0         &&
install -vm644  rng/* ${pkg}/i686/share/xml/docbook/schema/rng/5.0         &&
install -vm644  sch/* ${pkg}/i686/share/xml/docbook/schema/sch/5.0         &&
install -vm644  xsd/* ${pkg}/i686/share/xml/docbook/schema/xsd/5.0
echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
 
ARCH="noarch"

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
if [ ! -e /etc/xml/docbook-5.0 ]; then
    xmlcatalog --noout --create /etc/xml/docbook-5.0
fi &&

xmlcatalog --noout --add "public" \
  "-//OASIS//DTD DocBook XML 5.0//EN" \
  "file:///i686/share/xml/docbook/schema/dtd/5.0/docbook.dtd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "system" \
  "http://www.oasis-open.org/docbook/xml/5.0/dtd/docbook.dtd" \
  "file:///i686/share/xml/docbook/schema/dtd/5.0/docbook.dtd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "system" \
  "http://docbook.org/xml/5.0/dtd/docbook.dtd" \
  "file:///i686/share/xml/docbook/schema/dtd/5.0/docbook.dtd" \
  /etc/xml/docbook-5.0 &&

xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rng/docbook.rng" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbook.rng" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbook.rng" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbook.rng" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rng/docbookxi.rng" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbookxi.rng" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbookxi.rng" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbookxi.rng" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rnc/docbook.rnc" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbook.rnc" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbook.rnc" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbook.rnc" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rnc/docbookxi.rnc" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbookxi.rnc" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbookxi.rnc" \
  "file:///i686/share/xml/docbook/schema/rng/5.0/docbookxi.rnc" \
  /etc/xml/docbook-5.0 &&

xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/docbook.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/docbook.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/docbook.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/docbook.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/docbookxi.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/docbookxi.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/docbookxi.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/docbookxi.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/xi.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/xi.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/xi.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/xi.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/xlink.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/xlink.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/xlink.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/xlink.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/xml.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/xml.xsd" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/xml.xsd" \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/xml.xsd" \
  /etc/xml/docbook-5.0 &&

xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/sch/docbook.sch" \
  "file:///i686/share/xml/docbook/schema/sch/5.0/docbook.sch" \
  /etc/xml/docbook-5.0 &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/sch/docbook.sch" \
  "file:///i686/share/xml/docbook/schema/sch/5.0/docbook.sch" \
  /etc/xml/docbook-5.0
  
  xmlcatalog --noout --create /i686/share/xml/docbook/schema/dtd/5.0/catalog.xml &&

xmlcatalog --noout --add "public" \
  "-//OASIS//DTD DocBook XML 5.0//EN" \
  "docbook.dtd" /i686/share/xml/docbook/schema/dtd/5.0/catalog.xml &&
xmlcatalog --noout --add "system" \
  "http://www.oasis-open.org/docbook/xml/5.0/dtd/docbook.dtd" \
  "docbook.dtd" /i686/share/xml/docbook/schema/dtd/5.0/catalog.xml &&

xmlcatalog --noout --create /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbook.rng" \
  "docbook.rng" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rng/docbook.rng" \
  "docbook.rng" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbookxi.rng" \
  "docbookxi.rng" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rng/docbookxi.rng" \
  "docbookxi.rng" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbook.rnc" \
  "docbook.rnc" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rng/docbook.rnc" \
  "docbook.rnc" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/rng/docbookxi.rnc" \
  "docbookxi.rnc" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/rng/docbookxi.rnc" \
  "docbookxi.rnc" /i686/share/xml/docbook/schema/rng/5.0/catalog.xml &&

xmlcatalog --noout --create /i686/share/xml/docbook/schema/sch/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/sch/docbook.sch" \
  "docbook.sch" /i686/share/xml/docbook/schema/sch/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/sch/docbook.sch" \
  "docbook.sch" /i686/share/xml/docbook/schema/sch/5.0/catalog.xml &&

xmlcatalog --noout --create /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/docbook.xsd" \
  "docbook.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/docbook.xsd" \
  "docbook.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/docbookxi.xsd" \
  "docbookxi.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://www.oasis-open.org/docbook/xml/5.0/xsd/docbookxi.xsd" \
  "docbookxi.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
  "http://docbook.org/xml/5.0/xsd/xlink.xsd" \
  "xlink.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
   "http://www.oasis-open.org/docbook/xml/5.0/xsd/xlink.xsd" \
   "xlink.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
   "http://docbook.org/xml/5.0/xsd/xml.xsd" \
   "xml.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml &&
xmlcatalog --noout --add "uri" \
   "http://www.oasis-open.org/docbook/xml/5.0/xsd/xml.xsd" \
   "xml.xsd" /i686/share/xml/docbook/schema/xsd/5.0/catalog.xml
if [ ! -e /etc/xml/catalog ]; then
    xmlcatalog --noout --create /etc/xml/catalog
fi &&
xmlcatalog --noout --add "delegatePublic" \
  "-//OASIS//DTD DocBook XML 5.0//EN" \
  "file:///i686/share/xml/docbook/schema/dtd/5.0/catalog.xml" \
  /etc/xml/catalog &&
xmlcatalog --noout --add "delegateSystem" \
  "http://docbook.org/xml/5.0/dtd/" \
  "file:///i686/share/xml/docbook/schema/dtd/5.0/catalog.xml" \
  /etc/xml/catalog &&
xmlcatalog --noout --add "delegateURI" \
  "http://docbook.org/xml/5.0/dtd/" \
  "file:///i686/share/xml/docbook/schema/dtd/5.0/catalog.xml" \
  /etc/xml/catalog &&
xmlcatalog --noout --add "delegateURI" \
  "http://docbook.org/xml/5.0/rng/"  \
  "file:///i686/share/xml/docbook/schema/rng/5.0/catalog.xml" \
  /etc/xml/catalog &&
xmlcatalog --noout --add "delegateURI" \
  "http://docbook.org/xml/5.0/sch/"  \
  "file:///i686/share/xml/docbook/schema/sch/5.0/catalog.xml" \
  /etc/xml/catalog &&
xmlcatalog --noout --add "delegateURI" \
  "http://docbook.org/xml/5.0/xsd/"  \
  "file:///i686/share/xml/docbook/schema/xsd/5.0/catalog.xml" \
  /etc/xml/catalog
  
