#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
 
sed -i '/if Py/{s/Py/(Py/;s/)/))/}' python/{types.c,libxml.c}
sed -i '/_PyVerify_fd/,+1d' python/types.c
sed -i 's/ TRUE/ true/' encoding.c
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
setarch i386 ./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--docdir=/i686/share/doc/${packagedir} \
            --with-history   \
            --with-python=/i686/bin/python3 &&
setarch i386 make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
