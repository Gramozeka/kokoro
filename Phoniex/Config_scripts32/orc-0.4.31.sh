#!/bin/bash
###############################################################################
set -e
# https://gstreamer.freedesktop.org/src/orc/orc-0.4.31.tar.xz
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
