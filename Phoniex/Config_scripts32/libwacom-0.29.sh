#!/bin/bash
###############################################################################
set -e
# The libwacom package contains a library used to identify wacom tablets and their model-specific features.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/linuxwacom/libwacom-0.29.tar.bz2
# Download MD5 sum: 3a6b614b57518b25a102dcd9e51d6681
# Download size: 484 KB
# Estimated disk space required: 5.5 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# libwacom Dependencies
# Required
# libgudev-233
# Recommended
# libxml2-2.9.10
# Optional
# git-2.25.0, GTK+-2.24.32, and librsvg-2.46.4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
