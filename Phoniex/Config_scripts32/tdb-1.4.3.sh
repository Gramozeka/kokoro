#!/bin/bash
###############################################################################
set -e
# This is a simple database API. It was inspired by the realisation that in Samba we have several ad-hoc bits of code that essentially implement small databases for sharing structures between parts of Samba.
# The interface is based on gdbm. gdbm couldn't be use as we needed to be able to have multiple writers to the databases at one time.'
# Download
# https://www.samba.org/ftp/tdb/tdb-1.4.3.tar.gz
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
export PATH=$SAVE_PATH
make -j4

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
