#!/bin/bash
###############################################################################
set -e
# Libsndfile is a library of C routines for reading and writing files containing sampled audio data.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.mega-nerd.com/libsndfile/files/libsndfile-1.0.28.tar.gz
# Download MD5 sum: 646b5f98ce89ac60cdb060fcd398247c
# Download size: 1.1 MB
# Estimated disk space required: 34 MB (with tests)
# Estimated build time: 0.6 SBU (with tests)
# libsndfile Dependencies
# Recommended
# FLAC-1.3.3, libogg-1.3.4, and libvorbis-1.3.6
# Optional
# alsa-lib-1.2.1.2 and SQLite-3.31.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
