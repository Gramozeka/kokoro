#!/bin/bash
###############################################################################
set -e
###############################################################################
# The CDParanoia package contains a CD audio extraction tool. This is useful for extracting .wav files from audio CDs. A CDDA capable CDROM drive is needed. Practically all drives supported by Linux can be used.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.xiph.org/releases/cdparanoia/cdparanoia-III-10.2.src.tgz
# Download MD5 sum: b304bbe8ab63373924a744eac9ebc652
# Download size: 179 KB
# Estimated disk space required: 2.9 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/cdparanoia-III-10.2-gcc_fixes-1.patch
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
install -d ${pkg}/i686/bin
install -d ${pkg}/i686/include/cdda
install -d ${pkg}/i686/lib
install -m 0755 cdparanoia ${pkg}/i686/bin/
install -m 0644 utils.h paranoia/cdda_paranoia.h interface/cdda_interface.h \
	${pkg}/i686/include/cdda/
install -m 0755 paranoia/libcdda_paranoia.so.0.10.? \
	interface/libcdda_interface.so.0.10.? \
	${pkg}/i686/lib/
install -m 0644 paranoia/libcdda_paranoia.a interface/libcdda_interface.a \
	${pkg}/i686/lib/

ldconfig -n ${pkg}/i686/lib

( cd ${pkg}/i686/lib
  ln -sf libcdda_paranoia.so.0.10.? libcdda_paranoia.so
  ln -sf libcdda_interface.so.0.10.? libcdda_interface.so
)
chmod -v 755 ${pkg}/i686/lib/libcdda_*.so.0.10.2
echo "$1 ----- $(date)" >> ${destdir}/loginstall
rm -rf ${pkg}/{usr,etc,var,run,tmp,lib,bin,sbin,lib64}
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/cdparanoia-III-10.2-gcc_fixes-1.patch
autoreconf -vfi
# CFLAGS=$CFLAGS \
# CXXFLAGS=$CXXFLAGS \
LDFLAGS="-L/i686/lib" \
CPPFLAGS="-I/i686/include" \
OPT="-O2 -march=i586 -mtune=i686" \
./configure --prefix=/i686 \
--libdir=/i686/lib \
--disable-static \
--localstatedir=/var \
--includedir=/i686/include/cdda \
--mandir=/i686/share/man \
--build=${CLFS_TARGET}
make -j1 OPT="-O2 -march=i586 -mtune=i686"

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
