#!/bin/bash
###############################################################################
set -e
# The libmpeg2 package contains a library for decoding MPEG-2 and MPEG-1 video streams. The library is able to decode all MPEG streams that conform to certain restrictions: “constrained parameters” for MPEG-1, and “main profile” for MPEG-2. This is useful for programs and applications needing to decode MPEG-2 and MPEG-1 video streams.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://libmpeg2.sourceforge.net/files/libmpeg2-0.5.1.tar.gz
# Download (FTP): ftp://ftp.mirrorservice.org/sites/distfiles.gentoo.org/distfiles/libmpeg2-0.5.1.tar.gz
# Download MD5 sum: 0f92c7454e58379b4a5a378485bbd8ef
# Download size: 513 KB
# Estimated disk space required: 6 MB
# Estimated build time: 0.1 SBU
# libmpeg2 Dependencies
# Optional
# X Window System and SDL-1.2.15
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/static const/static/' libmpeg2/idct_mmx.c &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--enable-shared  \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
