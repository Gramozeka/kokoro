#!/bin/bash
###############################################################################
set -e
###############################################################################
# elogind is the systemd project's "logind", extracted out to be a standalone daemon. It integrates with Linux-PAM-1.3.1 to know the set of users that are logged in to a system and whether they are logged in graphically, on the console, or remotely. Elogind exposes this information via the standard org.freedesktop.login1 D-Bus interface, as well as through the file system using systemd's standard /run/systemd layout.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/elogind/elogind/archive/v241.4/elogind-241.4.tar.gz
# Download MD5 sum: fb5ba9da1115d68d2b6cdbcd10167b7d
# Download size: 1.3 MB
# Estimated disk space required: 56 MB (add 1 MB for tests)
# Estimated build time: 0.4 SBU (NINJAJOBS=1)
# elogind Dependencies
# Required
# dbus-1.12.16
# Recommended
# docbook-xml-4.5, docbook-xsl-1.79.2, and libxslt-1.1.34 (to build the man pages), Linux-PAM-1.3.1, and Polkit-0.116 (runtime)
# Optional
# For the tests: lxml-4.4.2, gobject-introspection-1.62.0, zsh-5.7.1, Valgrind-3.15.0, audit-userspace, bash-completion, kexec, and SELinux
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} /i686/bin/ninja install
ln -sfv  libelogind.pc ${pkg}/i686/lib/pkgconfig/libsystemd.pc &&
ln -sfvn elogind ${pkg}/i686/include/systemd
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree
rm -rf usr etc var run tmp lib bin sbin
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
 unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=/i686/lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dcgroup-controller=elogind  \
  -Drootlibdir=/i686/lib \
  -Drootprefix=/i686 \
  -Dpamlibdir=/i686/lib \
  ..
sed -i "s/-fPIC//g" build.ninja
/i686/bin/ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
