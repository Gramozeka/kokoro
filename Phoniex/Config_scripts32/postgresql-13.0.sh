#!/bin/bash
###############################################################################
set -e
###############################################################################
# PostgreSQL is an advanced object-relational database management system (ORDBMS), derived from the Berkeley Postgres database management system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.postgresql.org/pub/source/v12.1/postgresql-12.1.tar.bz2
# Download MD5 sum: 2ee1bd4ec5f49363a3f456f07e599b41
# Download size: 19 MB
# Estimated disk space required: 184 MB (add 36 MB for tests)
# Estimated build time: 1.0 SBU (with parallelism=4, add 0.1 SBU for tests)
# PostgreSQL Dependencies
# Optional
# Python-2.7.17, Tcl-8.6.10, libxml2-2.9.10, libxslt-1.1.34, OpenLDAP-2.4.48, Linux-PAM-1.3.1, MIT Kerberos V5-1.17.1 and Bonjour
# Optional (To Regenerate Documentation)
# fop-2.4, docbook-4.5, docbook-dsssl-1.79, DocBook-utils-0.6.14, OpenJade-1.3.2, and SGMLSpm-1.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/DEFAULT_PGSOCKET_DIR/s@/tmp@/run/postgresql@' src/include/pg_config_manual.h &&
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--enable-thread-safety \
--with-openssl \
--with-perl \
--with-tcl \
--build=${CLFS_TARGET} \
--docdir=/i686/share/doc/${packagedir}
make -j4
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
