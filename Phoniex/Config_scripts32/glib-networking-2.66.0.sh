#!/bin/bash
###############################################################################
set -e
# The GObject Introspection is used to describe the program APIs and collect them in a uniform, machine readable format.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.62/gobject-introspection-1.62.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.62/gobject-introspection-1.62.0.tar.xz
# Download MD5 sum: 37278eab3704e42234b6080b8cf241f1
# Download size: 950 KB
# Estimated disk space required: 40 MB (with tests)
# Estimated build time: 0.2 SBU (Using parallelism=4; with tests)
# Required
# GLib-2.62.4
# Optional
# Cairo-1.17.2+f93fc72c03e (required for the tests), Gjs-1.58.4 (to satisfy one test), GTK-Doc-1.32, Mako-1.1.1, and Markdown (to satisfy one test)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dopenssl=enabled \
  ..
  /i686/bin/ninja -j5
  export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
