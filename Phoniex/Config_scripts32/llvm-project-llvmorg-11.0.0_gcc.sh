
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

DESTDIR=${pkg} /i686/bin/ninja install
PY2=2.7
PY3=3.9
for i in ccc c++; do
  ln -s /i686/libexec/$i-analyzer \
    ${pkg}/i686/bin/$i-analyzer || exit 1
done

if [ ! -r ${pkg}/i686/bin/lit-cpuid ]; then
  cp -a bin/lit-cpuid ${pkg}/i686/bin/lit-cpuid
  chown root:root ${pkg}/i686/bin/lit-cpuid
  chmod 755 ${pkg}/i686/bin/lit-cpuid
fi

mv -f ${pkg}/i686/lib/{,LLVM_}libgomp.so

for pyver in ${PY2} ${PY3}; do
  mkdir -p "${pkg}/i686/lib/python$pyver/site-packages"
  cp -a ../clang/bindings/python/clang "${pkg}/i686/lib/python$pyver/site-packages/"
done

# # # rm -f "${pkg}/i686/lib64/python${PY2}/site-packages/six.py"

/i686/bin/python -m compileall "${pkg}/i686/lib64/python${PY2}/site-packages/clang"
/i686/bin/python -O -m compileall "${pkg}/i686/lib64/python${PY2}/site-packages/clang"
/i686/bin/python3 -m compileall "${pkg}/i686/lib64/python${PY3}/site-packages/clang"
/i686/bin/python3 -O -m compileall "${pkg}/i686/lib64/python${PY3}/site-packages/clang"
/i686/bin/python3 -m compileall "${pkg}/i686/lib64/python${PY3}/site-packages/lldb"
/i686/bin/python3 -O -m compileall "${pkg}/i686/lib64/python${PY3}/site-packages/lldb"
/i686/bin/python3 -m compileall "${pkg}/i686/share/scan-view"
/i686/bin/python3 -O -m compileall "${pkg}/i686/share/scan-view"
/i686/bin/python -m compileall "${pkg}/i686/share/clang"
/i686/bin/python -O -m compileall "${pkg}/i686/share/clang"
/i686/bin/python3 -m compileall "${pkg}/i686/share/opt-viewer"
/i686/bin/python3 -O -m compileall "${pkg}/i686/share/opt-viewer"

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p i686/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > i686/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_gcc*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
grep -rl '#!.*python' | xargs sed -i '1s/python$/python3/'
cd polly
# patch -Np1 -i ${PATCHSOURCE}/
cat ${PATCHSOURCE}/llvm.polly.hack.diff | patch -p1 --verbose || exit 1
cd ../clang
cat ${PATCHSOURCE}/clang.toolchains.i586.triple.diff | patch -p1 --verbose || exit 1
cd ../
mkdir build
cd build
  /i686/bin/cmake  \
    -DCMAKE_C_COMPILER="/i686/bin/gcc" \
    -DCMAKE_CXX_COMPILER="/i686/bin/g++" \
    -DCMAKE_C_FLAGS="-O2" \
    -DCMAKE_CXX_FLAGS="-O2" \
    -DPython3_EXECUTABLE="/i686/bin/python3" \
    -DCMAKE_INSTALL_PREFIX=/i686 \
    -DLLVM_BUILD_LLVM_DYLIB=ON  \
    -DLLVM_LINK_LLVM_DYLIB=ON \
    -DLLVM_LIBDIR_SUFFIX="" \
    -DCMAKE_BUILD_TYPE=Release \
    -DLLVM_USE_LINKER=gold \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_FFI=ON \
    -DLLVM_ENABLE_LIBEDIT=ON \
	-DLLVM_ENABLE_TERMINFO=ON \
	-DLLVM_ENABLE_LIBXML2=ON \
    -DLLVM_ENABLE_ASSERTIONS=OFF \
    -DLLVM_INSTALL_UTILS=ON \
    -DLLVM_BINUTILS_INCDIR=/i686/include \
    -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;libcxx;libcxxabi;lldb;compiler-rt;lld;polly;libclc;openmp" \
    -DLLVM_TARGETS_TO_BUILD="host;AMDGPU;BPF" \
    -DLLDB_USE_SYSTEM_SIX=1 \
    -DLLVM_DEFAULT_TARGET_TRIPLE=${CLFS_TARGET32} \
    -DLLVM_HOST_TRIPLE=${CLFS_TARGET32} \
    -Wno-dev \
    -G Ninja ../llvm
    /i686/bin/ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
