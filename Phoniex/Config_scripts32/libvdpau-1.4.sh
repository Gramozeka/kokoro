#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/Xorg/driver/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
SAVE_PATH=${PATH}
export PATH=$PATH32
LLVM_INSTALL_DIR=/i686 \
CLANG_INSTALL_DIR=/i686 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
LLVM_LDFLAGS="-L/i686/lib" \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
/i686/bin/ninja -j5 || exit 1
export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
