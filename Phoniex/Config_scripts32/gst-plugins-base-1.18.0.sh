#!/bin/bash
###############################################################################
set -e
# gstreamer is a streaming media framework that enables applications to share a common set of plugins for things like video encoding and decoding, audio encoding and decoding, audio and video filters, audio visualisation, web streaming and anything else that streams in real-time or otherwise. This package only provides base functionality and libraries. You may need at least gst-plugins-base-1.16.2 and one of Good, Bad, Ugly or Libav plugins.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-1.16.2.tar.xz
# Download MD5 sum: 0e661ed5bdf1d8996e430228d022628e
# Download size: 3.2 MB
# Estimated disk space required: 60 MB (with tests; add 36 MB for docs)
# Estimated build time: 0.4 SBU (Using parallelism=4; with tests; add 1.5 SBU for docs)
# gstreamer Dependencies
# Required
# GLib-2.62.4
# Recommended
# gobject-introspection-1.62.0
# Optional
# GTK+-3.24.13 (for examples), Gsl-2.6, GTK-Doc-1.32, Valgrind-3.15.0, and libunwind
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/meson setup \
  --prefix=/i686 \
  --libdir=lib \
  --libexecdir=/i686/libexec \
  --bindir=/i686/bin \
  --sbindir=/i686/sbin \
  --includedir=/i686/include \
  --datadir=/i686/share \
  --mandir=/i686/share/man \
  --localstatedir=/var \
  --buildtype=release \
         -Dgst_debug=false   \
       -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
  export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
