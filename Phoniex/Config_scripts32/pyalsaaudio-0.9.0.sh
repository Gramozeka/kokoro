###############################################################################
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
/i686/bin/python$USE_PY setup.py install \
--optimize=1  \
--root=${pkg} &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
# cd $TEMPBUILD
# rm -rf *
# package="$(basename $line).tar.*"
# packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
# cd ${packagedir}
# # sed -i 's|#include "alsa/asoundlib.h"|#include <asoundlib.h>|' pyalsa/alsacard.c
# USE_PY="2"
# CFLAGS="$CFLAGS -I/usr/include" \
# CXXFLAGS=$CXXFLAGS \
# python$USE_PY setup.py build
# pack_local64 ${packagedir}_$USE_PY
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
USE_PY="3"
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
/i686/bin/python$USE_PY setup.py build
pack_local64 ${packagedir}_$USE_PY
echo "###########################**** COMPLITE!!! ****##################################"
