#!/bin/bash
###############################################################################
set -e
# The HarfBuzz package contains an OpenType text shaping engine.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/harfbuzz/release/harfbuzz-2.6.4.tar.xz
# Download MD5 sum: 2b3a4dfdb3e5e50055f941978944da9f
# Download size: 5.7 MB
# Estimated disk space required: 163 MB (add 24 MB for tests)
# Estimated build time: 0.5 SBU (Using parallelism=4; add 0.4 SBU for tests)
# HarfBuzz Dependencies
# Recommended
# GLib-2.62.4 (required for Pango), Graphite2-1.3.13 (required for building texlive-20190410 or LibreOffice-6.3.4.2 with system harfbuzz), ICU-65.1, and FreeType-2.10.1 (after HarfBuzz-2.6.4 is installed, reinstall FreeType-2.10.1)
# Optional
# Cairo-1.17.2+f93fc72c03e (circular: build cairo and all its recommended dependencies, including harfbuzz, first, then rebuild harfbuzz if the cairo backend is needed), gobject-introspection-1.62.0, GTK-Doc-1.32, and FontTools (Python 2 or Python 3 module, for the testsuite)
# [Warning] Warning
# Recommended dependencies are not strictly required to build the package. However, you might not get expected results at runtime if you dont install them. Please do not report bugs with this package if you have not installed the recommended dependencies.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
SAVE_PATH=${PATH}
export PATH=$PATH32
PYTHON=/i686/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--sysconfdir=/etc \
--libdir=/i686/lib \
--disable-static \
--with-gobject \
--with-graphite2 \
--build=${CLFS_TARGET}  \
--docdir=/i686/share/doc/${packagedir}
make -j4
  export PATH=$SAVE_PATH
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
