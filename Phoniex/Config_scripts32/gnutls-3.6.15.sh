#!/bin/bash
###############################################################################
set -e
# The GnuTLS package contains libraries and userspace tools which provide a secure layer over a reliable transport layer. Currently the GnuTLS library implements the proposed standards by the IETFs TLS working group. Quoting from the TLS protocol specification:
# “The TLS protocol provides communications privacy over the Internet. The protocol allows client/server applications to communicate in a way that is designed to prevent eavesdropping, tampering, or message forgery.”
# GnuTLS provides support for TLS 1.3, TLS 1.2, TLS 1.1, TLS 1.0, and SSL 3.0 protocols, TLS extensions, including server name and max record size. Additionally, the library supports authentication using the SRP protocol, X.509 certificates and OpenPGP keys, along with support for the TLS Pre-Shared-Keys (PSK) extension, the Inner Application (TLS/IA) extension and X.509 and OpenPGP certificate handling.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/gnutls-3.6.11.1.tar.xz
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/gnutls/v3.6/gnutls-3.6.11.1.tar.xz
# Download MD5 sum: 3670ee0b0d95b3dee185eff2dc910ee7
# Download size: 5.6 MB
# Estimated disk space required: 146 MB (add 105 MB for tests)
# Estimated build time: 0.6 SBU (using parallelism=4; add 7.6 SBU for tests)
# GnuTLS Dependencies
# Required
# Nettle-3.5.1
# Recommended
# make-ca-1.5, libunistring-0.9.10, libtasn1-4.15.0, and p11-kit-0.23.18.1
# Optional
# Doxygen-1.8.17, GTK-Doc-1.32, Guile-3.0.0, libidn-1.35 or libidn2-2.3.0 Net-tools-CVS_20101030 (used during the test suite), texlive-20190410 or install-tl-unx, Unbound-1.9.6 (to build the DANE library), Valgrind-3.15.0 (used during the test suite), autogen, cmocka and datefudge (used during the test suite if the DANE library is built), and Trousers (Trusted Platform Module support)
# [Note] Note
# Note that if you do not install libtasn1-4.15.0, an older version shipped in the GnuTLS tarball will be used instead.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/i686 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET32}  \
--docdir=/i686/share/doc/${packagedir} \
            --disable-guile \
            --with-default-trust-store-pkcs11="pkcs11:" 
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
