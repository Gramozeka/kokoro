
###############################################################################
set -e
# The libwebp package contains a library and support programs to encode and decode images in WebP format.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://downloads.webmproject.org/releases/webp/libwebp-1.1.0.tar.gz
# Download MD5 sum: 7e047f2cbaf584dff7a8a7e0f8572f18
# Download size: 3.8 MB
# Estimated disk space required: 45 MB
# Estimated build time: 0.3 SBU
# libwebp Dependencies
# Recommended
# libjpeg-turbo-2.0.4, libpng-1.6.37, LibTIFF-4.1.0, and SDL-1.2.15
# Optional
# Freeglut-3.2.1 and giflib-5.2.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
            --enable-libwebpmux     \
            --enable-libwebpdemux   \
            --enable-libwebpdecoder \
            --enable-libwebpextras  \
            --enable-swap-16bit-csp \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
