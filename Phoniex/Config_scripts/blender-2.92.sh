
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# DESTDIR=${pkg} ninja install

# BLENDERBINS="blender"
# for bin in $BLENDERBINS ; do
#   mv ${pkg}/usr/bin/$bin ${pkg}/usr/bin/$bin.bin
#   cat <<EOF >${pkg}/usr/bin/$bin
# #!/bin/sh
# export PYTHONPATH=/usr/share/blender/2.92/python/lib64/python3.9
# export LD_LIBRARY_PATH=/usr/lib64/opencollada
# exec $bin.bin "\$@"
# EOF
#   chmod 0755 ${pkg}/usr/bin/$bin
# done
# 
# mkdir -p ${pkg}/usr/share/man/man1
# python3 ../doc/manpage/blender.1.py bin/blender ${pkg}/usr/share/man/man1/blender.1

# # python3 -m compileall "${pkg}/usr/share/blender"
# # python3 -O -m compileall "${pkg}/usr/share/blender"
install -Dm755 ../blender/release/bin/blender-softwaregl "${pkg}/usr/bin/blender-softwaregl"
install -Dm644 ../blender/release/freedesktop/org.blender.Blender.appdata.xml "${pkgdir}/usr/share/metainfo/org.blender.Blender.appdata.xml"

for dirs in ${pkg}/usr/share/blender/2.92/scripts/*;do
python3 -m compileall $dirs/
python3 -O -m compileall $dirs/
done

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
# rm -rf *
# package="$(basename $line).*"
packagedir=$(basename $line)
# 
# unpack ${SOURCE}/${package}
cd ${packagedir}
# export PYTHONHOME=/usr/lib64/python3.9
# export PYTHONPATH=/usr/lib64/python3.9
# # patch -p1 -i $PATCHSOURCE/blender.git-91aeb452ab251b307311fe869e8e14df945ec6bc.patch
# #     mkdir build
    cd    build
# CFLAGS=$CFLAGS \
# CXXFLAGS=$CXXFLAGS \
#              cmake  \
#              -G Ninja \
#              -DCMAKE_C_COMPILER_LAUNCHER=ccache \
#              -DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
#              -DCMAKE_C_FLAGS=$CFLAGS \
#              -DCMAKE_CXX_FLAGS=$CFLAGS \
#              -DCMAKE_INSTALL_PREFIX=/usr \
#              -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
#              -DWITH_INSTALL_PORTABLE:BOOL=OFF \
#              -DPYTHON_VERSION=3.9 \
#              -DPYTHON_INCLUDE_DIR:PATH=/usr/include/python3.9/ \
#     -DPYTHON_INCLUDE_CONFIG_DIR:PATH=/usr/include/python3.9/ \
#     -DPYTHON_SITE_PACKAGES:FILEPATH=/usr/lib64/python3.9/site-packages/ \
#     ../blender
# #     -DWITH_BUILDINFO:BOOL=ON \
# #     -DWITH_OPENCOLLADA:BOOL=ON \
# #     -DWITH_OPENCOLORIO:BOOL=ON \
# #     -DWITH_PYTHON_INSTALL:BOOL=OFF \
# #     -DWITH_CODEC_FFMPEG:BOOL=ON \
# #     -DWITH_OPENAL:BOOL=ON \
# #     -DWITH_JACK:BOOL=ON \
# #     -DWITH_JACK_DYNLOAD:BOOL=ON \
# #     -DPYTHON_LIBPATH:PATH=/usr/lib64 \
# #     -DWITH_CODEC_SNDFILE:BOOL=ON \
# #     -DWITH_FFTW3:BOOL=ON \
# #     -DWITH_IMAGE_OPENJPEG:BOOL=ON \
# #     -DWITH_SYSTEM_EIGEN3:BOOL=ON \
# #     -DWITH_SYSTEM_GLEW:BOOL=OFF \
# #     -DWITH_BOOST:BOOL=OFF \
# #     -DWITH_SYSTEM_LZO:BOOL=ON \
# #     -DWITH_MEM_JEMALLOC:BOOL=ON \
# #     -DWITH_MEM_VALGRIND:BOOL=ON \
# #     -DWITH_MOD_OCEANSIM:BOOL=ON \
# #     -DWITH_SDL:BOOL=ON \
# #     -DWITH_SDL_DYNLOAD:BOOL=ON \
# #     -DCMAKE_BUILD_TYPE=Release \
# #     -DUSD_LIBRARY:FILEPATH=/usr/lib64 \
# #     -DEMBREE_LIBRARY:FILEPATH=/usr/lib64 \
# #     -D_embree_LIBRARIES=/usr/lib64 \
# #     -DUSD_LIBRARY:FILEPATH=/usr/lib64/libusd.so \
# #     -DUSD_INCLUDE_DIR:PATH=/usr/include/pxr/ \
#              
# export CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:/usr/include/python3.9/"
# ninja
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# /usr/lib64/python3.9', '/usr/lib64/python3.9/lib-dynload', '/usr/lib64/python3.9/site-packages
#     
# cmake ../blender     -DCMAKE_INSTALL_PREFIX=/opt/blender     -DWITH_INSTALL_PORTABLE=OFF     -DWITH_BUILDINFO=OFF              -DPYTHON_VERSION=3.9              -DPYTHON_INCLUDE_DIR:PATH=/usr/include/python3.9/     -DPYTHON_INCLUDE_CONFIG_DIR:PATH=/usr/include/python3.9/     -DPYTHON_SITE_PACKAGES:FILEPATH=/usr/lib64/python3.9/site-packages/ -G Ninja
