
###############################################################################
set -e
###############################################################################
# Cbindgen can be used to generate C bindings for Rust code.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/eqrion/cbindgen/archive/v0.12.2/cbindgen-0.12.2.tar.gz
# Download MD5 sum: 85a5dd56c2f6cc3ffb04291610918761
# Download size: 148 KB
# Estimated disk space required: 91 MB (add 520 MB for tests)
# Estimated build time: 1.2 SBU (Using parallelism=4; add 0.5 SBU for tests)
# cbindgen Dependencies
# Required
# rustc-1.37.0
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/bin
install -Dm755 target/release/cbindgen ${pkg}/usr/bin/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cargo build --release
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
