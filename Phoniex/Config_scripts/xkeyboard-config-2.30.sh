
###############################################################################
set -e
###############################################################################
# The XKeyboardConfig package contains the keyboard configuration database for the X Window System.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.x.org/pub/individual/data/xkeyboard-config/xkeyboard-config-2.28.tar.bz2
# Download (FTP): ftp://ftp.x.org/pub/individual/data/xkeyboard-config/xkeyboard-config-2.28.tar.bz2
# Download MD5 sum: 5a968ab77846ff85a04242410b2a61de
# Download size: 1.6 MB
# Estimated disk space required: 14 MB
# Estimated build time: less than 0.1 SBU
# XKeyboardConfig Dependencies
# Required
# Xorg Libraries
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET}  \
--with-xkb-rules-symlink=xorg \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
