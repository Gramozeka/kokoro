
###############################################################################
set -e
# The GTK+ 3 package contains libraries used for creating graphical user interfaces for applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtk+/3.24/gtk+-3.24.13.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gtk+/3.24/gtk+-3.24.13.tar.xz
# Download MD5 sum: f65515e7bfa2199bd2188e871d69c686
# Download size: 22 MB
# Estimated disk space required: 465 MB (add 4 MB for tests)
# Estimated build time: 1.0 SBU (using parallelism=4, add 0.4 SBU for tests)
# GTK+ 3 Dependencies
# Required
# at-spi2-atk-2.34.1, FriBidi-1.0.8, gdk-pixbuf-2.40.0, libepoxy-1.5.4, and Pango-1.44.7
# Recommended
# adwaita-icon-theme-3.34.3 (default for some gtk+3 settings keys, also needed for tests), hicolor-icon-theme-0.17 (needed for tests), ISO Codes-4.4, libxkbcommon-0.10.0, sassc-3.6.1, Wayland-1.17.0, and wayland-protocols-1.18
# Recommended (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# colord-1.4.4, Cups-2.3.1, DocBook-utils-0.6.14, GTK-Doc-1.32, JSON-GLib-1.4.4, PyAtSpi2-2.34.0 (for tests), rest-0.8.1, and PAPI

###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
      -Dgtk_doc=false   \
      -Dman=true        \
      -Dbroadway_backend=true \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# /i686/bin/gtk-query-immodules-3.0 --update-cache
# /i686/bin/glib-compile-schemas /usr/share/glib-2.0/schemas
gtk-query-immodules-3.0 --update-cache
glib-compile-schemas /usr/share/glib-2.0/schemas
