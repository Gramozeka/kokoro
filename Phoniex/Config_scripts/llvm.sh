
cd $TEMPBUILD
rm -rf *
ARCH=x86_64
unset LDFLAGS LD_LIBRARY_PATH PKG_CONFIG_PATH LLVM_INSTALL_DIR CLANG_INSTALL_DIR
export CFLAGS="-O2 -fPIC"
export CXXFLAGS="-O2 -fPIC"

PY2=$(python -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())' | cut -f 2 -d n | cut -f 1 -d /)
PY3=$(python3 -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())' | cut -f 2 -d n | cut -f 1 -d /)
pkg=${destdir}/llvm-10.0.1

PATH=$(echo $PATH | sed "s|/usr/libexec/icecc/bin||g" | tr -s : | sed "s/^://g" | sed "s/:$//g")


tar xvf ${SOURCE}/llvm-10.0.1.src.tar.xz || exit 1

cd llvm-10.0.1/tools || cd llvm-10.0.1.src/tools || exit 1
  tar xvf ${SOURCE}/clang-10.0.1.src.tar.xz || exit 1
  mv clang-10.0.1 clang 2>/dev/null || mv clang-10.0.1.src clang || exit 1
  tar xvf ${SOURCE}/lldb-10.0.1.src.tar.xz || exit 1
  mv lldb-10.0.1 lldb 2>/dev/null || mv lldb-10.0.1.src lldb || exit 1
  tar xvf ${SOURCE}/lld-10.0.1.src.tar.xz || exit 1
  mv lld-10.0.1 lld 2>/dev/null || mv lld-10.0.1.src lld || exit 1
cd ../

cd tools/clang/tools || exit 1
  tar xvf ${SOURCE}/clang-tools-extra-10.0.1.src.tar.xz || exit 1
  mv clang-tools-extra-10.0.1 extra 2>/dev/null \
    || mv clang-tools-extra-10.0.1.src extra || exit 1
cd ../../../

cd projects || exit 1
  tar xvf ${SOURCE}/compiler-rt-10.0.1.src.tar.xz || exit 1
  mv compiler-rt-10.0.1 compiler-rt 2>/dev/null || mv compiler-rt-10.0.1.src compiler-rt || exit 1
  tar xvf ${SOURCE}/openmp-10.0.1.src.tar.xz || exit 1
  mv openmp-10.0.1 openmp 2>/dev/null || mv openmp-10.0.1.src openmp || exit 1
  tar xvf ${SOURCE}/libcxx-10.0.1.src.tar.xz || exit 1
  mv libcxx-10.0.1 libcxx 2>/dev/null || mv libcxx-10.0.1.src libcxx || exit 1
  tar xvf ${SOURCE}/libcxxabi-10.0.1.src.tar.xz || exit 1
  mv libcxxabi-10.0.1 libcxxabi 2>/dev/null || mv libcxxabi-10.0.1.src libcxxabi || exit 1
  tar xvf ${SOURCE}/polly-10.0.1.src.tar.xz || exit 1
  mv polly-10.0.1 polly 2>/dev/null || mv polly-10.0.1.src polly || exit 1
cd ../

zcat ${PATCHSOURCE}/llvm.polly.hack.diff.gz | patch -p1 --verbose || exit 1

case $ARCH in
  i?86) CLANGD="-DCLANG_ENABLE_CLANGD=OFF" ;;
     *) unset CLANGD ;;
esac
chown -R root:root .

mkdir build
cd build
  cmake -GNinja \
    -DCMAKE_C_COMPILER="gcc" \
    -DCMAKE_CXX_COMPILER="g++" \
    -DCMAKE_C_FLAGS:STRING="$CFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLLVM_LIBDIR_SUFFIX=64 \
    -DCMAKE_BUILD_TYPE=Release \
    -DGCC_INSTALL_PREFIX=/usr \
    -DLLVM_USE_LINKER=gold \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_FFI=ON \
          -DLLVM_BUILD_LLVM_DYLIB=ON                \
      -DLLVM_LINK_LLVM_DYLIB=ON                 \
    -DLLVM_INSTALL_UTILS=ON \
    -DLLVM_BINUTILS_INCDIR=/usr/include \
    -DLLVM_TARGETS_TO_BUILD="host;AMDGPU;BPF" \
    -DLLDB_USE_SYSTEM_SIX=1 \
    -DLLVM_DEFAULT_TARGET_TRIPLE:STRING=${CLFS_TARGET} \
    -DLLVM_HOST_TRIPLE:STRING=${CLFS_TARGET} \
    $CLANGD \
    ..  || exit 1

  ninja -j5 || exit 1
  DESTDIR=${pkg} ninja install || exit 1
cd ..

for i in ccc c++; do
  ln -s /usr/libexec/$i-analyzer \
    ${pkg}/usr/bin/$i-analyzer || exit 1
done

if [ ! -r ${pkg}/usr/bin/lit-cpuid ]; then
  cp -a build/bin/lit-cpuid ${pkg}/usr/bin/lit-cpuid
  chown root:root ${pkg}/usr/bin/lit-cpuid
  chmod 755 ${pkg}/usr/bin/lit-cpuid
fi

rm -f ${pkg}/usr/lib64/libgomp.so

for pyver in ${PY2} ${PY3}; do
  mkdir -p "${pkg}/usr/lib64/python$pyver/site-packages"
  cp -a tools/clang/bindings/python/clang "${pkg}/usr/lib64/python$pyver/site-packages/"
done

rm -f "${pkg}/usr/lib64/python${PY2}/site-packages/six.py"

python -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/clang"
python -O -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/clang"
python3 -m compileall "${pkg}/usr/lib64/python${PY3}/site-packages/clang"
python3 -O -m compileall "${pkg}/usr/lib64/python${PY3}/site-packages/clang"
python -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/lldb"
python -O -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/lldb"
python -m compileall "${pkg}/usr/share/scan-view"
python -O -m compileall "${pkg}/usr/share/scan-view"
python -m compileall "${pkg}/usr/share/clang"
python -O -m compileall "${pkg}/usr/share/clang"
python -m compileall "${pkg}/usr/share/opt-viewer"
python -O -m compileall "${pkg}/usr/share/opt-viewer"

( cd ${pkg}
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

find ${pkg}/usr/share/man -type f -exec gzip -9 {} \+
for i in $( find ${pkg}/usr/share/man -type l ) ; do
  ln -s $( readlink $i ).gz $i.gz
  rm $i
done


cd ${pkg}
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/llvm-10.0.1-$ARCH-$BUILD-tree
/sbin/makepkg -l y -c n ${destdir}/llvm-10.0.1-$ARCH-$BUILD.txz
installpkg ${destdir}/llvm-10.0.1-$ARCH-$BUILD.txz
rm -rf ${pkg}
USE_ARCH="64" 
ARCH="x86_64"
LDFLAGS="-L/lib64:/usr/lib64" 
LD_LIBRARY_PATH="/lib64:/usr/lib64" 
export CFLAGS="${BUILD64}"
export CXXFLAGS="${BUILD64}"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
export LLVM_INSTALL_DIR=/usr
export CLANG_INSTALL_DIR=/usr
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
