
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} LIBDIR="/usr/lib64" install
mv ${pkg}/usr/include/{iconv.h,libiconv.h}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
  mv ${pkg}/usr/bin/{iconv,libiconv}
  mv ${pkg}/usr/share/man/man1/{,lib}iconv.1
  mv ${pkg}/usr/share/man/man3/{,libiconv_}iconv.3
  mv ${pkg}/usr/share/man/man3/{,libiconv_}iconvctl.3
  mv ${pkg}/usr/share/man/man3/{,libiconv_}iconv_open.3
  mv ${pkg}/usr/share/man/man3/{,libiconv_}iconv_close.3
  mv ${pkg}/usr/share/man/man3/{,libiconv_}iconv_open_into.3
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed '/LD_RUN_PATH/d' -i Makefile.in
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
cp -f /usr/include/stdio.h srclib/stdio.in.h
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
