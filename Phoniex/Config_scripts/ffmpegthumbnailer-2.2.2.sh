
###############################################################################
# FFmpegthumbnailer is a lightweight video thumbnailer that can be used by file managers to create thumbnails for your video files. The thumbnailer uses ffmpeg to decode frames from the video files, so supported videoformats depend on the configuration flags of ffmpeg.
# 
# This thumbnailer was designed to be as fast and lightweight as possible. The only dependencies are ffmpeg and libpng/libjpeg.
# https://github.com/dirkvdb/ffmpegthumbnailer/archive/2.2.2.tar.gz
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake      -DCMAKE_C_FLAGS="$BUILD64" \
     -DCMAKE_CXX_FLAGS="$BUILD64" \
-DCMAKE_INSTALL_PREFIX=/usr \
 -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
 -DLIB_INSTALL_DIR=lib64 \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="64" \
    -DBUILD_TESTING=OFF \
        -DENABLE_GIO="yes" \
    -DENABLE_THUMBNAILER="yes" \
            -Wno-dev ..
make -j4 || make || exit 1

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
