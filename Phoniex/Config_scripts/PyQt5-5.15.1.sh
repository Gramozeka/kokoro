
set -e
###################
TEMP=${TEMPBUILD}
cd $TEMP
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}/usr
cd ${packagedir}
export NINJAJOBS=5
pip3 install toml
  sip-build \
    --confirm-license \
    --no-make \
    --api-dir /usr/share/qt5/qsci/api/python
  cd build
CFLAGS="-O2 -fPIC" \
CXXFLAGS="-O2 -fPIC" \
make
make DESTDIR=${pkg} INSTALL_ROOT=${pkg} install -j1

echo "###########################**** COMPLITE!!! ****##################################"
rm -r ${pkg}/usr/lib64/python*/site-packages/PyQt5/uic/port_v2
  python3 -m compileall -d / ${pkg}/usr/lib64
  python3 -O -m compileall -d / ${pkg}/usr/lib64
cd ${pkg}
mkdir -p usr/tree-info
tree > usr/tree-info/${packagedir}-tree
echo "###################!!!!!!!!!!!! COMPLITE! !!!!!!!!!!########################"
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home

