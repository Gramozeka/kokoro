
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_i686*$//" <<< $(basename $line))
package=${packagedir}.tar.*
unpack ${SOURCE}/${package}
cd ${packagedir}
pkg=${destdir}/${packagedir}
USE_ARCH="32"
ARCH="i686"
SAVE_path=${PATH}
PATH="${PATH32}:${PATH}"
export AR="/i686/bin/ar"
export AS="/i686/bin/as"
export RANLIB="/i686/bin/ranlib"
export LD="/i686/bin/ld"
export STRIP="/i686/bin/strip"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH32}
export LD_LIBRARY_PATH="/i686/lib" 
export LDFLAGS="-L/i686/lib" 
export CPPFLAGS="-I/i686/include" 
CC="gcc ${BUILD32}" CXX="g++ ${BUILD32}" \
setarch i386 make -j1 \
  CFLAGS="${BUILD32}" \
  PREFIX=/i686 \
  LIBDIR=/i686/lib \
  MANDIR=/i686/share/man \
  DESTDIR=${pkg} \
  BUILD_STATIC=no \
  default install

echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
