
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
#find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
#  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
#unpack ${SOURCE}/${package}
git clone https://github.com/GNOME/gnome-panel.git ${packagedir}
cd ${packagedir}
./autogen.sh
# patch -Np1 -i ${PATCHSOURCE}/
USE_ARCH="64" 
ARCH="x86_64"
LDFLAGS="-L/lib64:/usr/lib64" \
LD_LIBRARY_PATH="/lib64:/usr/lib64"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--enable-gtk-doc-html=no \
--disable-Werror \
--disable-maintainer-mode \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas

