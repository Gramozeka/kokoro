
###############################################################################
set -e
###############################################################################
# The xfsprogs package contains administration and debugging tools for the XFS file system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/linux/utils/fs/xfs/xfsprogs/xfsprogs-5.4.0.tar.xz
# Download MD5 sum: 61232b1cc453780517d9b0c12ff1699b
# Download size: 1.2 MB
# Estimated disk space required: 64 MB
# Estimated build time: 0.3 SBU (Using parallelism=4)
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/xfs
# Kernel Configuration
# Enable the following options in the kernel configuration and recompile the kernel:
# File systems --->
#   <*/M> XFS filesystem support [CONFIG_XFS_FS]
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install DIST_ROOT=${pkg}
make install-dev DIST_ROOT=${pkg}
chmod 755 ${pkg}/lib64/*
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
  --libdir=/lib64 \
  --sbindir=/sbin \
  --bindir=/usr/sbin \
  --enable-shared=yes \
  --enable-gettext=yes \
--sysconfdir=/etc \
--libexecdir=/usr/lib64 \
--localstatedir=/var \
--enable-readline \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j5 DEBUG=-DNDEBUG     \
     INSTALL_USER=root  \
     INSTALL_GROUP=root 
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
