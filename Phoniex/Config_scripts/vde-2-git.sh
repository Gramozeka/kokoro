
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/virtualsquare/vde-2.git ${packagedir}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
autoreconf -vfi
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j1 V=1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
