
###############################################################################
set -e
###############################################################################
# Newt is a programming library for color text mode, widget based user interfaces. It can be used to add stacked windows, entry widgets, checkboxes, radio buttons, labels, plain text fields, scrollbars, etc., to text mode user interfaces. Newt is based on the S-Lang library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://releases.pagure.org/newt/newt-0.52.21.tar.gz
# Download MD5 sum: a0a5fd6b53bb167a65e15996b249ebb5
# Download size: 172 KB
# Estimated disk space required: 6.4 MB
# Estimated build time: less than 0.1 SBU
# Newt Dependencies
# Required
# popt-1.16 and slang-2.3.2
# Recommended
# Tcl-8.6.10 and GPM-1.20.7 (runtime)
# Optional
# Python-2.7.17
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e 's/^LIBNEWT =/#&/' \
    -e '/install -m 644 $(LIBNEWT)/ s/^/#/' \
    -e 's/$(LIBNEWT)/$(LIBNEWTSONAME)/g' \
    -i Makefile.in                           &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-gpm-support \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
