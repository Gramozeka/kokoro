#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -p ${pkg}/sbin ${pkg}/bin ${pkg}/usr/sbin
cat arp > ${pkg}/sbin/arp
cat ifconfig > ${pkg}/sbin/ifconfig
cat rarp > ${pkg}/sbin/rarp
cat route > ${pkg}/sbin/route
cat mii-tool > ${pkg}/sbin/mii-tool
cat nameif > ${pkg}/sbin/nameif
cat netstat > ${pkg}/bin/netstat
cat plipconfig > ${pkg}/sbin/plipconfig
cat slattach > ${pkg}/usr/sbin/slattach
cat ipmaddr > ${pkg}/sbin/ipmaddr
cat iptunnel > ${pkg}/sbin/iptunnel
chmod 755 ${pkg}/sbin/* ${pkg}/bin/* ${pkg}/usr/sbin/*
mkdir -pv ${pkg}/bin
  cc -O2 -o ${pkg}/bin/ipmask ipmask.c
  strip --strip-unneeded ${pkg}/bin/ipmask
  chmod 755 ${pkg}/bin/ipmask

cat ipmask.8 | gzip -9c > ${pkg}/usr/share/man/man8/ipmask.8.gz
cd man/en_US
mkdir -p ${pkg}/usr/share/man/man{1,5,8}
cat ethers.5 | gzip -9c > ${pkg}/usr/share/man/man5/ethers.5.gz
for page in arp.8 ifconfig.8 mii-tool.8 nameif.8 netstat.8 rarp.8 route.8 \
  slattach.8 plipconfig.8 ; do
  cat $page | gzip -9c > ${pkg}/usr/share/man/man8/$page.gz
done

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
HAVE_FW_MASQUERADE=1 HAVE_ARP_TOOLS=1 HAVE_HOSTNAME_TOOLS=1 \
  HAVE_HOSTNAME_SYMLINKS=1 HAVE_IP_TOOLS=1 HAVE_MII=1 \
  HAVE_PLIP_TOOLS=1 HAVE_SERIAL_TOOLS=1 make || exit 1
strip --strip-unneeded ipmaddr iptunnel hostname arp ifconfig nameif rarp route netstat plipconfig slattach mii-tool
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
