
###############################################################################
set -e
###############################################################################
# Gparted is the Gnome Partition Editor, a Gtk 2 GUI for other command line tools that can create, reorganise or delete disk partitions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/gparted/gparted-1.1.0.tar.gz
# Download MD5 sum: 0da45cb522d766dfb4886fb3bdbc2634
# Download size: 3.8 MB
# Estimated disk space required: 90 MB (add 82 MB for tests)
# Estimated build time: 0.4 SBU (using parallelism=4; add 0.5 SBU for tests)
# Gparted Dependencies
# Required
# Gtkmm-3.24.2 and parted-3.3
# Optional
# btrfs-progs-5.4.1 (if using a btrfs filesystem) and udftools
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
