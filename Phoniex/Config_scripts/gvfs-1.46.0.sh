#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Gvfs package is a userspace virtual filesystem designed to work with the I/O abstractions of GLib's GIO library.'
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gvfs/1.42/gvfs-1.42.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gvfs/1.42/gvfs-1.42.2.tar.xz
# Download MD5 sum: 8dea2f6a3f327a814a347758d4350e4b
# Download size: 1.2 MB
# Estimated disk space required: 57 MB
# Estimated build time: 0.4 SBU
# Gvfs Dependencies
# Required
# dbus-1.12.16, GLib-2.62.4, libusb-1.0.23, libsecret-0.20.0 and libsoup-2.68.3
# Recommended
# Gcr-3.34.0, GTK+-3.24.13, libcdio-2.1.0, libgdata-0.17.11, libgudev-233, elogind-241.4, and UDisks-2.8.4
# Optional
# Apache-2.4.41, Avahi-0.7, BlueZ-5.52, dbus-glib-0.110, Fuse-3.9.0, gnome-online-accounts-3.34.1, GTK-Doc-1.32, libarchive-3.4.1, libgcrypt-1.8.5, libxml2-2.9.10, libxslt-1.1.34, OpenSSH-8.1p1, Samba-4.11.0, libbluray, libgphoto2, libimobiledevice, libmtp, libnfs, and Twisted
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
        -Dgphoto2=true   \
      -Dafc=false       \
      -Dbluray=false    \
      -Dnfs=true       \
      -Dmtp=true       \
      -Dsmb=true       \
      -Dtmpfilesdir=true  \
      -Ddnssd=true     \
      -Dgoa=false       \
      -Dgoogle=false    \
      -Dsystemduserunitdir=no \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
