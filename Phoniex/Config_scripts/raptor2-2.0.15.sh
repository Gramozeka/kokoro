
###############################################################################
set -e
###############################################################################
###############################################################################
# Raptor is a C library that provides a set of parsers and serializers that generate Resource Description Framework (RDF) triples.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://download.librdf.org/source/raptor2-2.0.15.tar.gz
# Download MD5 sum: a39f6c07ddb20d7dd2ff1f95fa21e2cd
# Download size: 1.9 MB
# Estimated disk space required: 28 MB (additional 1 MB for the tests)
# Estimated build time: 0.2 SBU (additional 0.5 SBU for the tests)
# Raptor Dependencies
# Required
# cURL-7.68.0 and libxslt-1.1.34
# Optional
# GTK-Doc-1.32, ICU-65.1 and libyajl
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-icu-config=/usr/bin/icu-config \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
