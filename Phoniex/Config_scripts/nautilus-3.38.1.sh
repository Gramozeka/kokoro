
###############################################################################
set -e
###############################################################################
# The Nautilus package contains the GNOME file manager.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/nautilus/3.34/nautilus-3.34.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/nautilus/3.34/nautilus-3.34.2.tar.xz
# Download MD5 sum: 88573061544040c0ff960f934aa83719
# Download size: 3.1 MB
# Estimated disk space required: 154 MB (with tests)
# Estimated build time: 1.0 SBU (with tests)
# Nautilus Dependencies
# Required
# bubblewrap-0.4.0, gexiv2-0.12.0, gnome-autoar-0.2.4, gnome-desktop-3.34.3, libnotify-0.7.8, libseccomp-2.4.2, and tracker-miners-2.3.1
# Recommended
# desktop-file-utils-0.24, Exempi-2.5.1, gobject-introspection-1.62.0, gst-plugins-base-1.16.2, and libexif-0.6.21
# Optional
# GTK-Doc-1.32
# Recommended (Runtime)
# adwaita-icon-theme-3.34.3 and Gvfs-1.42.2 (For hotplugging and device mounting to work)
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
      -Dselinux=false    \
      -Dpackagekit=false \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas
