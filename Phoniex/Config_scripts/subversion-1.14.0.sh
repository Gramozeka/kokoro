
###############################################################################
set -e
###############################################################################
# Subversion is a version control system that is designed to be a compelling replacement for CVS in the open source community. It extends and enhances CVS' feature set, while maintaining a similar interface for those already familiar with CVS. These instructions install the client and server software used to manipulate a Subversion repository. Creation of a repository is covered at Running a Subversion Server.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.apache.org/dist/subversion/subversion-1.13.0.tar.bz2
# Download MD5 sum: 3004b4dae18bf45a0b6ea4ef8820064d
# Download size: 8.1 MB
# Estimated disk space required: 176 MB (add 93 MB for bindings, 26 MB for docs, 1.1 GB for tests)
# Estimated build time: 0.5 SBU (Using parallelism=4; add 1.4 SBU for bindings, 29 SBU for tests)
# Subversion Dependencies
# Required
# Apr-Util-1.6.1 and SQLite-3.31.1
# Recommended
# Serf-1.3.9 (for handling http:// and https:// URLs)
# Optional
# Apache-2.4.41, Cyrus SASL-2.1.27, dbus-1.12.16, gnome-keyring-3.34.0, libsecret-0.20.0, Python-2.7.17 (with sqlite support for the tests), Ruby-2.7.0, SWIG-4.0.1 (for building Perl, Python and Ruby bindings), LZ4, and UTF8proc
# Optional (for the Java Bindings)
# One of OpenJDK-12.0.2, Dante or Jikes, and JUnit 4 (to test the Java bindings). Note that JUnit is included with apache-ant-1.10.7.'
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -m755 -d ${pkg}/usr/share/doc/${packagedir} &&
cp      -v -R doc/* ${pkg}/usr/share/doc/${packagedir}
make DESTDIR=${pkg} install-javahl
make DESTDIR=${pkg} install-swig-pl
make DESTDIR=${pkg} install-swig-py \
      swig_pydir=/usr/lib64/python3.9/site-packages/libsvn \
      swig_pydir_extra=/usr/lib64/python3.9/site-packages/svn
make DESTDIR=${pkg} install-swig-rb
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
PYTHON=python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
            --with-apache-libexecdir  \
            --with-lz4=internal       \
            --with-utf8proc=internal \
            --enable-javahl \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make javahl
make swig-pl
make swig-py \
     swig_pydir=/usr/lib64/python3.9/site-packages/libsvn \
     swig_pydir_extra=/usr/lib64/python3.9/site-packages/svn
make swig-rb
doxygen doc/doxygen.conf
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
