
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
 unpack ${SOURCE}/${package}
#git clone https://github.com/GNOME/gnome-shell-extensions.git ${packagedir}
cd ${packagedir}
mkdir meson-build
cd meson-build
CXXFLAGS=$CXXFLAGS \
CFLAGS="$CFLAGS" \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=plain \
-Dextension_set=all \
-Denable_extensions=user-theme \
  -Dsystemd=false \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas
