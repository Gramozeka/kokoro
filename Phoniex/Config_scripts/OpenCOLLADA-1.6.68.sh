
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}

chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;
sed -i 's/struct real_pcre;//' COLLADABaseUtils/include/COLLADABUPcreCompiledPattern.h
sed -i 's/typedef struct real_pcre pcre;/#include <pcre.h>/' COLLADABaseUtils/include/COLLADABUPcreCompiledPattern.h

sed -i 's|set(OPENCOLLADA_INST_LIBRARY ${CMAKE_INSTALL_PREFIX}/lib/opencollada)|set(OPENCOLLADA_INST_LIBRARY ${CMAKE_INSTALL_PREFIX}/lib64/opencollada)|' CMakeLists.txt
sed -i 's|set(OPENCOLLADA_INST_CMAKECONFIG ${OPENCOLLADA_INST_LIBRARY}/cmake)|set(OPENCOLLADA_INST_CMAKECONFIG ${CMAKE_INSTALL_PREFIX}/lib64/cmake/opencollada)|' CMakeLists.txt
# patch -Np1 -i ${PATCHSOURCE}/COLLADABUPcreCompiledPattern.diff
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/usr \
             -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
    -DLIB_INSTALL_DIR:PATH=lib64 \
    -DUSE_EXPAT:BOOL=OFF \
    -DUSE_LIBXML:BOOL=ON \
    -DUSE_SHARED:BOOL=ON \
    -DUSE_STATIC:BOOL=OFF \
             -Wno-dev ..
make -j4 || make || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
