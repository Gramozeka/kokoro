
###############################################################################
set -e
# libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression. libjpeg is a library that implements JPEG image encoding, decoding and transcoding.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/libjpeg-turbo/libjpeg-turbo-2.0.4.tar.gz
# Download MD5 sum: d01d9e0c28c27bc0de9f4e2e8ff49855
# Download size: 2.1 MB
# Estimated disk space required: 31 MB (with tests)
# Estimated build time: 0.3 SBU (using parallelism=4; with tests)
# libjpeg-turbo Dependencies
# Required
# CMake-3.16.3 and NASM-2.14.02 or yasm-1.3.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build &&
cd    build &&
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=RELEASE  \
      -DENABLE_STATIC=FALSE       \
      -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${packagedir} \
      -DCMAKE_INSTALL_DEFAULT_LIBDIR=lib64  \
      -DWITH_JPEG8=ON \
      .. && 
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
