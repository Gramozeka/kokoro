
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
./autogen.sh
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET} &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
