
###############################################################################
set -e
###############################################################################
# Brotli provides a general-purpose lossless compression algorithm that compresses data using a combination of a modern variant of the LZ77 algorithm, Huffman coding and 2nd order context modeling. Its libraries are particularly used for WOFF2 fonts on webpages.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/google/brotli/archive/v1.0.7/brotli-v1.0.7.tar.gz
# Download MD5 sum: 7b6edd4f2128f22794d0ca28c53898a5
# Download size: 23 MB
# Estimated disk space required: 43 MB (add 5 MB if installing both sets of python bindings, add 9 MB for the main test and 5MB for testing the bindings)
# Estimated build time: 0.2 SBU (add 0.3 SBU for the python bindings, and 1.2 SBU if testing them)
# Brotli Dependencies
# Required
# CMake-3.16.3
# Optional
# Lua-5.3.5 (to create Lua bindings) and Python-2.7.17 (to create python2 bindings)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
pushd ..               &&
python3 setup.py build &&
popd
make DESTDIR=${pkg} install
cd ..
python3 setup.py install  --optimize=1  --root=${pkg} &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's@-R..libdir.@@' scripts/*.pc.in
mkdir out &&
cd    out &&
# patch -Np1 -i ${PATCHSOURCE}/
LDFLAGS="${LDFLAGS} -lpthread -lcrypto" \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
      ..  &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
