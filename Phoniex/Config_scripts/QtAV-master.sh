
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

mkdir -pv ${pkg}/usr/share/{applications,pixmaps}
cp ../qtc_packaging/debian_generic/*.desktop ${pkg}/usr/share/applications
cp ../src/QtAV.svg ${pkg}/usr/share/pixmaps
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
mkdir -p build
cd build
  cmake \
  -DCMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT=/usr \
    -DCMAKE_C_FLAGS:STRING="${BUILD64}" \
    -DCMAKE_CXX_FLAGS:STRING="${BUILD64}" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DSYSCONF_INSTALL_DIR=/etc \
    -DQTAV_INSTALL_LIBS=/usr/lib64 \
    -DLIB_SUFFIX=64 \
    ..
    make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
