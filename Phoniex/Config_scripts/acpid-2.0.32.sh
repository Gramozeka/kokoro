
###############################################################################
set -e
###############################################################################
# The acpid (Advanced Configuration and Power Interface event daemon) is a completely flexible, totally extensible daemon for delivering ACPI events. It listens on netlink interface and when an event occurs, executes programs to handle the event. The programs it executes are configured through a set of configuration files, which can be dropped into place by packages or by the user.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/acpid2/acpid-2.0.32.tar.xz
# Download MD5 sum: 248995264b9d1cd8bdb923d5b190fd44
# Download size: 156 KB
# Estimated disk space required: 2.5 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -m755 -d ${pkg}/etc/acpi/events &&
cp -r samples ${pkg}/usr/share/doc/acpid-2.0.32

cat > ${pkg}/etc/acpi/events/lid << "EOF"
event=button/lid
action=/etc/acpi/lid.sh
EOF

cat > ${pkg}/etc/acpi/lid.sh << "EOF"
#!/bin/sh
/bin/grep -q open /proc/acpi/button/lid/LID/state && exit 0
/usr/sbin/pm-suspend
EOF
chmod +x ${pkg}/etc/acpi/lid.sh

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cd boot-script
make install-acpid
