
###############################################################################
set -e
###############################################################################
# SassC is a wrapper around libsass, a CSS pre-processor language.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/sass/sassc/archive/3.6.1/sassc-3.6.1.tar.gz
# Download MD5 sum: dd675920c7151e50e2d2ac14f0fb222a
# Download size: 28 KB
# Estimated disk space required: 4.4 MB
# Estimated build time: 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -fiv
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
