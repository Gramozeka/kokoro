
###############################################################################
set -e
# The libyaml package contains a C library for parsing and emitting YAML (YAML Ain't Markup Language) code.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/yaml/libyaml/archive/0.2.2/libyaml-dist-0.2.2.tar.gz
# Download MD5 sum: 2ad4119a57f94739cc39a1b482c81264
# Download size: 80 KB
# Estimated disk space required: 6.4 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# libyaml Dependencies
# Optional
# Doxygen-1.8.17'
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./bootstrap
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
