
###############################################################################
set -e
# Wayland is a project to define a protocol for a compositor to talk to its clients as well as a library implementation of the protocol.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://wayland.freedesktop.org/releases/wayland-1.17.0.tar.xz
# Download MD5 sum: d91f970aea11fd549eae023d06f91af3
# Download size: 428 KB
# Estimated disk space required: 13 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# Wayland Dependencies
# Required
# libxml2-2.9.10
# Optional
# Doxygen-1.8.17, Graphviz-2.42.3 and xmlto-0.0.28 (to build the API documentation) and docbook-xml-4.5, docbook-xsl-1.79.2 and libxslt-1.1.34 (to build the manual pages)
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET} --disable-documentation
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
