
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
 unpack ${SOURCE}/${package}
#git clone https://gitlab.com/accounts-sso/signond.git ${packagedir}
cd ${packagedir}
CWD=$(pwd)
 mkdir BUILDQT
cd BUILDQT
CWD=$(pwd)
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
OPENSSL_LIBS="-L/usr/lib64 -lssl -lcrypto" \
qmake \
CMAKE_CONFIG_PATH=/usr/lib64/cmake/SignOnQt5 \
LIBDIR=/usr/lib64 \
PREFIX=/usr \
../signon.pro   &&
make -j4
mkdir -pv $pkg
make install INSTALL_ROOT=${pkg}

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
