
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# git clone https://github.com/videolan/vlc.git ${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/vlc_demux.h/a #define LUA_COMPAT_APIINTCASTS' modules/lua/vlc.h   &&
sed -i '/LIBSSH2_VERSION_NUM/s/10801/10900/' modules/access/sftp.c &&
# patch -Np1 -i ${PATCHSOURCE}/video-output.diff
export CC=${CC:-"gcc"}
export CPP=${CPP:-"cpp"}
export CXX=${CXX:-"g++"}
export AR=${AR:-"gcc-ar"}
export NM=${NM:-"gcc-nm"}
export RANLIB=${RANLIB:-"gcc-ranlib"}
ARCH="x86_64"
export LDFLAGS="-L/usr/lib64"
export CFLAGS="-O2 -fPIC -m64"
export CXXFLAGS="-O2 -fPIC -m64"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
./bootstrap
PKG_CONFIG_PATH="${PKG_CONFIG_PATH64}" \
BUILDCC="gcc" \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
            --localstatedir=/var \
--disable-static \
  --localstatedir=/var \
  --disable-rpath \
  --disable-debug \
  --disable-freerdp \
  --disable-zvbi \
--enable-run-as-root \
  --enable-shared \
  --enable-ncurses \
    --disable-opencv \
    --disable-vpx \
     --disable-caca \
--build=${CLFS_TARGET} &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
