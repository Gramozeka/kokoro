
###############################################################################
set -e
# LibESMTP is a library to manage posting (or submission of) electronic
# mail using SMTP to a preconfigured Mail Transport Agent (MTA) such as
# Exim or Postfix.  It may be used as part of a Mail User Agent (MUA) or
# another program that must be able to post electronic mail but where mail
# functionality is not the program's primary purpose.
# LibESMTP is not intended to be used as part of a program that implements
# a Mail Transport Agent.
# It is hoped that the availability of a lightweight library implementing
# an SMTP client will both ease the task of coding for software authors
# and improve the quality of the resulting code.'
# https://github.com/jbouse-debian/libesmtp
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
