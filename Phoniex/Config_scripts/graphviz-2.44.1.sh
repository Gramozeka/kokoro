
###############################################################################
set -e
# The Graphviz package contains graph visualization software. Graph visualization is a way of representing structural information as diagrams of abstract graphs and networks. Graphviz has several main graph layout programs. It also has web and interactive graphical interfaces, auxiliary tools, libraries, and language bindings.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www2.graphviz.org/Packages/stable/portable_source/graphviz-2.42.3.tar.gz
# Download MD5 sum: 9f61dc85517957793c6bb24f0611eac1
# Download size: 25 MB
# Estimated disk space required: 214 MB
# Estimated build time: 1.0 SBU (using parallelism=4)
# Graphviz Dependencies
# [Note] Note
# Graphviz basic usage does not need any libraries out of what is found in the LFS book. Its “core” rendering engine allows to generate several graphic formats, such as Postscript, SVG, VML, .fig, and Tk. Those formats can be converted to almost any other, using for example tools from ImageMagick-7.0.8-60. The dependencies below add the ability to generate graph images in bitmap format, to display the graph image on screen, to edit a graph by seeing directly the result image, or to view large graphs. Since Graphviz is a dependency of several other packages in this book, it is suggested to first build it without any dependencies, then to rebuild it when you have built enough packages to suit your needs.
# Optional, for various bitmap outputs
# Pango-1.44.7, with Cairo-1.17.2+f93fc72c03e, Xorg Libraries, Fontconfig-2.13.1, and libpng-1.6.37, to generate images in bitmap SVG, postscript, PNG, and PDF formats, or displaying the image on screen
# Adding GTK+-2.24.32 with libjpeg-turbo-2.0.4 allows to add JPEG, BMP, TIF, and ICO formats, and to display the image in a GTK+ window
# GD Library may be used instead of Pango. It adds the ability to generate images in GIF, VRML, and GD formats, but Pango provides better outputs for the other formats, and is needed for displaying images
# Other formats may be added with libwebp-1.1.0 (WebP support is considered experimental), DevIL, libLASi, glitz, and libming (Adobe Flash)
# Optional, to load graphic images that may be displayed inside the nodes of a graph
# libgs.so from ghostscript-9.50, librsvg-2.46.4, and Poppler-0.84.0
# Optional, to build more tools
# Freeglut-3.2.1 (with libglade-2.6.4, GtkGLExt, and libGTS, for building the smyrna large graph viewer, which is considered experimental), Qt-5.14.0, for building the gvedit graph editor. Another graph editor, dotty needs only Xorg Libraries
# Optional (To Build Language Bindings)
# SWIG-4.0.1 (SWIG must be installed or no bindings will be built), GCC-9.2.0 (for the go language), Guile-3.0.0, OpenJDK-12.0.2, Lua-5.3.5, PHP-7.4.2, Python-2.7.17, Ruby-2.7.0, Tcl-8.6.10, Tk-8.6.10, Io, Mono, OCaml, and R
# Optional (building tools)
# Criterion (framework for tests), Electric Fence,
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vfi
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
 --exec-prefix=/usr \
 --libexecdir=/usr/libexec \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static PS2PDF=true \
--enable-php=no \
--build=${CLFS_TARGET}  \
 --datadir=/usr/share \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
