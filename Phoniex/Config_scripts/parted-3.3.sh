
###############################################################################
set -e
###############################################################################
# The Parted package is a disk partitioning and partition resizing tool.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/parted/parted-3.3.tar.xz
# Download (FTP): ftp://ftp.gnu.org/gnu/parted/parted-3.3.tar.xz
# Download MD5 sum: 090655d05f3c471aa8e15a27536889ec
# Download size: 1.7 MB
# Estimated disk space required: 34 MB (additional 3 MB for the tests and additional 1 MB for optional PDF and Postscript documentation)
# Estimated build time: 0.3 SBU (additional 1.7 SBU for the tests)
# Parted Dependencies
# Recommended
# LVM2-2.03.07 (device-mapper, required if building udisks)
# Optional
# dosfstools-4.1, Pth-2.0.7, texlive-20190410 (or install-tl-unx), and Digest::CRC (for tests)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -m755 -d ${pkg}/usr/share/doc/parted-3.3/html &&
install -v -m644    doc/html/* \
                    ${pkg}/usr/share/doc/parted-3.3/html &&
install -v -m644    doc/{FAT,API,parted.{txt,html}} \
                    ${pkg}/usr/share/doc/parted-3.3
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make -C doc html                                       &&
makeinfo --html      -o doc/html       doc/parted.texi &&
makeinfo --plaintext -o doc/parted.txt doc/parted.texi
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
