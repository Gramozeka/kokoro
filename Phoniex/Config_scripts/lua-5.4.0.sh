#!/bin/bash
###############################################################################
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
ARCH="x86_64" 
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}
mkdir -pv ${pkg}/usr/lib64/pkgconfig
cat > lua.pc << "EOF"
V=5.4
R=5.4.0

prefix=/usr
INSTALL_BIN=${prefix}/bin
INSTALL_INC=${prefix}/include
INSTALL_LIB=${prefix}/lib64
INSTALL_MAN=${prefix}/share/man/man1
INSTALL_LMOD=${prefix}/share/lua/${V}
INSTALL_CMOD=${prefix}/lib64/lua/${V}
exec_prefix=${prefix}
libdir=${exec_prefix}/lib64
includedir=${prefix}/include

Name: Lua
Description: An Extensible Extension Language
Version: ${R}
Requires:
Libs: -L${libdir} -llua -lm -ldl
Cflags: -I${includedir}
EOF
patch -Np1 -i ${PATCHSOURCE}/lua-5.4.0-shared_library-1.patch
sed -i "s|lib/lua|lib64/lua|" src/luaconf.h
sed -i '/#define LUA_ROOT/s:/usr/local/:/usr/:' src/luaconf.h &&
make MYCFLAGS="-DLUA_COMPAT_5_2 -DLUA_COMPAT_5_1" linux \
  CFLAGS="-O2 -fPIC -m64 \$(MYCFLAGS)" \
  INSTALL_TOP=/usr \
  INSTALL_LIB=/usr/lib64 \
  INSTALL_LMOD=/usr/share/lua/5.4 \
  INSTALL_CMOD=/usr/lib64/lua/5.4

make install \
  INSTALL_TOP=${pkg}/usr \
  INSTALL_LIB=${pkg}/usr/lib64 \
  INSTALL_LMOD=${pkg}/usr/share/lua/5.4 \
  INSTALL_CMOD=${pkg}/usr/lib64/lua/5.4  \
     INSTALL_DATA="cp -d"            \
     INSTALL_MAN=${pkg}/usr/share/man/man1 \
     TO_LIB="liblua.so liblua.so.5.4 liblua.so.5.4.0" \
     install
install -v -m644 -D lua.pc ${pkg}/usr/lib64/pkgconfig/lua.pc
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
