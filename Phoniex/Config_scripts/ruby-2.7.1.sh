#!/bin/bash
###############################################################################
set -e
# The Ruby package contains the Ruby development environment. This is useful for object-oriented scripting.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://cache.ruby-lang.org/pub/ruby/2.7/ruby-2.7.0.tar.xz
# Download MD5 sum: 22262b119bf22ebe6df363c5f1b68944
# Download size: 11 MB
# Estimated disk space required: 451 MB (add 214 MB for C API docs)
# Estimated build time: 1.7 SBU (using parallelism=4; add 4.8 SBU for tests; add 0.5 SBU for C API docs)
# Ruby Dependencies
# Optional
# Berkeley DB-5.3.28, Doxygen-1.8.17, Graphviz-2.42.3, libyaml-0.2.2, Tk-8.6.10, Valgrind-3.15.0, and DTrace
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/ruby-2.7.1-glibc_fix-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--enable-shared \
--build=${CLFS_TARGET}  \
--docdir=/usr/share/doc/${packagedir}
make -j4
make capi
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
