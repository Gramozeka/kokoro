
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
install -vdm755 ${pkg}/opt/jdk-14.0.1+7             &&
cp -Rv build/*/images/jdk/* ${pkg}/opt/jdk-14.0.1+7 &&
chown -R root:root ${pkg}/opt/jdk-14.0.1+7          &&
for s in 16 24 32 48; do
mkdir -pv ${pkg}/usr/share/icons/hicolor/${s}x${s}/apps
  install -vDm644 src/java.desktop/unix/classes/sun/awt/X11/java-icon${s}.png \
                  ${pkg}/usr/share/icons/hicolor/${s}x${s}/apps/java.png
done
mkdir -pv ${pkg}/usr/share/applications
cat > ${pkg}/usr/share/applications/openjdk-java.desktop << "EOF" &&
[Desktop Entry]
Name=OpenJDK Java 14.0.1 Runtime
Comment=OpenJDK Java 14.0.1 Runtime
Exec=/opt/jdk/bin/java -jar
Terminal=false
Type=Application
Icon=java
MimeType=application/x-java-archive;application/java-archive;application/x-jar;
NoDisplay=true
EOF
cat > ${pkg}/usr/share/applications/openjdk-jconsole.desktop << "EOF"
[Desktop Entry]
Name=OpenJDK Java 14.0.1 Console
Comment=OpenJDK Java 14.0.1 Console
Keywords=java;console;monitoring
Exec=/opt/jdk/bin/jconsole
Terminal=false
Type=Application
Icon=java
Categories=Application;System;
EOF

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/opt/jdk-14.0.1+7/man ]; then
  find ${pkg}/opt/jdk-14.0.1+7/man -type f -exec gzip -9 {} \;
fi
mv -vf ${pkg}/opt/jdk-14.0.1+7/man ${pkg}/usr/share/

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
tar -xf ${SOURCE}/jtreg-4.2-b13-517.tar.gz
patch -Np1 -i ${PATCHSOURCE}/openjdk-14.0.1-make_4.3_fix-1.patch
sed -i /sysctl/d \
    src/jdk.incubator.jpackage/unix/native/libapplauncher/PosixPlatform.cpp
export LDFLAGS="-L/usr/lib64 -lboost_system -liconv"
CFLAGS=$CFLAGS \
unset JAVA_HOME                                       &&
bash configure --enable-unlimited-crypto              \
               --with-extra-cflags="-O2 -m64 -fPIC -fcommon" \
               --with-extra-ldflags="-L/usr/lib64 -lboost_system -liconv" \
               --disable-warnings-as-errors           \
               --with-stdc++lib=dynamic               \
               --with-giflib=system                   \
               --with-jtreg=$TEMPBUILD/${packagedir}/jtreg                \
               --with-lcms=system                     \
               --with-libjpeg=system                  \
               --with-libpng=system                   \
               --with-zlib=system                     \
               --with-version-build="7"               \
               --with-version-pre=""                  \
               --with-version-opt=""                  \
               --with-jobs=4                          \
               --with-cacerts-file=/etc/pki/tls/java/cacerts &&
make images
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
ln -v -nsf jdk-14.0.1+7 /opt/jdk

