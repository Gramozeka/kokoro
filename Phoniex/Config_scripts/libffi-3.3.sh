
###############################################################################
pack_local () {
pkg=${destdir}/$1

if [ $USE_ARCH == 32 ]
then
mkdir -pv ${pkg}/i686/{bin,lib}
mkdir -pv ${pkg}/usr/lib
setarch i386 make DESTDIR=${pkg} install
ln -sv ../../i686/lib/libffi.so.7.1.0  ${pkg}/usr/lib/libffi.so

if [ -d ${pkg}/usr/share/man ]; then
  rm -rf  ${pkg}/usr/share/man
fi
else
rm -rf ${pkg}/i686
make DESTDIR=${pkg} install
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
fi


echo "$1 ----- $(date)" >> ${destdir}/loginstall

find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

cd ${pkg} &&
tar -cvpzf ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.tar.gz --one-file-system .
tar -xvpzf ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.tar.gz -C / --numeric-owner
rm -rf ${pkg}
}
###############################################################################
set -e
###############################################################################
# cd $TEMPBUILD
# rm -rf *
# package="$(basename $line).tar.*"
# packagedir=$(basename $line)
# USE_ARCH="32"
# unpack ${SOURCE}/${package}
# cd ${packagedir}
# CC="gcc ${BUILD32}" setarch i386 ./configure --prefix=/i686 --libdir=/i686/lib --disable-static --with-gcc-arch=i386
# 
# setarch i386 make
# pack_local ${packagedir}
# echo "###########################**** COMPLITE!!! ****##################################"
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
USE_ARCH="64"
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
    --libdir=/usr/lib64 \
    --disable-static
#     --with-gcc-arch=x86-64

make
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
