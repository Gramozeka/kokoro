
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# make DESTDIR=${pkg} install

  install -D out/gn "$pkg/usr/bin/gn"
  install -Dm644 -t "$pkg/usr/share/doc/${packagedir}" docs/*

  mkdir -p "$pkg/usr/share/vim/vimfiles"
  cp -r misc/vim/{autoload,ftdetect,ftplugin,syntax} \
    "$pkg/usr/share/vim/vimfiles/"

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
# package="$(basename $line).tar.*"
packagedir=$(basename $line)
git clone https://gn.googlesource.com/gn ${packagedir}
# unpack ${SOURCE}/${package}
cd ${packagedir}

python3 build/gen.py
ninja -C out
out/gn_unittests
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
