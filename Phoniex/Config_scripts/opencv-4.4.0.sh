
###############################################################################
pip install numpy
pip3 install numpy
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}

tar xf ${SOURCE}/opencv_contrib-4.4.0.tar.gz
mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_INSTALL_PREFIX=/usr      \
      -DWITH_XINE=ON                   \
          -DCMAKE_C_FLAGS=" ${BUILD64}" \
    -DCMAKE_CXX_FLAGS=" ${BUILD64}" \
    -DCMAKE_BUILD_TYPE="Release" \
        -DBUILD_SHARED_LIBS=ON \
    -DOPENCV_GENERATE_PKGCONFIG=ON \
    -DPYTHON2_PACKAGES_PATH=$(python2 -c "import site; print(site.getsitepackages()[0])") \
    -DPYTHON3_PACKAGES_PATH=$(python3 -c "import site; print(site.getsitepackages()[0])") \
      -DENABLE_CXX11=ON                \
      -DBUILD_PERF_TESTS=OFF           \
      -DWITH_XINE=ON                   \
      -DBUILD_TESTS=OFF                \
      -DENABLE_PRECOMPILED_HEADERS=OFF \
      -DCMAKE_SKIP_RPATH=ON            \
      -DBUILD_WITH_DEBUG_INFO=OFF      \
      -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.4.0/modules \
      -Wno-dev  ..                    &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
#

