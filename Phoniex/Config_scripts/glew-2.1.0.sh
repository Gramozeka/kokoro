
###############################################################################
set -e
# GLEW is the OpenGL Extension Wrangler Library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/glew/glew-2.1.0.tgz
# Download MD5 sum: b2ab12331033ddfaa50dc39345343980
# Download size: 747 KB
# Estimated disk space required: 16 MB
# Estimated build time: less than 0.1 SBU
# glew Dependencies
# Required
# Mesa-19.3.2
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

make install.all GLEW_DEST=${pkg}/usr || exit 1
chmod 755 ${pkg}/usr/lib64/libGLEW*.so.*
rm -f ${pkg}/usr/lib64/libGLEW*.a
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "s|@libdir@|/usr/lib64|g" glew.pc.in

# Use lib64 (if needed) in Makefile:
sed -i "s,/lib,/lib64,g" Makefile
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
make -j5 OPT="${BUILD64}" || make OPT="${BUILD64}" || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
