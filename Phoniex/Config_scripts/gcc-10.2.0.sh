
###############################################################################

###############################################################################
set -e
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
mkdir -pv ${pkg}/usr/share/gdb/auto-load/usr/lib32
cp -v ${pkg}/usr/lib32/*gdb.py ${pkg}/usr/share/gdb/auto-load/usr/lib32
mkdir -pv ${pkg}/usr/share/gdb/auto-load/usr/lib64
cp -v ${pkg}/usr/lib64/*gdb.py ${pkg}/usr/share/gdb/auto-load/usr/lib64
ln -v -sf ../lib64/gcc ${pkg}/usr/lib32/gcc
mkdir -pv ${pkg}/lib{,32,64}
mkdir -pv ${pkg}/usr/lib
mkdir -pv ${pkg}/i686/lib
ln -v -sf ../../usr/bin/cpp ${pkg}/i686/lib/cpp
ln -v -sf gcc ${pkg}/usr/bin/cc
install -v -dm755 ${pkg}/usr/lib64/bfd-plugins
ln -sfv ../../libexec/gcc/${CLFS_TARGET64}/10.2.0/liblto_plugin.so ${pkg}/usr/lib64/bfd-plugins/liblto_plugin.so
chown -v -R root:root \
    ${pkg}/usr/lib64/gcc/${CLFS_TARGET64}/10.2.0/include{,-fixed}
ln -v -sf ../usr/bin/cpp ${pkg}/lib64/cpp

install -v -dm755 ${pkg}/usr/lib32/bfd-plugins
ln -sfv ../../libexec/gcc/${CLFS_TARGET}/10.2.0/liblto_plugin.so ${pkg}/usr/lib32/bfd-plugins/liblto_plugin.so

(cd ${pkg}/usr/lib32
for f in *;do
ln -svfn ../lib32/$f ${pkg}/usr/lib/$f;done)

if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
( cd ${pkg}
  for file in $(find . -type f -name "*.la") ; do
    cat $file | sed -e 's%-L/gcc-[[:graph:]]* % %g' > $TEMPBUILD/tmp-la-file
    cat $TEMPBUILD/tmp-la-file > $file
  done
  rm $TEMPBUILD/tmp-la-file
)

# Don't ship .la files in /{,usr/}lib${LIBDIRSUFFIX}:
rm -f ${pkg}/{,usr/}lib64/*.la

# Strip bloated binaries and libraries:
( cd ${pkg}
  find . -name "lib*so*" -exec strip --strip-unneeded "{}" \;
  find . -name "lib*so*" -exec patchelf --remove-rpath "{}" \;
  find . -name "lib*a" -exec strip -g "{}" \;
  strip --strip-unneeded usr/bin/* 2> /dev/null
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
ARCH="x86_64"
  zcat $PATCHSOURCE/gcc-no_fixincludes.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1
mkdir -v ../gcc-build
cd ../gcc-build
export PKG_CONFIG_PATH="/usr/lib64/pkgconfig:/i686/lib/pkgconfig"
  LDFLAGS="-L/usr/lib64:/i686/lib" \
  CFLAGS="-O2 -fPIC" \
  CXXFLAGS="-O2 -fPIC" \
../${packagedir}/configure \
    --prefix=/usr \
    --libdir=/usr/lib64 \
    --enable-languages=c,c++,fortran,go,objc,obj-c++,lto \
    --with-system-zlib \
    --enable-install-libiberty \
     --enable-shared \
     --enable-plugin \
     --enable-lto \
     --verbose \
     --build=${CLFS_TARGET64} \
     --target=${CLFS_TARGET64} \
     --host=${CLFS_TARGET64}
     make -j5
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
