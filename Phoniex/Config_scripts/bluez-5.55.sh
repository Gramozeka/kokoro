
###############################################################################
set -e
# The BlueZ package contains the Bluetooth protocol stack for Linux.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/linux/bluetooth/bluez-5.52.tar.xz
# Download MD5 sum: a33eb9aadf1dd4153420958709d3ce60
# Download size: 1.9 MB
# Estimated disk space required: 81 MB (add 77 MB for tests)
# Estimated build time: 0.4 SBU (using parallelism=4; add 0.2 SBU for tests)
# BlueZ Dependencies
# Required
# dbus-1.12.16, GLib-2.62.4, and libical-3.0.7
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/usr/sbin
ln -svf ../libexec/bluetooth/bluetoothd ${pkg}/usr/sbin/bluetoothd

install -v -dm755 ${pkg}/etc/bluetooth &&
install -v -m644 src/main.conf ${pkg}/etc/bluetooth/main.conf

install -v -dm755 ${pkg}/usr/share/doc/${packagedir} &&
install -v -m644 doc/*.txt ${pkg}/usr/share/doc/${packagedir}
mkdir -pv ${pkg}/etc/bluetooth
cat > ${pkg}/etc/bluetooth/rfcomm.conf << "EOF"
# Start rfcomm.conf
# Set up the RFCOMM configuration of the Bluetooth subsystem in the Linux kernel.
# Use one line per command
# See the rfcomm man page for options


# End of rfcomm.conf
EOF

cat > ${pkg}/etc/bluetooth/uart.conf << "EOF"
# Start uart.conf
# Attach serial devices via UART HCI to BlueZ stack
# Use one line per device
# See the hciattach man page for options

# End of uart.conf
EOF

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
            --enable-library      \
            --disable-systemd \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
