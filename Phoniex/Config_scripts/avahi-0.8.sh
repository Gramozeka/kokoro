
###############################################################################
set -e
###############################################################################
# The Avahi package is a system which facilitates service discovery on a local network.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/lathiat/avahi/releases/download/v0.7/avahi-0.7.tar.gz
# Download MD5 sum: d76c59d0882ac6c256d70a2a585362a6
# Download size: 1.3 MB
# Estimated disk space required: 23 MB
# Estimated build time: 0.3 SBU
# Avahi Dependencies
# Required
# GLib-2.62.4
# Recommended
# gobject-introspection-1.62.0, GTK+-2.24.32, GTK+-3.24.13, libdaemon-0.14 and libglade-2.6.4
# Optional
# D-Bus Python-1.2.16, PyGTK-2.24.0, Doxygen-1.8.17 and xmltoman (for generating documentation)
###############################################################################
groupadd -fg 84 avahi &&
useradd -c "Avahi Daemon Owner" -d /var/run/avahi-daemon -u 84 \
        -g avahi -s /bin/false avahi || true
# # There should also be a dedicated priviliged access group for Avahi clients. Issue the following command as the root user:

groupadd -fg 86 netdev
###############################################################################
if (pkg-config --exists Qt5Core) 
then
echo "Qt5 enabled"
else
QT5="--disable-qt5"
fi

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--disable-monodoc    \
            --disable-python     \
            --disable-qt3        \
            --disable-qt4        \
            --with-distro=none   \
            --with-systemdsystemunitdir=no \
            --enable-compat-libdns_sd \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir} $QT5
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# cd boot-script
# make install-avahi
