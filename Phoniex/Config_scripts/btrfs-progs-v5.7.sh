
###############################################################################
set -e
# The btrfs-progs package contains administration and debugging tools for the B-tree file system (btrfs).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/linux/kernel/people/kdave/btrfs-progs/btrfs-progs-v5.4.1.tar.xz
# Download MD5 sum: 4fb3ac57dc4e0afcf723cdf34640179b
# Download size: 2.0 MB
# Estimated disk space required: 62 MB (add 71 MB for tests)
# Estimated build time: 0.2 SBU (add 15 SBU for tests)
# Btrfs-progs Dependencies
# Required
# LZO-2.10
# Recommended
# asciidoc-8.6.9 and xmlto-0.0.28 (both required to generate man pages)
# Optional
# LVM2-2.03.07 (dmsetup is used in tests), Python-2.7.17 (python bindings), reiserfsprogs-3.6.27 (for tests), and zstd (for tests)
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/usr/lib64
mkdir -pv  ${pkg}/sbin
ln -sfv ../../lib64/$(readlink ${pkg}/lib64/libbtrfs.so) ${pkg}/usr/lib64/libbtrfs.so &&
ln -sfv ../../lib64/$(readlink ${pkg}/lib64/libbtrfsutil.so) ${pkg}/usr/lib64/libbtrfsutil.so &&
rm -fv ${pkg}/lib64/libbtrfs.{a,so} ${pkg}/lib64/libbtrfsutil.{a,so} &&
mv -v ${pkg}/bin/{mkfs,fsck}.btrfs ${pkg}/sbin
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--bindir=/bin \
--libdir=/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
