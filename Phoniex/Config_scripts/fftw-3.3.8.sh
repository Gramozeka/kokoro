
###############################################################################
set -e
###############################################################################
###############################################################################
# FFTW is a C subroutine library for computing the discrete Fourier transform (DFT) in one or more dimensions, of arbitrary input size, and of both real and complex data (as well as of even/odd data, i.e. the discrete cosine/sine transforms or DCT/DST).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.fftw.org/fftw-3.3.8.tar.gz
# Download (FTP): ftp://ftp.fftw.org/pub/fftw/fftw-3.3.8.tar.gz
# Download MD5 sum: 8aac833c943d8e90d51b697b27d4384d
# Download size: 3.9 MB
# Estimated disk space required: 57 MB (add 1 MB for tests)
# Estimated build time: 1.6 SBU (using parallelism=4; add 1.9 SBU for tests)
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/fftw
# Installation of fftw
# [Note] Note
# We build fftw three times for different libraries in different numerical precisions: the default double precision floating point, the older 32-bit (single precision) version named float which sacrifices precision for speed, and the long double which offers increased precision at the cost of slower execution.
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "s|-mtune=native|$BUILD64|" configure
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
            --enable-shared  \
            --enable-threads \
            --enable-sse2    \
            --enable-avx
make -j4
make  DESTDIR=${pkg} install-strip
make clean
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
            --enable-shared  \
            --enable-threads \
            --enable-sse2    \
            --enable-avx     \
            --enable-float
make -j4
make  DESTDIR=${pkg} install-strip || exit 1
make clean
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
            --enable-shared  \
            --enable-threads \
            --enable-long-double
make -j4
make  DESTDIR=${pkg} install-strip
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-Multi-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-Multi-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-Multi-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
