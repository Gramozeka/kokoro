
###############################################################################
set -e
# The GStreamer Base Plug-ins is a well-groomed and well-maintained collection of GStreamer plug-ins and elements, spanning the range of possible types of elements one would want to write for GStreamer. You will need at least one of Good, Bad, Ugly or Libav plugins for GStreamer applications to function properly.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-1.18.0.tar.xz
# Download MD5 sum: 3fdb32823535799a748c1fc14f978e2c
# Download size: 3.8 MB
# Estimated disk space required: 93 MB (with tests; without docs)
# Estimated build time: 0.8 SBU (Using parallelism=4; with tests; without docs)
# GStreamer Base Plug-ins Dependencies
# Required
# gstreamer-1.18.0
# Recommended
# alsa-lib-1.2.1.2, CDParanoia-III-10.2 (for building the CDDA plugin), gobject-introspection-1.62.0, ISO Codes-4.4, libogg-1.3.4, libtheora-1.1.1, libvorbis-1.3.6, and Xorg Libraries
# Optional
# GTK+-3.24.13 (for examples), GTK-Doc-1.32, Opus-1.3.1, Qt-5.14.1 (for examples), SDL-1.2.15, Valgrind-3.15.0, libvisual, Orc, and Tremor
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
         -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
