
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install &&
make DESTDIR=${pkg} soinstall &&
mkdir -pv ${pkg}/usr/include/ghostscript
mkdir -pv ${pkg}/usr/share/doc
cp -a examples/ ${pkg}/usr/share/ghostscript/9.53.3/
install -v -m644 base/*.h ${pkg}/usr/include/ghostscript &&
ln -sfvn ../ghostscript/9.53.3 ${pkg}/usr/share/doc/${packagedir}
ln -v -s ghostscript ${pkg}/usr/include/ps
tar -xvf  ${SOURCE}/ghostscript-fonts-std-8.11.tar.gz -C ${pkg}/usr/share/ghostscript --no-same-owner &&
tar -xvf  ${SOURCE}/gnu-gs-fonts-other-6.0.tar.gz -C ${pkg}/usr/share/ghostscript --no-same-owner &&
mkdir -pv ${pkg}/usr/share/fonts
ln -sfvn ../ghostscript/fonts ${pkg}/usr/share/fonts/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
rm -rf freetype lcms2mt jpeg libpng openjpeg
rm -rf zlib &&
patch -Np1 -i $PATCHSOURCE/ghostscript-9.53.3-freetype_fix-1.patch
patch -Np1 -i $PATCHSOURCE/0001-detect-special-null-output-file-and-do-not-use-multi.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
            --disable-compile-inits \
            --enable-dynamic        \
            --with-system-libtiff   \
--build=${CLFS_TARGET} &&
make -j4
make so
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# fc-cache -v /usr/share/ghostscript/fonts/
