#!/bin/bash
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
# git clone https://github.com/ispc/ispc.git
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
patch -p1 -i $PATCHSOURCE/ispc-libclang-cpp.patch
sed -i '/debug-phase/d' tests/lit-tests/arg_parsing_errors.ispc
patch -p1 -i $PATCHSOURCE/ispc-llvm11.patch
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/usr \
             -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
             -DLIB_INSTALL_DIR=lib64 \
             -DISPC_INCLUDE_EXAMPLES=OFF \
             -DISPC_INCLUDE_TESTS=OFF \
             -DISPC_INCLUDE_BENCHMARKS=OFF \
             -DGENX_ENABLED=OFF \
             -DISPC_WINDOWS_TARGET=OFF \
             -DISPC_PS4_TARGET=OFF \
             -DISPC_IOS_TARGET=OFF \
             -DISPC_MACOS_TARGET=OFF \
             -DARM_ENABLED=OFF \
             -DISPC_NO_DUMPS=ON \
             -DCMAKE_BUILD_TYPE=Release  \
             -DLIB_SUFFIX="64" \
             -DBUILD_TESTING=OFF \
             -Wno-dev ..
make -j4 || make || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
