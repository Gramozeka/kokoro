
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
unpack ${SOURCE}/${package}
mkdir -pv ${pkg}/srv/www
cp -a ${packagedir} ${pkg}/srv/www/phpmyadmin
chown -R root:apache ${pkg}/srv/www/phpmyadmin
chmod 0750 ${pkg}/srv/www/phpmyadmin
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
