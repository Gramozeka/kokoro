
###############################################################################
set -e
###############################################################################
###############################################################################
# Redland is a set of free software C libraries that provide support for the Resource Description Framework (RDF).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://download.librdf.org/source/redland-1.0.17.tar.gz
# Download MD5 sum: e5be03eda13ef68aabab6e42aa67715e
# Download size: 1.6 MB
# Estimated disk space required: 18 MB
# Estimated build time: 0.2 SBU
# Redland Dependencies
# Required
# Rasqal-0.9.33
# Optional
# Berkeley DB-5.3.28, libiodbc-3.52.12, SQLite-3.31.1, MariaDB-10.4.12 or MySQL, PostgreSQL-12.1, virtuoso, and 3store
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
