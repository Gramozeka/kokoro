
###############################################################################
pack_local () {
pkg=${destdir}/$1

make install DESTDIR=${pkg} LIBDIR=/usr/lib64
rm -f ${pkg}/usr/lib64/libx86.a
mkdir -p ${pkg}/usr/doc/${packagedir}
cp -a COPYRIGHT ${pkg}/usr/doc/${packagedir}
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/libx86-add-pkgconfig.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/libx86-mmap-offset.patch.gz | patch -p1 --verbose || exit 1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make CFLAGS="-O2 -fPIC" BACKEND=x86emu LIBDIR=/usr/lib64 || exit 1
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
