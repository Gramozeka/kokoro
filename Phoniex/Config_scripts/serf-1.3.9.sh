
###############################################################################
# The Serf package contains a C-based HTTP client library built upon the Apache Portable Runtime (APR) library. It multiplexes connections, running the read/write communication asynchronously. Memory copies and transformations are kept to a minimum to provide high performance operation.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.apache.org/dist/serf/serf-1.3.9.tar.bz2
# Download MD5 sum: 370a6340ff20366ab088012cd13f2b57
# Download size: 144 KB
# Estimated disk space required: 3.2 MB
# Estimated build time: less than 0.1 SBU
# Serf Dependencies
# Required
# Apr-Util-1.6.1 and SCons-3.1.2
# Optional
# MIT Kerberos V5-1.17.1, for the GSSAPI
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "/Append/s:RPATH=libdir,::"          SConstruct &&
sed -i "/Default/s:lib_static,::"           SConstruct &&
sed -i "/Alias/s:install_static,::"         SConstruct &&
sed -i "/  print/{s/print/print(/; s/$/)/}" SConstruct &&
sed -i "/get_contents()/s/,/.decode()&/"    SConstruct &&
ARCH="x86_64"
scons PREFIX=/usr/ LIBDIR=/usr/lib64
scons install --install-sandbox=${pkg}
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
