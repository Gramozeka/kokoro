
###############################################################################
set -e
###############################################################################
# nghttp2 is an implementation of HTTP/2 and its header compression algorithm, HPACK.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/nghttp2/nghttp2/releases/download/v1.40.0/nghttp2-1.40.0.tar.xz
# Download MD5 sum: 8d1a6b96760254e4dd142d7176e8fb7c
# Download size: 1.6 MB
# Estimated disk space required: 17 MB
# Estimated build time: 0.1 SBU
# nghttp2 Dependencies
# Recommended
# libxml2-2.9.10
# Optional
# Boost-1.72.0, jansson-2.12, libevent-2.1.11, Python-2.7.17, CUnit (required for the testsuite), Cython, jemalloc, libev, mruby, Spdylay, and Sphinx.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHONPATH=/usr/lib64/python2.7 \
 CONFIG_SHELL=/bin/bash \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--enable-lib-only \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
