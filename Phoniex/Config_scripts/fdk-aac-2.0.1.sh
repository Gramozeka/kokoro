
###############################################################################
set -e
# fdk-aac package provides the Fraunhofer FDK AAC library, which is purported to be a high quality Advanced Audio Coding implementation.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/opencore-amr/fdk-aac-2.0.1.tar.gz
# Download MD5 sum: e8b0b38e837df455b8a1ba75417ff0ad
# Download size: 2.7 MB
# Estimated disk space required: 51 MB
# Estimated build time: 0.6 SBU
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
