
###############################################################################
set -e
# D-Bus is a message bus system, a simple way for applications to talk to one another. D-Bus supplies both a system daemon (for events such as “new hardware device added” or “printer queue changed”) and a per-user-login-session daemon (for general IPC needs among user applications). Also, the message bus is built on top of a general one-to-one message passing framework, which can be used by any two applications to communicate directly (without going through the message bus daemon).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://dbus.freedesktop.org/releases/dbus/dbus-1.12.16.tar.gz
# Download MD5 sum: 2dbeae80dfc9e3632320c6a53d5e8890
# Download size: 2.0 MB
# Estimated disk space required: 21 MB (add 17 MB for the tests)
# Estimated build time: 0.3 SBU (add 8.5 SBU for the tests)
# D-Bus Dependencies
# Recommended
# Xorg Libraries (for dbus-launch program) and elogind-241.4 (These are circular dependencies. First build without them, and then again after both packages are installed.)
# Optional
# For the tests: dbus-glib-0.110, D-Bus Python-1.2.16, PyGObject-3.34.0, and Valgrind-3.15.0; for documentation: Doxygen-1.8.17, xmlto-0.0.28, Ducktype(pip install mallard-ducktype), and Yelp Tools(http://ftp.gnome.org/pub/gnome/sources/yelp-tools/3.32/)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/lib64
chown -v root:messagebus ${pkg}/usr/libexec/dbus-daemon-launch-helper &&
chmod -v      4750       ${pkg}/usr/libexec/dbus-daemon-launch-helper
mv -v ${pkg}/usr/lib64/libdbus-1.so.* ${pkg}/lib64 &&
ln -sfv ../../lib64/$(readlink ${pkg}/usr/lib64/libdbus-1.so) ${pkg}/usr/lib64/libdbus-1.so
ln -sv ../var/lib/dbus/machine-id ${pkg}/etc
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/dbus-1.12.x-allow_root_globally.diff.gz | patch -p1 --verbose || exit 1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
            --enable-user-session                \
            --disable-doxygen-docs               \
            --disable-xml-docs                   \
            --disable-static                     \
            --with-systemduserunitdir=no         \
            --with-systemdsystemunitdir=no       \
            --docdir=/usr/share/doc/${packagedir} \
            --with-console-auth-dir=/run/console \
            --with-system-pid-file=/run/dbus/pid \
            --with-system-socket=/run/dbus/system_bus_socket &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
chown -v root:messagebus /usr/libexec/dbus-daemon-launch-helper &&
chmod -v      4750       /usr/libexec/dbus-daemon-launch-helper
cd boot-script
make install-dbus
dbus-uuidgen --ensure
