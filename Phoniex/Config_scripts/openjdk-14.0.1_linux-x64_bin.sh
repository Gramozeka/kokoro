
###############################################################################
# Java is different from most of the packages in LFS and BLFS. It is a programming language that works with files of byte codes to obtain instructions and executes then in a Java Virtual Machine (JVM). An introductory java program looks like:
# public class HelloWorld
# {
#     public static void main(String[] args)
#     {
#         System.out.println("Hello, World");
#     }
# }
# This program is saved as HelloWorld.java. The file name, HelloWorld, must match the class name. It is then converted into byte code with javac HelloWorld.java. The output file is HelloWorld.class. The program is executed with java HelloWorld. This creates a JVM and runs the code. The 'class' extension must not be specified.
# Several class files can be combined into one file with the jar command. This is similar to the standard tar command. For instance, the command jar cf myjar.jar *.class will combine all class files in a directory into one jar file. These act as library files.
# The JVM can search for and use classes in jar files automatically. It uses the CLASSPATH environment variable to search for jar files. This is a standard list of colon-separated directory names similar to the PATH environment variable.
# Binary JDK Information
# Creating a JVM from source requires a set of circular dependencies. The first thing that's needed is a set of programs called a Java Development Kit (JDK). This set of programs includes java, javac, jar, and several others. It also includes several base jar files.
# To start, we set up a binary installation of the JDK created by the BLFS editors. It is installed in the /opt directory to allow for multiple installations, including a source based version.
# This package is known to build and work properly using an LFS-9.0 platform.
# Binary Package Information
# Binary download (x86): http://anduin.linuxfromscratch.org/BLFS/OpenJDK/OpenJDK-11.0.2/OpenJDK-11.0.2+9-i686-bin.tar.xz
# Download MD5 sum: 2b917647040e9804e0f3ccd51f40394b
# Download size (binary): 158 MB
# Estimated disk space required: 273 MB
# Binary download (x86_64): https://download.java.net/java/GA/jdk12.0.2/e482c34c86bd4bf8b56c0b35558996b9/10/GPL/openjdk-14.0.1_linux-x64_bin.tar.gz
# Download MD5 sum: f5da6f4dec81bdd2a096184ec1d69216
# Download size (binary): 189 MB
# Estimated disk space required: 296 MB
# Java Binary Runtime Dependencies
# alsa-lib-1.2.1.2, Cups-2.3.1, giflib-5.2.1, and Xorg Libraries '
###############################################################################
cd $TEMPBUILD
rm -rf *
package=openjdk-14.0.1_linux-x64_bin.tar.gz
packagedir=jdk-14.0.1
unpack ${SOURCE}/${package}
cd ${packagedir}
pkg=${destdir}/${packagedir}
ARCH="x86_64"
install -vdm755 ${pkg}/opt/jdk-14.0.1 &&
mv -v * ${pkg}/opt/jdk-14.0.1         &&
chown -R root:root ${pkg}/opt/jdk-14.0.1
ln -sfn jdk-14.0.1 ${pkg}/opt/jdk
mkdir -pv ${pkg}/etc/profile.d
echo "###########################**** COMPLITE!!! ****##################################"
cat > ${pkg}/etc/profile.d/openjdk.sh << "EOF"
# Begin /etc/profile.d/openjdk.sh

# Set JAVA_HOME directory
JAVA_HOME=/opt/jdk

# Adjust PATH
pathappend $JAVA_HOME/bin

# Add to MANPATH
pathappend $JAVA_HOME/man MANPATH

# Auto Java CLASSPATH: Copy jar files to, or create symlinks in, the
# /usr/share/java directory. Note that having gcj jars with OpenJDK 8
# may lead to errors.

AUTO_CLASSPATH_DIR=/usr/share/java

pathprepend . CLASSPATH

for dir in `find ${AUTO_CLASSPATH_DIR} -type d 2>/dev/null`; do
    pathappend $dir CLASSPATH
done

for jar in `find ${AUTO_CLASSPATH_DIR} -name "*.jar" 2>/dev/null`; do
    pathappend $jar CLASSPATH
done

export JAVA_HOME
unset AUTO_CLASSPATH_DIR dir jar

# End /etc/profile.d/openjdk.sh
EOF
cat >> /etc/man_db.conf << "EOF" &&
# Begin Java addition
MANDATORY_MANPATH     /opt/jdk/man
MANPATH_MAP           /opt/jdk/bin     /opt/jdk/man
MANDB_MAP             /opt/jdk/man     /var/cache/man/jdk
# End Java addition
EOF
ln -sfv /etc/pki/tls/java/cacerts ${pkg}/opt/jdk/lib/security/cacerts
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home


source /etc/profile &&
mkdir -pv /var/cache/man
mandb -c /opt/jdk/man

