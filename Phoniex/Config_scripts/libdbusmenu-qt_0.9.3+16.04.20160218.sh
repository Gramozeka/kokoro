
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
mkdir -p build
cd build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
  cmake \
    -DCMAKE_C_FLAGS:STRING="${BUILD64}" \
    -DCMAKE_CXX_FLAGS:STRING="${BUILD64}" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DSYSCONF_INSTALL_DIR=/etc \
    -DLIB_SUFFIX=64 \
    -DCMAKE_BUILD_TYPE=Release  \
    -DWITH_DOC=ON \
    ..
    make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
