
###############################################################################
set -e
# GNU libffcall is a library which can be used to build foreign function call interfaces in embedded interpreters.
# Installed libraries and header files
# It installs a library libffcall.{a,so}; to link with it, use the compiler option -lffcall.
# It consists of two parts:
# avcall
# Calling C functions with variable arguments. Its include file is <avcall.h>.
# callback
# Closures with variable arguments as first-class C functions. Its include file is <callback.h>.
# Additionally, you can determine the libffcall version by including <ffcall-version.h>.
# For backward compatibility with versions 1.x, libraries libavcall.{a,so} and libcallback.{a,so} are installed as well. But they are deprecated; use libffcall.{a,so} instead.
# Downloading libffcall
# libffcall can be downloaded from https://ftp.gnu.org/gnu/libffcall/libffcall-2.2.tar.gz.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
  --enable-static=yes \
  --enable-shared=no \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
