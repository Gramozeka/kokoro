
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
unpack ${SOURCE}/libatomic_ops-7.6.10.tar.*
mv libatomic_ops-7.6.10 libatomic_ops
sed -i 's#pkgdata#doc#' doc/doc.am
sed -i 's#pkgdata#doc#' libatomic_ops/Makefile.am
sed -i 's#pkgdata#doc#' libatomic_ops/doc/Makefile.am

autoreconf -vif
automake --add-missing
CXXFLAGS=$CXXFLAGS \
CFLAGS="-O2 -fPIC -DUSE_GET_STACKBASE_FOR_MAIN" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
  --enable-cplusplus \
  --enable-large-config \
  --enable-parallel-mark \
  --enable-threads=posix \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
