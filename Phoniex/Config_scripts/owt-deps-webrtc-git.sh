
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# make DESTDIR=${pkg} install

  install -Dm644 -t ${pkg}/usr/lib64/ out/Release/obj/libwebrtc.a
  mkdir -p ${pkg}/usr/include/libwebrtc

  shopt -s globstar
  for i in ./**/*.h;do
    if [[ $i =~ "./test/" || $i =~ "./build/" || $i =~ "./third_party/" ]]; then
        continue
    fi
    install -Dm644 -t ${pkg}/usr/include/libwebrtc/$(dirname $i) $i
  done
  for i in third_party/abseil-cpp/**/*.h; do
    if [[ $i =~ "/test/" || $i =~ "/build/" ]]; then
        continue
    fi
    install -Dm644 -t ${pkg}/usr/include/libwebrtc/$(dirname $i) $i
  done
  for i in third_party/libyuv/**/*.h; do
    if [[ $i =~ "/test/" || $i =~ "/build/" ]]; then
        continue
    fi
    install -Dm644 -t ${pkg}/usr/include/libwebrtc/$(dirname $i) $i
  done
  
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
# package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/open-webrtc-toolkit/owt-deps-webrtc.git ${packagedir}
cp -vr ${packagedir} src
# unzip ${SOURCE}/depot_tools.zip -d depot_tools
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=`pwd`/depot_tools:"$PATH"
cp ${PATCHSOURCE}/owt-deps-webrtc/gclient-conf .gclient
DEPOT_TOOLS_UPDATE=0 depot_tools/gclient sync --no-history
cd src
# patch -Np1 -i ${PATCHSOURCE}/owt-deps-webrtc

  patch -Np1 < ${PATCHSOURCE}/owt-deps-webrtc/src.diff
  patch -Np1 < ${PATCHSOURCE}/owt-deps-webrtc/0001-IWYU-fix-missing-uint32_t-size_t-definitions.patch
  cd build
  patch -Np1 < ${PATCHSOURCE}/owt-deps-webrtc/build.diff
  cd ../third_party
  patch -Np1 < ${PATCHSOURCE}/owt-deps-webrtc/third_party.diff
  cd libsrtp
  patch -Np1 < ${PATCHSOURCE}/owt-deps-webrtc/libsrtp.diff
  
cd ../..

  mkdir out/Release
  cp ${PATCHSOURCE}/owt-deps-webrtc/args.gn out/Release/args.gn

gn gen out/Release
#   cd src
ninja -C out/Release webrtc
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
