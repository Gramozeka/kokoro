
###############################################################################
set -e
# The Tcl package contains the Tool Command Language, a robust general-purpose scripting language.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/tcl/tcl8.6.10-src.tar.gz
# Download MD5 sum: 97c55573f8520bcab74e21bfd8d0aadc
# Download size: 9.7 MB
# Estimated disk space required: 67 MB (including html documentation)
# Estimated build time: 0.7 SBU (add 2.7 SBU for tests)
# Additional Downloads
# Optional Documentation
# Download (HTTP): https://downloads.sourceforge.net/tcl/tcl8.6.10-html.tar.gz
# Download MD5 sum: a012711241ba3a5bd4a04e833001d489
# Download size: 1.2 MB
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make install-private-headers DESTDIR=${pkg}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
ln -v -sf tclsh8.6 ${pkg}/usr/bin/tclsh &&
chmod -v 755 ${pkg}/usr/lib64/libtcl8.6.so
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
mkdir -v -p ${pkg}/usr/share/doc/tcl-8.6.10 &&
cp -v -r  ../html/* ${pkg}/usr/share/doc/tcl-8.6.10
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}

cd ${packagedir}
tar -xf ${SOURCE}/tcl8.6.10-html.tar.gz --strip-components=1
export SRCDIR=`pwd` &&
cd unix &&
sed -i -e "s#TCL_LIBRARY='\$(prefix)/lib/tcl\$(VERSION)'#TCL_LIBRARY='\$(prefix)/lib64/tcl\$(VERSION)'#" configure
CFLAGS="$CFLAGS -DHAVE_USLEEP=1" \
CXXFLAGS="$CXXFLAGS -DHAVE_USLEEP=1" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--enable-shared \
--enable-64bit \
--mandir=/usr/share/man \
--build=${CLFS_TARGET}
make -j4
 sed -e "s@^\(TCL_SRC_DIR='\).*@\1/usr/include'@" \
     -e "/TCL_B/s@='\(-L\)\?.*unix@='\1/usr/lib64@" \
     -i tclConfig.sh                     &&
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
