
###############################################################################
set -e
###############################################################################
# The GNOME Themes Extra package, formerly known as GNOME Themes Standard, contains various components of the default GNOME theme.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/3.28/gnome-themes-extra-3.28.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/3.28/gnome-themes-extra-3.28.tar.xz
# Download MD5 sum: f9f2c6c521948da427f702372e16f826
# Download size: 2.8 MB
# Estimated disk space required: 40 MB
# Estimated build time: 0.3 SBU
# GNOME Themes Extra Dependencies
# Required
# GTK+-2.24.32 or GTK+-3.24.13 with librsvg-2.46.4 or both
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
