
###############################################################################
set -e
###############################################################################
# The Gedit package contains a lightweight UTF-8 text editor for the GNOME Desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gedit/3.34/gedit-3.34.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gedit/3.34/gedit-3.34.1.tar.xz
# Download MD5 sum: 4bca1d0a13db186cd84619b4bff42238
# Download size: 14 MB
# Estimated disk space required: 70 MB (with tests)
# Estimated build time: 0.4 SBU (using parallelism=4; with tests)
# Gedit Dependencies
# Required
# git-2.25.0, gsettings-desktop-schemas-3.34.0, gspell-1.8.2, gtksourceview4-4.4.0, itstool-2.0.6, and libpeas-1.24.1
# Recommended
# Gvfs-1.42.2 (runtime), ISO Codes-4.4, libsoup-2.68.3, and PyGObject-3.34.0 (Python 3 module)
# Optional
# GTK-Doc-1.32, Vala-0.46.5, and zeitgeist
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Ddocumentation=true \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas
