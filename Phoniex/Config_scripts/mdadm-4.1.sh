
###############################################################################
set -e
###############################################################################
# The mdadm package contains administration tools for software RAID.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/linux/utils/raid/mdadm/mdadm-4.1.tar.xz
# Download MD5 sum: 51bf3651bd73a06c413a2f964f299598
# Download size: 432 KB
# Estimated disk space required: 4.9 MB (10 MB with tests)
# Estimated build time: 0.1 SBU (tests take about an hour, only partially processor dependent)
# mdadm Dependencies
# Optional
# A MTA
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/mdadm
# [Caution] Caution
# Kernel versions in series 4.1 through 4.4.1 have a broken RAID implementation. Use a kernel with version at or above 4.4.2.
# Kernel Configuration
# Enable the following options in the kernel configuration and recompile the kernel, if necessary. Only the RAID types desired are required.
# Device Drivers --->
#   [*] Multiple devices driver support (RAID and LVM) ---> [CONFIG_MD]
#     <*> RAID support                                      [CONFIG_BLK_DEV_MD]
#     [*]   Autodetect RAID arrays during kernel boot       [CONFIG_MD_AUTODETECT]
#     <*/M>  Linear (append) mode                           [CONFIG_MD_LINEAR]
#     <*/M>  RAID-0 (striping) mode                         [CONFIG_MD_RAID0]
#     <*/M>  RAID-1 (mirroring) mode                        [CONFIG_MD_RAID1]
#     <*/M>  RAID-10 (mirrored striping) mode               [CONFIG_MD_RAID10]
#     <*/M>  RAID-4/RAID-5/RAID-6 mode                      [CONFIG_MD_RAID456]
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed 's@-Werror@@' -i Makefile
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
