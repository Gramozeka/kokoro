
###############################################################################
set -e
###############################################################################
# The LXDM is a lightweight Display Manager for the LXDE desktop. It can also be used as an alternative to other Display Managers such as GNOME's GDM or LightDM.'
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/lxdm/lxdm-0.5.3.tar.xz
# Download MD5 sum: 061caae432634e6db38bbdc84bc6ffa0
# Download size: 236 KB
# Estimated disk space required: 5.6 MB
# Estimated build time: less than 0.1 SBU
# LXDM Dependencies
# Required
# GTK+-2.24.32, ISO Codes-4.4, and librsvg-2.46.4 (runtime, for default theme background)
# Recommended
# LXSession-0.5.4 (for lxpolkit) or polkit-gnome-0.105
# Optional
# GTK+-3.24.13
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
cat > pam/lxdm << "EOF"
# Begin /etc/pam.d/lxdm

auth     requisite      pam_nologin.so
auth     required       pam_env.so
auth     required       pam_succeed_if.so user ingroup video
auth     include        system-auth

account  include        system-account

password include        system-password

session  required       pam_limits.so
session  include        system-session

# End /etc/pam.d/lxdm
EOF

sed -i 's:sysconfig/i18n:profile.d/i18n.sh:g' data/lxdm.in &&
sed -i 's:/etc/xprofile:/etc/profile:g' data/Xsession &&
sed -e 's/^bg/#&/'        \
    -e '/reset=1/ s/# //' \
    -e 's/logou$/logout/' \
    -e "/arg=/a arg=/usr/bin/X" \
    -i data/lxdm.conf.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
            --with-pam        \
            --with-systemdsystemunitdir=no \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cd boot-script
make install-lxdm
