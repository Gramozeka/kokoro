
###############################################################################
set -e
# The Little Color Management System is a small-footprint color management engine, with special focus on accuracy and performance. It uses the International Color Consortium standard (ICC), which is the modern standard for color management.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/lcms/lcms2-2.9.tar.gz
# Download MD5 sum: 8de1b7724f578d2995c8fdfa35c3ad0e
# Download size: 10 MB
# Estimated disk space required: 24 MB (with the tests)
# Estimated build time: 0.2 SBU (with the tests)
# Little CMS2 Dependencies
# Optional
# libjpeg-turbo-2.0.4 and LibTIFF-4.1.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
