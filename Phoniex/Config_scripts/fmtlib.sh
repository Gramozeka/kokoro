
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
# 	git clone https://github.com/fmtlib/fmt.git fmtlib
###############################################################################
	USE_ARCH="64" 
		ARCH="x86_64"
			LDFLAGS="-L/lib64:/usr/lib64"
				LD_LIBRARY_PATH="/lib64:/usr/lib64"
					PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
###############################################################################
	pack_local64 () {
	pkg=${destdir}/$1
	make DESTDIR=${pkg} install
###############################################################################

###############################################################################	
	find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
	find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
					  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
###############################################################################

###############################################################################
	if [ -d ${pkg}/usr/share/man ]; then
  		find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
	fi
###############################################################################

###############################################################################
	cd ${pkg} &&
	mkdir -p usr/tree-info
	find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree
###############################################################################

###############################################################################
	/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
	installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
	echo "$1 ----- $(date)" >> ${destdir}/loginstall	
	rm -rf ${pkg}
	cd $home
	}
###############################################################################
############ 
###############################################################################
	unpack ${SOURCE}/${package}
	cd ${packagedir}
	mkdir build
	cd    build
	cmake    \
	-DCMAKE_C_FLAGS="$BUILD64"               \
	-DCMAKE_CXX_FLAGS="$BUILD64"          \
	-DCMAKE_INSTALL_PREFIX=/usr                \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib64    \
	-DLIB_INSTALL_DIR=/usr/lib64                   \
	-DBUILD_SHARED_LIBS=ON                         \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX="64"                                           \
	-DX11_RENDER_SYSTEM=gl \
	-DBUILD_TESTING=OFF                                  \
	-Wno-dev ..
	make -j4 || make || exit 1

	pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


















