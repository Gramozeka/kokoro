
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
 unpack ${SOURCE}/${package}
#git clone https://gitlab.com/accounts-sso/libaccounts-qt.git ${packagedir}
cd ${packagedir}
 mkdir BUILDQT
cd BUILDQT
CWD=$(pwd)
LDFLAGS="-L/lib64:/usr/lib64" \
LD_LIBRARY_PATH="/lib64:/usr/lib64" \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
OPENSSL_LIBS="-L/usr/lib64 -lssl -lcrypto" \
qmake \
CMAKE_CONFIG_PATH=/usr/lib64/cmake \
LIBDIR=/usr/lib64 \
PREFIX=/usr \
../accounts-qt.pro   &&
make -j4
mkdir -pv $pkg
make install INSTALL_ROOT=${pkg}

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
