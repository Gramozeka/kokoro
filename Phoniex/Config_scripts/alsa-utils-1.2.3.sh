
###############################################################################
set -e
# The ALSA Utilities package contains various utilities which are useful for controlling your sound card.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.alsa-project.org/files/pub/utils/alsa-utils-1.2.1.tar.bz2
# Download (FTP): ftp://ftp.alsa-project.org/pub/utils/alsa-utils-1.2.1.tar.bz2
# Download MD5 sum: c4628bae7632937eac2de4cf2a3de82e
# Download size: 1.2 MB
# Estimated disk space required: 15 MB
# Estimated build time: 0.1 SBU
# ALSA Utilities Dependencies
# Required
# alsa-lib-1.2.1.2
# Optional
# fftw-3.3.8, libsamplerate-0.1.9, xmlto-0.0.28, and Dialog
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--disable-alsaconf \
--with-curses=ncursesw \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
