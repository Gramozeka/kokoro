pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
case ${packagedir} in
    libXaw3d-[0-9]* )
( cd ${pkg}/usr/lib64
  ln -sf libXaw3d.so.8 libXaw3d.so.6
  ln -sf libXaw3d.so.8 libXaw3d.so.7
)
    ;;
    pixman-[0-9]* )
( cd ${pkg}/usr/include
  ln -sf pixman-1/pixman-version.h .
  ln -sf pixman-1/pixman.h .
  ln -sf pixman-1 pixman
)
;;
esac
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
######################################################################################
while read -r line; do

    # Get the file name, ignoring comments and blank lines
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
    file=$(echo $line | cut -d" " -f2)

    temp_val=$(echo $file|sed 's|^.*/||')          # Remove directory
    packagedir=$(echo $temp_val|sed 's|\.tar.*||') # Package directory
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/Xorg/lib/${file}.*
cd ${packagedir}
chown -R root:root *
CONF_INDIVIDUAL=""

case ${packagedir} in
    libxcb-[0-9]* )
#     patch -Np1 -i ${SOURCE}/Xorg/lib/libxcb-1.12-python3-1.patch
sed -i "s/pthread-stubs//" configure
CONF_INDIVIDUAL="--docdir=/usr/share/doc/libxcb-1.13.1"
    ;;
    xcb-proto-[0-9]* )
CONF_INDIVIDUAL="PYTHON=/usr/bin/python3"
patch -Np1 -i ${PATCHSOURCE}/xcb-proto-1.14-python3_9.patch
    ;;
    libICE* )
      CONF_INDIVIDUAL=" ICE_LIBS=-lpthread "
    ;;
    libX11-[0-9]* )
      CONF_INDIVIDUAL=" --enable-loadable-i18n "
    ;;
    libFS-[0-9]* )
#     CONF_INDIVIDUAL=
#     patch_line="patch -Np1 -i ${PATCHSOURCE}/FSErrDis.diff"
        ;;
    libXpm-[0-9]* )
#     CONF_INDIVIDUAL=
#     patch_line="patch -Np1 -i ${PATCHSOURCE}/parse.diff"
        ;;

    libXfont2-[0-9]* )
      CONF_INDIVIDUAL=" --build=${CLFS_TARGET}"
#       patch_line="patch -Np1 -i ${PATCHSOURCE}/fontxlfd.diff"
    ;;

    libXt-[0-9]* )
      CONF_INDIVIDUAL="--with-appdefaultdir=/etc/X11/app-defaults "
    ;;

    libXaw3dXft-[0-9]* )
     CONF_INDIVIDUAL="  --enable-multiplane-bitmaps \
  --enable-gray-stipples \
  --enable-arrow-scrollbars \
  --enable-internationalization "
    ;;

    font-util-[0-9]* )
      CONF_INDIVIDUAL=" --with-fontrootdir=/usr/share/fonts "
    ;;
       pixman-[0-9]* )
      zcat  ${PATCHSOURCE}/pixman.remove.tests.that.fail.to.compile.diff.gz | patch -p1 --backup --suffix=.orig
    ;;
esac
case ${packagedir} in
    libvdpau-[0-9]* )

mkdir meson-build
cd meson-build
PYTHON=/usr/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  .. || exit 1
  ninja  -j5 || exit 1
pkg=${destdir}/${packagedir}
  DESTDIR=${pkg} ninja install || exit 1
cd ..
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg}
mkdir -pv ${pkg}/etc/profile.d
cat > ${pkg}/etc/profile.d/vdpau.sh << "EOF"
#!/bin/sh

# Disable debugging output of the vdpau backend
export VDPAU_LOG=0

# Use the vdpau backend of the nvidia binary driver
export VDPAU_DRIVER="nvidia"

# Use the vdpau backend of the nouveau driver
#export VDPAU_DRIVER="nouveau"

# Use the vdpau backend of the r300 driver
#export VDPAU_DRIVER="r300"

# Use the vdpau backend of the r600 driver
#export VDPAU_DRIVER="r600"

# Use the vdpau backend of the radeonsi driver
#export VDPAU_DRIVER="radeonsi"

# Use the va-api/opengl backend
#export VDPAU_DRIVER="va_gl"
EOF
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
ldconfig
continue
;;
    libdrm-[0-9]* )
mkdir meson-build
cd meson-build
PYTHON=/usr/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release -Dudev=true \
  .. || exit 1
  ninja  -j5 || exit 1
pkg=${destdir}/${packagedir}
  DESTDIR=${pkg} ninja install || exit 1
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
ldconfig
continue
;;
esac
$patch_line
patch_line=""
PYTHON=/usr/bin/python3 \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64  \
   --localstatedir=/var \
--disable-static ${CONF_INDIVIDUAL}
# \
# --build=${CLFS_TARGET} &&
make -j4

pack_local ${packagedir}
ldconfig
done < ${home}/${scriptsdir}/Xorg-lib-7.sh
echo "Complite all !"
