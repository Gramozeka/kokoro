
###############################################################################
pack_local () {
pkg=${destdir}/$1
make tooldir=/usr install DESTDIR=${pkg}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
rm -f ${pkg}/{,usr/}lib64/*.la
cd ${pkg} &&
tar -cvpzf ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.tar.gz --one-file-system .
tar -xvpzf ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.tar.gz -C / --numeric-owner
rm -rf ${pkg}
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-final*$//" <<< $(basename $line))
package="$packagedir.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}

zcat ${SOURCE}/binutils-2.20.51.0.2-libtool-lib64.patch.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/binutils-2.25-version.patch.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/binutils-2.25-set-long-long.patch.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/binutils-2.20.51.0.10-copy-osabi.patch.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/binutils-2.20.51.0.10-sec-merge-emit.patch.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/binutils-2.24-ldforcele.patch.gz | patch -p1 --verbose || exit 1
zcat ${SOURCE}/binutils-2.25.1-cleansweep.patch.gz | patch -p2 --verbose || exit 1

# Export the demangle.h header file:
zcat ${SOURCE}/binutils.export.demangle.h.diff.gz | patch -p1 --verbose || exit 1
# Don't check to see if "config.h" was included in the installed headers:
zcat ${SOURCE}/binutils.no-config-h-check.diff.gz | patch -p1 --verbose || exit 1
sed -i -e 's/%''{release}/Vaio/g' bfd/Makefile{.am,.in}
mkdir -v ../binutils-build
cd ../binutils-build
USE_ARCH="x86_64"
CFLAGS="-O2 -fPIC" \
../${packagedir}/configure \
    --prefix=/usr \
    --enable-shared \
    --enable-64-bit-bfd \
    --libdir=/usr/lib64 \
    --enable-gold=yes \
    --enable-multilib \
    --enable-plugins \
    --with-system-zlib \
    --enable-threads \
     --enable-install-libiberty \
       --enable-ld=default \
  --enable-initfini-array \
    --build=${CLFS_TARGET}
make  -j4 tooldir=/usr
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
