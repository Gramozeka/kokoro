
###############################################################################
set -e
###############################################################################
# The CUPS Filters package contains backends, filters and other software that was once part of the core CUPS distribution but is no longer maintained by Apple Inc.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.openprinting.org/download/cups-filters/cups-filters-1.25.6.tar.xz
# Download MD5 sum: f6872e6d606be77ddd0f556386bd9cf6
# Download size: 1.4 MB
# Estimated disk space required: 42 MB (with tests), 15MB installed
# Estimated build time: 0.4 SBU (with tests)
# CUPS Filters Dependencies
# Required
# Cups-2.3.1, GLib-2.62.4, ghostscript-9.50, Little CMS-2.9, Poppler-0.84.0, and Qpdf-9.1.1
# Recommended
# libjpeg-turbo-2.0.4, libpng-1.6.37, LibTIFF-4.1.0 and mupdf-1.16.1 (mutool)
# Optional
# Avahi-0.7, Dejavu fonts (required for the tests), OpenLDAP-2.4.48, PHP-7.4.2 (use of this might be broken), Liblouis (Braille), and Liblouisutdml (Braille)
# Optional printer drivers (runtime)
# Gutenprint-5.3.3 (for supported printers), or other printer drivers, such as hplip
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
