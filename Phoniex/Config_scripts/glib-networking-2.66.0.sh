
###############################################################################
set -e
# The GLib Networking package contains Network related gio modules for GLib.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/glib-networking/2.62/glib-networking-2.62.3.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/glib-networking/2.62/glib-networking-2.62.3.tar.xz
# Download MD5 sum: a758ca62bd54982a798b39c744cbf783
# Download size: 180 KB
# Estimated disk space required: 4.4 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# GLib Networking Dependencies
# Required
# GLib-2.62.4, GnuTLS-3.6.11.1, and gsettings-desktop-schemas-3.34.0
# Recommended
# make-ca-1.5
# Optional
# libproxy
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
    -Dopenssl=enabled \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
