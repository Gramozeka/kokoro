
###############################################################################
set -e
###############################################################################
# The GNOME Bluetooth package contains tools for managing and manipulating Bluetooth devices using the GNOME Desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-bluetooth/3.34/gnome-bluetooth-3.34.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-bluetooth/3.34/gnome-bluetooth-3.34.0.tar.xz
# Download MD5 sum: 0c567e124a52e8ddc31c8bed0c3e57a1
# Download size: 344 KB
# Estimated disk space required: 14 MB
# Estimated build time: less than 0.1 SBU (Using parallelism=4)
# GNOME Bluetooth Dependencies
# Required
# GTK+-3.24.13, itstool-2.0.6, libcanberra-0.30, and libnotify-0.7.8
# Recommended
# gobject-introspection-1.62.0
# Optional
# GTK-Doc-1.32
# Runtime Dependencies
# BlueZ-5.52 and elogind-243.4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
