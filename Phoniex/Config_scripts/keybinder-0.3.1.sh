
###############################################################################
set -e
###############################################################################
# The keybinder package contains a utility library registering global X keyboard shortcuts.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/kupferlauncher/keybinder/releases/download/v0.3.1/keybinder-0.3.1.tar.gz
# Download MD5 sum: a6d7caae0dcb040b08692b008a68a507
# Download size: 384 KB
# Estimated disk space required: 2.8 MB
# Estimated build time: less than 0.1 SBU
# keybinder Dependencies
# Required
# GTK+-2.24.32
# Recommended
# gobject-introspection-1.62.0 and PyGTK-2.24.0
# Optional
# GTK-Doc-1.32 and Lua-5.3.5 (currently broken, because older lua version is required)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
 --disable-lua \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
