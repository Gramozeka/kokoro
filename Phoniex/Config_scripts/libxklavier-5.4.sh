
###############################################################################
set -e
# The libxklavier package contains a utility library for X keyboard.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://people.freedesktop.org/~svu/libxklavier-5.4.tar.bz2
# Download MD5 sum: 13af74dcb6011ecedf1e3ed122bd31fa
# Download size: 384 KB
# Estimated disk space required: 5.2 MB
# Estimated build time: less than 0.1 SBU
# libxklavier Dependencies
# Required
# GLib-2.62.4, ISO Codes-4.4, libxml2-2.9.10 and Xorg Libraries
# Recommended
# gobject-introspection-1.62.0
# Optional
# GTK-Doc-1.32 and Vala-0.46.5
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
