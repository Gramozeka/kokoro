
###############################################################################
set -e
# The libbytesize package is a library facilitates the common operations with sizes in bytes.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/storaged-project/libbytesize/releases/download/2.1/libbytesize-2.1.tar.gz
# Download MD5 sum: 145dd7e853f7b7cbe3bb264eff306a4b
# Download size: 432 KB
# Estimated disk space required: 5.7 MB
# Estimated build time: 0.1 SBU
# libbytesize Dependencies
# Required
# pcre2-10.34
# Optional
# GTK-Doc-1.32, six-1.14.0 (needed for tests and python bindings), pocketlint (python module for one test), and polib (python module for one test)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
