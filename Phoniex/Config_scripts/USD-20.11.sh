
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mv -v ${pkg}/usr/cmake ${pkg}/usr/lib64/
mv -v ${pkg}/usr/pxrConfig.cmake ${pkg}/usr/lib64/cmake/
mv -v ${pkg}/usr/lib/python/pxr/* ${pkg}/usr/lib64/python3.9/pxr/
rm -rf ${pkg}/usr/lib/python
mv -vf ${pkg}/usr/lib/usd/* ${pkg}/usr/lib64/usd/
rm -rf ${pkg}/usr/lib/usd
mv -vf ${pkg}/usr/lib/* ${pkg}/usr/lib64/
rm -rf ${pkg}/usr/lib
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's|set(libInstallPrefix "lib/python/pxr/${pyModuleName}")|set(libInstallPrefix "lib64/python3.9/pxr/${pyModuleName}")|' cmake/macros/Private.cmake
sed -i 's|libPythonPrefix lib/python|libPythonPrefix lib64/python3.9|' cmake/macros/Private.cmake
sed -i 's|_pxr_add_rpath(rpath "${CMAKE_INSTALL_PREFIX}/lib")|_pxr_add_rpath(rpath "${CMAKE_INSTALL_PREFIX}/lib64")|' cmake/macros/Private.cmake
sed -i 's|CMAKE_INSTALL_PREFIX}/lib|CMAKE_INSTALL_PREFIX}/lib64|g' cmake/macros/Public.cmake
sed -i 's|python;$ENV{PYTHONPATH}")|python3.9;$ENV{PYTHONPATH}")|' cmake/macros/Public.cmake
sed -i 's|PXR_INSTALL_SUBDIR}/lib|PXR_INSTALL_SUBDIR}/lib64|' cmake/macros/Private.cmake
sed -i 's|/lib/python/|/lib64/python3.9/|' cmake/macros/Public.cmake
sed -i 's|PXR_INSTALL_SUBDIR}/lib|PXR_INSTALL_SUBDIR}/lib64|g' cmake/macros/Public.cmake
sed -i 's|/lib/|/lib64/|g' cmake/macros/Public.cmake
sed -i 's|"tests/lib"|"tests/lib64"|g' cmake/macros/Public.cmake
sed -i 's|lib/usd|lib64/usd|g' cmake/macros/Public.cmake
sed -i 's|plugin/usd|share/usd/plugin/usd|g' cmake/macros/Public.cmake
# CMAKE_INSTALL_PREFIX}/lib")
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_CXX_STANDARD=11 \
             -DCMAKE_INSTALL_PREFIX=/usr \
             -DPYTHON_EXECUTABLE=/usr/bin/python3 \
             -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
             -DLIB_INSTALL_DIR=lib64 \
             -DCMAKE_BUILD_TYPE=Release         \
             -DBUILD_TESTING=FALSE \
             -DPXR_BUILD_PRMAN_PLUGIN=FALSE \
             -DPXR_BUILD_IMAGING=FALSE \
             -DPXR_ENABLE_HDF5_SUPPORT=FALSE \
             -DPXR_BUILD_EMBREE_PLUGIN=FALSE \
             -DPXR_BUILD_TESTS=FALSE \
             -DBoost_NO_BOOST_CMAKE=ON \
             -Wno-dev ..
make -j4 || make || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
