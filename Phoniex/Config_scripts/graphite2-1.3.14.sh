
###############################################################################
set -e
# Graphite2 is a rendering engine for graphite fonts. These are TrueType fonts with additional tables containing smart rendering information and were originally developed to support complex non-Roman writing systems. They may contain rules for e.g. ligatures, glyph substitution, kerning, justification - this can make them useful even on text written in Roman writing systems such as English. Note that firefox by default provides an internal copy of the graphite engine and cannot use a system version (although it can now be patched to use it), but it too should benefit from the availability of graphite fonts.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/silnrsi/graphite/releases/download/1.3.13/graphite2-1.3.13.tgz
# Download MD5 sum: 29616d4f9651706036ca25c111508272
# Download size: 6.4 MB
# Estimated disk space required: 27 MB (with tests add docs)
# Estimated build time: 0.2 SBU (with tests and docs)
# Graphite2 Dependencies
# Required
# CMake-3.16.3
# Optional
# FreeType-2.10.1, Python-2.7.17, and silgraphite to build the comparerender test and benchmarking tool, and if that is present, HarfBuzz-2.6.4 to add more functionality to it (this is a circular dependency, you would need to first build graphite2 without harfbuzz).
# To build the documentation: asciidoc-8.6.9, Doxygen-1.8.17, texlive-20190410 (or install-tl-unx), and dblatex (for PDF docs)
# To execute the test suite you will need FontTools (Python 3 module), otherwise, the "cmp" tests fail.
# Optional (at runtime)
# You will need at least one suitable graphite font for the package to be useful.
###############################################################################
pip install fonttools
pip3 install fonttools
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -p build
cd build
  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLIB_SUFFIX="64" \
    -DCMAKE_BUILD_TYPE=Release \
    -Wno-dev ..
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
