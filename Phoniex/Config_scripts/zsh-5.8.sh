
###############################################################################
set -e
###############################################################################
# The zsh package contains a command interpreter (shell) usable as an interactive login shell and as a shell script command processor. Of the standard shells, zsh most closely resembles ksh but includes many enhancements.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.zsh.org/pub/zsh-5.7.1.tar.xz
# Download MD5 sum: 374f9fdd121b5b90e07abfcad7df0627
# Download size: 3.0 MB
# Estimated disk space required: 68 MB (includes documentation and tests)
# Estimated build time: 1.5 SBU (Using parallelism=4; includes documentation and tests)
# Additional Downloads
# Optional Documentation: http://www.zsh.org/pub/zsh-5.7.1-doc.tar.xz
# Documentation MD5 sum: 08f2f78aae9d739db9e30365a228a6fb
# Documentation download size: 3.0 MB
# [Note] Note
# When there is a new zsh release, the old files shown above are moved to a new server directory: http://www.zsh.org/pub/old/.
# zsh Dependencies
# Optional
# libcap-2.31 with PAM, PCRE-8.43, and Valgrind-3.15.0,
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
            --bindir=/bin         \
            --sysconfdir=/etc/zsh \
            --enable-etcdir=/etc/zsh   \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cat >> /etc/shells << "EOF"
/bin/zsh
EOF
