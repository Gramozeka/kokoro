
###############################################################################
set -e
###############################################################################
groupadd -g 23 slocate &&
useradd -c "mlocate" -d /var/lib/mlocate -u 23 \
        -g slocate -s /bin/false slocate
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -p ${pkg}/etc
cat > ${pkg}/etc/updatedb.conf << "EOF"
PRUNE_BIND_MOUNTS = "yes"
PRUNEFS = "9p afs anon_inodefs auto autofs bdev binfmt_misc cgroup cifs coda configfs cpuset debugfs devpts ecryptfs exofs fuse fuse.sshfs fusectl gfs gfs2 gpfs hugetlbfs inotifyfs iso9660 jffs2 lustre mqueue ncpfs nfs nfs4 nfsd pipefs proc ramfs rootfs rpc_pipefs securityfs selinuxfs sfs sockfs sysfs tmpfs ubifs udf usbfs ceph fuse.ceph"
PRUNENAMES = ".git .hg .svn .bzr .arch-ids {arch} CVS"
PRUNEPATHS = "/afs /dev /media /mnt /net /proc /sys /tmp /usr/tmp /var/cache/ccache /var/lib/ceph /var/spool/cups /var/tmp"
EOF

chown root:root ${pkg}/etc/updatedb.conf
chmod 644 ${pkg}/etc/updatedb.conf

mv ${pkg}/usr/bin/locate ${pkg}/usr/bin/mlocate
 ln -sf mlocate ${pkg}/usr/bin/locate

 mkdir -p ${pkg}/usr/libexec
 cat > ${pkg}/usr/libexec/mlocate-run-updatedb << "EOF"
 #!/bin/sh

nodevs=$(< /proc/filesystems awk '$1 == "nodev" && $2 != "rootfs" && $2 != "zfs" { print $2 }')
/usr/bin/updatedb -f "$nodevs"
 
EOF

chown root:root ${pkg}/usr/libexec/mlocate-run-updatedb
chmod 755 ${pkg}/usr/libexec/mlocate-run-updatedb

mkdir -p ${pkg}/etc/cron.daily
 cat > ${pkg}/etc/cron.daily/mlocate << "EOF"
#!/bin/sh
ionice -c3 nice -n 19 /usr/libexec/mlocate-run-updatedb

EOF

chown root:root ${pkg}/etc/cron.daily/mlocate
chmod 755 ${pkg}/etc/cron.daily/mlocate

chown root:slocate ${pkg}/usr/bin/mlocate
chmod 2711 ${pkg}/usr/bin/mlocate
mkdir -p ${pkg}/var/lib/mlocate
chown root:slocate ${pkg}/var/lib/mlocate
chmod 750 ${pkg}/var/lib/mlocate

 
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4 groupname=slocate
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
