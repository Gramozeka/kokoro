
###############################################################################
set -e
# The PCRE2 package contains a new generation of the Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/pcre/pcre2-10.34.tar.bz2
# Download MD5 sum: d280b62ded13f9ccf2fac16ee5286366
# Download size: 1.6 MB
# Estimated disk space required: 18 MB (with tests)
# Estimated build time: 0.5 SBU (with tests)
# PCRE2 Dependencies
# Optional
# Valgrind-3.15.0 and libedit
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--docdir=/usr/share/doc/${packagedir} \
            --enable-unicode                    \
            --enable-jit                        \
            --enable-pcre2-16                   \
            --enable-pcre2-32                   \
            --enable-pcre2grep-libz             \
            --enable-pcre2grep-libbz2           \
            --enable-pcre2test-libreadline      \
            --disable-static                    &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
