
###############################################################################
set -e
# The Gtkmm package provides a C++ interface to GTK+ 2. It can be installed alongside Gtkmm-3.24.2 (the GTK+ 3 version) with no namespace conflicts.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtkmm/2.24/gtkmm-2.24.5.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gtkmm/2.24/gtkmm-2.24.5.tar.xz
# Download MD5 sum: 6c59ae8bbff48fad9132f23af347acf1
# Download size: 11 MB
# Estimated disk space required: 426 MB (with tests)
# Estimated build time: 3.4 SBU (with tests)
# Gtkmm Dependencies
# Required
# Atkmm-2.28.0, GTK+-2.24.32 and Pangomm-2.42.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/^libdocdir =/ s/$(book_name)/gtkmm-2.24.5/' \
    -i docs/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
