
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/#include <iostream>/a #include <atomic>' src/ui/tool/node.cpp
    mkdir build
    cd    build
# export MAGICK_CODER_MODULE_PATH="/usr/lib64/ImageMagick-6.9.10/modules-Q16HDRI/coders"
# export PKG_CONFIG_PATH="$PKG_CONFIG_PATH/magick-6:$PKG_CONFIG_PATH"
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/usr \
             -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
             -DCMAKE_BUILD_TYPE=Release         \
             -DWITH_DBUS=ON \
             -DWITH_IMAGE_MAGICK=ON \
             -Wno-dev ..
make -j4 || make || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
