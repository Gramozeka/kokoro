
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/xfce/${package}
cd ${packagedir}
sed -i '/G_BEGIN_DECLS/a #include <libxfce4panel/libxfce4panel.h>' \
          panel-plugin/notification-plugin.h
sed -e "s/panel_slice_new0/g_slice_new0/g" -i panel-plugin/notification-plugin.c
sed -e "s/panel_slice_free/g_slice_free/g" -i panel-plugin/notification-plugin.c
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
