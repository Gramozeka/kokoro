
###############################################################################
set -e
# OpenJPEG is an open-source implementation of the JPEG-2000 standard. OpenJPEG fully respects the JPEG-2000 specifications and can compress/decompress lossless 16-bit images.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/uclouvain/openjpeg/archive/v2.3.1/openjpeg-2.3.1.tar.gz
# Download MD5 sum: 3b9941dc7a52f0376694adb15a72903f
# Download size: 2.1 MB
# Estimated disk space required: 15 MB
# Estimated build time: 0.3 SBU
# OpenJPEG Dependencies
# Required
# CMake-3.16.3
# Optional
# Little CMS-2.9, libpng-1.6.37, LibTIFF-4.1.0, and Doxygen-1.8.17 (to build the API documentation)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
pushd ../doc &&
  for man in man/man?/* ; do
      install -v -D -m 644 $man ${pkg}/usr/share/$man
  done 
popd
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-n2*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/openjpeg2_remove-thirdparty.patch.gz | patch -p1 --verbose || exit 1
mkdir -v build &&
cd       build &&

JAVA_HOME=/opt/jdk \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DJAVA_SOURCE_VERSION=12 \
      -DJAVA_TARGET_VERSION=12 \
       -DOPENJPEG_INSTALL_LIB_DIR=lib64 \
        -DBUILD_JPIP:BOOL=ON \
    -DBUILD_JPWL:BOOL=ON \
    -DBUILD_LUTS_GENERATOR:BOOL=ON \
    -DBUILD_MJ2:BOOL=ON \
      -DBUILD_STATIC_LIBS=OFF .. &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
