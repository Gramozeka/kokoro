
###############################################################################
set -e
###############################################################################
# The gspell package provides a flexible API to add spell checking to a GTK+ application.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnome.org/pub/gnome/sources/gspell/1.8/gspell-1.8.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gspell/1.8/gspell-1.8.2.tar.xz
# Download MD5 sum: 4f857382bc9d8d4afe1e67e5b5b9dbff
# Download size: 416 KB
# Estimated disk space required: 12 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# gspell Dependencies
# Required
# enchant-2.2.7
# Optional
# gobject-introspection-1.62.0, GTK-Doc-1.32, ISO Codes-4.4, Vala-0.46.5, and Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
