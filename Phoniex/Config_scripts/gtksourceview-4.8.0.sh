
###############################################################################
set -e

###############################################################################
# The GtkSourceView package contains libraries used for extending the GTK+ text functions to include syntax highlighting.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtksourceview/4.4/gtksourceview-4.4.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gtksourceview/4.4/gtksourceview-4.4.0.tar.xz
# Download MD5 sum: 5c47e314517692f2622a0b724c65e628
# Download size: 1.1 MB
# Estimated disk space required: 36 MB (with tests)
# Estimated build time: 0.1 SBU (with tests; both using parallelism=4)
# GtkSourceView Dependencies
# Required
# GTK+-3.24.13
# Recommended
# gobject-introspection-1.62.0
# Optional
# Vala-0.46.5, Valgrind-3.15.0, GTK-Doc-1.32, itstool-2.0.6, fop-2.4 (or dblatex), and Glade
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
