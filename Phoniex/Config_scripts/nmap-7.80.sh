
###############################################################################
set -e
###############################################################################
# Nmap is a utility for network exploration and security auditing. It supports ping scanning, port scanning and TCP/IP fingerprinting.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://nmap.org/dist/nmap-7.80.tar.bz2
# Download MD5 sum: d37b75b06d1d40f27b76d60db420a1f5
# Download size: 10 MB
# Estimated disk space required: 119 MB (add 25 MB for tests)
# Estimated build time: 0.9 SBU (add 0.1 SBU for tests)
# Nmap Dependencies
# Recommended
# [Note] Note
# These packages are recommended because if they're not installed, the build process will compile and link against its own (often older) version.
# 
# libpcap-1.9.1, Lua-5.3.5, PCRE-8.43, and liblinear-230
# 
# Optional
# PyGTK-2.24.0 (required for zenmap), Python-2.7.17 (required for ndiff) and Subversion-1.13.0 (required for nmap-update), and libssh2-1.9.0'
###############################################################################
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}/usr
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-liblua=included \
--without-nmap-update \
--docdir=/usr/share/doc/${packagedir} &&
make -j4 &&
make install DESTDIR=${pkg} &&
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
