
###############################################################################
set -e
###############################################################################
# The PIN-Entry package contains a collection of simple PIN or pass-phrase entry dialogs which utilize the Assuan protocol as described by the Ägypten project. PIN-Entry programs are usually invoked by the gpg-agent daemon, but can be run from the command line as well. There are programs for various text-based and GUI environments, including interfaces designed for Ncurses (text-based), and for the common GTK and Qt toolkits.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/pinentry/pinentry-1.1.0.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/pinentry/pinentry-1.1.0.tar.bz2
# Download MD5 sum: 3829315cb0a1e9cedc05ffe6def7a2c6
# Download size: 460 KB
# Estimated disk space required: 11 MB
# Estimated build time: less than 0.1 SBU
# PIN-Entry Dependencies
# Required
# Libassuan-2.5.3 and libgpg-error-1.36
# Optional
# Emacs-26.3, FLTK-1.3.5, Gcr-3.34.0, GTK+-2.24.32, GTK+-3.24.13, libsecret-0.20.0, and Qt-5.14.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
case $1 in
 02)
dopopt="--enable-pinentry-gtk2=yes"
;;
gnome)
dopopt="--enable-pinentry-gtk2=yes --enable-pinentry-gnome3=yes"
;;
05)
dopopt="--enable-pinentry-gtk2=yes --enable-pinentry-gnome3=yes --enable-pinentry-qt=yes"
;;
esac
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
 --enable-pinentry-tty \
 $dopopt \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
