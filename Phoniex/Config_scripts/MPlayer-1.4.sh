
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
mkdir -pv ${pkg}/usr/share/pixmaps
mkdir -pv ${pkg}/etc/mplayer
ln -svf ../icons/hicolor/48x48/apps/mplayer.png \
        ${pkg}/usr/share/pixmaps/mplayer.png
mkdir -pv  ${pkg}/usr/share/doc/${packagedir} &&
install -v -m644    DOCS/HTML/en/* \
                    ${pkg}/usr/share/doc/${packagedir}
install -v -m644 etc/codecs.conf ${pkg}/etc/mplayer
install -v -m644 etc/*.conf ${pkg}/etc/mplayer
tar -xvf  ${SOURCE}/Clearlooks-2.0.tar.bz2 \
    -C    ${pkg}/usr/share/mplayer/skins &&
ln  -sfvn Clearlooks ${pkg}/usr/share/mplayer/skins/default
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unset CPPFLAGS CFLAGS LDFLAGS YASMFLAGS
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np0 -i $PATCHSOURCE/MPlayer-1.3.0-x264_fix-1.patch

./configure --prefix=/usr \
--libdir=/usr/lib64 \
            --confdir=/etc/mplayer   \
            --enable-dynamic-plugins \
            --enable-menu            \
            --enable-gui \
            --enable-mencoder \
            --enable-libdv \
            --enable-menu \
            --extra-cflags="-m64 -O2 -fPIC" \
            --extra-ldflags="-L/usr/lib64" \
--language-doc="ru" \
--language-man="ru" \
--language-msg="ru" \
--language="ru"

make -j4
make doc
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
gtk-update-icon-cache &&
update-desktop-database
