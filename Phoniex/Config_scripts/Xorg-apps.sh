
set -e
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
  case ${packagedir} in
      beforelight-[0-9]* )
  mv -vf app-defaults/Beforelight ${pkg}/etc/X11/app-defaults
        ;;
     esac
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
######################################################################################
while read -r line; do

    # Get the file name, ignoring comments and blank lines
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
    file=$(echo $line | cut -d" " -f2)

    temp_val=$(echo $file|sed 's|^.*/||')          # Remove directory
    packagedir=$(echo $temp_val|sed 's|\.tar.*||') # Package directory
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/Xorg/app/${file}
cd ${packagedir}

case ${packagedir} in
xdm-[0-9]* )
zcat ${PATCHSOURCE}/xdm.glibc.crypt.diff.gz | patch -p1 --verbose
 zcat ${PATCHSOURCE}/xdm-1.1.11-arc4random-include.patch.gz | patch -p1 --verbose
 zcat ${PATCHSOURCE}/xdm-1.1.11-setproctitle-include.patch.gz | patch -p1 --verbose
 zcat ${PATCHSOURCE}/xdm-consolekit.patch.gz | patch -p1 --verbose
 
  ;;
       luit-[0-9]* )
         sed -i -e "/D_XOPEN/s/5/6/" configure
       ;;
       xkeyboard-config-[0-9]* )
        CONF_INDIVIDUAL="--with-xkb-rules-symlink=xorg "
        ;;
     esac
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
--sysconfdir=/etc \
   --localstatedir=/var \
--disable-static ${CONF_INDIVIDUAL}
# \
# --build=${CLFS_TARGET} &&
make -j4

pack_local ${packagedir}
ldconfig
done < ${home}/${scriptsdir}/Xorg-app-7.sh

