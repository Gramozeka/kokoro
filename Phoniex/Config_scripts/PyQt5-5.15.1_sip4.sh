
set -e
###################
TEMP=${TEMPBUILD}
cd $TEMP
# rm -rf *
packagedir=$(sed -e "s/\_sip4*$//" <<< $(basename $line))
package="$packagedir.tar.*"
# unpack ${SOURCE}/${package}
pkg=${destdir}/${packagedir}
# mkdir -pv ${pkg}/usr
# cd ${packagedir}
# export NINJAJOBS=5
# # pip3 install toml
# python3 configure.py \
#   --confirm-license \
#   --verbose \
#   --qsci-api \
#   -q /usr/bin/qmake-qt5 \
#   || exit 1
# CFLAGS="-O2 -fPIC" \
# CXXFLAGS="-O2 -fPIC" \
# make -j5
# make DESTDIR=${pkg} INSTALL_ROOT=${pkg} install -j1
# 
# echo "###########################**** COMPLITE!!! ****##################################"
# rm -r ${pkg}/usr/lib64/python*/site-packages/PyQt5/uic/port_v2
# 
# find $pkg | xargs file | grep -e "executable" -e "shared object" \
#   | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
# 
# # Remove rpaths:
# for file in $(find $pkg | xargs file | grep -e "executable" -e "shared object" | grep ELF | cut -f 1 -d : 2> /dev/null) ; do
#   if [ ! "$(patchelf --print-rpath $file 2> /dev/null)" = "" ]; then
#     patchelf --remove-rpath $file
#   fi
# done
# 
#   python3 -m compileall -d / ${pkg}/usr/lib64
#   python3 -O -m compileall -d / ${pkg}/usr/lib64
#   python3 -m compileall -d / ${pkg}/usr/share
#   python3 -O -m compileall -d / ${pkg}/usr/share
cd ${pkg}
mkdir -p usr/tree-info
tree > usr/tree-info/$(basename $line)-tree
echo "###################!!!!!!!!!!!! COMPLITE! !!!!!!!!!!########################"
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$(basename $line).txz
installpkg ${destdir}/1-repo/$(basename $line).txz
rm -rf ${pkg}
cd $home

