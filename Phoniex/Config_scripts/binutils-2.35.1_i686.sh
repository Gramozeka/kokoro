
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
  find ${pkg}/i686/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_i686*$//" <<< $(basename $line))
package="${packagedir}.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -v ../binutils-build
cd ../binutils-build
USE_ARCH="32"
PATH=$PATH32:$PATH \
CPPFLAGS="-I/i686/include" \
CFLAGS=${BUILD32} \
CXXFLAGS=${BUILD32} \
CC="gcc ${BUILD32}" \
CXX="g++ ${BUILD32}" \
PKG_CONFIG_PATH="/i686/lib/pkgconfig" \
setarch i386 ../${packagedir}/configure \
    --prefix=/i686 \
    --enable-shared \
    --enable-gold=yes \
    --enable-plugins \
    --with-system-zlib \
    --enable-threads \
    --enable-64-bit-bfd \
     --enable-install-libiberty \
    --build=${CLFS_TARGET32}
setarch i386 make  -j4 tooldir=/i686
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
