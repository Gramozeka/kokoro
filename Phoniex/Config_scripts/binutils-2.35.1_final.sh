cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_final*$//" <<< $(basename $line))
package="${packagedir}.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
mkdir -v ../binutils-build
cd ../binutils-build
../${packagedir}/configure \
    --prefix=/usr \
    --enable-shared \
    --enable-64-bit-bfd \
    --libdir=/usr/lib64 \
    --enable-gold=yes \
    --enable-lto=yes \
    --enable-ld=default \
    --enable-plugins \
    --with-system-zlib \
    --enable-threads \
     --enable-install-libiberty \
    --build=${CLFS_TARGET}
make  -j4 tooldir=/usr
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
