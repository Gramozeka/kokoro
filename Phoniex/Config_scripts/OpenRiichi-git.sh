
###############################################################################
set -e
###############################################################################

###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} ninja install

mkdir -pv ${pkg}/usr/share/{applications,pixmaps}

cat >> ${pkg}/usr/share/applications/OpenRiichi.desktop << "EOF"
[Desktop Entry]
Type=Application
Version=0.2.1.1
Name=OpenRiichi
GenericName=Japanese Mahjong
GenericName[ru]=Японский Маджонг
Comment=OpenRiichi is an open source Japanese Mahjong
Exec=OpenRiichi --search-directory /usr/share/OpenRiichi
Icon=OpenRiichi
Categories=Game;BoardGame;
Keywords=Mahjong;board;strategy;
EOF
mkdir -pv ${pkg}/install
cat >> ${pkg}/install/doinst.sh << "EOF"
( /usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1 )
EOF

cp -v ${pkg}/usr/share/OpenRiichi/Data/Icon.png ${pkg}/usr/share/pixmaps/OpenRiichi.png

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
# git clone --recurse-submodules https://github.com/FluffyStuff/OpenRiichi.git $(basename $line)
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
update-desktop-database
glib-compile-schemas /usr/share/glib-2.0/schemas

