#!/bin/bash
###############################################################################
set -e
# The cURL package contains an utility and a library used for transferring files with URL syntax to any of the following protocols: FTP, FTPS, HTTP, HTTPS, SCP, SFTP, TFTP, TELNET, DICT, LDAP, LDAPS and FILE. Its ability to both download and upload files can be incorporated into other programs to support functions like streaming media.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://curl.haxx.se/download/curl-7.68.0.tar.xz
# Download MD5 sum: ce97d54a5f93607714ceb7f46a0a28f3
# Download size: 2.3 MB
# Estimated disk space required: 77 MB (add 15 MB for tests)
# Estimated build time: 0.4 SBU (add 16 SBU for tests)
# cURL Dependencies
# Recommended
# make-ca-1.5 (runtime)
# Optional
# Brotli-1.0.7, c-ares-1.15.0, GnuTLS-3.6.11.1, libidn2-2.3.0, libpsl-0.21.0, libssh2-1.9.0, MIT Kerberos V5-1.17.1, nghttp2-1.40.0, OpenLDAP-2.4.48, Samba-4.11.0, libmetalink, librtmp, and SPNEGO
# Optional if Running the Test Suite
# stunnel-5.56 (for the HTTPS and FTPS tests) and Valgrind-3.15.0 (this will slow the tests down and may cause failures.)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
if (pkg-config --exists libcares) 
then 
ARES="--enable-ares"
fi

if (pkg-config --exists krb5-gssapi) 
then 
GSSAPI="--with-gssapi"
fi
if (pkg-config --exists libssh2) 
then 
SSH="--with-libssh2"
fi
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
            --enable-threaded-resolver \
            --with-ca-path=/etc/ssl/certs \
            $GSSAPI $SSH $ARES \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
