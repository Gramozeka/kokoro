###############################################################################
set -e
# The volume_key package provides a library for manipulating storage volume encryption keys and storing them separately from volumes to handle forgotten passphrases.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/felixonmars/volume_key/archive/volume_key-0.3.12.tar.gz
# Download MD5 sum: d1c76f24e08ddd8c1787687d0af5a814
# Download size: 196 KB
# Estimated disk space required: 11 MB
# Estimated build time: 0.2 SBU
# volume_key Dependencies
# Required
# cryptsetup-2.0.6, GLib-2.62.4, GPGME-1.13.1, and NSS-3.49.2
# Recommended
# SWIG-4.0.1
# Optional
# Python-2.7.17
###############################################################################
VERSION=${VERSION:-3.57}
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/include/nss
mkdir -pv ${pkg}/usr/{bin,lib64}
mkdir -pv ${pkg}/usr/lib64/pkgconfig

cat > ${pkg}/usr/lib64/pkgconfig/nss.pc << "EOF" &&
prefix=/usr
exec_prefix=/usr
libdir=/usr/lib64
includedir=/usr/include/nss

Name: NSS
Description: Network Security Services
Version: 3.57
Requires: nspr >= 4.25 sqlite3
Libs: -L${libdir} -lnss3 -lsmime3 -lssl3 -lsoftokn3  -lnssutil3
Cflags: -I${includedir}
EOF
chmod -v 644  ${pkg}/usr/lib64/pkgconfig/nss.pc
  ln -s nspr.pc ${pkg}/usr/lib64/pkgconfig/mozilla-nspr.pc
  ln -s nss.pc ${pkg}/usr/lib64/pkgconfig/mozilla-nss.pc

sed -e "s,@prefix@,/usr,g" \
    -e "s,@MOD_MAJOR_VERSION@,$(printf $VERSION | cut -d. -f1),g" \
    -e "s,@MOD_MINOR_VERSION@,$(printf $VERSION | cut -d. -f2),g" \
    -e "s,@MOD_PATCH_VERSION@,$(printf $VERSION | cut -d. -f3),g" \
    pkg/pkg-config/nss-config.in > ${pkg}/usr/bin/nss-config
cd ../dist                                                          &&
chmod 755 ${pkg}/usr/bin/nss-config
install -v -m755 Release/lib/*.so              ${pkg}/usr/lib64             &&
install -v -m644 Release/lib/{*.chk,*.a} ${pkg}/usr/lib64             &&

install -v -m755 -d                           ${pkg}/usr/include/      &&
mv -vf {public,private}/*     ${pkg}/usr/include/     &&
chmod -v 644             ${pkg}/usr/include/*    &&

cp -var Release/bin/* \
${pkg}/usr/bin &&
ln -sfv ./pkcs11/p11-kit-trust.so ${pkg}/usr/lib64/libnssckbi.so

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
cd nss &&
export BUILD_OPT=1
export USE_SYSTEM_ZLIB=1
export ZLIB_LIBS=-lz
export NSDISTMODE="copy"
export NSS_ENABLE_WERROR=0
export NSPR_INCLUDE_DIR=/usr/include/nspr
export USE_64=1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./build.sh \
 --opt \
--disable-tests \
 --system-sqlite \
--with-nspr=/usr/include/nspr:/usr/lib64  \
--enable-legacy-db \
 --enable-libpkix
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
