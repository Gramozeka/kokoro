
###############################################################################
set -e
# The xmlto package is a front-end to a XSL toolchain. It chooses an appropriate stylesheet for the conversion you want and applies it using an external XSLT processor. It also performs any necessary post-processing.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://releases.pagure.org/xmlto/xmlto-0.0.28.tar.bz2
# Download MD5 sum: 93bab48d446c826399d130d959fe676f
# Download size: 128 KB
# Estimated disk space required: 1.5 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# xmlto Dependencies
# Required
# docbook-xml-4.5, docbook-xsl-1.79.2, and libxslt-1.1.34
# Optional (for DVI, PDF, and postscript backend post-processing)
# fop-2.4, dblatex, and PassiveTeX
# Optional (for text backend post-processing)
# One of Links-2.20.2, Lynx-2.8.9rel.1, W3m, or ELinks
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
