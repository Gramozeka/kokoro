
###############################################################################
set -e
# The libgudev package contains GObject bindings for libudev.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libgudev/233/libgudev-233.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libgudev/233/libgudev-233.tar.xz
# Download MD5 sum: d59a317a40aaa02a2226056c0bb4d3e1
# Download size: 268 KB
# Estimated disk space required: 3.1 MB
# Estimated build time: less than 0.1 SBU
# Required
# GLib-2.62.4
# Optional
# gobject-introspection-1.62.0 (for gir-data, needed for GNOME), GTK-Doc-1.32, and umockdev
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--disable-umockdev \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
