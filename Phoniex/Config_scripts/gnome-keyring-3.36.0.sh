
###############################################################################
set -e
###############################################################################
# The GNOME Keyring package contains a daemon that keeps passwords and other secrets for users.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-keyring/3.34/gnome-keyring-3.34.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-keyring/3.34/gnome-keyring-3.34.0.tar.xz
# Download MD5 sum: 7c8fd85e46ed4ba1add0288b2ead9aec
# Download size: 1.3 MB
# Estimated disk space required: 142 MB (add 1 MB for tests)
# Estimated build time: 0.3 SBU (Using parallelism=4; add 0.3 SBU for tests)
# GNOME Keyring Dependencies
# Required
# dbus-1.12.16 and Gcr-3.34.0
# Recommended
# Linux-PAM-1.3.1, libxslt-1.1.34, and OpenSSH-8.1p1
# Optional
# LCOV, libcap-ng, GnuPG-2.2.19, and Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -r 's:"(/desktop):"/org/gnome\1:' schema/*.xml &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--with-pam-dir=/lib64/security \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
