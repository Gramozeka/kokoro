
###############################################################################
set -e
###############################################################################
###############################################################################
# Rasqal is a C library that handles Resource Description Framework (RDF) query language syntaxes, query construction, and execution of queries returning results as bindings, boolean, RDF graphs/triples or syntaxes.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://download.librdf.org/source/rasqal-0.9.33.tar.gz
# Download MD5 sum: 1f5def51ca0026cd192958ef07228b52
# Download size: 1.6 MB
# Estimated disk space required: 22 MB (additional 4 MB for the tests)
# Estimated build time: 0.3 SBU (additional 0.7 SBU for the tests)
# Rasqal Dependencies
# Required
# Raptor-2.0.15
# Optional
# PCRE-8.43 and libgcrypt-1.8.5
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
