
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

install -v -d ${pkg}/usr/bin  &&
install -v -m755 bin/collateindex.pl ${pkg}/usr/bin                      &&
chown -R root:root . &&
install -v -d -m755 ${pkg}/usr/share/man/man1 &&
install -v -m644 bin/collateindex.pl.1 ${pkg}/usr/share/man/man1         &&
install -v -d -m755 ${pkg}/usr/share/sgml/docbook/dsssl-stylesheets-1.79 &&
cp -v -R * ${pkg}/usr/share/sgml/docbook/dsssl-stylesheets-1.79          &&

if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
tar -xf ${SOURCE}/docbook-dsssl-doc-1.79.tar.bz2 --strip-components=1
USE_ARCH="64" 
ARCH="noarch"

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
install-catalog --add /etc/sgml/dsssl-docbook-stylesheets.cat \
    /usr/share/sgml/docbook/dsssl-stylesheets-1.79/catalog         &&

install-catalog --add /etc/sgml/dsssl-docbook-stylesheets.cat \
    /usr/share/sgml/docbook/dsssl-stylesheets-1.79/common/catalog  &&

install-catalog --add /etc/sgml/sgml-docbook.cat              \
    /etc/sgml/dsssl-docbook-stylesheets.cat

