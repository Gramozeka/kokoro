
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
#  unpack ${SOURCE}/${package}
git clone https://gitlab.com/accounts-sso/accounts-qml-module ${packagedir}
cd ${packagedir}
#   sed -e 's|src \\|src|' -e '/tests/d' -i signon-ui.pro
#   # Fake user ID to bypass Google blacklist
# patch -Np1 -i ${PATCHSOURCE}/fake-user-agent.patch
CWD=$(pwd)
USE_ARCH="64" 
ARCH="x86_64"
LDFLAGS="-L/lib64:/usr/lib64" 
LD_LIBRARY_PATH="/lib64:/usr/lib64" 
export CFLAGS="${BUILD64}"
export CXXFLAGS="${BUILD64}"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
OPENSSL_LIBS="-L/usr/lib64 -lssl -lcrypto" \
qmake \
CMAKE_CONFIG_PATH=/usr/lib64/cmake \
LIBDIR=/usr/lib64 \
PREFIX=/usr   &&
make -j4
mkdir -pv $pkg
make install INSTALL_ROOT=${pkg}

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
