
###############################################################################
set -e
# FLTK (pronounced "fulltick") is a cross-platform C++ GUI toolkit. FLTK provides modern GUI functionality and supports 3D graphics via OpenGL and its built-in GLUT emulation libraries used for creating graphical user interfaces for applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://fltk.org/pub/fltk/1.3.5/fltk-1.3.5-source.tar.gz
# Download MD5 sum: e85017defd5a03ae82e634311db87bbf
# Download size: 5.1 MB
# Estimated disk space required: 117 MB (with documentation)
# Estimated build time: 0.2 SBU (Using parallelism=4)
# FLTK Dependencies
# Required
# Xorg Libraries
# Recommended
# hicolor-icon-theme-0.17, libjpeg-turbo-2.0.4, and libpng-1.6.37
# Optional
# alsa-lib-1.2.1.2, desktop-file-utils-0.24, Doxygen-1.8.17, GLU-9.0.1, Mesa-19.3.3, and texlive-20190410 (or install-tl-unx)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} docdir=/usr/share/doc/fltk-1.3.5  install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line)-source.tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -e '/cat./d' documentation/Makefile       &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--enable-shared  \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make -C documentation html
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
