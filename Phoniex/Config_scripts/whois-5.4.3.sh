
###############################################################################
set -e
###############################################################################
# Whois is a client-side application which queries the whois directory service for information pertaining to a particular domain name. This package will install two programs by default: whois and mkpasswd. The mkpasswd command is also installed by the Expect-5.45.4 package.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/rfc1036/whois/archive/v5.4.3/whois-5.4.3.tar.gz
# Download MD5 sum: 381dce8db7c6e38ef013b5d6527f494c
# Download size: 100 KB
# Estimated disk space required: 1.2 MB
# Estimated build time: less than 0.1 SBU
# Whois Dependencies
# Optional
# libidn-1.35 or libidn2-2.3.0
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# make DESTDIR=${pkg} install
make prefix=${pkg}/usr install-whois
make prefix=${pkg}/usr install-mkpasswd
make prefix=${pkg}/usr install-pos
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
