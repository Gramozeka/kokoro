
###############################################################################
set -e
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-64//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
if (pkg-config --exists libsasl2) 
then 
LDAP="--with-ldap"
fi

cd src &&
set -e
sed -e 's@\^u}@^u cols 300}@' \
    -i tests/dejagnu/config/default.exp &&

sed -e '/eq 0/{N;s/12 //}' \
    -i plugins/kdb/db2/libdb2/test/run.test &&

autoconf &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
            --sysconfdir=/etc        \
            --localstatedir=/var/lib \
            --with-system-et         \
            --with-system-ss         \
            --with-system-verto=no   \
            --enable-dns-for-realm $LDAP \
--build=${CLFS_TARGET} &&
make -j4

pkg=${destdir}/${packagedir}
make install  DESTDIR=${pkg} && echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
install -v -dm755 ${pkg}/usr/share/doc/krb5-1.18 &&
cp -vfr ../doc/*  ${pkg}/usr/share/doc/krb5-1.18
cd ${pkg}
mkdir -pv ${pkg}/{bin,lib64,etc}
for f in gssapi_krb5 gssrpc k5crypto kadm5clnt kadm5srv \
         kdb5 kdb_ldap krad krb5 krb5support verto ; do

    find ${pkg}/usr/lib64 -type f -name "lib64$f*.so*" -exec chmod -v 755 {} \;    
done          &&

mv -v ${pkg}/usr/lib64/lib64krb5.so.3*        ${pkg}/lib64 &&
mv -v ${pkg}/usr/lib64/lib64k5crypto.so.3*    ${pkg}/lib64 &&
mv -v ${pkg}/usr/lib64/lib64krb5support.so.0* ${pkg}/lib64 &&

ln -v -sf ../../lib64/lib64krb5.so.3.3        ${pkg}/usr/lib64/lib64krb5.so        &&
ln -v -sf ../../lib64/lib64k5crypto.so.3.1    ${pkg}/usr/lib64/lib64k5crypto.so    &&
ln -v -sf ../../lib64/lib64krb5support.so.0.1 ${pkg}/usr/lib64/lib64krb5support.so &&

mv -v ${pkg}/usr/bin/ksu ${pkg}/bin &&
chmod -v 755 ${pkg}/bin/ksu   &&

cat > ${pkg}/etc/krb5.conf << "EOF"
# Begin /etc/krb5.conf

[lib64defaults]
    default_realm = <EXAMPLE.ORG>
    encrypt = true

[realms]
    <EXAMPLE.ORG> = {
        kdc = <belgarath.example.org>
        admin_server = <belgarath.example.org>
        dict_file = /usr/share/dict/words
    }

[domain_realm]
    .<example.org> = <EXAMPLE.ORG>

[logging]
    kdc = SYSLOG:INFO:AUTH
    admin_server = SYSLOG:INFO:AUTH
    default = SYSLOG:DEBUG:DAEMON

# End /etc/krb5.conf
EOF
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
