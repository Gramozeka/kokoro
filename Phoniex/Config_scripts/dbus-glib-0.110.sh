
###############################################################################
set -e
# The D-Bus GLib package contains GLib interfaces to the D-Bus API.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://dbus.freedesktop.org/releases/dbus-glib/dbus-glib-0.110.tar.gz
# Download MD5 sum: d7cebf1d69445cbd28b4983392145192
# Download size: 820 KB
# Estimated disk space required: 10 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# D-Bus GLib Dependencies
# Required
# dbus-1.12.16 and GLib-2.62.4
# Optional
# GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
