
###############################################################################
set -e
###############################################################################
# Mercurial is a distributed source control management tool similar to Git and Bazaar. Mercurial is written in Python and is used by projects such as Mozilla and Vim.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.mercurial-scm.org/release/mercurial-5.2.2.tar.gz
# Download MD5 sum: f2251256063f20864fa9fed10ace76d5
# Download size: 7.0 MB
# Estimated disk space required: 75 MB (with docs, add 855 MB for tests)
# Estimated build time: 0.5 SBU (with docs, add 11 SBU for tests using -j4)
# Mercurial Dependencies
# Required
# Python-2.7.17
# Optional
# docutils-0.16 (required to build the documentation), git-2.25.0, GnuPG-2.2.19 (gpg2 with Python bindings), OpenSSH-8.1p1 (runtime, to access ssh://... repositories), Pygments-2.5.2, Subversion-1.13.0 (with Python bindings), Bazaar, CVS, pyflakes, and pyOpenSSL
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} PREFIX=/usr install-bin
make DESTDIR=${pkg} PREFIX=/usr install-doc
install -v -d -m755 ${pkg}/etc/mercurial &&
cat > ${pkg}/etc/mercurial/hgrc << "EOF"
[web]
cacerts = /etc/pki/tls/certs/ca-bundle.crt
EOF
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
pip2 install pyopenssl
pip3 install pyopenssl
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
export PYTHON=python3
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make build
2to3 -w doc/hgmanpage.py &&
PYTHON=python3 make doc
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
