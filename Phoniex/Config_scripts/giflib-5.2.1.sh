
###############################################################################
set -e
# The giflib package contains libraries for reading and writing GIFs as well as programs for converting and working with GIF files.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://sourceforge.net/projects/giflib/files/giflib-5.2.1.tar.gz
# Download (HTTP) MD5 sum: 6f03aee4ebe54ac2cc1ab3e4b0a049e5
# Download (HTTP) size: 436 KB
# Estimated disk space required: 3.2 MB (with documentation)
# Estimated build time: less than 0.1 SBU (with documentation)
# giflib Dependencies
# Required
# xmlto-0.0.28
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}
make PREFIX=/usr LIBDIR=/usr/lib64 MANDIR=/usr/share/man/man1 install DESTDIR=${pkg}
rm -vf ${pkg}/usr/lib64/*.a
rm -vf ${pkg}/usr/lib64/*.la
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
