
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
# package="$(basename $line).tar.*"
packagedir=$(basename $line)
git clone git://git.freedesktop.org/git/plymouth ${packagedir}
# unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./autogen.sh
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
            --localstatedir=/var \
--build=${CLFS_TARGET} \
 --with-system-root-install \
  --with-udev \
  --enable-drm \
    --enable-gdm-transition \
      --enable-pango  \
    --disable-systemd-integration \
  --enable-gtk
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
