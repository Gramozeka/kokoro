
###############################################################################
set -e
###############################################################################
# The GNOME Settings Daemon is responsible for setting various parameters of a GNOME Session and the applications that run under it.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-settings-daemon/3.34/gnome-settings-daemon-3.34.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-settings-daemon/3.34/gnome-settings-daemon-3.34.2.tar.xz
# Download MD5 sum: 493332fa0f36645188468fed41c0060b
# Download size: 1.3 MB
# Estimated disk space required: 29 MB
# Estimated build time: 0.1 SBU (Using parallelism=4)
# GNOME Settings Daemon Dependencies
# Required
# colord-1.4.4, Fontconfig-2.13.1, GeoClue-2.5.5, geocode-glib-3.26.1, gnome-desktop-3.34.3, Little CMS-2.9, libcanberra-0.30, libgweather-3.34.0, libnotify-0.7.8, librsvg-2.46.4, libwacom-0.29, PulseAudio-13.0, elogind-241.4, UPower-0.99.11, and Xorg Wacom Driver-0.39.0
# Recommended
# ALSA-1.2.1, Cups-2.3.1, NetworkManager-1.22.4, NSS-3.49.2, and Wayland-1.17.0
# [Note] Note
# Recommended dependencies are not strictly required for this package to build and function, but you may not get expected results at runtime if you don't install them.'
# Recommended (Runtime)
# blocaled-0.2
# Optional
# python-dbusmock and umockdev (required for the tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
