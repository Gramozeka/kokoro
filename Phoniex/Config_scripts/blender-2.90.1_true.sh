
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

BLENDERBINS="blender"
for bin in $BLENDERBINS ; do
  mv ${pkg}/usr/bin/$bin ${pkg}/usr/bin/$bin.bin
  cat <<EOF >${pkg}/usr/bin/$bin
#!/bin/sh
#export PYTHONPATH=/usr/share/blender/2.90.1/python/lib64/python3.9
export LD_LIBRARY_PATH=/usr/lib64/opencollada
exec $bin.bin "\$@"
EOF
  chmod 0755 ${pkg}/usr/bin/$bin
done

mkdir -p ${pkg}/usr/share/man/man1
python3 ../doc/manpage/blender.1.py bin/blender ${pkg}/usr/share/man/man1/blender.1
  python3 -m compileall ${pkg}/usr/share/blender
  python3 -O -m compileall ${pkg}/usr/share/blender
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
    mkdir build
    cd    build
export CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:/usr/include/python3.9/"
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/usr \
             -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
    -DWITH_INSTALL_PORTABLE:BOOL=OFF \
    -DWITH_BUILDINFO:BOOL=ON \
    -DWITH_OPENCOLLADA:BOOL=ON \
    -DWITH_OPENCOLORIO:BOOL=ON \
    -DPYTHON_VERSION=3.9 \
    -DWITH_PYTHON_INSTALL=OFF \
    -DWITH_PYTHON_MODULE=ON \
    -DWITH_CODEC_FFMPEG:BOOL=ON \
    -DWITH_OPENAL:BOOL=ON \
    -DWITH_JACK:BOOL=ON \
    -DWITH_JACK_DYNLOAD:BOOL=ON \
    -DPYTHON_LIBPATH:PATH=/usr/lib64 \
    -DWITH_CODEC_SNDFILE:BOOL=ON \
    -DWITH_FFTW3:BOOL=ON \
    -DWITH_IMAGE_OPENJPEG:BOOL=ON \
    -DWITH_SYSTEM_LZO:BOOL=ON \
    -DWITH_MEM_JEMALLOC:BOOL=ON \
    -DWITH_MEM_VALGRIND:BOOL=ON \
    -DWITH_MOD_OCEANSIM:BOOL=ON \
    -DWITH_SDL:BOOL=ON \
    -DWITH_SDL_DYNLOAD:BOOL=ON \
    -DCMAKE_BUILD_TYPE=Release \
    -DWITH_CYCLES_CUDA_BINARIES=OFF \
    -DUSD_LIBRARY:FILEPATH=/usr/lib64 \
    -DEMBREE_LIBRARY:FILEPATH=/usr/lib64 \
    -Wno-dev ..
make -j4 || make || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

#     

