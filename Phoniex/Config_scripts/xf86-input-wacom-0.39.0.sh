
###############################################################################
set -e
# The Xorg Wacom Driver package contains the X.Org X11 driver and SDK for Wacom and Wacom-like tablets. It is not required to use a Wacom tablet, the xf86-input-evdev driver can handle these devices without problems.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/linuxwacom/xf86-input-wacom/releases/download/xf86-input-wacom-0.39.0/xf86-input-wacom-0.39.0.tar.bz2
# Download MD5 sum: 9ee7bf6969002d6cfe9301354a72d7b0
# Download size: 608 KB
# Estimated disk space required: 12 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# Xorg Wacom Drivers Dependencies
# Required
# Xorg-Server-1.20.7
# Optional
# Doxygen-1.8.17 and Graphviz-2.42.3
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-systemd-unit-dir=no \
            --with-udev-rules-dir=/lib/udev/rules.d \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
