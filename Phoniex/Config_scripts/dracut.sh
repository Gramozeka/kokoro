###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
git clone https://github.com/dracutdevs/dracut.git ${packagedir}
# unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np0 -i ${PATCHSOURCE}/fix-bash-5.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--bindir=/bin \
--sbindir=/sbin \
--sysconfdir=/etc \
            --localstatedir=/var \
--build=${CLFS_TARGET} &&

make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
