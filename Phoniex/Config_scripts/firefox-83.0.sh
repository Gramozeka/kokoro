
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line)b10.source.tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# sed -i "s|57|58|" security/nss/TAG-INFO
# patch -Np1 -i ${PATCHSOURCE}/ff-83.0.2.diff
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}
patch -Np1 -i ${PATCHSOURCE}/0001-Use-remoting-name-for-GDK-application-names.patch
cat > mozconfig << "EOF"
# ac_add_options --with-mozilla-api-keyfile=$PWD/mozilla-key
ac_add_options --with-google-location-service-api-keyfile=$PWD/google-key
ac_add_options --with-system-libevent
ac_add_options --with-system-webp
ac_add_options --with-system-nspr
ac_add_options --with-system-nss
ac_add_options --with-system-icu
ac_add_options --enable-official-branding
ac_add_options --disable-debug-symbols
ac_add_options --disable-elf-hack
ac_add_options --prefix=/usr
ac_add_options --libdir=/usr/lib64
ac_add_options --enable-application=browser
ac_add_options --disable-crashreporter
ac_add_options --disable-updater
ac_add_options --disable-tests
ac_add_options --enable-optimize
ac_add_options --enable-system-ffi
ac_add_options --enable-system-pixman
ac_add_options --with-system-jpeg
ac_add_options --with-system-png
ac_add_options --with-system-zlib
unset MOZ_TELEMETRY_REPORTING

mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/firefox-build-dir
EOF
echo "AIzaSyDxKL42zsPjbke5O8_rPVpVrLrJ8aeE9rQ" > $PWD/google-key
echo "613364a7-9418-4c86-bcee-57e32fd70c23" > $PWD/mozilla-key
export MACH_USE_SYSTEM_PYTHON=1
export "CC=clang --target=${CLFS_TARGET}"
export "CXX=clang++ --target=${CLFS_TARGET}"
export AR=llvm-ar
export NM=llvm-nm
export RANLIB=llvm-ranlib
export MOZBUILD_STATE_PATH=${PWD}/mozbuild &&
export SHELL=/bin/bash &&
./mach build
./mach buildsymbols
DESTDIR=${pkg} ./mach install                                                  &&

mkdir -pv  ${pkg}/usr/lib64/mozilla/plugins                             &&
ln    -sfv ../../mozilla/plugins ${pkg}/usr/lib64/firefox/browser/

unset CC CXX MOZBUILD_STATE_PATH

mkdir -pv ${pkg}/usr/share/applications &&
mkdir -pv ${pkg}/usr/share/pixmaps &&

cat > ${pkg}/usr/share/applications/firefox.desktop << "EOF" &&
[Desktop Entry]
Encoding=UTF-8
Name=Firefox Web Browser
Comment=Browse the World Wide Web
GenericName=Web Browser
Exec=firefox %u
Terminal=false
Type=Application
Icon=firefox
Categories=GNOME;GTK;Network;WebBrowser;
MimeType=application/xhtml+xml;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
EOF

ln -sfv /usr/lib64/firefox/browser/chrome/icons/default/default128.png \
        ${pkg}/usr/share/pixmaps/firefox.png
        
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
echo "###########################**** COMPLITE!!! ****##################################"
