
###############################################################################
set -e
# The Doxygen package contains a documentation system for C++, C, Java, Objective-C, Corba IDL and to some
# extent PHP, C# and D. It is useful for generating HTML documentation and/or an off-line reference manual
# from a set of documented source files. There is also support for generating output in RTF, PostScript,
# hyperlinked PDF, compressed HTML, and Unix man pages. The documentation is extracted directly from the
# sources, which makes it much easier to keep the documentation consistent with the source code.
# You can also configure Doxygen to extract the code structure from undocumented source files. This is very
# useful to quickly find your way in large source distributions. Used along with Graphviz, you can also visualize
# the relations between the various elements by means of include dependency graphs, inheritance diagrams,
# and collaboration diagrams, which are all generated automatically.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://doxygen.nl/files/doxygen-1.8.17.src.tar.gz
# Download MD5 sum: 7997a15c73a8bd6d003eaba5c2ee2b47
# Download size: 4.9 MB
# Estimated disk space required: 159 MB (with tests)
# Estimated build time: 0.9 SBU (using parallelism=4; with tests)
# Doxygen Dependencies
# Required
# CMake-3.16.3 and git-2.25.0
# Optional
# Graphviz-2.42.3, ghostscript-9.50, libxml2-2.9.10 (required for the tests), LLVM-9.0.1 (with clang), Python-2.7.17, Qt-5.14.0 (for doxywizard), texlive-20190410 (or install-tl-unx), and xapian-1.4.14 (for
# doxyindexer)
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/usr/share/man/man1
install -vm644 ../doc/*.1 /usr/share/man/man1
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-Qt*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -v build &&
cd       build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -G "Unix Makefiles"         \
      -DCMAKE_BUILD_TYPE=Release  \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -Dbuild_wizard=ON \
      -Dbuild_search=ON \
      -Duse_libclang=ON \
      -Wno-dev .. &&
make -j4
# cmake -DDOC_INSTALL_DIR=share/doc/ ${packagedir} -Dbuild_doc=ON .. 
# make docs
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
dot -c
