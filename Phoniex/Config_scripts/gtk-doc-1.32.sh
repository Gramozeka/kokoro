#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GTK-Doc package contains a code documenter. This is useful for extracting specially formatted comments from the code to create API documentation. This package is optional; if it is not installed, packages will not build the documentation. This does not mean that you will not have any documentation. If GTK-Doc is not available, the install process will copy any pre-built documentation to your system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtk-doc/1.32/gtk-doc-1.32.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gtk-doc/1.32/gtk-doc-1.32.tar.xz
# Download MD5 sum: 07764836262e154e94922e5f2aa476ae
# Download size: 748 KB
# Estimated disk space required: 14 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# GTK-Doc Dependencies
# Required
# docbook-xml-4.5, docbook-xsl-1.79.2, itstool-2.0.6, and libxslt-1.1.34
# Recommended
# Pygments-2.5.2 at run time
# Optional
# For tests: dblatex or fop-2.4 (XML PDF support), GLib-2.62.4, Which-2.21, and Python modules lxml-4.4.2, anytree, and parameterized
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/gtk-doc-1.32-glib2_fixes-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
