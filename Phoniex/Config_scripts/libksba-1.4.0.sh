
###############################################################################
set -e
###############################################################################
# The Libksba package contains a library used to make X.509 certificates as well as making the CMS (Cryptographic Message Syntax) easily accessible by other applications. Both specifications are building blocks of S/MIME and TLS. The library does not rely on another cryptographic library but provides hooks for easy integration with Libgcrypt.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/libksba/libksba-1.3.5.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/libksba/libksba-1.3.5.tar.bz2
# Download MD5 sum: 8302a3e263a7c630aa7dea7d341f07a2
# Download size: 608 KB
# Estimated disk space required: 9.2 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# Libksba Dependencies
# Required
# libgpg-error-1.36
# Optional
# Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
