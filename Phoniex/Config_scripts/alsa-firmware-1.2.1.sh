
###############################################################################
set -e
###############################################################################
# The ALSA Firmware package contains firmware for certain sound cards.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.alsa-project.org/files/pub/firmware/alsa-firmware-1.2.1.tar.bz2
# Download (FTP): ftp://ftp.alsa-project.org/pub/firmware/alsa-firmware-1.2.1.tar.bz2
# Download MD5 sum: f8458efd25e6d6600dbc7aedf98f83a3
# Download size: 4.9 MB
# Estimated disk space required: 38 MB
# Estimated build time: less than 0.1 SBU
# ALSA Firmware Dependencies
# Required
# alsa-tools-1.1.7
# Optional
# AS31 (for rebuilding the firmware from source)
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
