
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install INSTALLDIRS=vendor DESTDIR=${pkg}
mkdir -p ${pkg}/usr
mkdir -p ${pkg}/var/lib/net-snmp/{cert,mib}_indexes
mkdir -p ${pkg}/etc/snmp
zcat ${SOURCE}/snmpd.conf.gz > ${pkg}/etc/snmp/snmpd.conf
mkdir -p ${pkg}/etc/rc.d
cat ${SOURCE}/rc.snmpd > ${pkg}/etc/rc.d/rc.snmpd

( cd $pkg ; find . -name perllocal.pod -exec rm "{}" \+ )
find $pkg -name .packlist | while read plist ; do
  sed -e "s%$pkg%%g" \
      -re "s%\.([1-9]n?|3pm)$%&.gz%g   # extend man filenames for .gz" \
      ${plist} > ${plist}.new
      mv -f ${plist}.new ${plist}
done

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \+ -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \+

zcat ${PATCHSOURCE}/net-snmp-5.8-multilib.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/net-snmp-5.7.2-cert-path.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/net-snmp-5.8-Remove-U64-typedef.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/net-snmp-5.7.3-iterator-fix.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/net-snmp-5.8-modern-rpm-api.patch.gz | patch -p1 --verbose || exit 1
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
  --enable-ipv6 \
  --disable-debugging \
  --enable-static=no \
  --with-libwrap \
  --with-mysql \
  --with-perl-modules \
  --with-default-snmp-version=3 \
  --with-sys-contact="root@example.org" \
  --with-sys-location="unknown" \
  --with-logfile="/var/log/snmpd.log" \
  --with-persistent-directory="/var/lib/net-snmp" \
  --without-rpm \
  --program-suffix= \
  --program-prefix= \
  --with-mib-modules="ucd-snmp/lmsensorsMib ucd-snmp/diskio" \
  --with-python-modules \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4 INSTALLDIRS=vendor 
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
