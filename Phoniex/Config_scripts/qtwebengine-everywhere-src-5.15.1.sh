
set -e
###################
TEMP=${TEMPBUILD}
cd $TEMP
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
pkg=${destdir}/${packagedir}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/qtwebengine-everywhere-src-5.15.1-ICU68-1.patch
export NINJAJOBS=5
find -type f -name "*.pr[io]" |
  xargs sed -i -e 's|INCLUDEPATH += |&$$QTWEBENGINE_ROOT/include |'
sed -e '/link_pulseaudio/s/false/true/' \
    -i src/3rdparty/chromium/media/media_options.gni
sed -i 's/NINJAJOBS/NINJA_JOBS/' src/core/gn_run.pro
if [ -d BUILDQT ]; then
rm -rfv BUILDQT
mkdir -v BUILDQT
cd BUILDQT
else
mkdir -v BUILDQT
cd BUILDQT
fi
CWD=$(pwd)
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
OPENSSL_LIBS="-L/usr/lib64 -lssl -lcrypto" \
qmake .. -- -webengine-proprietary-codecs \
-system-webengine-ffmpeg \
-proprietary-codecs -system-ffmpeg -webengine-icu
NINJAJOBS=5 make
make INSTALL_ROOT=${pkg} install

echo "###########################**** COMPLITE!!! ****##################################"
find ${pkg}/ -name \*.prl \
   -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;
cd ${pkg}
mkdir -p usr/tree-info
tree > usr/tree-info/${packagedir}-tree
echo "###################!!!!!!!!!!!! COMPLITE! !!!!!!!!!!########################"
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home

