
###############################################################################
set -e
###############################################################################
# GNU Clisp is a Common Lisp implementation which includes an interpreter, compiler, debugger, and many extensions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/clisp/latest/clisp-2.49.tar.bz2
# Download (FTP): ftp://ftp.gnu.org/gnu/clisp/latest/clisp-2.49.tar.bz2
# Download MD5 sum: 1962b99d5e530390ec3829236d168649
# Download size: 7.8 MB
# Estimated disk space required: 163 MB (add 8 MB for tests)
# Estimated build time: 0.9 SBU (1.2 SBU with tests)
# Additional Downloads
# Optional patch: http://www.linuxfromscratch.org/patches/blfs/svn/clisp-2.49-readline7_fixes-1.patch (required if building against libffcall)
# Clisp Dependencies
# Recommended
# libsigsegv-2.12
# Optional
# libffcall
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -e '/socket/d' -e '/"streams"/d' tests/tests.lisp
patch -Np1 -i ${PATCHSOURCE}/clisp-2.49-readline7_fixes-1.patch
mkdir build &&
cd    build &&
SHELL=/bin/bash \
CFLAGS="${BUILD64}" \
../configure --srcdir=../ \
--prefix=/usr \
--libdir=/usr/lib64 \
--with-libsigsegv-prefix=/usr \
--with-libffcall-prefix=/usr \
--docdir=/usr/share/doc/${packagedir}
ulimit -s 16384 &&
make -j1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
