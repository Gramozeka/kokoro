
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -f -i
sed -e "s/mutter-clutter-5/mutter-clutter-6/g" -i configure
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-systemduserunitdir=no \
--build=${CLFS_TARGET}
make
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas

