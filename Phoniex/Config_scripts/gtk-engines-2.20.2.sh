
###############################################################################
set -e
###############################################################################
# The GTK Engines package contains eight themes/engines and two additional engines for GTK2.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtk-engines/2.20/gtk-engines-2.20.2.tar.bz2
# Download (FTP): http://ftp.gnome.org/pub/gnome/sources/gtk-engines/2.20/gtk-engines-2.20.2.tar.bz2
# Download MD5 sum: 5deb287bc6075dc21812130604c7dc4f
# Download size: 676 KB
# Estimated disk space required: 19 MB
# Estimated build time: 0.4 SBU
# GTK Engines Dependencies
# Required
# GTK+-2.24.32
# Optional
# Lua-5.3.5 and Which-2.21 (required for test suite)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
