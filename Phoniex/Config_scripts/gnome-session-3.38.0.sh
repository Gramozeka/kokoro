
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/session.diff
# sed 's@/bin/sh@/bin/sh -l@' -i gnome-session/gnome-session.in
sed 's@/bin/sh@/bin/sh -l@' -i gnome-session/gnome-session.in
sed -i "/  systemd_dep/,+3d;/if enable_systemd/a \    systemd_userunitdir = '/tmp\'" meson.build
mkdir meson-build
cd meson-build
CFLAGS="${BUILD64}" \
CXXFLAGS="${BUILD64}" \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dsystemd_journal=false \
  ..
  ninja -j5
pack ${packagedir}_fix
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas
