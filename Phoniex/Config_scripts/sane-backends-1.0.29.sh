
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
mkdir -pv ${pkg}/etc/udev/rules.d
install -m 644 -v tools/udev/libsane.rules           \
                  ${pkg}/etc/udev/rules.d/65-scanner.rules &&
chgrp -v scanner  ${pkg}/var/lock/sane
cat >> ${pkg}/etc/sane.d/net.conf << "EOF"
connect_timeout = 60
<server_ip>
EOF
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
  if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
if ! grep "scanner" /etc/group
then
groupadd -g 70 scanner
fi
###############################################################################
cd $TEMPBUILD
rm -rf *
package="sane-backends-1.0.29.tar.gz"
packagedir=sane-backends-1.0.29

unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
            --localstatedir=/var \
            --with-group=scanner \
            --with-docdir=/usr/share/doc/${packagedir} \
--build=${CLFS_TARGET} &&
make -j4
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
###############################################################################
###############################################################################
pack_local2 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
mkdir -pv ${pkg}/usr/share/sane
install -v -m644 doc/sane.png xscanimage-icon-48x48-2.png \
    ${pkg}/usr/share/sane
mkdir -pv ${pkg}/usr/lib64/gimp/2.0/plug-ins
ln -v -s ../../../../bin/xscanimage ${pkg}/usr/lib64/gimp/2.0/plug-ins

mkdir -pv ${pkg}/usr/share/{applications,pixmaps}               &&

cat > ${pkg}/usr/share/applications/xscanimage.desktop << "EOF" &&
[Desktop Entry]
Encoding=UTF-8
Name=XScanImage - Scanning
Comment=Acquire images from a scanner
Exec=xscanimage
Icon=xscanimage
Terminal=false
Type=Application
Categories=Application;Graphics
EOF

ln -svf ../sane/xscanimage-icon-48x48-2.png ${pkg}/usr/share/pixmaps/xscanimage.png

find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
  if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="sane-frontends-1.0.14.tar.gz"
packagedir="sane-frontends-1.0.14"

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -e "/SANE_CAP_ALWAYS_SETTABLE/d" src/gtkglue.c
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
            --localstatedir=/var \
            --with-group=scanner \
            --with-docdir=/usr/share/doc/${packagedir} \
--build=${CLFS_TARGET} &&
make -j4
pack_local2 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cat >> /etc/sane.d/net.conf << "EOF"
connect_timeout = 60
<server_ip>
EOF
