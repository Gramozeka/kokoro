
###############################################################################
set -e
# The Simple DirectMedia Layer Version 2 (SDL2 for short) is a cross-platform library designed to make it easy to write multimedia software, such as games and emulators.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.libsdl.org/release/${packagedir}.tar.gz
# Download MD5 sum: 5a2114f2a6f348bdab5bf52b994811db
# Download size: 5.3 MB
# Estimated disk space required: 169 MB (with docs)
# Estimated build time: 0.4 SBU (using parallelism=4; with docs)
# Additional Downloads
# Required patch (for i686 systems): http://www.linuxfromscratch.org/patches/blfs/svn/${packagedir}-opengl_include_fix-1.patch
# SDL2 Dependencies
# Optional
# ALSA-1.2.1, Doxygen-1.8.17 (to create documentation), ibus-1.5.21, NASM-2.14.02, PulseAudio-13.0, X Window System, DirectFB, and fcitx
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
pushd docs  &&
  doxygen   &&
popd
make DESTDIR=${pkg} install
rm -v ${pkg}/usr/lib64/libSDL2*.a
install -v -m755 -d        ${pkg}/usr/share/doc/${packagedir}/html &&
cp -Rv  docs/output/html/* ${pkg}/usr/share/doc/${packagedir}/html
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
