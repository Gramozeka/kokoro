
cd $TEMPBUILD
# rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
# unpack ${SOURCE}/${package}
# git clone https://github.com/qt/qtwebkit.git ${packagedir}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/QTWebkit_ICU_68.1.diff
# mkdir -pv /usr/src/qtwebkit.git
# rsync -aAXv --exclude=".*" ./ /usr/src/qtwebkit.git
CWD=$(pwd)
 mkdir BUILDQT
cd BUILDQT
CWD=$(pwd)
export NINJAJOBS=5
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
OPENSSL_LIBS="-L/usr/lib64 -lssl -lcrypto" \
qmake ../WebKit.pro   &&
make -j5 || make -j1 || exit 1
mkdir -pv $pkg
make install INSTALL_ROOT=${pkg}
find ${pkg}/ -name \*.prl \
   -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
