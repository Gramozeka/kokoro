
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

install -m755 -d ${pkg}/usr/{bin,lib64,include/gsm,share/{man/man{1,3}}}

  make -j1 INSTALL_ROOT=${pkg}/usr \
  GSM_INSTALL_LIB=${pkg}/usr/lib64 \
    GSM_INSTALL_INC=${pkg}/usr/include/gsm \
    GSM_INSTALL_MAN=${pkg}/usr/share/man/man3 \
    TOAST_INSTALL_MAN=${pkg}/usr/share/man/man1 \
    install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np0 -i ${PATCHSOURCE}/gsm-shared.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make CCFLAGS="-c ${CFLAGS} -fPIC"
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
