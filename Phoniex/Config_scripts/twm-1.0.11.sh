
###############################################################################
set -e
###############################################################################
# The twm package contains a very minimal window manager.
# This package is not a part of the Xorg katamari and is provided only as a dependency to other packages or for testing the completed Xorg installation.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.x.org/pub/individual/app/twm-1.0.10.tar.bz2
# Download (FTP): ftp://ftp.x.org/pub/individual/app/twm-1.0.10.tar.bz2
# Download MD5 sum: e322c08eeb635f924ede5b8eba3db54e
# Download size: 284 KB
# Estimated disk space required: 3.7 MB
# Estimated build time: less than 0.1 SBU
# twm Dependencies
# Required
# Xorg-Server-1.20.7
# Recommended
# Xorg Legacy Fonts
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -e '/^rcdir =/s,^\(rcdir = \).*,\1/etc/X11/app-defaults,' src/Makefile.in &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
