
###############################################################################
set -e
# This package, from the WebM project, provides the reference implementations of the VP8 Codec, used in most current html5 video, and of the next-generation VP9 Codec.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/webmproject/libvpx/archive/v1.8.2/libvpx-1.8.2.tar.gz
# Download MD5 sum: 6dbccca688886c66a216d7e445525bce
# Download size: 5.1 MB
# Estimated disk space required: 56 MB
# Estimated build time: 0.5 SBU (using parallelism=4)
# libvpx Dependencies
# Required
# yasm-1.3.0 or NASM-2.14.02, and Which-2.21 (so configure can find yasm)
# Optional
# Doxygen-1.8.17 and PHP-7.4.2 (to build the documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/cp -p/cp/' build/make/Makefile &&

mkdir libvpx-build            &&
cd    libvpx-build            &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../configure --prefix=/usr \
--libdir=/usr/lib64 \
                 --disable-debug-libs \
    --disable-debug \
    --enable-vp8 \
    --enable-postproc \
    --enable-vp9 \
    --enable-vp9-postproc \
    --enable-vp9-highbitdepth \
    --enable-shared \
    --disable-static \
             --target=x86_64-linux-gcc --enable-pic 
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
