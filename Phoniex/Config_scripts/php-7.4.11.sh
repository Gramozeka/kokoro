#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -m755 -d ${pkg}/etc
install -v -m644 php.ini-production ${pkg}/etc/php.ini &&

install -v -m755 -d ${pkg}/usr/share/doc/${packagedir} &&
install -v -m644    CODING_STANDARDS* EXTENSIONS NEWS README* UPGRADING* \
                    ${pkg}/usr/share/doc/${packagedir}

    
tar -xvf ${SOURCE}/php_manual_ru.tar.gz \
    -C ${pkg}/usr/share/doc/${packagedir} --no-same-owner

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
zcat ${PATCHSOURCE}/php.ini-development.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/php.ini-development.diff.gz | patch -p1 --verbose php.ini-production || exit 1
zcat ${PATCHSOURCE}/php-fpm.conf.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/php.imap.api.diff.gz | patch -p1 --verbose || exit 1
EXTENSION_DIR=/usr/lib64/php/extensions \
CFLAGS=$CFLAGS \
CXXFLAGS="$CXXFLAGS -DU_USING_ICU_NAMESPACE=1" \
LIBS="-L/usr/lib64  -liconv -lcharset -lc-client" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
            --datadir=/usr/share/php     \
            --mandir=/usr/share/man      \
            --without-pear               \
            --enable-fpm                 \
            --with-fpm-user=apache       \
            --with-fpm-group=apache      \
            --with-config-file-path=/etc \
  --with-apxs2=/usr/bin/apxs \
  --enable-fpm \
  --with-fpm-user=apache \
  --with-fpm-group=apache \
  --enable-maintainer-zts \
  --enable-pcntl \
  --enable-mbregex \
  --enable-tokenizer=shared \
  --with-config-file-scan-dir=/etc/php.d \
  --with-config-file-path=/etc \
  --with-libdir=lib64 \
  --with-layout=PHP \
  --disable-sigchild \
  --with-libxml \
  --with-xmlrpc=shared \
  --with-expat \
  --enable-simplexml \
  --enable-xmlreader=shared \
  --enable-dom=shared \
  --enable-filter \
  --disable-debug \
  --with-openssl=shared \
  --with-kerberos \
  --with-external-pcre \
  --with-zlib=shared,/usr \
  --enable-bcmath=shared \
  --with-bz2=shared,/usr \
  --enable-calendar=shared \
  --enable-ctype=shared \
  --with-curl=shared \
  --enable-dba=shared \
  --with-gdbm=/usr \
  --with-db5=/usr \
  --enable-exif=shared \
  --enable-ftp=shared \
  --enable-gd=shared \
  --with-external-gd \
  --with-jpeg \
  --with-xpm \
  --with-gettext=shared,/usr \
  --with-gmp=shared,/usr \
  --with-iconv=shared \
  --with-imap-ssl=/usr \
  --with-imap \
  --with-ldap=shared \
  --enable-mbstring=shared \
  --enable-mysqlnd=shared \
  --with-mysqli=shared,mysqlnd \
  --with-mysql-sock=/run/mysqld/mysqld.sock \
  --with-iodbc=shared,/usr \
  --enable-pdo=shared \
  --with-pdo-mysql=shared,mysqlnd \
  --with-pdo-sqlite=shared,/usr \
  --with-pdo-odbc=shared,iODBC,/usr \
  --with-pspell=shared,/usr \
  --with-enchant=shared,/usr \
  --enable-shmop=shared \
  --with-snmp=shared,/usr \
  --enable-soap=shared \
  --enable-sockets \
  --with-sqlite3=shared \
  --enable-sysvmsg \
  --enable-sysvsem \
  --enable-sysvshm \
  --with-xsl=shared,/usr \
  --with-zip=shared \
  --with-tsrm-pthreads \
  --enable-intl=shared \
  --enable-opcache \
  --enable-shared=yes \
  --enable-static=no \
  --with-gnu-ld \
  --with-pic \
  --enable-phpdbg \
  --with-sodium \
  --with-password-argon2 \
  --without-readline \
  --with-libedit \
  --with-pear \
  --with-tidy=shared \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# wget http://pear.php.net/go-pear.phar
# php ./go-pear.phar

