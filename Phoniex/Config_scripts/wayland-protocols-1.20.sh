
###############################################################################
set -e
###############################################################################
# The Wayland-Protocols package contains additional Wayland protocols that add functionality outside of protocols already in the Wayland core.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://wayland.freedesktop.org/releases/wayland-protocols-1.18.tar.xz
# Download MD5 sum: af38f22d8e233c2f2e00ddc8dcc94694
# Download size: 108 KB
# Estimated disk space required: 1.2 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# Wayland-protocols Dependencies
# Required
# Wayland-1.17.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
