
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
USE_ARCH="64" 
ARCH="x86_64"
# LDFLAGS="-L/lib64:/usr/lib64" \
# LD_LIBRARY_PATH="/lib64:/usr/lib64"
# export CFLAGS="${BUILD64}"
# export CXXFLAGS="${BUILD64}"
# export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
EFI32_FLAGS=" --with-platform=efi --target=i386 --program-prefix= " \
EFI_FLAGS=" --with-platform=efi --target=x86_64 --program-prefix= " \
CFLAGS="-O2" \
./configure --prefix=/usr          \
            --sbindir=/sbin        \
            --sysconfdir=/etc      \
            --disable-efiemu       \
            --disable-werror       \
            --enable-grub-mkfont   \
            --enable-grub-themes   \
            --enable-grub-mount

make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
