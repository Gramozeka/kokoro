
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/bin
cp snowball ${pkg}/bin
cp stemwords ${pkg}/bin
mkdir -pv ${pkg}/usr/include
mkdir -pv ${pkg}/usr/lib64
 cp -var include/libstemmer.h ${pkg}/usr/include/libstemmer.h
  chmod 644 ${pkg}/usr/include/libstemmer.h
cp -var libstemmer.so.0.0.0 ${pkg}/usr/lib64/libstemmer.so.0.0.0
  ln -s libstemmer.so.0.0.0 ${pkg}/usr/lib64/libstemmer.so.0
  ln -s libstemmer.so.0 ${pkg}/usr/lib64/libstemmer.so
chmod 755 ${pkg}/usr/lib64/libstemmer.so.0.0.0

echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/dinamiclib.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
