
###############################################################################
set -e
# The GStreamer Bad Plug-ins package contains a set of plug-ins that aren't up to par compared to the rest. They might be close to being good quality, but they're missing something - be it a good code review, some documentation, a set of tests, a real live maintainer, or some actual wide use.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-1.18.0.tar.xz
# Download MD5 sum: ccc7404230afddec723bbdb63c89feec
# Download size: 4.7 MB
# Estimated disk space required: 110 MB (with tests)
# Estimated build time: 0.9 SBU (Using parallelism=4; with tests)
# GStreamer Bad Plug-ins Dependencies
# Required
# gst-plugins-base-1.18.0
# Recommended
# libdvdread-6.0.2, libdvdnav-6.0.1, and SoundTouch-2.1.0
# Optional
# BlueZ-5.52, Clutter-1.26.2, cURL-7.68.0, FAAC-1.29.9.2, FAAD2-2.8.8, fdk-aac-2.0.1, GnuTLS-3.6.11.1, GTK-Doc-1.32, GTK+-2.24.32 or GTK+-3.24.13, Little CMS-2.9, libass-0.14.0, libexif-0.6.21, libgcrypt-1.8.5, libgudev-233, libmpeg2-0.5.1, libssh2-1.9.0, libusb-1.0.23, libvdpau-1.3, libwebp-1.1.0, neon-0.30.2, Nettle-3.5.1, opencv-4.2.0 (with additional modules), OpenJPEG-2.3.1, Opus-1.3.1, Qt-5.14.1 (for examples), SDL-1.2.15, Valgrind-3.15.0, Wayland-1.17.0 (GTK+-3.24.13 must have been compiled with wayland support), x265-3.2.1, Xorg Libraries, bs2b, Chromaprint, daala, Flite, Game Music Emu, GSM, LADSPA, libmimic, libmms, libofa, MJPEG Tools, OpenAL, Orc, VO AAC, VO AMRWB, and ZBAR
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
         -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
