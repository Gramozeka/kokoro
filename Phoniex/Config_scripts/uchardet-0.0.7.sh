
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
	unpack ${SOURCE}/${package}
	cd ${packagedir}
	mkdir build
	cd    build
	CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
	cmake    \
	-DCMAKE_C_FLAGS="$BUILD64"               \
	-DCMAKE_CXX_FLAGS="$BUILD64"          \
	-DCMAKE_INSTALL_PREFIX=/usr                \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib64    \
	-DLIB_INSTALL_DIR=/usr/lib64                   \
	-DBUILD_SHARED_LIBS=ON                         \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX="64"                                           \
	-DBUILD_TESTING=OFF                                  \
	-Wno-dev ..
	make -j4 || make || exit 1

	pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


















