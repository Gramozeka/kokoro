
###############################################################################
set -e
###############################################################################
# The gptfdisk package is a set of programs for creation and maintenance of GUID Partition Table (GPT) disk drives. A GPT partitioned disk is required for drives greater than 2 TB and is a modern replacement for legacy PC-BIOS partitioned disk drives that use a Master Boot Record (MBR). The main program, gdisk, has an inteface similar to the classic fdisk program.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/gptfdisk/gptfdisk-1.0.4.tar.gz
# Download MD5 sum: 5ecc3c44913bb6b53d3708d1ac7ac295
# Download size: 200 KB
# Estimated disk space required: 2.3 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Recommended patch: http://www.linuxfromscratch.org/patches/blfs/svn/gptfdisk-1.0.4-convenience-1.patch
# gptfdisk Dependencies
# Required
# popt-1.16
# Optional
# ICU-65.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/gptfdisk-1.0.5-convenience-1.patch
sed -i 's|ncursesw/||' gptcurses.cc &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
