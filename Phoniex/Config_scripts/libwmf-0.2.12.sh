
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
#patch -Np1 -i ${PATCHSOURCE}/mypaint-brushes-1.3.0-automake_1.16-1.patch &&
#./autogen.sh
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
