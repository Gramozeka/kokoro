
###############################################################################
set -e
# The GStreamer Ugly Plug-ins is a set of plug-ins considered by the GStreamer developers to have good quality and correct functionality, but distributing them might pose problems. The license on either the plug-ins or the supporting libraries might not be how the GStreamer developers would like. The code might be widely known to present patent problems.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gst-plugins-ugly/gst-plugins-ugly-1.18.0.tar.xz
# Download MD5 sum: 10283ff5ef1e34d462dde77042e329bd
# Download size: 876 KB
# Estimated disk space required: 11 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# GStreamer Ugly Plug-ins Dependencies
# Required
# gst-plugins-base-1.18.0
# Recommended
# liba52-0.7.4 (needed to play DVD's), libdvdread-6.0.2, and x264-20190815-2245'
# Optional
# GTK-Doc-1.32, libmpeg2-0.5.1, libcdio-2.1.0 (for CD-ROM drive access), Valgrind-3.15.0, libsidplay, OpenCore AMR, Orc, and TwoLame
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
         -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
