
###############################################################################
set -e
# PulseAudio is a sound system for POSIX OSes, meaning that it is a proxy for sound applications. It allows you to do advanced operations on your sound data as it passes between your application and your hardware. Things like transferring the audio to a different machine, changing the sample format or channel count and mixing several sounds into one are easily achieved using a sound server.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/pulseaudio/releases/pulseaudio-13.0.tar.xz
# Download MD5 sum: e41d606f90254ed45c90520faf83d95c
# Download size: 1.8 MB
# Estimated disk space required: 93 MB (add 2 MB for tests)
# Estimated build time: 0.5 SBU (Using parallelism=4; add 0.6 SBU for tests)
# PulseAudio Dependencies
# Required
# libsndfile-1.0.28
# Recommended
# alsa-lib-1.2.1.2, dbus-1.12.16, elogind-241.4, GLib-2.62.4, libcap-2.31 with PAM, Speex-1.2.0 and Xorg Libraries
# Optional
# Avahi-0.7, BlueZ-5.52, fftw-3.3.8, GConf-3.2.6, GTK+-3.24.13, libsamplerate-0.1.9, SBC-1.4 (Bluetooth support), Valgrind-3.15.0, JACK, libasyncns, LIRC, ORC, soxr, TDB, WebRTC
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
upgradepkg --reinstall --install-new ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_full*$//" <<< $(basename $line))
package="$packagedir.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/020_no-parallel-make.diff.gz | patch -p0 --verbose || exit 1
zcat ${PATCHSOURCE}/030_posix-completion.diff.gz | patch -p0 --verbose || exit 1

# https://bugzilla.redhat.com/show_bug.cgi?id=1234710
zcat ${PATCHSOURCE}/0001-client-conf-Add-allow-autospawn-for-root.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/0002-allow-autospawn-for-root-default.diff.gz | patch -p1 --verbose || exit 1

# Do not log a warning every time root uses PulseAudio:
zcat ${PATCHSOURCE}/0003-no-root-warn.diff.gz | patch -p1 --verbose || exit 1

sed -i -e '/@PA_BINARY@/ imkdir -p \$HOME/.config/pulse' src/daemon/start-pulseaudio-x11.in
echo "X-MATE-Autostart-Phase=Initialization" >> src/daemon/pulseaudio.desktop.in

# If autospawn isn't working, start-pulseaudio-x11 should start it manually:
zcat ${PATCHSOURCE}/pulseaudio-autostart.patch.gz | patch -p1 --verbose || exit 1

# Disable "flat volumes" to work more like ALSA's mixer.  POLA.
# Have a google for "flat volumes" for more rationale on us not
# using the upstream default here.
sed -i 's/; flat-volumes = yes/flat-volumes = no/g' ./src/daemon/daemon.conf.in
# patch -Np1 -i ${PATCHSOURCE}/
autoreconf -vfi
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
            --disable-rpath              \
            --enable-samplerate          \
            --with-fftw                  \
            --with-speex                 \
            --with-soxr                  \
            --with-systemduserunitdir=no \
  --with-system-user="audio" \
  --with-system-group="pulse" \
  --with-access-group="pulse" \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
