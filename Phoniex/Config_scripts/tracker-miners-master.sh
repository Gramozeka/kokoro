
###############################################################################
set -e
###############################################################################
# The Tracker-miners package contains a set of data extractors for Tracker.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/tracker-miners/2.3/tracker-miners-2.3.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/tracker-miners/2.3/tracker-miners-2.3.1.tar.xz
# Download MD5 sum: 0321d8f4ad27e3fccdbc8384e5b3cb83
# Download size: 3.0 MB
# Estimated disk space required: 30 MB (with tests)
# Estimated build time: 0.4 SBU (with tests)
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/tracker-miners-2.3.1-upstream_fixes-1.patch
# Tracker-miners Dependencies
# Required
# gst-plugins-base-1.16.2, Tracker-2.3.1, Exempi-2.5.1, and gexiv2-0.12.0
# Recommended
# FFmpeg-4.2.2, FLAC-1.3.3, giflib-5.2.1, ICU-65.1, libexif-0.6.21, libgrss-0.7.0, libgxps-0.3.1, and Poppler-0.84.0
# Optional
# CMake-3.16.3, DConf-0.34.0, libgsf-1.14.46, libseccomp-2.4.2, taglib-1.11.1, totem-pl-parser-3.26.4, UPower-0.99.11, libcue, libosinfo, and gupnp
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/tracker-miners-2.3.1-upstream_fixes-1.patch
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dsystemd_user_services=false \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
