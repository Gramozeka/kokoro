
###############################################################################
pack_local () {
pkg=${destdir}/$1
make install-strip DESTDIR=${pkg} && echo "$1 ----- $(date)" >> ${destdir}/loginstall

# we use $VIRTGROUP as our virtualization group, fix auth permissions, and
# consider the fact that by default we got no certs
sed -i \
  -e "s|^\#unix_sock_group\ =\ \"libvirt\"|unix_sock_group = \"$VIRTGROUP\"|" \
  -e "s|^\#unix_sock_rw_perms\ =\ \"0770\"|unix_sock_rw_perms = \"0770\"|" \
  -e "s|^\#auth_unix_ro.*|auth_unix_ro = \"none\"|" \
  -e "s|^\#auth_unix_rw.*|auth_unix_rw = \"none\"|" \
  -e "s|^\#listen_tls|listen_tls|" \
  ${pkg}/etc/libvirt/libvirtd.conf

# still, we use $VIRTGROUP as our virtualization group
sed -i \
  -e "s|^\#group\ =\ \"root\"|group = \"$VIRTGROUP\"|" \
  ${pkg}/etc/libvirt/qemu.conf

# disable seccomp support or else VMs won't start with new libvirt/qemu combo
sed -i  "s|^\#seccomp_sandbox = 1|seccomp_sandbox = 0|" \
  ${pkg}/etc/libvirt/qemu.conf


find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
VIRTGROUP="kvm"
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "s|(prefix)/lib/sysctl|(sysconfdir)/sysctl|" src/Makefile.in
patch -p1 < $PATCHSOURCE/use-virtgroup-in-polkit-rules.diff
sed -i -e "s,@VIRTGROUP@,$VIRTGROUP,g" src/remote/libvirtd.rules
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
            --localstatedir=/var \
  --enable-static=no \
  --enable-shared=yes \
  --with-yajl \
  --with-qemu-group=$VIRTGROUP
make -j4
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
