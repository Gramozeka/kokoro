
###############################################################################
set -e
# The ALSA Plugins package contains plugins for various audio libraries and sound servers.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.alsa-project.org/files/pub/plugins/alsa-plugins-1.2.1.tar.bz2
# Download (FTP): ftp://ftp.alsa-project.org/pub/plugins/alsa-plugins-1.2.1.tar.bz2
# Download MD5 sum: 5b11cd3ec92e5f9190ec378565b529e8
# Download size: 400 KB
# Estimated disk space required: 4.8 MB
# Estimated build time: less than 0.1 SBU
# ALSA Plugins Dependencies
# Required
# alsa-lib-1.2.1.2
# Optional
# FFmpeg-4.2.2, libsamplerate-0.1.9, PulseAudio-13.0, Speex-1.2.0, JACK, and maemo
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
