
set -e
###################
TEMP=${TEMPBUILD}
cd $TEMP
 rm -rf *

packagedir=qt-everywhere-src-5.15.1
pkg=${destdir}/${packagedir}
srcdir=${packagedir}
mkdir -v ${packagedir}
package="qt-everywhere-src-5.15.1.tar.xz"
unpack ${SOURCE}/${package}
# git clone https://code.qt.io/qt/qt5.git ${packagedir}
cd ${srcdir}
#  git checkout 5.15.1
# git submodule update --init --recursive
# ./init-repository --module-subset=all

sed -i 's/python /python3 /' qtdeclarative/qtdeclarative.pro \
                              qtdeclarative/src/3rdparty/masm/masm.pri &&
if [ -d BUILDQT ]; then
rm -rfv BUILDQT
mkdir -v BUILDQT
cd BUILDQT
else
mkdir -v BUILDQT
cd BUILDQT
fi
CWD=$(pwd)
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
ICU_PREFIX=/usr \
ICU__INCDIR=/usr/include/unicode \
ICU_LIBS="-L/usr/lib64 -licui18n -licuuc -licudata"   \
OPENSSL_LIBS="-L/usr/lib64 -lssl -lcrypto"            \
../configure                                          \
-prefix                   /usr                        \
-bindir                   /usr/bin                    \
-libdir                   /usr/lib64                  \
-archdatadir              /usr/lib64/qt5              \
-plugindir                /usr/lib64/qt5/plugins      \
-importdir                /usr/lib64/qt5/imports      \
-headerdir                /usr/include/qt5            \
-datadir                  /usr/share/qt5              \
-docdir                   /usr/share/doc/qt5          \
-translationdir           /usr/share/qt5/translations \
-examplesdir              /usr/share/doc/qt5/examples \
-sysconfdir               /etc/xdg                    \
-force-pkg-config                                     \
-ccache                                               \
            -confirm-license                          \
            -opensource                               \
            -dbus-linked                              \
            -openssl-linked                           \
            -system-harfbuzz                          \
            -system-sqlite                            \
            -no-rpath                                 \
            -skip qtwebengine                                      
make -j5 || make -j1 || exit 1
make INSTALL_ROOT=${pkg} install
echo "###########################**** COMPLITE!!! ****##################################"
find ${pkg}/ -name \*.prl \
   -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;
# 
# cd ${pkg}/usr/bin
# for file in moc uic rcc qmake lconvert lrelease lupdate; do
#   ln -sfrvn $file $file-qt5
# done
rm -v ${pkg}/usr/lib64/*.la
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree
find ${pkg} | xargs file | grep -e "executable" -e "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
# rm -rf ${pkg}
# cd $home

