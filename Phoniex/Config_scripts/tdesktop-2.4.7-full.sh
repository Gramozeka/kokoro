
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} ninja install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
# git clone --recursive https://github.com/telegramdesktop/tdesktop.git ${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}/cmake
    patch -R -Np1 -i ${PATCHSOURCE}/Add_external_jpeg.patch
    patch -R -Np1 -i ${PATCHSOURCE}/Update-webrtc-packaged-build-for-tg_owt.patch
    patch -R -Np1 -i ${PATCHSOURCE}/Use-tg_owt-webrtc-fork.patch
    sed 's|set(webrtc_build_loc ${webrtc_loc}/out/$<CONFIG>/obj)|set(webrtc_build_loc /usr/lib)|' -i external/webrtc/CMakeLists.txt
# patch -Np1 -i ${PATCHSOURCE}/Use-tg_owt-webrtc-fork.patch
cd ..
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake \
        -G Ninja \
        -DCMAKE_INSTALL_PREFIX="/usr" \
        -DCMAKE_BUILD_TYPE=Release \
-DTDESKTOP_API_ID=2127679 \
-DTDESKTOP_API_HASH=8c97b9b4a0c70d2b2c258a9e24d332b8 \
-DDESKTOP_APP_USE_PACKAGED=OFF \
        -DTDESKTOP_DISABLE_REGISTER_CUSTOM_SCHEME=ON \
        -DTDESKTOP_LAUNCHER_BASENAME="telegram" \
        -DDESKTOP_APP_SPECIAL_TARGET="" \
        -DDESKTOP_APP_WEBRTC_LOCATION=/usr/include/libwebrtc \
-DDESKTOP_APP_DISABLE_CRASH_REPORTS=OFF ..
ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
