
###############################################################################
set -e
###############################################################################
# The libdaemon package is a lightweight C library that eases the writing of UNIX daemons.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://0pointer.de/lennart/projects/libdaemon/libdaemon-0.14.tar.gz
# Download MD5 sum: 509dc27107c21bcd9fbf2f95f5669563
# Download size: 332 KB
# Estimated disk space required: 3 MB
# Estimated build time: 0.1 SBU
# libdaemon Dependencies
# Optional
# Doxygen-1.8.17 and Lynx-2.8.9rel.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
