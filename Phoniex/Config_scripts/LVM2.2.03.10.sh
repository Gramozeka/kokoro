
###############################################################################
set -e
###############################################################################
# The LVM2 package is a set of tools that manage logical partitions. It allows spanning of file systems across multiple physical disks and disk partitions and provides for dynamic growing or shrinking of logical partitions, mirroring and low storage footprint snapshots.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://sourceware.org/ftp/lvm2/LVM2.2.03.07.tgz
# Download (FTP): ftp://sourceware.org/pub/lvm2/LVM2.2.03.07.tgz
# Download MD5 sum: 5c06a01e1adacb96761188d6a8a17a2e
# Download size: 2.4 MB
# Estimated disk space required: 34 MB (add at least 594 MB for tests in the /tmp directory; additionally transient files can grow up to around 500 MB)
# Estimated build time: 0.2 SBU (using parallelism=4; add 15 SBU for tests)
# LVM2 Dependencies
# Required
# libaio-0.3.112
# Optional
# mdadm-4.1, reiserfsprogs-3.6.27, Valgrind-3.15.0, Which-2.21, xfsprogs-5.4.0 (all five may be used, but are not required, for tests), and thin-provisioning-tools
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/create-dm-run-dir.diff.gz | patch -p1 --verbose || exit 1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/lib64 \
  --enable-cmdlib \
  --enable-dmeventd \
  --with-usrlibdir=/usr/lib64 \
  --enable-realtime \
  --enable-pkgconfig \
  --enable-udev_sync \
  --enable-udev_rules \
  --with-udev-prefix="" \
  --with-device-uid=0 \
  --with-device-gid=6 \
  --with-device-mode=0660 \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
