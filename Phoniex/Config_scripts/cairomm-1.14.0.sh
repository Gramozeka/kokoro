
###############################################################################
set -e
# The Cairomm package provides a C++ interface to Cairo.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.cairographics.org/releases/cairomm-1.12.2.tar.gz
# Download MD5 sum: 9d2282ea34cf9aaa89208bb4bb911909
# Download size: 1.3 MB
# Estimated disk space required: 11 MB
# Estimated build time: 0.1 SBU
# Cairomm Dependencies
# Required
# Cairo-1.17.2+f93fc72c03e and libsigc++-2.10.2
# Optional
# Boost-1.72.0 and Doxygen-1.8.17
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
      -Dbuild-tests=true  \
      -Dboost-shared=true \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
