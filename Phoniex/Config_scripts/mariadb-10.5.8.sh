
###############################################################################
set -e
###############################################################################
groupadd -g 40 mysql &&
useradd -c "MySQL Server" -d /srv/mysql -g mysql -s /bin/false -u 40 mysql
###############################################################################
# MariaDB is a community-developed fork and a drop-in replacement for the MySQL relational database management system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.mariadb.org/interstitial/mariadb-10.4.11/source/mariadb-10.4.11.tar.gz
# Download (FTP): ftp://mirrors.fe.up.pt/pub/mariadb/mariadb-10.4.11/source/mariadb-10.4.11.tar.gz
# Download MD5 sum: d0de881ab8ead46928cafb7d558535c1
# Download size: 75 MB
# Estimated disk space required: 1.5 GB
# Estimated build time: 13 SBU (with parallelism=4, add 0.4 SBU for tests)
# [Note] Note
# The installed size of MariaDB is 473 MB, but this can be reduced by about 200 MB, if desired, by removing the /usr/share/mysql/test directory after installation.
# MariaDB Dependencies
# Required
# CMake-3.16.3
# Recommended
# libevent-2.1.11
# Optional
# Boost-1.72.0, libaio-0.3.112, libxml2-2.9.10, Linux-PAM-1.3.1, MIT Kerberos V5-1.17.1, PCRE-8.43, Ruby-2.7.0, unixODBC-2.3.7, Valgrind-3.15.0, Groonga, KyTea, Judy, lz4, MeCab, MessagePack, mruby, Sphinx, TokuDB, and ZeroMQ
# For security reasons, running the server as an unprivileged user and group is strongly encouraged. Issue the following (as root) to create the user and group:
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
cd ..
mkdir -pv ${pkg}/usr/man/man1
install -m755 debian/additions/innotop/innotop ${pkg}/usr/bin/
install -m644 debian/additions/innotop/innotop.1 ${pkg}/usr/man/man1/
install -m755 debian/additions/mariadb-report ${pkg}/usr/bin/
install -m644 debian/additions/mariadb-report.1 ${pkg}/usr/man/man1/
install -v -dm 755 ${pkg}/etc/mysql &&
cat > ${pkg}/etc/mysql/my.cnf << "EOF"
# Begin /etc/mysql/my.cnf

# The following options will be passed to all MySQL clients
[client]
#password       = your_password
port            = 3306
socket          = /run/mysqld/mysqld.sock

# The MySQL server
[mysqld]
port            = 3306
socket          = /run/mysqld/mysqld.sock
datadir         = /srv/mysql
skip-external-locking
key_buffer_size = 16M
max_allowed_packet = 1M
sort_buffer_size = 512K
net_buffer_length = 16K
myisam_sort_buffer_size = 8M

# Don't listen on a TCP/IP port at all.
skip-networking

# required unique id between 1 and 2^32 - 1
server-id       = 1

# Uncomment the following if you are using BDB tables
#bdb_cache_size = 4M
#bdb_max_lock = 10000

# InnoDB tables are now used by default
innodb_data_home_dir = /srv/mysql
innodb_log_group_home_dir = /srv/mysql
# All the innodb_xxx values below are the default ones:
innodb_data_file_path = ibdata1:12M:autoextend
# You can set .._buffer_pool_size up to 50 - 80 %
# of RAM but beware of setting memory usage too high
innodb_buffer_pool_size = 128M
innodb_log_file_size = 48M
innodb_log_buffer_size = 16M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50

[mysqldump]
quick
max_allowed_packet = 16M

[mysql]
no-auto-rehash
# Remove the next comment character if you are not familiar with SQL
#safe-updates

[isamchk]
key_buffer = 20M
sort_buffer_size = 20M
read_buffer = 2M
write_buffer = 2M

[myisamchk]
key_buffer_size = 20M
sort_buffer_size = 20M
read_buffer = 2M
write_buffer = 2M

[mysqlhotcopy]
interactive-timeout

# End /etc/mysql/my.cnf
EOF
cp -vr debian/additions/mariadb.conf.d ${pkg}/etc/mysql/
cp -v debian/additions/mariadb.cnf ${pkg}/etc/mysql/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# sed -i "s@data/test@\${INSTALL_MYSQLTESTDIR}@g" sql/CMakeLists.txt &&
mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_BUILD_TYPE=Release                      \
      -DCMAKE_INSTALL_PREFIX=/usr                     \
      -DINSTALL_DOCDIR=share/doc/${packagedir}       \
      -DINSTALL_DOCREADMEDIR=share/doc/${packagedir} \
      -DINSTALL_MANDIR=share/man                      \
      -DINSTALL_MYSQLSHAREDIR=share/mysql             \
      -DINSTALL_MYSQLTESTDIR=share/mysql/test         \
      -DINSTALL_PLUGINDIR=lib64/mysql/plugin            \
       -DINSTALL_LIBDIR=lib64            \
      -DINSTALL_SBINDIR=sbin                          \
      -DINSTALL_SCRIPTDIR=bin                         \
      -DINSTALL_SQLBENCHDIR=share/mysql/bench         \
      -DINSTALL_SUPPORTFILESDIR=share/mysql           \
      -DMYSQL_DATADIR=/srv/mysql                      \
      -DMYSQL_UNIX_ADDR=/run/mysqld/mysqld.sock       \
      -DWITH_EXTRA_CHARSETS=complex                   \
      -DWITH_EMBEDDED_SERVER=ON                       \
      -DSKIP_TESTS=ON                                 \
      -DTOKUDB_OK=0                                   \
      .. &&
make -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
