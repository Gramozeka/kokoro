
###############################################################################
set -e
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
pkg=${destdir}/${packagedir}
mkdir -p ${pkg}/usr/share/fonts/TTF/
cp -a ttf/*.ttf ${pkg}/usr/share/fonts/TTF/
mkdir -p ${pkg}/etc/fonts/conf.avail
mkdir -p ${pkg}/etc/fonts/conf.d
( cd fontconfig
  for file in * ; do
    cp -a $file ${pkg}/etc/fonts/conf.avail
    ( cd ${pkg}/etc/fonts/conf.d ; ln -sf ../conf.avail/$file . )
  done
)
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home
