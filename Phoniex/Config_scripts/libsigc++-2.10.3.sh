
###############################################################################
set -e
# The libsigc++ package implements a typesafe callback system for standard C++.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libsigc++/2.10/libsigc++-2.10.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libsigc++/2.10/libsigc++-2.10.2.tar.xz
# Download MD5 sum: 1b067bfae0b502e6a5127336cb09d2dd
# Download size: 3.8 MB
# Estimated disk space required: 65 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# libsigc++ Dependencies
# Optional
# Doxygen-1.8.17 and libxslt-1.1.34 (for documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/^libdocdir =/ s/$(book_name)/libsigc++-2.10.3/' -i docs/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
