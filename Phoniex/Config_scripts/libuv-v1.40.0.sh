
###############################################################################
set -e
# The libuv package is a multi-platform support library with a focus on asynchronous I/O.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://dist.libuv.org/dist/v1.34.2/libuv-v1.34.2.tar.gz
# Download MD5 sum: a10243f691f7707f1d903d80e347b549
# Download size: 1.2 MB
# Estimated disk space required: 24 MB (with tests)
# Estimated build time: 0.6 SBU (with tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./autogen.sh
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
