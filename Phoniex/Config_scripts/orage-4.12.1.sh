
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/xfce/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
zcat ${PATCHSOURCE}//orage.libical3.diff.gz | patch -p1 --verbose || exit 1

# Yes, libical's pkgconfig file is incomplete, it seems
CFLAGS="$CFLAGS -I/usr/include/libical" \
CXXFLAGS="$CFLAGS -I/usr/include/libical" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
