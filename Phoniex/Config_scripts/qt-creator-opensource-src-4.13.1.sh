
###############################################################################
pack_local () {
pkg=${destdir}/$1
make install INSTALL_ROOT=${pkg}  && echo "$1 ----- $(date)" >> ${destdir}/loginstall
make docs html_docs
make install_docs INSTALL_ROOT=${pkg}
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
mkdir qt-creator-build
cd qt-creator-build
qmake ../qtcreator.pro \
  QMAKE_CXXFLAGS="-O2 -fPIC" \
  QTC_PREFIX=/usr \
  IDE_LIBRARY_BASENAME=lib64 \
  LLVM_INSTALL_DIR=/usr
make qmake_all
make -j 5
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
