
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
install -v -m755 -d ${pkg}/usr/share/xml/docbook/xsl-stylesheets-1.79.2 &&

cp -v -R VERSION assembly common eclipse epub epub3 extensions fo        \
         highlighting html htmlhelp images javahelp lib manpages params  \
         profiling roundtrip slides template tests tools webhelp website \
         xhtml xhtml-1_1 xhtml5                                          \
    ${pkg}/usr/share/xml/docbook/xsl-stylesheets-1.79.2 &&

ln -s VERSION ${pkg}/usr/share/xml/docbook/xsl-stylesheets-1.79.2/VERSION.xsl &&
install -v -m755 -d ${pkg}/usr/share/doc/docbook-xsl-1.79.2
install -v -m644 -D README \
                    ${pkg}/usr/share/doc/docbook-xsl-1.79.2/README.txt &&
install -v -m644    RELEASE-NOTES* NEWS* \
                    ${pkg}/usr/share/doc/docbook-xsl-1.79.2
cp -v -R doc/* ${pkg}/usr/share/doc/docbook-xsl-1.79.2
echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
USE_ARCH="64" 
ARCH="noarch"
patch -Np1 -i ${PATCHSOURCE}/docbook-xsl-1.79.2-stack_fix-1.patch
tar -xf ${SOURCE}/docbook-xsl-doc-1.79.2.tar.bz2 --strip-components=1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
if [ ! -d /etc/xml ]; then install -v -m755 -d /etc/xml; fi &&
if [ ! -f /etc/xml/catalog ]; then
    xmlcatalog --noout --create /etc/xml/catalog
fi &&
xmlcatalog --noout --add "rewriteSystem" \
           "http://docbook.sourceforge.net/release/xsl/1.79.2" \
           "/usr/share/xml/docbook/xsl-stylesheets-1.79.2" \
    /etc/xml/catalog &&

xmlcatalog --noout --add "rewriteURI" \
           "http://docbook.sourceforge.net/release/xsl/1.79.2" \
           "/usr/share/xml/docbook/xsl-stylesheets-1.79.2" \
    /etc/xml/catalog &&

xmlcatalog --noout --add "rewriteSystem" \
           "http://docbook.sourceforge.net/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-1.79.2" \
    /etc/xml/catalog &&

xmlcatalog --noout --add "rewriteURI" \
           "http://docbook.sourceforge.net/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-1.79.2" \
    /etc/xml/catalog
    

