
###############################################################################
set -e
# The libgpg-error package contains a library that defines common error values for all GnuPG components.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.36.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/libgpg-error/libgpg-error-1.36.tar.bz2
# Download MD5 sum: eff437f397e858a9127b76c0d87fa5ed
# Download size: 900 KB
# Estimated disk space required: 9.8 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# sed -i 's/namespace/pkg_&/' src/Makefile.{am,in} src/mkstrtable.awk
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
