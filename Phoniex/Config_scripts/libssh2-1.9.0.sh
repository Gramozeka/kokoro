
###############################################################################
set -e
###############################################################################
# Libssh2 package is a client-side C library implementing the SSH2 protocol.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.libssh2.org/download/libssh2-1.9.0.tar.gz
# Download MD5 sum: 1beefafe8963982adc84b408b2959927
# Download size: 868 KB
# Estimated disk space required: 13 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# libssh2 Dependencies
# Optional
# GnuPG-2.2.19, libgcrypt-1.8.5, and OpenSSH-8.1p1 (all three required for the testsuite)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
