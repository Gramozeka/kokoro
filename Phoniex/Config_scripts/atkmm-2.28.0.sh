
###############################################################################
set -e
# Atkmm is the official C++ interface for the ATK accessibility toolkit library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/atkmm/2.28/atkmm-2.28.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/atkmm/2.28/atkmm-2.28.0.tar.xz
# Download MD5 sum: 6194ac577f15567adfa3c923944c6651
# Download size: 872 KB
# Estimated disk space required: 16 MB
# Estimated build time: 0.2 SBU
# Atkmm Dependencies
# Required
# ATK-2.34.1 and GLibmm-2.62.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/^libdocdir =/ s/$(book_name)/atkmm-2.28.0/' \
    -i doc/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
