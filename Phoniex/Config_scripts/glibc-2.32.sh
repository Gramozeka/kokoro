cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/$packagedir
apply_patches() {
  zcat ${SOURCE}/glibc.locale.no-archive.diff.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc.ru_RU.CP1251.diff.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc-c-utf8-locale.patch.gz | patch -p1 --verbose || exit 1
  zcat ${SOURCE}/glibc-2.29.en_US.no.am.pm.date.format.diff.gz | patch -p1 --verbose || exit 1
}

###############################################################################


###############################################################################
# cd $TEMPBUILD
# rm -rf *
# package="$(basename $line).tar.*"
# packagedir=$(basename $line)
# 
# unpack ${SOURCE}/${package}
# cd ${packagedir}
# patch -Np1 -i ${SOURCE}/glibc-2.32-fhs-1.patch
# apply_patches
# if [ ! $? = 0 ]; then
#   exit 1
# fi
# mkdir -v ../glibc-build
# cd ../glibc-build
# export USE_ARCH=32
# export PKG_CONFIG_PATH=${PKG_CONFIG_PATH32}
# export LD_LIBRARY_PATH="/i686/lib:/usr/lib32" 
# export LDFLAGS="-L/i686/lib:/usr/lib32" 
# export CPPFLAGS="-I/i686/include"
# ARCH="i686"
# SAVE_path=${PATH}
# PATH="${PATH32}:${PATH}"
# CFLAGS="-O2 -march=i686 -mtune=i686" \
# CXXFLAGS="-O2 -march=i686 -mtune=i686" \
# CC="gcc -m32" CXX="-m32" \
# PKG_CONFIG_PATH=${PKG_CONFIG_PATH32} \
# ../${packagedir}/configure \
#     --prefix=/i686 \
#     --enable-kernel=3.2.0                 \
#     --libdir=/i686/lib \
#     --libexecdir=/i686/lib/glibc \
#     --enable-stack-protector=strong \
#     --with-binutils=/i686  \
#     --with-cpu=i686 \
#          --enable-add-ons \
#   --enable-obsolete-nsl \
#   --enable-obsolete-rpc \
#   --disable-werror      \
#    --build=${CLFS_TARGET32} \
#    libc_cv_slibdir=/i686/lib
# 
# make -j4
# sed '/test-installation/s@$(PERL)@echo not running@' -i ../${packagedir}/Makefile
# pack_local ${packagedir}
# export PATH=${SAVE_path}
# unset AR AS RANLIB LD STRIP LD_LIBRARY_PATH LDFLAGS CPPFLAGS
# source /etc/profile &&
echo "###########################**** COMPLITE!!! ****##################################"
# export MACHTYPE=${CLFS_TARGET}
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${SOURCE}/glibc-2.32-fhs-1.patch
apply_patches
if [ ! $? = 0 ]; then
  exit 1
fi
mkdir -v ../glibc-build
cd ../glibc-build
USE_ARCH="64" 
ARCH="x86_64"
LDFLAGS="-L/lib64:/usr/lib64" 
LD_LIBRARY_PATH="/lib64:/usr/lib64" 
export CFLAGS="${BUILD64}"
export CXXFLAGS="${BUILD64}"
export CLFS_TARGET=${CLFS_TARGET64}
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
CC="gcc ${BUILD64}" \
CXX="g++ ${BUILD64}" \
../${packagedir}/configure \
    --prefix=/usr \
    --enable-kernel=3.2.0                 \
    --libexecdir=/usr/lib64/glibc \
    --libdir=/usr/lib64 \
           --enable-add-ons \
  --enable-obsolete-nsl \
  --enable-obsolete-rpc \
  --enable-multi-arch \
    --enable-stack-protector=strong \
    --with-headers=/usr/include            \
    --disable-werror                       \
    libc_cv_slibdir=/lib64 --build=${CLFS_TARGET64}
make -j4
sed '/test-installation/s@$(PERL)@echo not running@' -i ../${packagedir}/Makefile
make install install_root=${pkg}
make -j4 localedata/install-locales install_root=${pkg} DESTDIR=${pkg}
tar -xf ${SOURCE}/tzdata2020d.tar.gz
ZONEINFO=${pkg}/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}
for tz in etcetera southamerica northamerica europe africa antarctica  \
          asia australasia backward; do
    zic -L /dev/null   -d $ZONEINFO       ${tz}
    zic -L /dev/null   -d $ZONEINFO/posix ${tz}
    zic -L leapseconds -d $ZONEINFO/right ${tz}
done
cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO
ln -sv /usr/share/zoneinfo/Asia/Yekaterinburg ${pkg}/etc/localtime
cp -v ../${packagedir}/nscd/nscd.conf ${pkg}/etc/nscd.conf
mkdir -pv ${pkg}/var/cache/nscd
save_lib="ld-2.32.so libc-2.32.so libpthread-2.32.so libthread_db-1.0.so"
cd ${pkg}/lib64
for LIB in $save_lib; do
    objcopy --only-keep-debug $LIB $LIB.dbg
    strip --strip-unneeded $LIB
    objcopy --add-gnu-debuglink=$LIB.dbg $LIB 
done   
find ${pkg}/usr/lib64 -type f -name \*.a \
   -exec strip --strip-debug {} ';' &&
find ${pkg}/lib64 ${pkg}/usr/lib64 -type f -name \*.so* ! -name \*dbg \
   -exec strip --strip-unneeded {} ';' &&
find ${pkg}/sbin ${pkg}/usr/{bin,sbin} -type f \
    -exec strip --strip-all {} ';' &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
wait
cd ${pkg} &&
mkdir -pv ${pkg}/usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > ${pkg}/usr/tree-info/glibc-2.32-$USE_ARCH-Full-tree
tar -cvpzf ${destdir}/1-repo/glibc-2.32-$USE_ARCH-Full.tar.gz --one-file-system .
tar -xvpzf ${destdir}/1-repo/glibc-2.32-$USE_ARCH-Full.tar.gz -C / --numeric-owner
rm -rf ${pkg}
echo "########################### COMPLITE ! ##############################"

