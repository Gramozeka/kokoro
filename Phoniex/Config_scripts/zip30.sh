
###############################################################################
# The Zip package contains Zip utilities. These are useful for compressing files into ZIP archives.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/infozip/zip30.tar.gz
# Download (FTP): ftp://ftp.info-zip.org/pub/infozip/src/zip30.tgz
# Download MD5 sum: 7b74551e63f8ee6aab6fbc86676c0d37
# Download size: 1.1 MB
# Estimated disk space required: 6.4 MB
# Estimated build time: 0.1 SBU
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -f unix/Makefile generic_gcc

pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}/usr/share/man/man1
mkdir -pv ${pkg}/usr/bin
make prefix=${pkg} MANDIR=/usr/share/man/man1 -f unix/Makefile install && echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
