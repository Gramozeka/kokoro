###############################################################################
set -e
###############################################################################
# libblockdev is a C library supporting GObject Introspection for manipulation of block devices. It has a plugin-based architecture where each technology (like LVM, Btrfs, MD RAID, Swap,...) is implemented in a separate plugin, possibly with multiple implementations (e.g. using LVM CLI or the new LVM DBus API).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/storaged-project/libblockdev/releases/download/2.23-1/libblockdev-2.23.tar.gz
# Download MD5 sum: b956653be98a677cf004c07b302941b9
# Download size: 840 KB
# Estimated disk space required: 11 MB
# Estimated build time: 0.2 SBU
# libblockdev Dependencies
# Required
# gobject-introspection-1.62.0, libbytesize-2.1, libyaml-0.2.2, parted-3.3, and volume_key-0.3.12
# Optional
# btrfs-progs-5.4.1, GTK-Doc-1.32, mdadm-4.1, dmraid, bcachefs, and ndctl
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
            --with-python3    \
            --without-gtk-doc \
            --without-nvdimm  \
            --without-dm \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
