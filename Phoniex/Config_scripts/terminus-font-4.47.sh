
###############################################################################
pack_local () {
pkg=${destdir}/$1
make install install-uni install-ref install-psf DESTDIR=${pkg} &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
#patch -Np1 -i ${PATCHSOURCE}/terminus-font.Makefile.paths.diff
# make || exit 1
# make psf
./configure --prefix=/usr --psfdir=/usr/share/consolefonts \
--x11dir=/usr/share/fonts/misc
pack_local ${packagedir}
if [ -x /usr/bin/mkfontdir ]; then
  /usr/bin/mkfontscale /usr/share/fonts
  /usr/bin/mkfontdir /usr/share/fonts
fi
if [ -x /usr/bin/fc-cache ]; then
  /usr/bin/fc-cache -fv /usr/share/fonts
fi
echo "###########################**** COMPLITE!!! ****##################################"
