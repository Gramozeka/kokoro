
###############################################################################
set -e
###############################################################################
# PostgreSQL is an advanced object-relational database management system (ORDBMS), derived from the Berkeley Postgres database management system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.postgresql.org/pub/source/v12.1/postgresql-12.1.tar.bz2
# Download MD5 sum: 2ee1bd4ec5f49363a3f456f07e599b41
# Download size: 19 MB
# Estimated disk space required: 184 MB (add 36 MB for tests)
# Estimated build time: 1.0 SBU (with parallelism=4, add 0.1 SBU for tests)
# PostgreSQL Dependencies
# Optional
# Python-2.7.17, Tcl-8.6.10, libxml2-2.9.10, libxslt-1.1.34, OpenLDAP-2.4.48, Linux-PAM-1.3.1, MIT Kerberos V5-1.17.1 and Bonjour
# Optional (To Regenerate Documentation)
# fop-2.4, docbook-4.5, docbook-dsssl-1.79, DocBook-utils-0.6.14, OpenJade-1.3.2, and SGMLSpm-1.1
###############################################################################
groupadd -g 41 postgres &&
useradd -c "PostgreSQL Server" -g postgres -d /srv/pgsql/data \
        -u 41 postgres
install -v -dm700 /srv/pgsql/data &&
install -v -dm755 /run/postgresql &&
chown -Rv postgres:postgres /srv/pgsql /run/postgresql
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make DESTDIR=${pkg} install-docs
make -C contrib DESTDIR=${pkg} install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/DEFAULT_PGSOCKET_DIR/s@/tmp@/run/postgresql@' src/include/pg_config_manual.h &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--enable-thread-safety \
--with-openssl \
--with-perl \
--with-tcl \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make -j4 -C contrib
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
