
###############################################################################
set -e
###############################################################################
# The libgweather package is a library used to access weather information from online services for numerous locations.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libgweather/3.34/libgweather-3.34.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libgweather/3.34/libgweather-3.34.0.tar.xz
# Download MD5 sum: 52c3b1e27887fc88f862c92c42d930c1
# Download size: 2.6 MB
# Estimated disk space required: 92 MB
# Estimated build time: 0.2 SBU
# libgweather Dependencies
# Required
# geocode-glib-3.26.1, GTK+-3.24.13, and libsoup-2.68.3
# Recommended
# gobject-introspection-1.62.0, libxml2-2.9.10, and Vala-0.46.5
# Optional
# Glade and GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '338a "KX26",' libgweather/test_libgweather.c
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
