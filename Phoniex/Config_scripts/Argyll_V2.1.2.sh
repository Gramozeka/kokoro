
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install


rm -f bin/License.txt
rm -f ref/License.txt
rm -f ref/afiles
rm -f doc/afiles

mkdir -p ${pkg}/usr/bin
mkdir -p ${pkg}/usr/share/argyllcms/{ref,doc/ccmxs}
mkdir -p ${pkg}/lib/udev/rules.d

install -m 0755 bin/* ${pkg}/usr/bin
install -m 0644 ref/*.* ${pkg}/usr/share/argyllcms/ref
install -m 0644 profile/*.sp ${pkg}/usr/share/argyllcms/ref
install -m 0644 scanin/*.c?? ${pkg}/usr/share/argyllcms/ref

install -m 0644 doc/ccmxs/*.ccmx ${pkg}/usr/share/argyllcms/doc/ccmxs
install -m 0644 doc/*.* ${pkg}/usr/share/argyllcms/doc

install -m 0644 usb/55-Argyll.rules ${pkg}/lib/udev/rules.d

echo "$1 ----- $(date)" >> ${destdir}/loginstall
# find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
sh ./makeall.sh
sh ./makeinstall.sh
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
