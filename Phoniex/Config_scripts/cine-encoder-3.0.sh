
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# make DESTDIR=${pkg} install
make INSTALL_ROOT=${pkg} install
mkdir -pv ${pkg}/usr/share/{icons,pixmaps,applications}
cat > ${pkg}/usr/share/applications/cine-encoder.desktop << EOF
[Desktop Entry]
Name=cine-encoder
Comment=GUI For Ffmpeg
GenericName=GUI Ffmpeg
Comment[ru]=Интерфейс Ffmpeg
GenericName[ru]=Интерфейс Ffmpeg
Name[ru]=cine_encoder
Icon[ru]=cine_encoder
Exec=/usr/bin/cine_encoder
Icon=cine_encoder
Terminal=false
Encoding=UTF-8
MimeType=audio/ac3;audio/mp4;audio/mpeg;audio/vnd.rn-realaudio;audio/vorbis;audio/x-adpcm;audio/x-matroska;audio/x-mp2;audio/x-mp3;audio/x-ms-wma;audio/x-vorbis;audio/x-wav;audio/mpegurl;audio/x-mpegurl;audio/x-pn-realaudio;audio/x-scpls;audio/aac;audio/flac;audio/ogg;video/avi;video/mp4;video/flv;video/mpeg;video/quicktime;video/vnd.rn-realvideo;video/x-matroska;video/x-ms-asf;video/x-msvideo;video/x-ms-wmv;video/x-ogm+ogg;video/x-theora;video/webm;
Type=Application
X-KDE-StartupNotify=false
EOF
for s in 16 20 24; do
mkdir -pv ${pkg}/usr/share/icons/hicolor/${s}x${s}/apps
  install -vDm644 app/icons/${s}x${s}/cil-*.png \
                  ${pkg}/usr/share/icons/hicolor/${s}x${s}/apps/
done
  install -vDm644 app/cine-encoder.png \
                  ${pkg}/usr/share/pixmaps/
                  echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "s|/usr/lib/|/usr/lib64|g" Makefile
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
