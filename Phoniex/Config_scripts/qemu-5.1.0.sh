
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
mkdir -pv ${pkg}/lib/udev/rules.d
cat > ${pkg}/lib/udev/rules.d/65-kvm.rules << "EOF"
KERNEL=="kvm", GROUP="kvm", MODE="0660"
EOF
chgrp kvm  ${pkg}/usr/libexec/qemu-bridge-helper &&
chmod 4750 ${pkg}/usr/libexec/qemu-bridge-helper

ln -sv qemu-system-$(uname -m) ${pkg}/usr/bin/qemu
rm -rf ${pkg}/var
install -vdm 755 ${pkg}/etc/qemu &&
echo allow br0 > ${pkg}/etc/qemu/bridge.conf
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
if [ $(uname -m) = i686 ]; then
   QEMU_ARCH=i386-softmmu
else
   QEMU_ARCH=x86_64-softmmu
fi
mkdir -vp build &&
cd        build &&
../configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
            --localstatedir=/var \
--audio-drv-list=alsa,pa \
--python=python3 \
--docdir=/usr/share/doc/${packagedir} \
  --enable-gtk \
  --enable-system \
  --enable-kvm \
  --disable-debug-info \
  --enable-virtfs \
  --enable-sdl \
  --enable-sdl-image \
  --enable-curses \
  --enable-libusb  \
   --enable-libxml2 \
   --enable-virglrenderer \
  --enable-jemalloc \
  --enable-nettle \
  --enable-xkbcommon  \
  --enable-vnc --enable-vnc-sasl --enable-vnc-jpeg --enable-spice --enable-opengl --enable-usb-redir
# make config-all-devices.mak config-all-disas.mak

make -j4 V=1
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
