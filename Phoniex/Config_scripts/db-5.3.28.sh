
###############################################################################
set -e
# The Berkeley DB package contains programs and utilities used by many other applications for database related functions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://anduin.linuxfromscratch.org/BLFS/bdb/db-5.3.28.tar.gz
# Download MD5 sum: b99454564d5b4479750567031d66fe24
# Download size: 34 MB
# Estimated disk space required: 265 MB
# Estimated build time: 0.6 SBU
# Berkeley DB Dependencies
# Optional
# Tcl-8.6.10 and Sharutils-4.15.2 (for the uudecode command)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make docdir=/usr/share/doc/${packagedir} DESTDIR=${pkg} install
 chown -v -R root:root                        \
      ${pkg}/usr/bin/db_*                          \
      ${pkg}/usr/include/db{,_185,_cxx}.h          \
      ${pkg}/usr/lib64/libdb*.{so,la}                \
      ${pkg}/usr/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/\(__atomic_compare_exchange\)/\1_db/' src/dbinc/atomic.h
mkdir builddir
cd build_unix                         &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../dist/configure --prefix=/usr      \
--libdir=/usr/lib64 \
                  --enable-compat185 \
                  --enable-dbm       \
                  --disable-static   \
                  --enable-cxx      --enable-shared   \
                  --enable-tcl --with-tcl=/usr/lib64 \
--build=${CLFS_TARGET} &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
