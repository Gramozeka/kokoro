
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake      -DCMAKE_C_FLAGS="$BUILD64" \
     -DCMAKE_CXX_FLAGS="$BUILD64" \
-DCMAKE_INSTALL_PREFIX=/usr \
 -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
 -DLIB_INSTALL_DIR=/usr/lib64 \
      -DSYSCONF_INSTALL_DIR=/etc  \
      -DENABLE_INSTALLED_TESTS=ON \
      -DENABLE_PST_IMPORT=OFF     \
      -DENABLE_YTNEF=OFF          \
      -DENABLE_CONTACT_MAPS=ON   \
      -G Ninja .. &&
ninja -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
