
###############################################################################
packagedir=$(sed -e "s/\-final*$//" <<< $(basename $line))
package="$packagedir.tar.*"
pkg=${destdir}/${packagedir}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/LIBTOOL_INSTALL/d' c++/Makefile.in

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib64 \
  --enable-colorfgbg \
  --enable-hard-tabs \
  --enable-overwrite \
  --enable-xmc-glitch \
   --with-chtype=long \
  --with-mmask-t=long \
  --with-cxx-shared \
  --with-ospeed=unsigned \
  --with-termlib=tinfo \
  --with-ticlib=tic \
  --with-gpm \
  --with-shared \
  --without-debug \
  --disable-static \
  --without-profile \
  --enable-symlinks \
  --enable-pc-files \
  --with-pkg-config-libdir=/usr/lib64/pkgconfig \
  --without-ada
make
make DESTDIR=${pkg} install
make distclean
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib64 \
  --enable-colorfgbg \
  --enable-hard-tabs \
  --enable-overwrite \
  --enable-xmc-glitch \
   --with-chtype=long \
  --with-mmask-t=long \
  --with-cxx-shared \
  --with-ospeed=unsigned \
  --with-termlib=tinfo \
  --with-ticlib=tic \
  --with-gpm \
  --with-shared \
  --without-debug \
  --disable-static \
  --without-profile \
  --enable-symlinks \
  --enable-pc-files \
  --with-pkg-config-libdir=/usr/lib64/pkgconfig \
  --without-ada \
            --enable-widec
make
# make install
make DESTDIR=${pkg} install
make distclean
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr    \
  --libdir=/usr/lib64 \
            --with-shared    \
            --without-normal \
            --without-debug  \
            --without-cxx-binding \
            --with-abi-version=5 
make sources libs
cp -av lib/lib*.so.5* ${pkg}/usr/lib64
# Make a termcap file in case anyone actually needs it:
echo "#       /etc/termcap" > misc/termcap
grep \$Revision: misc/terminfo.src >> misc/termcap
grep \$Date: misc/terminfo.src >> misc/termcap
echo "#" >> misc/termcap
echo "#       Converted from terminfo.src for Vaio:" >> misc/termcap
echo "#       tic -C -t terminfo.src > termcap" >> misc/termcap
echo "#" >> misc/termcap
echo "#       This file is included to support legacy applications using libtermcap." >> misc/termcap
echo "#       Modern applications will use ncurses/terminfo instead." >> misc/termcap
echo "#" >> misc/termcap
echo "#------------------------------------------------------------------------------" >> misc/termcap
echo "#" >> misc/termcap
tic -C -t misc/terminfo.src >> misc/termcap
touch -r misc/terminfo.src misc/termcap
mkdir ${pkg}/etc
cp -a misc/termcap ${pkg}/etc/termcap

# Strip stuff:

# Remove static libraries:
# for file in libform.a libformw.a libmenu.a libmenuw.a libncurses++.a \
#   libncurses++w.a libncurses.a libncursesw.a libpanel.a libpanelw.a \
#   libtic.a libtinfo.a libcurses.a ; do
  rm -fv ${pkg}/usr/lib64/*.a
# done
mkdir -p ${pkg}/lib64
( cd ${pkg}/usr/lib64
  chmod 755 *.so
#  chmod 644 *.a
  mv libncurses.so.6* ${pkg}/lib64
  mv libncursesw.so.6* ${pkg}/lib64
  mv libtinfo.so.6* ${pkg}/lib64
  rm -f libtinfo.so.6
  ln -sf ../../lib64/libtinfo.so.6 .
)

# Use linker loader scripts to make sure -tinfo dependency works:
( cd ${pkg}/usr/lib64
  rm -f libcurses.so libcursesw.so libncurses.so libncursesw.so libtermcap.so
  echo "INPUT(-lncurses)" > libcurses.so
  echo "INPUT(-lncursesw)" > libcursesw.so
  echo "INPUT(libncurses.so.6 -ltinfo)" > libncurses.so
  echo "INPUT(libncursesw.so.6 -ltinfo)" > libncursesw.so
  echo "INPUT(-ltinfo)" > libtermcap.so
)
if [ -d ${pkg}/usr/share/man ]; then
echo n|find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
echo n|find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
find ${pkg} -depth -print0  -type f -name "*.la"  -exec rm -fv {} \;
find ${pkg} -depth -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall &&
rm -rf ${pkg}
cd $home
