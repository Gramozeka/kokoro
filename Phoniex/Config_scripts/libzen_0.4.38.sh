
###############################################################################
set -e
###############################################################################
# https://mediaarea.net/download/source/libmediainfo/20.08/libmediainfo_20.08.tar.xz
# https://mediaarea.net/download/source/libzen/0.4.38/libzen_0.4.38.tar.bz2
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

  install -dm 755 ${pkg}/usr/include/ZenLib
  install -m 644 "$srcdir"/ZenLib/Source/ZenLib/*.h ${pkg}/usr/include/ZenLib

  for i in HTTP_Client Format/Html Format/Http; do
    install -dm0755 ${pkg}/usr/include/ZenLib/$i
    install -m0644 "$srcdir"/ZenLib/Source/ZenLib/$i/*.h ${pkg}/usr/include/ZenLib/$i
  done

  install -dm 755 ${pkg}/usr/lib64/pkgconfig
  install -m 644 "$srcdir"/ZenLib/Project/GNU/Library/libzen.pc ${pkg}/usr/lib64/pkgconfig

  sed -i -e 's|Version: $|Version: '20.08'|g' ${pkg}/usr/lib64/pkgconfig/libzen.pc


echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
srcdir=$TEMPBUILD
# cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
  cd ZenLib/Project/GNU/Library

  ./autogen.sh
  CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
  ./configure \
  --prefix=/usr \
  --libdir=/usr/lib64 \
  --enable-shared
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
