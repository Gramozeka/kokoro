
###############################################################################
set -e
# The libsoup is a HTTP client/server library for GNOME. It uses GObject and the GLib main loop to integrate with GNOME applications and it also has an asynchronous API for use in threaded applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libsoup/2.68/libsoup-2.68.3.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libsoup/2.68/libsoup-2.68.3.tar.xz
# Download MD5 sum: 29ee2ee7017945b64ede063b1396011c
# Download size: 1.4 MB
# Estimated disk space required: 19 MB (with tests)
# Estimated build time: 0.3 SBU (Using parallelism=4; with tests)
# libsoup Dependencies
# Required
# glib-networking-2.62.3, libpsl-0.21.0, libxml2-2.9.10 and SQLite-3.31.1
# Recommended
# gobject-introspection-1.62.0 and Vala-0.46.5
# Optional
# Apache-2.4.41 (required to run the test suite), Brotli-1.0.7, cURL-7.68.0 (required to run the test suite), MIT Kerberos V5-1.17.1 (required to run the test suite), GTK-Doc-1.32, PHP-7.4.2 compiled with XMLRPC-EPI support (only used for the XMLRPC regression tests) and Samba-4.11.0 (ntlm_auth is required to run the test suite).
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/libsoup-2.72.0-testsuite_fix-1.patch
mkdir meson-build
cd meson-build
CFLAGS="$CFLAGS -pthread" \
CXXFLAGS="$CXXFLAGS -pthread" \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dvapi=enabled \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
