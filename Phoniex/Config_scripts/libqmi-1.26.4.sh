
###############################################################################
set -e
###############################################################################
# The libqmi package contains a GLib-based library for talking to WWAN modems and devices which speak the Qualcomm MSM Interface (QMI) protocol.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/libqmi/libqmi-1.24.4.tar.xz
# Download MD5 sum: be6539fde54fec1fc9d852db201c8560
# Download size: 976 KB
# Estimated disk space required: 77 MB (with tests)
# Estimated build time: 0.2 SBU (Using parallelism=4; with tests)
# libqmi Dependencies
# Required
# GLib-2.62.4
# Recommended
# libmbim-1.22.0
# Optional
# GTK-Doc-1.32 and help2man
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
