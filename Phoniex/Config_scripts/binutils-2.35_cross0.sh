
#######################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_cross0*$//" <<< $(basename $line))
package="$packagedir.tar.*"

unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -v build
cd       build
CC="gcc -m32" \
CXX="g++ -m32" \
AR=ar AS=as \
PKG_CONFIG_PATH=/i686/lib/pkgconfig \
setarch i386 ../configure \
    --prefix=/i686 \
    --host=i686-unknown-linux \
    --target=${CLFS_TARGET32} \
    --with-sysroot=/i686 \
    --with-lib-path=/i686/lib \
    --disable-nls \
    --disable-static \
    --enable-gold=yes \
    --enable-plugins \
    --enable-threads \
    --disable-werror
setarch i386 make -j4
make install
echo "###########################**** COMPLITE!!! ****##################################"
