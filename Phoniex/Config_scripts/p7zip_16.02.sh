
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/p7zip_fix_gcc-10.2.diff
pkg=${destdir}/${packagedir}
make all3 \
  OPTFLAGS="$CFLAGS" \
  DEST_HOME=/usr \
  DEST_SHARE_DOC=/usr/share/doc/${packagedir} \
  DEST_SHARE=/usr/lib64/p7zip \
  DEST_DIR=$pkg

    sed -i "s|/usr/lib/|/usr/lib64/|g" CPP/7zip/UI/GUI/makefile.depend
    make 7zG \
    OPTFLAGS="$CFLAGS" \
    DEST_HOME=/usr \
    DEST_SHARE_DOC=/usr/share/doc/${packagedir} \
    DEST_SHARE=/usr/lib64/p7zip \
    DEST_DIR=$pkg  
  
  make install \
    OPTFLAGS="$CFLAGS" \
    DEST_HOME=/usr \
    DEST_SHARE_DOC=/usr/share/doc/${packagedir} \
    DEST_SHARE=/usr/lib64/p7zip \
    DEST_DIR=$pkg 
  
install -m 0755 contrib/gzip-like_CLI_wrapper_for_7z/p7zip $pkg/usr/bin/
install -m 0644 contrib/gzip-like_CLI_wrapper_for_7z/man1/p7zip.1 $pkg/usr/man/man1/

mkdir -p $pkg/usr/share/mc/extfs/
install -m 755 contrib/VirtualFileSystemForMidnightCommander/u7z \
  $pkg/usr/share/mc/extfs/

  install -m 555 $pkg/usr/bin/7z $pkg/usr/bin/7zG
  sed -i "s|/usr/lib64/p7zip/7z|/usr/lib64/p7zip/7zG|" $pkg/usr/bin/7zG

  install -m 555 bin/7zG $pkg/usr/lib64/p7zip/7zG
  cp -r GUI/Lang $pkg/usr/lib64/p7zip/Lang
  find $pkg/usr/lib64/p7zip/Lang -type d -exec chmod 555 {} \;
  find $pkg/usr/lib64/p7zip/Lang -type f -exec chmod 444 {} \;

  install -m 555 GUI/p7zipForFilemanager  $pkg/usr/bin/p7zipForFilemanager

  install -m 444 -D GUI/p7zip_16_ok.png $pkg/usr/share/icons/hicolor/16x16/apps/p7zip.png

  mkdir -p $pkg/usr/share/kde4/services/ServiceMenus
  cp GUI/kde4/*.desktop $pkg/usr/share/kde4/services/ServiceMenus/
mv -v $pkg/usr/man $pkg/usr/share/man
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!! ****##################################"
