
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/cogl.patch
autoreconf -vfi
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--enable-gles1 \
--enable-gles2         \
--enable-{kms,wayland,xlib}-egl-platform                    \
--enable-wayland-egl-server     \
--enable-cogl-gst \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
gtk-query-immodules-3.0 --update-cache
glib-compile-schemas /usr/share/glib-2.0/schemas
