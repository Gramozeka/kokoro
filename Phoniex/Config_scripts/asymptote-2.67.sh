
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).src.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
# rm doc/{GaussianSurface.asy,axis3.asy,cube.asy,generalaxis3.asy,grid3xyz.asy}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
  --enable-gc=system \
  --enable-offscreen \
  --with-latex=/usr/share/texmf-dist/tex/latex \
  --with-context=/usr/share/texmf-dist/tex/context \
  --datarootdir=/usr/share/texmf-dist \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
