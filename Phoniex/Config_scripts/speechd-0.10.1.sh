
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
git clone https://github.com/brailcom/speechd.git ${packagedir}
#unpack ${SOURCE}/${package}
cd ${packagedir}
export SED=sed
rm -f ABOUT-NLS
rm -f po/remove-potcdate.sin
autoreconf -fi

# See https://savannah.gnu.org/bugs/index.php?54809
rm -f \
./po/remove-potcdate.sin \
./po/quot.sed \
./po/boldquot.sed \
./po/en@quot.header \
./po/en@boldquot.header \
./po/insert-header.sin \
./po/Rules-quot \
./ABOUT-NLS

touch ABOUT-NLS
touch po/remove-potcdate.sin
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHON=/usr/bin/python3.9 ./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--with-pulse \
--with-alsa \
--with-espeak \
  --with-espeak-ng \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--with-systemdsystemunitdir=no \
--docdir=/usr/share/doc/${packagedir}
make -j4 all
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
