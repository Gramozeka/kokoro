
###############################################################################
set -e
# The libgcrypt package contains a general purpose crypto library based on the code used in GnuPG. The library provides a high level interface to cryptographic building blocks using an extendable and flexible API.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-1.8.5.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/libgcrypt/libgcrypt-1.8.5.tar.bz2
# Download MD5 sum: 348cc4601ca34307fc6cd6c945467743
# Download size: 2.9 MB
# Estimated disk space required: 44 MB (with tests; add 8 MB for building docs)
# Estimated build time: 0.3 SBU (with docs; add 0.5 SBU for tests)
# libgcrypt Dependencies
# Required
# libgpg-error-1.36
# Optional
# Pth-2.0.7 and texlive-20190410 (or install-tl-unx)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -dm755   ${pkg}/usr/share/doc/${packagedir} &&
install -v -m644    README doc/{README.apichanges,fips*,libgcrypt*} \
                    ${pkg}/usr/share/doc/${packagedir} &&

install -v -dm755   ${pkg}/usr/share/doc/${packagedir}/html &&
install -v -m644 doc/gcrypt.html/* \
                    ${pkg}/usr/share/doc/${packagedir}/html &&
install -v -m644 doc/gcrypt_nochunks.html \
                    ${pkg}/usr/share/doc/${packagedir}      &&
install -v -m644 doc/gcrypt.{txt,texi} \
                    ${pkg}/usr/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make -C doc html                                                       &&
makeinfo --html --no-split -o doc/gcrypt_nochunks.html doc/gcrypt.texi &&
makeinfo --plaintext       -o doc/gcrypt.txt           doc/gcrypt.texi
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
