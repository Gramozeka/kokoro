
###############################################################################
set -e
###############################################################################
# iw is a new nl80211 based CLI configuration utility for wireless devices. It supports all new drivers that have been added to the kernel recently. The old tool iwconfig, which uses Wireless Extensions interface, is deprecated and it's strongly recommended to switch to iw and nl80211.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/software/network/iw/iw-5.4.tar.xz
# Download MD5 sum: 0d8bd345449c14249e39ce4b1f09cd64
# Download size: 136 KB
# Estimated disk space required: 3.5 MB
# Estimated build time: less than 0.1 SBU
# iw Dependencies
# Required
# libnl-3.5.0
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/iw
# Kernel Configuration
# To use iw, the kernel must have the appropriate drivers and other support available. The appropriate bus must also be available. For older laptops, the PCMCIA bus (CONFIG_PCCARD) needs to be built. In some cases, this bus support will also need to be built for embedded iw cards. The appropriate bridge support also needs to be built. For many modern laptops, the CardBus host bridge (CONFIG_YENTA) will be needed.
# In addition to the bus, the actual driver for the specific wireless card must also be available. There are many wireless cards and they don't all work with Linux. The first place to look for card support is the kernel. The drivers are located in Device Drivers → Network Device Support → Wireless LAN (non-hamradio). There are also external drivers available for some very common cards. For more information, look at the user notes.
# After the correct drivers are loaded, the interface will appear in /proc/net/wireless.
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg}  SBINDIR=/sbin install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "/INSTALL.*gz/s/.gz//" Makefile &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
