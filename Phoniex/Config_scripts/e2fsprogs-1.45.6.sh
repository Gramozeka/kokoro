
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
mkdir -v build
cd       build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../configure --prefix=/usr           \
             --bindir=/bin           \
             --libdir=/usr/lib64     \
             --with-root-prefix=""   \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck \
    --build=${CLFS_TARGET} \
    --docdir=/usr/share/doc/${packagedir}
make -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
