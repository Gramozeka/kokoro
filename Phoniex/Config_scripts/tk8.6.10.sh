
###############################################################################
set -e
# The Tk package contains a TCL GUI Toolkit.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/tcl/tk8.6.10-src.tar.gz
# Download MD5 sum: 602a47ad9ecac7bf655ada729d140a94
# Download size: 4.2 MB
# Estimated disk space required: 24 MB
# Estimated build time: 0.3 SBU (add 2.0 SBU for tests)
# Tk Dependencies
# Required
# Tcl-8.6.10 and Xorg Libraries
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make install-private-headers DESTDIR=${pkg} &&
ln -v -sf wish8.6 ${pkg}/usr/bin/wish &&
chmod -v 755 ${pkg}/usr/lib64/libtk8.6.so
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -e "s#^TK_LIBRARY='\$(prefix)/lib/#TK_LIBRARY='\$(libdir)/#" \
  unix/configure
cd unix &&
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
  --enable-shared \
  --enable-threads \
  --enable-64bit \
--build=${CLFS_TARGET}
make -j4
 sed -e "s@^\(TK_SRC_DIR='\).*@\1/usr/include'@" \
     -e "/TK_B/s@='\(-L\)\?.*unix@='\1/usr/lib64@" \
     -i tkConfig.sh
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
