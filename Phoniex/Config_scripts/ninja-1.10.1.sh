
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr
mkdir -pv ${pkg}/usr/{bin,share}
mkdir -pv ${pkg}/usr/share/{vim/vim81/syntax,bash-completion/completions,zsh/site-functions/_ninja,emacs/site-lisp,doc/ninja-1.9.0}
install -vm755 ninja ${pkg}/usr/bin/
install -vDm644 misc/bash-completion ${pkg}/usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion  ${pkg}/usr/share/zsh/site-functions/_ninja
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
export NINJAJOBS=4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
USE_ARCH="64" 
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/int Guess/a \
  int   j = 0;\
  char* jobs = getenv( "NINJAJOBS" );\
  if ( jobs != NULL ) j = atoi( jobs );\
  if ( j > 0 ) return j;\
' src/ninja.cc
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
python3 configure.py --bootstrap
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
