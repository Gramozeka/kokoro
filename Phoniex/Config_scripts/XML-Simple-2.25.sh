
###############################################################################
set -e
###############################################################################
# # # # # cd $TEMPBUILD
# # # # # rm -rf *
# # # # # package="$(basename $line).tar.*"
# # # # # packagedir=$(basename $line)
# # # # # 
# # # # # unpack ${SOURCE}/${package}
# # # # # cd ${packagedir}
# # # # # setarch i386 /i686/bin/perl Makefile.PL
# # # # # 
# # # # # make -j4
# # # # # echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
# # # # # make install
# # # # # 
# # # # # echo "###########################**** COMPLITE!!! ****##################################"
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
perl Makefile.PL

make -j4
make install
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
