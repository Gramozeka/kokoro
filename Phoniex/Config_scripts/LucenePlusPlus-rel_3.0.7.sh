
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
# git clone https://github.com/luceneplusplus/LucenePlusPlus.git ${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/LucenePlusPlus-git_libstemmer_utf8.diff
sed -i "s/include(CMakeExternal.txt)/#include(CMakeExternal.txt)/" CMakeLists.txt
sed -i "s/enable_testing()/#enable_testing()/" CMakeLists.txt
    mkdir build
    cd    build
             cmake  \
             -DCMAKE_C_FLAGS="$CFLAGS -fcommon -Wno-builtin-macro-redefined" \
             -DCMAKE_CXX_FLAGS="$CXXFLAGS -fcommon -Wno-builtin-macro-redefined"\
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLIB_DESTINATION=/usr/lib64 \
    -DCMAKE_CXX_FLAGS='-DBOOST_VARIANT_USE_RELAXED_GET_BY_DEFAULT' \
    -DENABLE_TEST=OFF \
    -DENABLE_DEMO=OFF \
    -DCMAKE_BUILD_TYPE=Release ..
make -j4 || make || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
