
###############################################################################
pack_local () {
pkg=${destdir}/$1
make distro-pack-install DESTDIR=${pkg} && echo "$1 ----- $(date)" >> ${destdir}/loginstall
rm -rf  ${pkg}/*gid_Module_*
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.xz"
packagedir=$(basename $line)
tar -xvf ${SOURCE}/${package} --no-overwrite-dir &&
cd ${packagedir}
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;
# sed -i 's/loaded;/bool{loaded};/' sfx2/source/appl/shutdownicon.cxx
install -dm755 external/tarballs &&
ln -sv ${SOURCE}/libreoffice-dictionaries-7.0.2.2.tar.xz external/tarballs/ &&
ln -sv ${SOURCE}/libreoffice-help-7.0.2.2.tar.xz         external/tarballs/
ln -sv ${SOURCE}/libreoffice-translations-7.0.2.2.tar.xz external/tarballs/
export LO_PREFIX="/usr"
# # # sed -e "/gzip -f/d"   \
# # #     -e "s|.1.gz|.1|g" \
# # #     -i bin/distro-install-desktop-integration &&
# # # 
# # # sed -e "/distro-install-file-lists/d" -i Makefile.in &&
# # # 
# # # sed -e '/JAVA_SOURCE_VER/s/6/7/' \
# # #     -e '/JAVA_TARGET_VER/s/6/7/' \
# # #     -i configure.ac &&
# # # 
# # # sed -e 's/globalParams = new GlobalParams()/globalParams.reset(new GlobalParams())/' \
# # #     -i sdext/source/pdfimport/xpdfwrapper/wrapper_gpl.cxx &&
# # # 
# # # sed -e 's/printPath( GfxPath/printPath(const GfxPath/' \
# # #     -e 's/  GfxSubpath/ const GfxSubpath/' \
# # #     -i sdext/source/pdfimport/xpdfwrapper/pdfioutdev_gpl.* &&
sed -e "/gzip -f/d"   \
    -e "s|.1.gz|.1|g" \
    -i bin/distro-install-desktop-integration &&

sed -e "/distro-install-file-lists/d" -i Makefile.in &&

sed -e 's@getURI()->c_str()@getURI().c_str()@' \
    -i sdext/source/pdfimport/xpdfwrapper/pdfioutdev_gpl.cxx &&
./autogen.sh  --prefix=$LO_PREFIX         \
             --sysconfdir=/etc           \
            --libdir=/usr/lib64 \
            --localstatedir=/var \
             --with-vendor=BLFS          \
             --with-lang='ru en-US'      \
             --with-help                 \
             --with-myspell-dicts        \
             --without-junit             \
             --without-system-dicts      \
             --disable-firebird-sdbc     \
             --disable-skia              \
             --without-fonts             \
             --enable-gtk3-kde5 \
             --enable-kf5 \
             --disable-odk               \
             --enable-release-build=yes  \
             --enable-python=system      \
             --with-system-apr           \
             --with-system-boost         \
             --with-boost-libdir=/usr/lib64 \
             --with-system-clucene       \
             --with-system-curl          \
             --with-system-epoxy         \
             --with-system-expat         \
             --with-system-glm           \
             --with-system-gpgmepp       \
             --with-system-graphite      \
             --with-system-harfbuzz      \
             --with-system-icu           \
             --with-system-jpeg          \
             --with-system-lcms2         \
             --with-system-libatomic_ops \
             --with-system-libpng        \
             --with-system-libxml        \
             --with-system-neon          \
             --with-system-nss           \
             --with-system-odbc          \
             --with-system-openldap      \
             --with-system-openssl       \
             --with-system-poppler       \
             --with-system-postgresql    \
             --with-system-redland       \
             --with-system-serf          \
             --with-system-zlib          \
             --with-parallelism=4 \
             --with-ant-home=$ANT_HOME \
             --with-jdk-home=$JAVA_HOME
sed -i 's@ -a `id -u` = 0@ -a `id -u` = 2000@g' Makefile
# patch -Np0 -i $PATCHSOURCE/no-check-if-root.diff
export LDFLAGS="-L/usr/lib64 -lboost_system -liconv -lcharset"
make build-nocheck
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
update-desktop-database
