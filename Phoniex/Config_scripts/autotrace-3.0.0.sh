
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
# # git clone https://github.com/sachac/autotrace.git ${packagedir}
# # cd ${packagedir}
# # git checkout graphicsmagick
# # ./autogen.sh

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
  --with-pstoedit \
  --enable-static=no \
  --enable-shared=yes \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
