
###############################################################################
set -e
###############################################################################
# The Emacs package contains an extensible, customizable, self-documenting real-time display editor.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.xz
# Download (FTP): ftp://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.xz
# Download MD5 sum: 0a2e4b965d31a7cb1930eae3b79df793
# Download size: 42 MB
# Estimated disk space required: 434 MB
# Estimated build time: 0.6 SBU (Using parallelism=4)
# Emacs Dependencies
# Recommended
# giflib-5.2.1, GnuTLS-3.6.11.1, and LibTIFF-4.1.0
# Optional
# X Window System, alsa-lib-1.2.1.2, dbus-1.12.16, GConf-3.2.6, gobject-introspection-1.62.0, gsettings-desktop-schemas-3.34.0, GPM-1.20.7, GTK+-2.24.32 or GTK+-3.24.13, ImageMagick-6.9.10-60 libraries (see command explanations), libjpeg-turbo-2.0.4, libpng-1.6.37, librsvg-2.46.4, libxml2-2.9.10, MIT Kerberos V5-1.17.1, Valgrind-3.15.0, intlfonts, libungif, libotf and m17n-lib - to correctly display such complex scripts as Indic and Khmer, and also for scripts that require Arabic shaping support (Arabic and Farsi), and libXaw3d
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
chown -v -R root:root ${pkg}/usr/share/emacs/27.1 &&
rm -vf ${pkg}/usr/lib/systemd/user/emacs.service
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
