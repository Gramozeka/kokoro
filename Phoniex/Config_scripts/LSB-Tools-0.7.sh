
###############################################################################
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/{lib,sbin}
# python2 setup.py install  --optimize=1  --root=${pkg} &&
python3 setup.py install  --optimize=1  --root=${pkg} &&
# 
# ln -sv /usr/lib/lsb/install_initd ${pkg}/usr/sbin/install_initd &&
# ln -sv /usr/lib/lsb/remove_initd  ${pkg}/usr/sbin/remove_initd
# rm ${pkg}/usr/{install_initd.8,LICENSE,INSTALL,lsb_release.1,remove_initd.8}
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
USE_ARCH="64"
ARCH="x86_64" 
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
# python2 setup.py build
python3 setup.py build
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

