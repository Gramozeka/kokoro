
###############################################################################
set -e
###############################################################################
# groupadd -g 21 gdm &&
# useradd -c "GDM Daemon Owner" -d /var/lib/gdm -u 21 \
#         -g gdm -s /bin/false gdm &&
# passwd -ql gdm
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -r '/(^| )systemd_dep/d' meson.build
sed 's@systemd@elogind@' -i data/pam-lfs/gdm-launch-environment.pam &&
# patch -Np1 -i ${PATCHSOURCE}/
# CFLAGS="$CFLAGS" \
# ./configure --prefix=/usr \
# --sysconfdir=/etc \
# --libdir=/usr/lib64 \
# --disable-static \
# --localstatedir=/var \
# --without-plymouth    \
#             --enable-gdm-xsession \
#             --with-initial-vt=7 \
#             --with-systemdsystemunitdir=no \
#             --with-pam-mod-dir=/lib64/security \
# --build=${CLFS_TARGET} \
# --docdir=/usr/share/doc/${packagedir}
# make -j4
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
      -Dgdm-xsession=true         \
      -Dpam-mod-dir=/lib64/security \
      -Dsystemd-journal=false     \
      -Dsystemdsystemunitdir=/tmp \
      -Dsystemduserunitdir=/tmp \
       -Dinitial-vt=7 \
  ..
  ninja -j5


pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# cat >> /etc/elogind/logind.conf << "EOF"
# AllowSuspend=no
# AllowHibernation=no
# EOF
