
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
case $stage in
2)
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
;;
1)
;;
esac
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/make-abicheck-non-fatal.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--with-gtk=2 \
--with-opengl \
--enable-unicode \
--enable-graphics_ctx \
--enable-mediactrl \
--with-regex=builtin \
--with-libpng=sys \
--with-libxpm=sys \
--with-libjpeg=sys \
--with-libtiff=sys \
--disable-precomp-headers \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir} 
  make -j4
  make -C locale allmo
stage=1
pack_local64 ${packagedir}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/make-abicheck-non-fatal.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--with-gtk=3 \
--with-opengl \
--enable-unicode \
--enable-graphics_ctx \
--enable-mediactrl \
--enable-webview \
--with-regex=builtin \
--with-libpng=sys \
--with-libxpm=sys \
--with-libjpeg=sys \
--with-libtiff=sys \
--disable-precomp-headers \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
  make -j4
  make -C locale allmo
stage=2
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
