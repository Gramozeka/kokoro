
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=/usr \
-DLIB_SUFFIX="64" \
-DCMAKE_CXX_FLAGS="${BUILD64}"  \
-DCMAKE_C_FLAGS="${BUILD64}" \
.. &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
