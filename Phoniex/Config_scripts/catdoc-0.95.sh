
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/bin
cd src
/usr/bin/install -c -m 755 catdoc ${pkg}/usr/bin/catdoc
/usr/bin/install -c -m 755 xls2csv ${pkg}/usr/bin/xls2csv
/usr/bin/install -c -m 755 catppt ${pkg}/usr/bin/catppt
/usr/bin/install -c -m 755 wordview ${pkg}/usr/bin/wordview
cd ..
mkdir -pv ${pkg}/usr/share/man/man1
cd doc
/usr/bin/install -c -m 644 catdoc.1 ${pkg}/usr/share/man/man1/catdoc.1
/usr/bin/install -c -m 644 xls2csv.1 ${pkg}/usr/share/man/man1/xls2csv.1
/usr/bin/install -c -m 644 catppt.1 ${pkg}/usr/share/man/man1/catppt.1
/usr/bin/install -c -m 644 wordview.1 ${pkg}/usr/share/man/man1/wordview.1
cd ..
mkdir -pv ${pkg}/usr/share/catdoc
cd charsets
/usr/bin/install -c -m 644 ascii.spc ${pkg}/usr/share/catdoc/ascii.specchars 
/usr/bin/install -c -m 644 tex.spc ${pkg}/usr/share/catdoc/tex.specchars 
/usr/bin/install -c -m 644 ascii.rpl ${pkg}/usr/share/catdoc/ascii.replchars
/usr/bin/install -c -m 644 tex.rpl ${pkg}/usr/share/catdoc/tex.replchars
for i in *.txt; do\
   /usr/bin/install -c -m 0644 $i ${pkg}/usr/share/catdoc;\
done
echo "$1 ----- $(date)" >> ${destdir}/loginstall
# find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
