#!/bin/bash
###############################################################################
set -e
# The International Components for Unicode (ICU) package is a mature, widely used set of C/C++ libraries providing Unicode and Globalization support for software applications. ICU is widely portable and gives applications the same results on all platforms.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://github.com/unicode-org/icu/releases/download/release-65-1/icu4c-65_1-src.tgz
# Download MD5 sum: d1ff436e26cabcb28e6cb383d32d1339
# Download size: 23 MB
# Estimated disk space required: 306 MB (add 34 MB for tests)
# Estimated build time: 0.8 SBU (Using parallelism=4; add 1.9 SBU for tests)
# ICU Dependencies
# Optional
# LLVM-9.0.1 (with Clang), and Doxygen-1.8.17 (for documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd icu
cd source
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
SHELL=/bin/bash \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
    --enable-shared \
    --disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
