
###############################################################################
# The Apache Ant package is a Java-based build tool. In theory, it is like the make command, but without make's wrinkles. Ant is different. Instead of a model that is extended with shell-based commands, Ant is extended using Java classes. Instead of writing shell commands, the configuration files are XML-based, calling out a target tree that executes various tasks. Each task is run by an object that implements a particular task interface.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.apache.org/dist/ant/source/apache-ant-1.10.7-src.tar.xz
# Download MD5 sum: ab7ccbece38850f2fe9258d0ce965786
# Download size: 4.1 MB
# Estimated disk space required: 142 MB
# Estimated build time: 0.3 SBU (excluding download time)
# Apache Ant Dependencies
# Required
# A JDK (Java Binary or OpenJDK-12.0.2) and GLib-2.62.4 '
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line)-src.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/--add-modules java.activation/-html4/' build.xml
mkdir -pv ${pkg}/opt
./bootstrap.sh
bootstrap/bin/ant -f fetch.xml -Ddest=optional
./build.sh -Ddist.dir=$TEMPBUILD/${packagedir} dist
cp -rv $TEMPBUILD/${packagedir} ${pkg}/opt/            &&
chown -R root:root ${pkg}/opt/${packagedir} &&
cd ${pkg}/opt
ln -sfv ${packagedir} ant

mkdir -pv ${pkg}/etc/profile.d
cat > ${pkg}/etc/profile.d/ant.sh << EOF
# Begin /etc/profile.d/ant.sh

pathappend /opt/ant/bin
export ANT_HOME=/opt/ant

# End /etc/profile.d/ant.sh
EOF
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-tree
cd ${pkg}
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home

echo "###########################**** COMPLITE!!! ****##################################"

