
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/{usr,bin}
make prefix=/usr lib=lib64 DESTDIR=${pkg} install                                 &&
mv ${pkg}/usr/bin/traceroute ${pkg}/bin                              &&
ln -sv -f traceroute ${pkg}/bin/traceroute6                    &&
ln -sv -f traceroute.8 ${pkg}/usr/share/man/man8/traceroute6.8 &&
rm -fv ${pkg}/usr/share/man/man1/traceroute.1

# make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
USE_ARCH="64"
ARCH="x86_64" 
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make  VPATH=/usr/lib64
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
