
###############################################################################
set -e
# libclc is an open source, BSD licensed implementation of the library
# requirements of the OpenCL C programming language, as specified by the
# OpenCL 1.1 Specification. The following sections of the specification
# impose library requirements:
# 
#   * 6.1: Supported Data Types
#   * 6.2.3: Explicit Conversions
#   * 6.2.4.2: Reinterpreting Types Using as_type() and as_typen()
#   * 6.9: Preprocessor Directives and Macros
#   * 6.11: Built-in Functions
#   * 9.3: Double Precision Floating-Point
#   * 9.4: 64-bit Atomics
#   * 9.5: Writing to 3D image memory objects
#   * 9.6: Half Precision Floating-Point
# 
# libclc is intended to be used with the Clang compilers OpenCL frontend.
# 
# libclc is designed to be portable and extensible. To this end, it provides
# generic implementations of most library requirements, allowing the target
# to override the generic implementation at the granularity of individual
# functions.
# https://github.com/llvm/llvm-project
# libclc currently only supports the PTX target, but support for more
# targets is welcome.
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/usr/lib64/pkgconfig
cp libclc.pc ${pkg}/usr/lib64/pkgconfig/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
# rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build
cd build
cmake \
    -DCMAKE_C_COMPILER="clang" \
    -DCMAKE_CXX_COMPILER="clang++" \
    -DCMAKE_C_FLAGS:STRING="$CFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLLVM_LIBDIR_SUFFIX=64 \
    -DCMAKE_BUILD_TYPE=Release \
    ..

make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
