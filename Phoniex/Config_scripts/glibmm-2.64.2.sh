
###############################################################################
set -e
# The GLibmm package is a set of C++ bindings for GLib.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/glibmm/2.62/glibmm-2.62.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/glibmm/2.62/glibmm-2.62.0.tar.xz
# Download MD5 sum: 7da228e3f0c6a10024b9a7708c53691e
# Download size: 7.1 MB
# Estimated disk space required: 210 MB (with tests)
# Estimated build time: 1.3 SBU (Using parallelism=4; with tests)
# GLibmm Dependencies
# Required
# GLib-2.62.4 and libsigc++-2.10.2
# Optional
# Doxygen-1.8.17, glib-networking-2.62.3 (for tests), GnuTLS-3.6.11.1 (for tests), and libxslt-1.1.34
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/^libdocdir =/ s/$(book_name)/glibmm-2.64.2/' \
    -i docs/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
