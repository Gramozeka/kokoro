
###############################################################################
set -e
# The libsecret package contains a GObject based library for accessing the Secret Service API.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libsecret/0.20/libsecret-0.20.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libsecret/0.20/libsecret-0.20.0.tar.xz
# Download MD5 sum: 335750caeed47f50496b3b0e6a1875ff
# Download size: 516 KB
# Estimated disk space required: 12 MB (with tests)
# Estimated build time: 0.2 SBU (wtih tests)
# libsecret Dependencies
# Required
# GLib-2.62.4
# Recommended
# gobject-introspection-1.62.0, libgcrypt-1.8.5, and Vala-0.46.5
# Optional
# GTK-Doc-1.32 and docbook-xml-4.5, docbook-xsl-1.79.2, libxslt-1.1.34 (to build manual pages), and Valgrind-3.15.0 (can be used in tests)
# Optional (Required for the testsuite)
# D-Bus Python-1.2.16, Gjs-1.58.4, and PyGObject-3.34.0 (Python 3 module)
# Runtime Dependency
# gnome-keyring-3.34.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dgtk_doc=false \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
