#!/bin/bash
###############################################################################
set -e
# The Gdk Pixbuf package is a toolkit for image loading and pixel buffer manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate images. In the past it was distributed as part of GTK+ 2 but it was split off into a separate package in preparation for the change to GTK+ 3.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gdk-pixbuf/2.40/gdk-pixbuf-2.40.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gdk-pixbuf/2.40/gdk-pixbuf-2.40.0.tar.xz
# Download MD5 sum: 05eb1ebc258ba905f1c8644ef49de064
# Download size: 5.3 MB
# Estimated disk space required: 64 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# Gdk Pixbuf Dependencies
# Required
# GLib-2.62.4, libjpeg-turbo-2.0.4, libpng-1.6.37, and shared-mime-info-1.15
# Recommended
# librsvg-2.46.4 (runtime dependency, needed for loading symbolic icons), LibTIFF-4.1.0, and Xorg Libraries (Many GTK+ applications require gdk-pixbuf-xlib).
# Optional (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# JasPer-2.0.14, GTK-Doc-1.32, and MLib
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# /i686/bin/gdk-pixbuf-query-loaders --update-cache
gdk-pixbuf-query-loaders --update-cache
