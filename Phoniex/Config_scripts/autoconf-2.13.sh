
###############################################################################
set -e
###############################################################################
# Autoconf2.13 is an old version of Autoconf . This old version accepts switches which are not valid in more recent versions. Now that firefox has started to use python2 for configuring, this old version is required even if configure files have not been changed.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/autoconf/autoconf-2.13.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/autoconf/autoconf-2.13.tar.gz
# Download MD5 sum: 9de56d4a161a723228220b0f425dc711
# Download size: 434 KB
# Estimated disk space required: 2.8 MB
# Estimated build time: less than 0.1 SBU (additional 0.1 SBU for the tests)
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/autoconf-2.13-consolidated_fixes-1.patch
# Autoconf2.13 Dependencies
# Optional
# DejaGnu-1.6.2 (Required for the tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/autoconf-2.13-consolidated_fixes-1.patch
mv -v autoconf.texi autoconf213.texi                      &&
rm -v autoconf.info                                       &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--build=${CLFS_TARGET}  \
--program-suffix=2.13
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
