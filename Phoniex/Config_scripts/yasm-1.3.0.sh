
###############################################################################
set -e
# Yasm is a complete rewrite of the NASM-2.14.02 assembler. It supports the x86 and AMD64 instruction sets, accepts NASM and GAS assembler syntaxes and outputs binary, ELF32 and ELF64 object formats.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
# Download MD5 sum: fc9e586751ff789b34b1f21d572d96af
# Download size: 1.5 MB
# Estimated disk space required: 27 MB (additional 12 MB for the tests)
# Estimated build time: 0.1 SBU (additional 0.1 SBU for the tests)
# yasm Dependencies
# Optional
# Python-2.7.17 and Cython
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's#) ytasm.*#)#' Makefile.in &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
