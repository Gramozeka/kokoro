
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

DESTDIR=${pkg} make install
PY2=2.7
PY3=3.8
for i in ccc c++; do
  ln -s /usr/libexec/$i-analyzer \
    ${pkg}/usr/bin/$i-analyzer || exit 1
done

if [ ! -r ${pkg}/usr/bin/lit-cpuid ]; then
  cp -a bin/lit-cpuid ${pkg}/usr/bin/lit-cpuid
  chown root:root ${pkg}/usr/bin/lit-cpuid
  chmod 755 ${pkg}/usr/bin/lit-cpuid
fi

rm -f ${pkg}/usr/lib64/libgomp.so

for pyver in ${PY2} ${PY3}; do
  mkdir -p "${pkg}/usr/lib64/python$pyver/site-packages"
  cp -a ../clang/bindings/python/clang "${pkg}/usr/lib64/python$pyver/site-packages/"
done

# # # rm -f "${pkg}/usr/lib64/python${PY2}/site-packages/six.py"

python -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/clang"
python -O -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/clang"
python3 -m compileall "${pkg}/usr/lib64/python${PY3}/site-packages/clang"
python3 -O -m compileall "${pkg}/usr/lib64/python${PY3}/site-packages/clang"
# # python -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/lldb"
# # python -O -m compileall "${pkg}/usr/lib64/python${PY2}/site-packages/lldb"
# python -m compileall "${pkg}/usr/share/scan-view"
# python -O -m compileall "${pkg}/usr/share/scan-view"
# python -m compileall "${pkg}/usr/share/clang"
# python -O -m compileall "${pkg}/usr/share/clang"
# python -m compileall "${pkg}/usr/share/opt-viewer"
# python -O -m compileall "${pkg}/usr/share/opt-viewer"

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
# git clone https://github.com/llvm/llvm-project $(basename $line)
# cd $(basename $line)
# git submodule update --init --recursive
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
grep -rl '#!.*python' | xargs sed -i '1s/python$/python3/'
cd polly
# patch -Np1 -i ${PATCHSOURCE}/
cat ${PATCHSOURCE}/llvm.polly.hack.diff | patch -p1 --verbose || exit 1
cd ../
mkdir build
cd build
PKG_CONFIG_PATH="/usr/lib64/pkgconfig:/i686/lib/pkgconfig" \
LDFLAGS="-L/usr/lib64:/i686/lib" \
  cmake -G 'Unix Makefiles' \
    -DCMAKE_C_COMPILER="gcc" \
    -DCMAKE_CXX_COMPILER="g++" \
    -DCMAKE_C_FLAGS:STRING="$CFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLLVM_BUILD_LLVM_DYLIB=ON  \
    -DLLVM_LINK_LLVM_DYLIB=ON \
    -DLLVM_LIBDIR_SUFFIX="64" \
    -DCMAKE_BUILD_TYPE=Release \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_FFI=ON \
    -DLLVM_ENABLE_ASSERTIONS=OFF \
    -DLLVM_INSTALL_UTILS=ON \
    -DLLVM_BINUTILS_INCDIR=/usr/include \
    -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;libcxx;libcxxabi;lldb;compiler-rt;lld;polly;libclc;openmp" \
    -DLLVM_TARGETS_TO_BUILD="host;AMDGPU;BPF" \
    -DLLDB_USE_SYSTEM_SIX=1 \
    -DLLVM_DEFAULT_TARGET_TRIPLE:STRING=${CLFS_TARGET} \
    -DLLVM_HOST_TRIPLE:STRING=${CLFS_TARGET} \
    -Wno-dev \
    ../llvm
    make -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
python -m compileall "/usr/share/scan-view"
python -O -m compileall "/usr/share/scan-view"
python -m compileall "/usr/share/clang"
python -O -m compileall "/usr/share/clang"
python -m compileall "/usr/share/opt-viewer"
python -O -m compileall "/usr/share/opt-viewer"
python -m compileall "/usr/lib64/python2.7/site-packages/lldb"
python -O -m compileall "/usr/lib64/python2.7/site-packages/lldb"
