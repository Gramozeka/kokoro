
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/bin
install -D -m 0755 pngcrush ${pkg}/usr/bin/pngcrush
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
#patch -Np1 -i ${PATCHSOURCE}/mypaint-brushes-1.3.0-automake_1.16-1.patch &&
#./autogen.sh
sed -i '/typedef long ptrdiff_t;/s,long,__PTRDIFF_TYPE__,' zutil.h
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
