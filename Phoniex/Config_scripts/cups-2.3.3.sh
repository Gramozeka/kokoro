
###############################################################################
set -e
###############################################################################
# The Common Unix Printing System (CUPS) is a print spooler and associated utilities. It is based on the "Internet Printing Protocol" and provides printing services to most PostScript and raster printers.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/apple/cups/releases/download/v2.3.1/cups-2.3.1-source.tar.gz
# Download MD5 sum: 8ad8897c97cf4d90f20dac4318f47421
# Download size: 7.8 MB
# Estimated disk space required: 64 MB (Add 42 MB for tests)
# Estimated build time: 0.5 SBU (Add 0.9 SBU for tests)
# Cups Dependencies
# Required
# GnuTLS-3.6.11.1
# Recommended
# colord-1.4.4, dbus-1.12.16, libusb-1.0.23, Linux-PAM-1.3.1, and xdg-utils-1.1.3
# Optional
# Avahi-0.7, libpaper-1.1.24+nmu5, MIT Kerberos V5-1.17.1, OpenJDK-12.0.2, PHP-7.4.2, and Python-2.7.17
# Required (Postinstall)
# cups-filters-1.25.6
# Optional (Postinstall)
# Gutenprint-5.3.3 and hplip (HP printers)
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/cups
# Kernel Configuration
# [Note] Note
# There used to be a conflict between the Cups libusb backend and the usblp kernel driver. This is no longer the case and cups will work with both of these enabled.
# If you want to use the kernel usblp driver (for example, if you wish to use escputil from Gutenprint-5.3.3) enable the following options in your kernel configuration and recompile the kernel:
# Device Drivers  --->
#   [*] USB support  --->                          [CONFIG_USB_SUPPORT]
#     <*/M>  OHCI HCD (USB 1.1) support            [CONFIG_USB_OHCI_HCD]
#     <*/M>  UHCI HCD (most Intel and VIA) support [CONFIG_USB_UHCI_HCD]
#     <*/M>  USB Printer support                   [CONFIG_USB_PRINTER]
# If you have a parallel printer, enable the following options in your kernel configuration and recompile the kernel:
# Device Drivers  --->
#   <*/M> Parallel port support  --->    [CONFIG_PARPORT]
#     <*/M> PC-style hardware            [CONFIG_PARPORT_PC]
#   Character devices  --->
#     <*/M> Parallel printer support     [CONFIG_PRINTER]
###############################################################################
useradd -c "Print Service User" -d /var/spool/cups -g lp -s /bin/false -u 9 lp || true
groupadd -g 19 lpadmin || true
usermod -a -G lpadmin root

###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
rm -rf ${pkg}/tmp/cupsinit &&
mkdir -pv ${pkg}/etc/{cups,pam.d}
echo "127.0.0.1 /run/cups/cups.sock" > ${pkg}/etc/cups/client.conf
cat > ${pkg}/etc/pam.d/cups << "EOF"
# Begin /etc/pam.d/cups

auth    include system-auth
account include system-account
session include system-session

# End /etc/pam.d/cups
EOF
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/stat.h/a #include <asm-generic/ioctls.h>' tools/ipptool.c  &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
            --disable-systemd            \
            --with-rcdir=/tmp/cupsinit   \
            --with-system-groups=lpadmin \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
gtk-update-icon-cache -qtf /usr/share/icons/hicolor
