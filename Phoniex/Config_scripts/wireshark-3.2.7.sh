
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
###############################################################################
	USE_ARCH="64" 
		ARCH="x86_64"
			LDFLAGS="-L/lib64:/usr/lib64"
				LD_LIBRARY_PATH="/lib64:/usr/lib64"
					PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
###############################################################################
	pack_local64 () {
	pkg=${destdir}/$1
	 DESTDIR=${pkg} ninja install
###############################################################################
install -v -m755 -d ${pkg}/usr/share/doc/${packagedir} &&
install -v -m644    ../README.linux ../doc/README.* ../doc/{*.pod,randpkt.txt} \
                    ${pkg}/usr/share/doc/${packagedir} &&
cp config.h ${pkg}/usr/include/wireshark/
cp version.h ${pkg}/usr/include/wireshark/
pushd ${pkg}/usr/share/doc/${packagedir} &&
   for FILENAME in ../../wireshark/*.html; do
      ln -s -v -f $FILENAME .
   done &&
popd
unset FILENAME
chown -v root:wireshark ${pkg}/usr/bin/{tshark,dumpcap} &&
chmod -v 6550 ${pkg}/usr/bin/{tshark,dumpcap}
mkdir -pv ${pkg}/usr/lib64/python3.9/site-packages
cp -v ../tools/{wireshark_be.py,wireshark_gen.py} ${pkg}/usr/lib64/python3.9/site-packages/
###############################################################################	
	find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
	find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
					  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
###############################################################################

###############################################################################
	if [ -d ${pkg}/usr/share/man ]; then
  		find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
	fi
###############################################################################

###############################################################################
	cd ${pkg} &&
	mkdir -p usr/tree-info
	find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree
###############################################################################

###############################################################################
	/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
	installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
	echo "$1 ----- $(date)" >> ${destdir}/loginstall	
	rm -rf ${pkg}
	cd $home
	}
###############################################################################
############ 
groupadd -g 62 wireshark || true
###############################################################################
	unpack ${SOURCE}/${package}
	cd ${packagedir}
# 	patch -Np1 -i ${PATCHSOURCE}/sharkd_session.diff
	mkdir build
	cd    build
	CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
	cmake    \
	-DCMAKE_C_FLAGS="$BUILD64"               \
	-DCMAKE_CXX_FLAGS="$BUILD64"          \
	-DCMAKE_INSTALL_PREFIX=/usr                \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib64    \
	-DLIB_INSTALL_DIR=/usr/lib64                   \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX="64"                                           \
	      -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${packagedir} \
      -G Ninja  ..
	ninja -j4

	pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

chown -v root:wireshark /usr/bin/{tshark,dumpcap} &&
chmod -v 6550 /usr/bin/{tshark,dumpcap}

# 	-DG_DISABLE_DEPRECATED =OFF                                  \
# 	-DG_DISABLE_SINGLE_INCLUDES=OFF                                  \
















