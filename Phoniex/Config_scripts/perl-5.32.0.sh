
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
patch -Np1 -i ${SOURCE}/perl.configure.multilib.patch
sed -i -e '/libc/s#/lib/#/lib64/#' hints/linux.sh
echo 'installstyle="lib64/perl5"' >> hints/linux.sh
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./Configure -des \
  -Dprefix=/usr \
  -Darchlib="/usr/lib64/perl5" \
  -Dvendorprefix=/usr \
  -Dinstallprefix=/usr \
  -Dusethreads -Duseithreads \
  -Duseshrplib \
  -Dlibpth="/usr/local/lib64 /usr/lib64 /lib64" \
 -Dcc="gcc ${BUILD64}" \
   -Dldflags="-L/lib64:/usr/lib64" \
  -Ubincompat5005 \
  -Uversiononly \
                    -Dman1dir=/usr/share/man/man1 \
                  -Dman3dir=/usr/share/man/man3 \
  -Dpager='/usr/bin/less -isr' \
  -Darchname=x86_64-linux || exit 1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
