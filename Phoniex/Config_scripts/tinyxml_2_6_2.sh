
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
  install -d -m 0755 ${pkg}/usr/{lib64,include}
  install -m 0755 "libtinyxml.so.0.2.6.2" ${pkg}/usr/lib64/
  install -m 0644 "tinyxml.h" tinystr.h ${pkg}/usr/include
  install -Dm644 readme.txt "${pkg}/usr/share/licenses/tinyxml/LICENSE"
  install -Dm644 "${SOURCE}/tinyxml.pc" "${pkg}/usr/lib64/pkgconfig/tinyxml.pc"

  cd "${pkg}/usr/lib64"
  ln -s "libtinyxml.so.0.2.6.2" "libtinyxml.so.0"
  ln -s "libtinyxml.so.0.2.6.2" "libtinyxml.so"

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd tinyxml
# patch -Np1 -i ${PATCHSOURCE}/

  patch -p0 -i ${PATCHSOURCE}//entity.patch

  # Make TINYXML_USE_STL permanently defined in tinyxml.h
  patch -p1 -i ${PATCHSOURCE}/tinyxml-2.5.3-stl.patch

  # Fix Makefile
  setconf Makefile TINYXML_USE_STL YES
  setconf Makefile RELEASE_CFLAGS "$CXXFLAGS -fPIC"

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
  make
  g++ -fPIC "$CXXFLAGS" -shared -o "libtinyxml.so.0.2.6.2" \
      -Wl,-soname,"libtinyxml.so.0" $(ls *.o | grep -v xmltest)
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
