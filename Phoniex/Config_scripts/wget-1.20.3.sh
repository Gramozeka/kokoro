
###############################################################################
set -e
###############################################################################
# The Wget package contains a utility useful for non-interactive downloading of files from the Web.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/wget/wget-1.20.3.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/wget/wget-1.20.3.tar.gz
# Download MD5 sum: db4e6dc7977cbddcd543b240079a4899
# Download size: 4.2 MB
# Estimated disk space required: 60 MB (with tests)
# Estimated build time: 0.4 SBU (with tests)
# Wget Dependencies
# Recommended
# make-ca-1.5 (runtime)
# Optional
# GnuTLS-3.6.11.1, HTTP-Daemon-6.05 (for the test suite), IO-Socket-SSL-2.066 (for the test suite), libidn2-2.3.0, libpsl-0.21.0, PCRE-8.43 or pcre2-10.34, and Valgrind-3.15.0 (for the test suite)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-ssl=openssl \
--build=${CLFS_TARGET}  \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
