
###############################################################################
set -e
###############################################################################
# The Totem PL Parser package contains a simple GObject-based library used to parse multiple playlist formats.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/totem-pl-parser/3.26/totem-pl-parser-3.26.4.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/totem-pl-parser/3.26/totem-pl-parser-3.26.4.tar.xz
# Download MD5 sum: 70fc2731fd2c7876bf1ba81e7918a0b5
# Download size: 1.3 MB
# Estimated disk space required: 7.6 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# Totem PL Parser Dependencies
# Required
# libsoup-2.68.3
# Recommended
# gobject-introspection-1.62.0, libarchive-3.4.1, and libgcrypt-1.8.5
# Optional
# CMake-3.16.3 (for CMake Bindings), GTK-Doc-1.32, Gvfs-1.42.2 (for some tests), LCOV, and libquvi >= 0.9.1 and libquvi-scripts - if they are installed, then lua-socket (git) is necessary for the tests
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
