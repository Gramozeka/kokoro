
###############################################################################
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# python3 setup.py install  --optimize=1  --root=${pkg} &&
make DESTDIR=${pkg} install  -C sipgen
make DESTDIR=${pkg} install
  mv ${pkg}/usr/include/{python*/sip.h,}
  rm -r ${pkg}/usr/include/python*
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
python build.py prepare
    mkdir build
    cd    build

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
python  ../configure.py
make
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

