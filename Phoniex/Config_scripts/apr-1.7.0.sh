
###############################################################################
set -e
# The Apache Portable Runtime (APR) is a supporting library for the Apache web server. It provides a set of application programming interfaces (APIs) that map to the underlying Operating System (OS). Where the OS doesnt support a particular function, APR will provide an emulation. Thus programmers can use the APR to make a program portable across different platforms.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.apache.org/dist/apr/apr-1.7.0.tar.bz2
# Download (FTP): ftp://ftp.mirrorservice.org/sites/ftp.apache.org/apr/apr-1.7.0.tar.bz2
# Download MD5 sum: 7a14a83d664e87599ea25ff4432e48a7
# Download size: 852 KB
# Estimated disk space required: 11 MB (additional 4 MB for the tests)
# Estimated build time: 0.2 SBU (add 1.7 SBU for tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir} \
--with-installbuilddir=/usr/share/apr-1/build &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
