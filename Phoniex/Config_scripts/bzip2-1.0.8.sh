
###############################################################################
pack_local () {
pkg=${destdir}/$1
rm -rf ${pkg}/i686
mkdir -pv ${pkg}/usr/{bin,lib64}
mkdir -pv  ${pkg}/{bin,lib64}
make CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" PREFIX=${pkg}/usr install
cp -vf bzip2-shared ${pkg}/bin/bzip2
cp -avf libbz2.so* ${pkg}/lib64
ln -svf ../../lib64/libbz2.so.1.0 ${pkg}/usr/lib64/libbz2.so
rm -vf ${pkg}/usr/bin/{bunzip2,bzcat,bzip2}
ln -svf bzip2 ${pkg}/bin/bunzip2
ln -svf bzip2 ${pkg}/bin/bzcat

if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
USE_ARCH="64"
unpack ${SOURCE}/${package}
cd ${packagedir}
rm -rf /usr/bin/bzegrep
rm -rf /usr/bin/bzfgrep
rm -rf /usr/bin/bzless
rm -rf /usr/bin/bzcmp
ARCH="x86_64"
patch -Np1 -i ${SOURCE}/bzip2-1.0.8-install_docs-1.patch
sed -i -e 's:ln -s -f $(PREFIX)/bin/:ln -s :' Makefile
sed -i 's@X)/man@X)/share/man@g' ./Makefile
sed -i 's@/lib\(/\| \|$\)@/lib64\1@g' Makefile
make -f Makefile-libbz2_so CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}"
make clean
make CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}"
pack_local ${packagedir}

