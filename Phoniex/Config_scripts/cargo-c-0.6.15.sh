###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# make DESTDIR=${pkg} install
  cargo install \
    --frozen \
    --offline \
    --no-track \
    --path . \
    --root ${pkg}/opt/rustc
# install -D -m755 "${packagedir}/target/release/"cargo-c{build,install,api} -t "${pkg}/usr/bin"
# install -D -m755 "${packagedir}/target/release/"libcargo_c.rlib -t "${pkg}/usr/lib64"
echo "$1 ----- $(date)" >> ${destdir}/loginstall
# find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
# find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
#   | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
# if [ -d ${pkg}/usr/share/man ]; then
#   find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
# fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
#     ln -sf "../${packagedir}.Cargo.lock" "${packagedir}/Cargo.lock"
#     cargo fetch --manifest-path="${packagedir}/Cargo.toml"
#     cargo build --release --manifest-path="${packagedir}/Cargo.toml"
#   cargo fetch \
#     --locked \
#     --offline \
#     --manifest-path Cargo.toml
      cargo build \
    --release \
    --frozen \
    --manifest-path Cargo.toml
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
