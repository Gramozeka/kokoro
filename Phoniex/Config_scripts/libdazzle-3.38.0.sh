
###############################################################################
set -e
# libdazzle is a companion library to GObject and GTK+ that adds APIs for special graphical effects.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libdazzle/3.34/libdazzle-3.34.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libdazzle/3.34/libdazzle-3.34.1.tar.xz
# Download MD5 sum: e796a92dd3f529616ed388c15208359b
# Download size: 432 KB
# Estimated disk space required: 34 MB (with tests)
# Estimated build time: 0.2 SBU (uning parallelism=4; with tests)
# libdazzle Dependencies
# Required
# GTK+-3.24.13
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
