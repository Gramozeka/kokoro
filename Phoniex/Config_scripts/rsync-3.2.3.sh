
###############################################################################
set -e
###############################################################################
groupadd -g 48 rsyncd || true
useradd -c "rsyncd Daemon" -d /home/rsync -g rsyncd \
    -s /bin/false -u 48 rsyncd || true
###############################################################################
# The rsync package contains the rsync utility. This is useful for synchronizing large file archives over a network.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.samba.org/ftp/rsync/src/rsync-3.1.3.tar.gz
# Download MD5 sum: 1581a588fde9d89f6bc6201e8129afaf
# Download size: 884 KB
# Estimated disk space required: 11 MB (with tests - additional 45 MB for HTML API documentation)
# Estimated build time: 0.5 SBU (with tests)
# rsync Dependencies
# Recommended
# popt-1.16
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--disable-lz4    \
 --without-included-zlib \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
