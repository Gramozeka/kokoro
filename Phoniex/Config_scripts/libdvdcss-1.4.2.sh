
###############################################################################
set -e
# libdvdcss is a simple library designed for accessing DVDs as a block device without having to bother about the decryption.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://get.videolan.org/libdvdcss/1.4.2/libdvdcss-1.4.2.tar.bz2
# Download MD5 sum: 7b74f2e142b13c9de6dc8d807ab912d4
# Download size: 360 KB
# Estimated disk space required: 3.2 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
