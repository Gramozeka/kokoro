###############################################################################
pack_local64 () {
pkg=${destdir}/$1
cd ../speech_tools
  install -dm755 ${pkg}/usr/{bin,lib64,include/speech_tools/{,instantiate,ling_class,rxp,sigpr,unix}}

  #binaries
  #move binaries over wrappers (FS#7864)
  for i in $(grep -l 'EST shared script' bin/*); do
    cp -f main/$(basename $i) bin;
  done
  install -m755 -t ${pkg}/usr/bin bin/[a-z]*
  rm -f ${pkg}/usr/bin/{est_gdb,est_examples,est_program}

  #libraries
  install -m755 -t ${pkg}/usr/lib64 lib/lib*.so.*
  ln -sf libestbase.so.${pkgver}.1 ${pkg}/usr/lib64/libestbase.so
  ln -sf libestools.so.${pkgver}.1 ${pkg}/usr/lib64/libestools.so
  ln -sf libeststring.so.1.2 ${pkg}/usr/lib64/libeststring.so

  #headers
  for dir in {.,instantiate,ling_class,rxp,sigpr,unix}; do
    install -m644 -t ${pkg}/usr/include/speech_tools/$dir include/$dir/*.h
  done
# 
# 
# make DESTDIR=${pkg} install
cd ../festival
  #binaries
  install -m755 src/main/festival ${pkg}/usr/bin/
  install -m755 src/main/festival_client ${pkg}/usr/bin/
  install -m755 examples/benchmark ${pkg}/usr/bin/
  install -m755 examples/dumpfeats ${pkg}/usr/bin/
  install -m755 examples/durmeanstd ${pkg}/usr/bin/
  install -m755 examples/latest ${pkg}/usr/bin/
  install -m755 examples/make_utts ${pkg}/usr/bin/
  install -m755 examples/powmeanstd ${pkg}/usr/bin/
  install -m755 examples/run-festival-script ${pkg}/usr/bin/
  install -m755 examples/saytime ${pkg}/usr/bin/
  install -m755 examples/scfg_parse_text ${pkg}/usr/bin/
  install -m755 examples/text2pos ${pkg}/usr/bin/
  install -m755 examples/text2wave ${pkg}/usr/bin
  install -m755 examples/text2utt ${pkg}/usr/bin

  #libraries
  install -m755 src/lib/libFestival.so.* ${pkg}/usr/lib64/
  ln -sf libFestival.so.2.5.0.0 ${pkg}/usr/lib64/libFestival.so

  #headers
  install -dm755 ${pkg}/usr/include/festival
  install -m644 -t ${pkg}/usr/include/festival src/include/*.h

  mkdir -p ${pkg}/usr/share/festival
  cp -aR lib/* ${pkg}/usr/share/festival
  rm -fv $(find ${pkg}/usr/share/festival -name Makefile)
  rm -fv $(find ${pkg}/usr/bin -name Makefile)

  #create voices directory
  install -dm755 ${pkg}/usr/share/festival/voices/russian

  # Ok now some general cleanups
  for i in $(find ${pkg}/usr/include/ -type f); do
    sed -i -e 's,"EST.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/\",speech_tools/,g' \
      -e 's,"siod.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/\",speech_tools/,g' \
      -e 's,"instantiate/.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/instantiate/\",speech_tools/instantiate/,g' -e 's,"instantiate,instantiate,g' \
      -e 's,"ling_class/.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/ling_class/\",speech_tools/ling_class/,g' -e 's,"ling_class,ling_class,g' \
      -e 's,"rxp/.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/rxp/\",speech_tools/rxp/,g' -e 's,"rxp,rxp,g' \
      -e 's,"sigpr/.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/sigpr/\",speech_tools/sigpr/,g' -e 's,"sigpr,sigpr,g' \
      -e 's,"unix/.*\.h",\<speech_tools/&\>,g' -e 's,speech_tools/unix/\",speech_tools/unix/,g' -e 's,\.h\">,.h\>,g' -e 's,"unix,unix,g' \
      -e 's,"festival\.h",\<festival/festival.h\>,g' \
      -e 's,"ModuleDescription\.h",\<festival/ModuleDescription.h\>,g' \
      -e 's,"Phone\.h",\<festival/Phone.h\>,g' $i
  done

unpack ${SOURCE}/festival/msu_ru_nsh_clunits-0.5.tar.bz2
mv msu_ru_nsh_clunits  ${pkg}/usr/share/festival/voices/russian/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/festival/${package}
unpack ${SOURCE}/festival/speech_tools-2.5.0-release.tar.gz


  patch -Np0 -i ${SOURCE}/festival/speechconfig.patch
  patch -Np0 -i ${SOURCE}/festival/festconfig.patch

  # build shared libs - taken from Mageia
  patch -Np0 -i ${SOURCE}/festival/festival-shared-build.patch

  patch -p0 -i ${SOURCE}/festival/festival-2.5.0-compile.patch # Fix build (OpenMandriva)

  # Avoid make failure on making scripts and docs
  sed -i "s|examples bin doc|examples|" festival/Makefile
# patch -Np1 -i ${PATCHSOURCE}/
cd speech_tools

patch -Np1 -i ${SOURCE}/festival/editline.diff
CFLAGS="$CFLAGS -fcommon" \
CXXFLAGS=$CFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make OPTIMISE_CXXFLAGS="${CXXFLAGS}  -fcommon" OPTIMISE_CCFLAGS="${CFLAGS}  -fcommon"

cd ../festival
CFLAGS="$CFLAGS -fcommon" \
CXXFLAGS=$CFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} 
make OPTIMISE_CXXFLAGS="${CXXFLAGS}  -fcommon" OPTIMISE_CCFLAGS="${CFLAGS}  -fcommon"
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
