
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vfi
sed -i '/tp_print/d' python/enum.c
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS="$CFLAGS -DNDEBUG" \
CXXFLAGS=$CXXFLAGS \
PKG_CONFIG_PATH=/usr/lib64/pkgconfig \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-qt --with-gtk=gtk3 --with-dbusconfdir=/usr/share \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
