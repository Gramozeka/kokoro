
###############################################################################
set -e
###############################################################################
# GeoClue is a modular geoinformation service built on top of the D-Bus messaging system. The goal of the GeoClue project is to make creating location-aware applications as simple as possible.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gitlab.freedesktop.org/geoclue/geoclue/-/archive/2.5.5/geoclue-2.5.5.tar.bz2
# Download MD5 sum: 1efaaec52a9589cf98e53be5a88d529c
# Download size: 84 KB
# Estimated disk space required: 6.4 MB
# Estimated build time: less than 0.1 SBU
# GeoClue Dependencies
# Required
# JSON-GLib-1.4.4 and libsoup-2.68.3
# Recommended
# ModemManager-1.12.4, Vala-0.46.5, and Avahi-0.7
# Optional
# GTK-Doc-1.32 and libnotify-0.7.8
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dgtk-doc=false \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
