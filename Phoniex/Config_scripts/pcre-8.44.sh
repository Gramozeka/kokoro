
###############################################################################
set -e
# The PCRE package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.pcre.org/pub/pcre/pcre-8.43.tar.bz2
# Download (FTP): ftp://ftp.pcre.org/pub/pcre/pcre-8.43.tar.bz2
# Download MD5 sum: 636222e79e392c3d95dcc545f24f98c4
# Download size: 1.5 MB
# Estimated disk space required: 19 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# PCRE Dependencies
# Optional
# Valgrind-3.15.0
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/lib64
mv -v ${pkg}/usr/lib64/libpcre.so.* ${pkg}/lib64 &&
ln -sfv ../../lib64/$(readlink ${pkg}/usr/lib64/libpcre.so) ${pkg}/usr/lib64/libpcre.so
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--docdir=/usr/share/doc/${packagedir} \
            --enable-unicode-properties       \
            --enable-pcre16                   \
            --enable-pcre32                   \
            --enable-pcregrep-libz            \
            --enable-pcregrep-libbz2          \
            --enable-pcretest-libreadline &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
