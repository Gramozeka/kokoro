
###############################################################################
set -e
###############################################################################
pack_local32 () {
pkg=${destdir}/$1
make prefix=/i686  libdir=/i686/lib install DESTDIR=${pkg}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/i686/share/man ]; then
rm -rf ${pkg}/i686/share/man
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
setarch i386 make prefix=/usr  libdir=/usr/lib64 install DESTDIR=${pkg}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
USE_ARCH="32"
ARCH="i686"
SAVE_path=${PATH}
PATH="${PATH32}:${PATH}"
export AR="/i686/bin/ar"
export AS="/i686/bin/as"
export RANLIB="/i686/bin/ranlib"
export LD="/i686/bin/ld"
export STRIP="/i686/bin/strip"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH32}
export LD_LIBRARY_PATH="/i686/lib" 
export LDFLAGS="-L/i686/lib" 
export CPPFLAGS="-I/i686/include" 
CC="gcc ${BUILD32}" CXX="g++ ${BUILD32}" \
setarch i386 make  -j4
pack_local32 ${packagedir}
export PATH=${SAVE_path}
unset AR AS RANLIB LD STRIP LD_LIBRARY_PATH LDFLAGS CPPFLAGS
source /etc/profile &&
echo "###########################**** COMPLITE!!! ****##################################"
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
USE_ARCH="64"
ARCH="x86_64" 
LDFLAGS="-L/lib64:/usr/lib64" \
LD_LIBRARY_PATH="/lib64:/usr/lib64"
PKG_CONFIG_PATH=${PKG_CONFIG_PATH64} \
CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
