
###############################################################################
set -e
# The At-Spi2 Atk package contains a library that bridges ATK to At-Spi2 D-Bus service.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/2.34/at-spi2-atk-2.34.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/2.34/at-spi2-atk-2.34.1.tar.xz
# Download MD5 sum: e0f99641c5a403041c4214be04722e15
# Download size: 96 KB
# Estimated disk space required: 9.4 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# At-Spi2 Atk Dependencies
# Required
# at-spi2-core-2.34.0 and ATK-2.34.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas
