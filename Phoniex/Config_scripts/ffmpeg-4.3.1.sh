
###############################################################################
set -e
# FFmpeg is a solution to record, convert and stream audio and video. It is a very fast video and audio converter and it can also acquire from a live audio/video source. Designed to be intuitive, the command-line interface (ffmpeg) tries to figure out all the parameters, when possible. FFmpeg can also convert from any sample rate to any other, and resize video on the fly with a high quality polyphase filter. FFmpeg can use a Video4Linux compatible video source and any Open Sound System audio source.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ffmpeg.org/releases/ffmpeg-4.2.2.tar.xz
# Download MD5 sum: 348956fc2faa57a2f79bbb84ded9fbc3
# Download size: 8.7 MB
# Estimated disk space required: 160 MB (add 3.5 GB for the FATE suite/tests, add 655 MB for docs)
# Estimated build time: 1.4 SBU (using parallelism=4; add 5.9 SBU (with THREADS=4) to run the FATE suite after sample files are downloaded; add 0.5 SBU for docs)
# FFmpeg Dependencies
# Recommended
# libass-0.14.0, fdk-aac-2.0.1, FreeType-2.10.1, LAME-3.100, libtheora-1.1.1, libvorbis-1.3.6, libvpx-1.8.2, Opus-1.3.1, x264-20190815-2245, x265-3.2.1, and yasm-1.3.0
# Recommended for desktop use
# alsa-lib-1.2.1.2, libva-2.6.1, libvdpau-1.3 (with the corresponding driver package), and SDL2-2.0.10,
# Optional
# Fontconfig-2.13.1, frei0r-plugins-1.7.0, libcdio-2.1.0 (to identify and play CDs), libwebp-1.1.0, opencv-4.2.0, OpenJPEG-2.3.1, GnuTLS-3.6.11.1, PulseAudio-13.0, Speex-1.2.0, texlive-20190410 (or install-tl-unx) for PDF and PS documentation, v4l-utils-1.18.0, XviD-1.3.7, X Window System, Flite, GSM, libaacplus, libbluray, libcaca, libcelt, libdc1394, libdca, libiec61883, libilbc, libmodplug, libnut (Git checkout), librtmp, libssh, OpenAL, OpenCore AMR, Schroedinger, TwoLAME, vo-aaenc, vo-amrwbenc, and ZVBI
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -m755    tools/qt-faststart ${pkg}/usr/bin
doxygen doc/Doxyfile
install -v -m755 -d           ${pkg}/usr/share/doc/${packagedir} &&
install -v -m644    doc/*.txt ${pkg}/usr/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/-lflite"/-lflite -lasound"/' configure &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--cc="gcc ${BUILD64}" \
    --{,sh}libdir=/usr/lib64 \
    --enable-pthreads \
    --enable-postproc \
            --enable-gpl         \
            --enable-version3    \
            --enable-nonfree     \
            --disable-static     \
            --enable-shared      \
            --disable-debug      \
            --enable-libdrm      \
            --enable-avresample  \
            --enable-libass      \
            --enable-libfdk-aac  \
            --enable-libfreetype \
            --enable-libmp3lame  \
            --enable-libopus     \
            --enable-libtheora   \
            --enable-libvorbis   \
            --enable-libvpx      \
            --enable-libpulse    \
            --enable-libx264     \
            --enable-libx265     \
             --disable-doc    \
            --docdir=/usr/share/doc/${packagedir}
make -j4
gcc tools/qt-faststart.c -o tools/qt-faststart
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
