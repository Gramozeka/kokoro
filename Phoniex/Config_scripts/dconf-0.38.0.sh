
###############################################################################
set -e
###############################################################################
# The DConf package contains a low-level configuration system. Its main purpose is to provide a backend to GSettings on platforms that don't' already have configuration storage systems.
# The DConf-Editor, as the name suggests, is a graphical editor for the DConf database. Installation is optional, because gsettings from GLib-2.62.4 provides similar functionality on the commandline.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/dconf/0.34/dconf-0.34.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/dconf/0.34/dconf-0.34.0.tar.xz
# Download MD5 sum: a3ab18ed51a0494a1c8436fef20cc1b0
# Download size: 112 KB
# Estimated disk space required: 6.9 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# Additional Downloads
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/dconf-editor/3.34/dconf-editor-3.34.3.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/dconf-editor/3.34/dconf-editor-3.34.3.tar.xz
# Download MD5 sum: 4a2cf532dee3ab48eae500140450d605
# Download size: 552 KB
# Estimated disk space required: 28 MB
# Estimated build time: less than 0.1 SBU (Using parallelism=4)
# DConf Dependencies
# Required
# dbus-1.12.16, GLib-2.62.4, GTK+-3.24.13 (for the editor), and libxml2-2.9.10 (for the editor)
# Recommended
# libxslt-1.1.34 and Vala-0.46.5
# Optional
# GTK-Doc-1.32 and bash-completion
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# sed -i 's|link_whole|link_with|' client/meson.build &&
# sed -i 's/module/& | grep -v mangle_path/' gsettings/abicheck.sh
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
