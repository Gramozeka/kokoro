
###############################################################################
groupadd -g 83 ldap &&
useradd  -c "OpenLDAP Daemon Owner" \
         -d /var/lib/openldap -u 83 \
         -g ldap -s /bin/false ldap
###############################################################################
set -e
# The OpenLDAP package provides an open source implementation of the Lightweight Directory Access Protocol.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (FTP): ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/openldap-2.4.48.tgz
# Download MD5 sum: 0729a0711fe096831dedc159e0bbe73f
# Download size: 5.5 MB
# Estimated disk space required: 49 MB (client), 89 MB (server)
# Estimated build time: 0.4 SBU (client using parallelism=4), 1.2 SBU (server)
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/openldap-2.4.48-consolidated-1.patch
# OpenLDAP Dependencies
# Recommended
# Cyrus SASL-2.1.27
# Optional
# ICU-65.1, GnuTLS-3.6.11.1, Pth-2.0.7, unixODBC-2.3.7, MariaDB-10.4.11 or PostgreSQL-12.1 or MySQL, OpenSLP, and Berkeley DB-5.3.28 (for slapd, but deprecated)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/var/lib
mkdir -pv ${pkg}/etc/openldap
mkdir -pv ${pkg}/usr/share/doc
rm -rf ${pkg}/var/run
install -v -dm700 -o ldap -g ldap ${pkg}/var/lib/openldap     &&

install -v -dm700 -o ldap -g ldap ${pkg}/etc/openldap/slapd.d &&
chmod   -v    640     ${pkg}/etc/openldap/slapd.{conf,ldif}   &&
chown   -v  root:ldap ${pkg}/etc/openldap/slapd.{conf,ldif}   &&

install -v -dm755 ${pkg}/usr/share/doc/${packagedir} &&
cp      -vfr      doc/{drafts,rfc,guide} \
                  ${pkg}/usr/share/doc/${packagedir}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/openldap-2.4.53-consolidated-1.patch &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoconf &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib64 \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --disable-static      \
            --disable-debug       \
            --with-tls=openssl    \
            --with-cyrus-sasl     \
            --enable-dynamic      \
            --enable-crypt        \
            --enable-spasswd      \
            --enable-slapd        \
            --enable-modules      \
            --enable-rlookups     \
            --enable-backends=mod \
            --disable-ndb         \
            --disable-sql         \
            --disable-shell       \
            --disable-bdb         \
            --disable-hdb         \
            --enable-overlays=mod  --build=${CLFS_TARGET} &&

make depend &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


