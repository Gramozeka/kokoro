
###############################################################################
set -e
###############################################################################
# GDB, the GNU Project debugger, allows you to see what is going on “inside” another program while it executes -- or what another program was doing at the moment it crashed. Note that GDB is most effective when tracing programs and libraries that were built with debugging symbols and not stripped.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/gdb/gdb-8.3.1.tar.xz
# Download (FTP): ftp://ftp.gnu.org/gnu/gdb/gdb-8.3.1.tar.xz
# Download MD5 sum: 73b6a5d8141672c62bf851cd34c4aa83
# Download size: 20 MB
# Estimated disk space required: 625 MB (add 57 MB for tests, add 748 MB for docs)
# Estimated build time: 1.9 SBU (Using parallelism=4; add 66 SBU for tests, add 0.6 SBU for docs)
# GDB Dependencies
# Recommended Runtime Dependency
# six-1.14.0 (Python 3 module, required at run-time to use GDB scripts from various LFS/BLFS packages with Python 3 installed in LFS)
# Optional
# DejaGnu-1.6.2 (required for tests), Doxygen-1.8.17, GCC-9.2.0 (ada and gfortran are used for tests), Guile-3.0.0, Python-2.7.17, rustc-1.37.0 (used for some tests), Valgrind-3.15.0, and SystemTap (run-time, used for tests)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -d ${pkg}/usr/share/doc/${packagedir} &&
rm -rf gdb/doc/doxy/xml &&
cp -Rv gdb/doc/doxy ${pkg}/usr/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
PATH=$(echo $PATH | sed "s|/usr/libexec/icecc/bin||g" | tr -s : | sed "s/^://g" | sed "s/:$//g")
sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure
# patch -Np1 -i ${PATCHSOURCE}/
mkdir build &&
cd    build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
    --disable-nls \
    --enable-source-highlight \
    --enable-tui \
    --with-system-readline \
    --with-python=/usr/bin/python3 \
    --with-guile=guile-3.0 \
    --with-system-gdbinit=/etc/gdb/gdbinit \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
make -C gdb/doc doxy
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
