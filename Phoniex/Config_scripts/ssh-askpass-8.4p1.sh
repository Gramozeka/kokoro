
###############################################################################
set -e
###############################################################################
# The ssh-askpass is a generic executable name for many packages, with similar names, that provide a interactive X service to grab password for packages requiring administrative privileges to be run. It prompts the user with a window box where the necessary password can be inserted. Here, we choose Damien Miller's package distributed in the OpenSSH tarball.'
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-8.1p1.tar.gz
# Download (FTP): ftp://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-8.1p1.tar.gz
# Download MD5 sum: bf050f002fe510e1daecd39044e1122d
# Download size: 1.5 MB
# Estimated disk space required: 8.6 MB
# Estimated build time: less than 0.1 SBU
# ssh-askpass Dependencies
# Required
# GTK+-2.24.32, Sudo-1.8.30 (runtime), Xorg Libraries, and X Window System (runtime)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1

install -v -d -m755 ${pkg}/usr/libexec/openssh/contrib  &&
install -v -m755    gnome-ssh-askpass2 \
                    ${pkg}/usr/libexec/openssh/contrib  &&
ln -sv -f contrib/gnome-ssh-askpass2 \
                    ${pkg}/usr/libexec/openssh/ssh-askpass

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package=openssh-8.4p1.tar.gz
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd openssh-8.4p1
# patch -Np1 -i ${PATCHSOURCE}/

cd contrib &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make gnome-ssh-askpass2
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cat >> /etc/sudo.conf << "EOF" &&
# Path to askpass helper program
Path askpass /usr/libexec/openssh/ssh-askpass
EOF
chmod -v 0644 /etc/sudo.conf
