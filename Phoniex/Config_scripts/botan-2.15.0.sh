
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
python configure.py \
  --prefix=/usr \
  --libdir=lib64 \
  --with-doxygen \
--docdir=share/doc/${packagedir}
make
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
