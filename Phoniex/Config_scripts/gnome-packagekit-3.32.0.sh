
###############################################################################
set -e
###############################################################################

###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} ninja install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
USE_ARCH="64" 
ARCH="x86_64"
LDFLAGS="-L/lib64:/usr/lib64" 
LD_LIBRARY_PATH="/lib64:/usr/lib64" 
export CFLAGS="${BUILD64}"
export CXXFLAGS="${BUILD64}"
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
# CFLAGS="-O2 -fno-strict-aliasing" \
meson setup \
  --prefix=/usr \
  --libdir=lib64 \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
update-desktop-database
glib-compile-schemas /usr/share/glib-2.0/schemas

