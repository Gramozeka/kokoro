
###############################################################################
set -e
###############################################################################
# The rest package contains a library that was designed to make it easier to access web services that claim to be "RESTful". It includes convenience wrappers for libsoup and libxml to ease remote use of the RESTful API.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/rest/0.8/rest-0.8.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/rest/0.8/rest-0.8.1.tar.xz
# Download MD5 sum: ece4547298a81105f307369d73c21b9d
# Download size: 332 KB
# Estimated disk space required: 12.5 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# rest Dependencies
# Required
# make-ca-1.5 and libsoup-2.68.3
# Recommended
# gobject-introspection-1.62.0
# Optional
# GTK-Doc-1.32 and LCOV
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--with-ca-certificates=/etc/pki/tls/certs/ca-bundle.crt \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
