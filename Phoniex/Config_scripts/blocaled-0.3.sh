
###############################################################################
set -e
###############################################################################
# blocaled is an implementation of the org.freedesktop.locale1 D–Bus protocol, which normally comes with systemd. It is needed by the GNOME desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/pierre-labastie/blocaled/releases/download/v0.2/blocaled-0.2.tar.xz
# Download MD5 sum: bb9935b6ef727d50c7a733e9fdee40bf
# Download size: 256 KB
# Estimated disk space required: 4.2 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# blocaled Dependencies
# Required
# Polkit-0.116 and libdaemon-0.14
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/etc/profile.d
cat > ${pkg}/etc/profile.d/i18n.sh << "EOF"
# Begin /etc/profile.d/i18n.sh

if [ -r /etc/locale.conf ]; then source /etc/locale.conf; fi

if [ -n "$LANG" ];              then export LANG; fi
if [ -n "$LC_TYPE" ];           then export LC_TYPE; fi
if [ -n "$LC_NUMERIC" ];        then export LC_NUMERIC; fi
if [ -n "$LC_TIME" ];           then export LC_TIME; fi
if [ -n "$LC_COLLATE" ];        then export LC_COLLATE; fi
if [ -n "$LC_MONETARY" ];       then export LC_MONETARY; fi
if [ -n "$LC_MESSAGES" ];       then export LC_MESSAGES; fi
if [ -n "$LC_PAPER" ];          then export LC_PAPER; fi
if [ -n "$LC_NAME" ];           then export LC_NAME; fi
if [ -n "$LC_ADDRESS" ];        then export LC_ADDRESS; fi
if [ -n "$LC_TELEPHONE" ];      then export LC_TELEPHONE; fi
if [ -n "$LC_MEASUREMENT" ];    then export LC_MEASUREMENT; fi
if [ -n "$LC_IDENTIFICATION" ]; then export LC_IDENTIFICATION; fi

# End /etc/profile.d/i18n.sh
EOF

cat > ${pkg}/etc/locale.conf << EOF
# Begin /etc/locale.conf

# LANG=C
# LC_TYPE=C
# LC_NUMERIC=C
# LC_TIME=C
# LC_COLLATE=C
# LC_MONETARY=C
# LC_MESSAGES=C
# LC_PAPER=C
#  LC_NAME=C
# LC_ADDRESS=C
# LC_TELEPHONE=C
# LC_MEASUREMENT=C
# LC_IDENTIFICATION=C
# LC_ALL=C

LANG=ru_RU.UTF-8
LC_TYPE=ru_RU.UTF-8
LC_NUMERIC=ru_RU.UTF-8
LC_TIME=ru_RU.UTF-8
LC_COLLATE=ru_RU.UTF-8
LC_MONETARY=ru_RU.UTF-8
LC_MESSAGES=ru_RU.UTF-8
LC_PAPER=ru_RU.UTF-8
 LC_NAME=ru_RU.UTF-8
LC_ADDRESS=ru_RU.UTF-8
LC_TELEPHONE=ru_RU.UTF-8
LC_MEASUREMENT=ru_RU.UTF-8
LC_IDENTIFICATION=ru_RU.UTF-8
LC_ALL=ru_RU.UTF-8

# End /etc/locale.conf
EOF


echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib64 \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
