#!/bin/bash
texlive-20200406-source
dvisvgm-2.10
ghostscript-9.53.3
asymptote-2.67
dblatex-0.3.11py3
djvulibre-3.5.27
bcg729-master
libspectre-0.2.9
chmlib-0.40
libsmi-0.5.0
libmaxminddb-1.4.3
poppler-20.10.0
snappy-1.1.8
gtk-vnc-1.0.0
spice-protocol-0.14.0
spice-0.14.2
spice-gtk-0.37
xf86-video-qxl-0.1.5
x11spice-master
spice-vdagent-0.19.0
Catch2-2.11.0
spice-streaming-agent-master
usbredir-git
jemalloc-git
wolfssl-git
# vde-2-git
virglrenderer-master
SDL_image-1.2.12
SDL2_image-2.0.5
SDL2_mixer-2.0.4
libiconv-1.16
qemu-5.1.0
# # # aqemu-git
virt-viewer-9.0
osinfo-db-tools-1.7.0
osinfo-db-20200203
libosinfo-1.7.1
urlgrabber-4.0.0
lloyd-yajl-66cb08c
libvirt-6.8.0
libvirt-glib-3.0.0
libvirt-python-6.8.0
virt-manager-3.1.0

wxWidgets-3.0.5
asciidoctor-2.0.10
libpcap-1.9.1
wireshark-3.2.7
dreamchess-0.3.0
########################LibreOffice##########
raptor2-2.0.15
rasqal-0.9.33
redland-1.0.17
cppunit-1.15.1
librevenge-0.0.4
libfreehand-git
xmlsec1-1.2.30
poppler-20.10.0
Sub-Identify-0.14
SUPER-1.20190531
Module-Build-0.4231
Test-Warnings-0.030
Test-MockModule-v0.173.0
Archive-Zip-1.68
# # # # # # # # #  kde install 
libreoffice-7.0.3.1
########################LibreOffice##########
zbar-0.23.1
scrcpy-1.16
libnatspec-0.3.0
# xarchiver-0.5.4# нужен xfce
gsmartcontrol-1.1.3
flatbuffers
fmtlib
fstrcmp-0.7.D001
libfakekey-0.3
qt-creator-opensource-src-4.13.1
# php-git
# xclip-git
# # kodi
# libgd-2.3.0
# tcp_wrappers-7.6
# tcpdump-4.9.3
# net-snmp-5.9
# libsodium-1.0.18
# phc-winner-argon2-20190702
# alpine-2.23.2
# pinepgp-0.18.0
# libiodbc-3.52.13
# php-7.4.11
composer-setup
phpMyAdmin-5.0.3-all-languages
# qt-creator-opensource-src-4.13.1
# nginx-1.19.2-x86_64-1_SBo
########kde-apps
# #  krita ON
sip-4.19.21
# для простоты python -> python3.8 python-config -> python3-config, потом надо подумать как это автоматизировать.
sip-5.4.0
PyQt5_sip-12.8.1
PyQt-builder-1.5.0
PyOpenGL-3.1.5
PyQt5-5.15.1
# PyQt5-5.15.1_sip4 ### для теста, работает. На будущее можно и так. Без него тоже работает, как вариант оставил.
libheif-1.9.1
setconf-0.7.7
tinyxml_2_6_2
yaml-cpp-0.6.3
OpenColorIO-1.1.1
dcmtk-3.6.5
pybind11-2.6.0
robin-map-0.6.3
boost_1_74_0-withTest
oiio-Release-2.2.7.0
eigen-3.3.8
quazip-1.1
Vc-1.4.1
kseexpr-4.0.0.0
krita-4.4.1
# # # krita OFF
parallel-20200922
libstatgrab-0.92
muparser-master
cifs-utils-6.11
expect5.45.4
soxr-0.1.3-Source
webrtc-audio-processing-0.3.1
sox-14.4.2
pulseaudio-14.0
SDL-1.2.15
SDL2-2.0.12
SDL2_image-2.0.5
SDL2_mixer-2.0.4
XML-XQL-0.68
libcdr-0.1.6
gtkspell3-3.0.10
inkscape-1.0.1
######code::blocks#########
googletest-git
LucenePlusPlus-rel_3.0.7
poedit-2.4.1
gamin-0.1.10
Catch-1.11.0
codeblocks-Revision12235
###########OpenJDK########
### pandoc из апстрима! см. файл pandoc
jdk14u-jdk-14.0.1+7
stop
digikam-7.1.0
# yarn-v1.22.10
# lxqt-git.sh
