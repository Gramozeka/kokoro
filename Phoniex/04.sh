#!/bin/bash
gpgme-1.14.0
volume_key-volume_key-0.3.12
dosfstools-4.1
parted-3.3
libblockdev-2.24
gptfdisk-1.0.5
fuse-3.9.3
ntfs-3g_ntfsprogs-2017.3.23
sg3_utils-1.45
btrfs-progs-v5.7
libndp-1.7
fuse-2.9.9
exfat
chrpath-0.16
pywbem-1.0.3
libconfig-1.7.2
libstoragemgmt-1.8.5
udisks-2.9.1
sshfs-3.7.0
smartmontools-7.1
libnice-0.1.17
hdparm-9.58
nfs-utils-2.5.1
autofs-5.1.6
gst-plugins-bad-1.18.0
gst-plugins-ugly-1.18.0
gspell-1.8.4
rest-0.8.1
pyatspi2-2.38.0
gtksourceview-3.24.11
graphene-1.10.2
gtk-+4-master
gtksourceview-4.8.0
meson-0.56.0
glade-3.38.1
libpeas-1.28.0
samba-4.12.7
libgphoto2-2.5.26
pycryptodome-3.9.8
talloc-2.3.1
kyotocabinet-1.2.78
anthy-9100h
libhangul-libhangul-0.1.0
libplist-2.2.0
libusbmuxd-2.0.2
libimobiledevice-1.3.0
libnfs-4.0.0
libmtp-1.1.17
gvfs-1.46.1
amtk-5.2.0
uchardet-0.0.7
tepl-5.0.0
# gedit-3.36.2
gedit-3.38.0
exempi-2.5.2
bubblewrap-0.4.1
gexiv2-0.12.1
gnome-autoar-0.2.4
cython-3.0a6
libseccomp-2.5.0
colm-0.13.0.7
kelbt-0.16
ragel-7.0.0.12
snowball-2.0.0
lmdb-LMDB_0.9.24
appstream-0.12.11
appstream-glib-0.7.18
newt-0.52.21
wpa_supplicant-2.9
libnfnetlink-1.0.1
libnetfilter_conntrack-1.0.8
# dnsmasq - slak-src
NetworkManager-1.26.2
tracker-2.3.6
tracker-master
tracker-miners-master
libgsf-1.14.47
totem-pl-parser-3.26.5
libcue-2.2.1
tracker-miners-2.3.5
# # # gtk-sharp-2.99.3
gmime-3.2.7
gnome-desktop-3.38.1
nautilus-3.38.1
udftools
vte-0.62.1
geocode-glib-3.26.1
telepathy-glib-0.24.1
cbindgen-0.15.0
liboauth-1.0.3
http-parser-2.9.4
node-v14.15.0
# # # vte-master
polkit-gnome-0.105
blocaled-0.3
libmbim-1.24.2
libqmi-1.26.4
ModemManager-1.14.2
mobile-broadband-provider-info-20190618
NetworkManager-1.26.2
dbus-python-1.2.16
avahi-0.8
wlroots-master
sway-master
geoclue-2.5.6
libgweather-3.36.1
gnome-terminal-3.38.1
gnome-settings-daemon-3.38.1
libnma-1.8.30
# # # # libappindicator-12.10.0 сломан навсегда
network-manager-applet-1.18.0
# # #  xfce.sh
accountsservice-0.6.55
lxdm-0.5.3
# libwnck-2.30.7
libwnck-3.36.0
woff2-1.0.2
hyphen-2.8.8
libwpe-1.8.0
wpebackend-fdo-1.8.0
gtk+-2.24.32
gtk+-3.24.23
colord-gtk-0.2.0
rest-0.8.1
xdg-dbus-proxy-0.1.2
curl-7.73.0
webkitgtk-2.30.2
hunspell-1.7.0
hunspell-dictionaries-ru
jam-2.6.1
# # # Argyll_V2.1.2 - убит навсегда
# # # ppp-2.4.7 slack version!

samba-4.12.7
stop
next
pipewire-0.3.13
telepathy-mission-control-5.16.6
pavucontrol-4.0
autoconf-archive-2019.01.06
sound-theme-freedesktop-0.8
unrarsrc-5.9.4
gst-libav-1.18.0
gstreamer-vaapi-1.18.0
audiofile-0.3.6
libmusicbrainz-5.1.0
sane-backends-1.0.29
xsane-0.999
libbluray-1.2.0
libdmapsharing-3.9.10

pypubsub-4.0.3
pathlib2-2.3.5
wxWidgets-3.0.5
wxPython-4.0.1
p7zip_16.02
libzen_0.4.38
libmediainfo_20.08
# cine-encoder-3.0
# easytag-master
picard-release-2.5.1

zlib-1.2.11
xxHash-0.8.0
gn-git
owt-deps-webrtc-git
microsoft-GSL-3.1.0
range-v3-0.11.0
expected-1.0.0
AviSynthPlus-3.6.1
ladspa_sdk_1.15
aom-v2.0.0
libraw1394-2.1.2
cargo-c-0.6.15
rav1e-0.3.4
libavc1394-0.5.4
libiec61883-1.2.0
dav1d-0.7.1
gsm-1.0.19
gmmlib-intel-gmmlib-20.3.3
media-driver-intel-media-20.3.0
MediaSDK-intel-mediasdk-20.3.0
SPIRV-LLVM-Translator-11.0.0
opencl-clang-11.0.0
ocl-icd-2.2.13
intel-graphics-compiler-igc-1.0.5435
compute-runtime-20.44.18297
libmodplug-0.8.9.0
opencore-amr-0.1.5
srt-1.4.2
georgmartius-vid.stab-f400b54
vmaf-1.5.3
xvidcore-1.3.7
libomxil-bellagio-0.9.3
teckit-2.5.10
potrace-1.16
poppler-20.10.0
yelp-xsl-3.38.1
plymouth-git
libportal-0.3
xdg-desktop-portal-1.8.0
xdg-desktop-portal-gtk-1.8.0
cdrtools-3.02
dvd+rw-tools-7.1
cdrdao-1.2.4
libburn-1.5.2
libisofs-1.5.2
libisoburn-1.5.2
portaudio-git
espeak-1.48.15
espeak-ng-1.50
dotconf-1.3
pyatspi2-2.38.0
yelp-tools-3.38.0
double-conversion-3.1.5
speechd-0.10.1
orca-3.38.0
soxr-0.1.3-Source
ffmpeg-4.3.1_TELE1
MPlayer-1.4
audacious-4.0.5
audacious-plugins-4.0.5
nss-3.58
node-v14.15.0
gccmakedep-1.0.3
lndir-1.0.3
makedepend-1.0.5
xorg-cf-files-1.0.6
imake-1.0.8
Python-3.9.0
eudev-3.2.9
firefox-83.0

