#!/bin/bash
set -e
unpack () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar  xvjf $1  2> /dev/null   ;;
      *.tar.gz)    tar xvzf $1   2> /dev/null  ;;
      *.tar.xz)    tar xvJf $1   2> /dev/null  ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1   2> /dev/null   ;;
      *.tbz2)      tar xvjf $1  2> /dev/null   ;;
      *.tgz)       tar xvzf $1  2> /dev/null   ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *.lz)        tar xvf $1   2> /dev/null   ;;
      *)           echo "\`$1': Unknown method of file compression" ;;
    esac
  else
    echo "\`$1' no found"
  fi
}

BUILD=$1
read_list=$1
arg1=$2
arg2=$3
home=$(pwd)
if [ -n "$1" ]
then
scriptsdir="Config_scripts"
else
echo "Не указан входящий файл!"
exit 1
fi
TEMPBUILD=/phoniex/system/Building
SOURCE=/phoniex/sources
PATCHSOURCE=/phoniex/Patches
destdir=/phoniex/Base-Pack
######################################################################################
USE_ARCH="64" 
ARCH="x86_64"
LDFLAGS="-L/lib64:/usr/lib64" 
LD_LIBRARY_PATH="/lib64:/usr/lib64" 
export CFLAGS="${BUILD64}"
export CXXFLAGS="${BUILD64}"
export CLFS_TARGET=${CLFS_TARGET64}
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
export LLVM_INSTALL_DIR=/usr
export CLANG_INSTALL_DIR=/usr
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
######################################################################################
pack () {
pkg=${destdir}/$1

if [[ $(basename $(pwd)) == "meson-build" ]]
then
DESTDIR=${pkg} ninja install
else
make DESTDIR=${pkg} install
fi
echo "$1 ----- $(date)" >> ${destdir}/loginstall
if [ -d ${pkg}/usr/share/man ]; then
echo n|find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
echo n|find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
find ${pkg} -depth -print0  -type f -name "*.la"  -exec rm -fv {} \;
find ${pkg} -depth -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg} &&
mkdir -p usr/tree-info
find ./ ! -type d | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
######################################################################################
######################################################################################
while read -r line; do
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
if [ -n "$2" ]
then
if [ -n "$3" ]
then
case $arg2 in
"one" )
if [ $line != $arg1 ]
then
continue
else

    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
    . ${home}/${scriptsdir}/${line}.sh
    wait

break

fi
;;
"more" )
if [ $line != $arg1 ]; then continue; else
arg2="0"
fi
;;
esac
else

if [ $line != $arg1 ]
then
continue
else
    file=$(echo $line | cut -d" " -f2)
echo "второй аргумент отсутствует, будет собран только $file"
    . ${home}/${scriptsdir}/${line}.sh
    wait

break
fi
fi
fi
    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
    . ${home}/${scriptsdir}/${line}.sh
    wait
    unset -f pack_local
    unset -f pack_local32
    unset -f pack_local64

done < $1.sh
exit
