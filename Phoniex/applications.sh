#!/bin/bash
akonadi-20.08.3
kmime-20.08.3
akonadi-mime-20.08.3
ffmpegthumbs-20.08.3
akonadi-notes-20.08.3
kcron-20.08.3
kdb-3.2.0
kdebugsettings-20.08.3
kdeedu-data-20.08.3
libkcddb-20.08.3
libkexiv2-20.08.3
libkipi-20.08.3
konsole-20.08.3
kate-20.08.3
baloo-widgets-20.08.3
dolphin-20.08.3
dolphin-plugins-20.08.3
dragon-20.08.3
kfind-20.08.3
konqueror-20.08.3
kdialog-20.08.3
libappimage-1.0.3.1
kdsoap-1.9.1
kio-extras-20.08.3
libkdegames-20.08.3
rinutils-0.6.0
freecell-solver-6.0.1
kpat-20.08.3
kimap-20.08.3
akonadi-contacts-20.08.3
kmbox-20.08.3
kldap-20.08.3
kgpg-20.08.3
gwenview-20.08.3
libkleo-20.08.3
grantleetheme-20.08.3
kpimtextedit-20.08.3
kidentitymanagement-20.08.3
kcalutils-20.08.3
akonadi-search-20.08.3
libkdepim-20.08.3
pimcommon-20.08.3
kdepim-apps-libs-20.08.3
libgravatar-20.08.3
ksmtp-20.08.3
kontactinterface-20.08.3
libkgapi-20.08.3
kmailtransport-20.08.3
akonadi-calendar-20.08.3
libksieve-20.08.3
messagelib-20.08.3
mailimporter-20.08.3
mailcommon-20.08.3
ktnef-20.08.3
kmail-20.08.3
kmail-account-wizard-20.08.3
calendarsupport-20.08.3
akonadi-calendar-tools-20.08.3
grantlee-editor-20.08.3
akonadi-import-wizard-20.08.3
akonadiconsole-20.08.3
kdiagram-2.7.0
eventviews-20.08.3
incidenceeditor-20.08.3
kpkpass-20.08.3
kitinerary-20.08.3
# kitinerary-master
kdepim-addons-20.08.3
kalarmcal-20.08.3
kalarm-20.08.3
kdepim-runtime-20.08.3
pim-sieve-editor-20.08.3
pim-data-exporter-20.08.3
khelpcenter-20.08.3
okular-20.08.3
kdf-20.08.3
akregator-20.08.3
analitza-20.08.3
ark-20.08.3
# blinken-20.08.3
filelight-20.08.3
kaccounts-integration-20.08.3
kaccounts-providers-20.08.3
kaddressbook-20.08.3
kcalc-20.08.3
kcolorchooser-20.08.3
kdegraphics-thumbnailers-20.08.3
kdesdk-kioslaves-20.08.3
kdesdk-thumbnailers-20.08.3
# kig-20.08.3
# klettres-20.08.3
libkdcraw-20.08.3
kmix-20.08.3
knotes-20.08.3
libkomparediff2-20.08.3
kompare-20.08.3
krdc-20.08.3
# ktouch-20.08.3
poxml-20.08.3
kpmcore-4.1.0
partitionmanager-4.1.0
kwalletmanager-20.08.3
kdenlive-20.08.3
keditbookmarks-20.08.3
libkmahjongg-20.08.3
kmahjongg-20.08.3
libktorrent-2.2.0
ktorrent-5.2.0
# ktorrent-master
kdeconnect-kde-1.4
kleopatra-20.08.3
kontact-20.08.3
korganizer-20.08.3
juk-20.08.3
k3b-20.08.3
#kamera-20.08.3
kapptemplate-20.08.3
# kbreakout-20.08.3
kcharselect-20.08.3
# kfourinline-20.08.3
kget-20.08.3
kimagemapeditor-20.08.3
ksystemlog-20.08.3
ktp-accounts-kcm-20.08.3
ktp-approver-20.08.3
ktp-common-internals-20.08.3

#ktp-call-ui-20.08.3
ktp-contact-list-20.08.3
ktp-contact-runner-20.08.3
ktp-desktop-applets-20.08.3
ktp-filetransfer-handler-20.08.3
ktp-kded-module-20.08.3
ktp-send-file-20.08.3
ktp-text-ui-20.08.3
ktp-auth-handler-20.08.3
libkcompactdisc-20.08.3
spectacle-20.08.3
falkon-3.1.0
libksane-20.08.3
#knemo-frameworks.zip
kblocks-20.08.3
#Latte-Dock-master
# artikulate-20.08.3
audiocd-kio-20.08.3
# cantor-20.08.3
cervisia-20.08.3
#kalgebra-20.08.3
kalzium-20.08.3
kamoso-20.08.3
libkeduvocdocument-20.08.3
# kanagram-20.08.3
kbackup-20.08.3
# kblog-20.08.3
# kbruch-20.08.3
kcachegrind-20.08.3
okteta-0.26.4
# kdevelop-5.4.2
# kde-dev-utils-20.08.3
# kde-dev-scripts-20.08.3
#kdevelop-pg-qt-2.2.0
# kdev-python-5.4.2
#kdev-php-5.4.2
kirigami-gallery-20.08.3
kdegraphics-mobipocket-20.08.3
kdenetwork-filesharing-20.08.3
kfloppy-20.08.3
# kgeography-20.08.3
# khangman-20.08.3
# kiten-20.08.3
kmag-20.08.3
kmousetool-20.08.3
kmouth-20.08.3
#kmplot-20.08.3
kolourpaint-20.08.3
kopete-20.08.3
kqtquickcharts-20.08.3
#krfb-20.08.3
kross-interpreters-20.08.3
kruler-20.08.3
# kspaceduel-20.08.3
#kteatime-20.08.3
ktimer-20.08.3
#kturtle-20.08.3
kwave-20.08.3
# kwordquiz-20.08.3
#marble-20.08.3
#libkgeomap-20.08.3
lokalize-20.08.3
mbox-importer-20.08.3
fluidsynth-2.1.5
#minuet-20.08.3
# parley-20.08.3
print-manager-20.08.3
#rocs-20.08.3
signon-kwallet-extension-20.08.3
# eigen-3.3.7
# step-20.08.3
svgpart-20.08.3
sweeper-20.08.3
umbrello-20.08.3
zeroconf-ioslave-20.08.3
#libkvkontakte-5.0.0
#lensfun-0.3.95
# kuser-16.04.1
# digikam-6.4.0
# 

