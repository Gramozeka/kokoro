#!/bin/bash
# set -u
# set +h
unpack () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xvjf $1    ;;
      *.tar.gz)    tar xvzf $1    ;;
      *.tar.xz)    tar xvJf $1    ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1     ;;
      *.tbz2)      tar xvjf $1    ;;
      *.tgz)       tar xvzf $1    ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *)           echo "\`$1': Unknown method of file compression" ;;
    esac
  else
    echo "\`$1' no found"
  fi
}

arg1=$2
arg2=$3
home=$(pwd)
if [ -n "$1" ]
then
scriptsdir="Config_scripts"
else 
echo "Не указан входящий файл!"
exit 1
fi
SOURCE=${home}/sources
# PATCHSOURCE=${home}/Patches
destdir=${home}/Base-Pack
TEMPBUILD=${home}/Building
if ! [ -d $TEMPBUILD ]
then
mkdir -pv $destdir
mkdir -pv $TEMPBUILD
fi

######################################################################################
BUILD=${scriptsdir}
######################################################################################
while read -r line; do
#source /etc/profile &&
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi

    file=$(echo $line | cut -d" " -f2)

cp -v /home/mike/phoniex/system/Config_scripts/$file.sh Temp/
cp -v /home/build/sources/$file.* sources/

done < $1.sh
exit
