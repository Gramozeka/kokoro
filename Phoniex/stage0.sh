#!/bin/bash
set -e
unpack () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xvjf $1    ;;
      *.tar.gz)    tar xvzf $1    ;;
      *.tar.xz)    tar xvJf $1    ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1     ;;
      *.tbz2)      tar xvjf $1    ;;
      *.tgz)       tar xvzf $1    ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *)           echo "\`$1': Unknown method of file compression" ;;
    esac
  else
    echo "\`$1' no foud"
  fi
}


home=$(pwd)
TEMPBUILD=/tmp/Building
SOURCE=/phoniex/sources
PATCHSOURCE=/phoniex/Patches
destdir=/tmp/Base-Pack/1-repo
if ! [ -d $TEMPBUILD ]
then
mkdir -pv ../Base-Pack/1-repo
ln -sv ../../home/phoniex/Base-Pack /tmp/Base-Pack
mkdir -pv $TEMPBUILD
fi
##################################################################
cd $TEMPBUILD
rm -rf *

package="which-2.21.tar.gz"
packagedir=$(sed -e "s/\.tar\.[a-zA-z1-9]*$//" <<< $package)
unpack ${SOURCE}/${package}
cd ${packagedir}
CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
./configure --prefix=/usr --libdir=/usr/lib64
make
make install
echo "###########################**** COMPLITE!!! ****##################################"
##################################################################
cd $TEMPBUILD
rm -rf *
package="dialog-1.3-20200327.tar.gz"
packagedir=$(sed -e "s/\.tar\.[a-zA-z1-9]*$//" <<< $package)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/dialog.all.use_height.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/dialog.smaller.min.height.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/dialog.no.aspect.ratio.autoajust.patch.gz | patch -p1 --verbose || exit 1
USE_ARCH="64"
ARCH="x86_64" CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
./configure --prefix=/usr \
  --mandir=/usr/man \
  --disable-static \
  --enable-nls \
  --with-ncursesw \
  --enable-widec \
  --build=$CLFS_TARGET
make -j4
make install
echo "###########################**** COMPLITE!!! ****##################################"
##################################################################
cd $TEMPBUILD
rm -rf *
package="tree-1.8.0.tar.gz"
packagedir=$(sed -e "s/\.tar\.[a-zA-z1-9]*$//" <<< $package)
unpack ${SOURCE}/${package}
cd ${packagedir}

USE_ARCH="64"
ARCH="x86_64" CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
make -j4
cat tree > /usr/bin/tree
chmod 755 /usr/bin/tree
echo "###########################**** COMPLITE!!! ****##################################"
##################################################################
cd $TEMPBUILD
rm -rf *
package="dosfstools-4.1.tar.xz"
packagedir=$(sed -e "s/\.tar\.[a-zA-z1-9]*$//" <<< $package)
unpack ${SOURCE}/${package}
cd ${packagedir}
USE_ARCH="64"
ARCH="x86_64" CC="gcc ${BUILD64}" CXX="g++ ${BUILD64}" \
./configure \
  --prefix=/usr \
  --sbindir=/sbin \
  --libdir=/usr/lib64 \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --docdir=/usr/doc/dosfstools-4.1 \
  --enable-compat-symlinks \
    --build=$CLFS_TARGET
make -j4
make install
cd ${home}

# cp -v Pkgtool/sbin/* /sbin
# installpkg Pkgtool/pkgtools-14.2-noarch-13.txz
# cp -fv Pkgtool/sbin/installpkg /sbin
echo "###########################**** COMPLITE!!! ****##################################"
