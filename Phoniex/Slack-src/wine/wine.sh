#!/bin/bash

# Slackware build script for wine

# Copyright 2011 David Woodfall
# Copyright 2006-2009  Robby Workman  Northport, AL, USA
# All rights reserved.

# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=wine
VERSION=${VERSION:-5.19}
BUILD=${BUILD:-2}
TAG=${TAG:-_Mike}
CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP

rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.xz
# rm -rf wine-staging
# tar xvf $CWD/wine-staging.tar.xz
# git clone https://github.com/wine-mirror/wine.git $PRGNAM-$VERSION
# git clone https://github.com/wine-staging/wine-staging.git
# 
# cd wine-staging
# ./patches/patchinstall.sh DESTDIR=../$PRGNAM-$VERSION --all --force-autoconf
# cd ../$PRGNAM-$VERSION
cd $PRGNAM-$VERSION
chown -R root:root .
find -L . \
	\( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
	-o -perm 511 \) -exec chmod 755 {} \; -o \
	\( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
	-o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

	mkdir wine64
	cd wine64
# LDFLAGS="-L/usr/lib64 -ldl" \
PKG_CONFIG_PATH="$PKG_CONFIG_PATH64" \
	CFLAGS="-O2" \
	CXXFLAGS="-O2" \
../configure \
		--prefix=/usr \
		--libdir=/usr/lib64 \
		--localstatedir=/var \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--enable-win64 \
		--with-x \
		--with-gnutls \
		--with-opengl \
		--disable-tests \
		--without-hal --build=$CLFS_TARGET
		make -j5 depend
	make -j5
	make install DESTDIR=$PKG
	cd ..
mkdir wine32
cd wine32
# SAVE_path=${PATH}
# PATH="${PATH32}:${PATH}"
 PKG_CONFIG_PATH="$PKG_CONFIG_PATH32:$PKG_CONFIG_PATH64" \
	CFLAGS="-O2" \
	CXXFLAGS="-O2" \
../configure \
--with-wine64=../wine64 \
		--prefix=/usr \
		--libdir=/usr/lib \
		--localstatedir=/var \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--with-x \
		--with-gnutls \
		--with-opengl \
		--disable-tests \
		--without-hal --build=$CLFS_TARGET
# 		--build=$CLFS_TARGET \
#         --with-udev \
# 		--with-xinput \
# 		--with-xinput2 \
# 		--with-glu \
# 		--with-va
# 		
		make -j5 depend
	make -j5
	make install DESTDIR=$PKG
# 	cd ../wine64
# 	make install DESTDIR=$PKG
	cd ..



# export PATH="${SAVE_path}"
# export USE_ARCH="64"
# unset PKG_CONFIG_PATH
# export PKG_CONFIG_PATH=${PKG_CONFIG_PATH64}
find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
	| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG/usr/share/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/share/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh
export ARCH="86_64"
cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
upgradepkg --reinstall --install-new $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
mv -vf $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz} $CWD
