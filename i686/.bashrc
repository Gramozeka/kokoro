set +h
umask 022
CLFS=/i686
LC_ALL=POSIX
PATH=/cross-tools/bin:/bin:/usr/bin
alias gcc='gcc -m32'
alias g++='g++ -m32'
export MACHTYPE=i686-pc-linux-gnu
export CLFS LC_ALL PATH
unset CFLAGS CXXFLAGS PKG_CONFIG_PATH
export CLFS_HOST="i686-cross-linux-gnu"
export CLFS_TARGET="i686-pc-linux-gnu"
export CC="i686-pc-linux-gnu-gcc"
export CXX="i686-pc-linux-gnu-g++"
export AR="i686-pc-linux-gnu-ar"
export AS="i686-pc-linux-gnu-as"
export RANLIB="i686-pc-linux-gnu-ranlib"
export LD="i686-pc-linux-gnu-ld"
export STRIP="i686-pc-linux-gnu-strip"
