#!/bin/bash
gmp-6.2.0
mpfr-4.1.0
mpc-1.2.0
isl-0.22.1
zlib-1.2.11
binutils-2.35
gcc-10.2.0
ncurses-6.2
bash-5.0
bzip2-1.0.8
check-0.15.2
coreutils-8.32
diffutils-3.7
file-5.39
findutils-4.7.0
gawk-5.1.0
gettext-0.21
grep-3.4
gzip-1.10
make-4.3
patch-2.7.6
sed-4.8
tar-1.32
texinfo-6.7
util-linux-2.36
vim-8.2.1361
xz-5.2.5
stop_chroot
m4-1.4.18
bison-3.7.2
perl-5.32.0
Python-3.8.5
# multiarch_wrapper






