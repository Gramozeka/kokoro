#!/bin/bash
akonadi-20.08.2
kmime-20.08.2
akonadi-mime-20.08.2
ffmpegthumbs-20.08.2
akonadi-notes-20.08.2
kcron-20.08.2
kdb-3.2.0
kdebugsettings-20.08.2
kdeedu-data-20.08.2
libkcddb-20.08.2
libkexiv2-20.08.2
libkipi-20.08.2
konsole-20.08.2
kate-20.08.2
baloo-widgets-20.08.2
dolphin-20.08.2
dolphin-plugins-20.08.2
dragon-20.08.2
kfind-20.08.2
konqueror-20.08.2
kdialog-20.08.2
libappimage-1.0.3.1
kdsoap-1.9.1
kio-extras-20.08.2
libkdegames-20.08.2
rinutils-0.6.0
freecell-solver-6.0.1
kpat-20.08.2
kimap-20.08.2
akonadi-contacts-20.08.2
kmbox-20.08.2
kldap-20.08.2
kgpg-20.08.2
gwenview-20.08.2
libkleo-20.08.2
grantleetheme-20.08.2
kpimtextedit-20.08.2
kidentitymanagement-20.08.2
kcalutils-20.08.2
akonadi-search-20.08.2
libkdepim-20.08.2
pimcommon-20.08.2
kdepim-apps-libs-20.08.2
libgravatar-20.08.2
ksmtp-20.08.2
kontactinterface-20.08.2
libkgapi-20.08.2
kmailtransport-20.08.2
akonadi-calendar-20.08.2
libksieve-20.08.2
messagelib-20.08.2
mailimporter-20.08.2
mailcommon-20.08.2
ktnef-20.08.2
kmail-20.08.2
kmail-account-wizard-20.08.2
calendarsupport-20.08.2
akonadi-calendar-tools-20.08.2
grantlee-editor-20.08.2
akonadi-import-wizard-20.08.2
akonadiconsole-20.08.2
kdiagram-2.7.0
eventviews-20.08.2
incidenceeditor-20.08.2
kpkpass-20.08.2
kitinerary-20.08.2
# kitinerary-master
kdepim-addons-20.08.2
kalarmcal-20.08.2
kalarm-20.08.2
kdepim-runtime-20.08.2
pim-sieve-editor-20.08.2
pim-data-exporter-20.08.2
khelpcenter-20.08.2
okular-20.08.2
kdf-20.08.2
akregator-20.08.2
analitza-20.08.2
ark-20.08.2
# blinken-20.08.2
filelight-20.08.2
kaccounts-integration-20.08.2
kaccounts-providers-20.08.2
kaddressbook-20.08.2
kcalc-20.08.2
kcolorchooser-20.08.2
kdegraphics-thumbnailers-20.08.2
kdesdk-kioslaves-20.08.2
kdesdk-thumbnailers-20.08.2
# kig-20.08.2
# klettres-20.08.2
libkdcraw-20.08.2
kmix-20.08.2
knotes-20.08.2
libkomparediff2-20.08.2
kompare-20.08.2
krdc-20.08.2
# ktouch-20.08.2
poxml-20.08.2
kpmcore-4.1.0
partitionmanager-4.1.0
kwalletmanager-20.08.2
kdenlive-20.08.2
keditbookmarks-20.08.2
libkmahjongg-20.08.2
kmahjongg-20.08.2
libktorrent-2.2.0
ktorrent-5.2.0
# ktorrent-master
kdeconnect-kde-1.4
kleopatra-20.08.2
kontact-20.08.2
korganizer-20.08.2
juk-20.08.2
k3b-20.08.2
#kamera-20.08.2
kapptemplate-20.08.2
# kbreakout-20.08.2
kcharselect-20.08.2
# kfourinline-20.08.2
kget-20.08.2
kimagemapeditor-20.08.2
ksystemlog-20.08.2
ktp-accounts-kcm-20.08.2
ktp-approver-20.08.2
ktp-common-internals-20.08.2

#ktp-call-ui-20.08.2
ktp-contact-list-20.08.2
ktp-contact-runner-20.08.2
ktp-desktop-applets-20.08.2
ktp-filetransfer-handler-20.08.2
ktp-kded-module-20.08.2
ktp-send-file-20.08.2
ktp-text-ui-20.08.2
ktp-auth-handler-20.08.2
libkcompactdisc-20.08.2
spectacle-20.08.2
# falkon-master
libksane-20.08.2
#knemo-frameworks.zip
kblocks-20.08.2
#Latte-Dock-master
# artikulate-20.08.2
audiocd-kio-20.08.2
# cantor-20.08.2
cervisia-20.08.2
#kalgebra-20.08.2
kalzium-20.08.2
kamoso-20.08.2
libkeduvocdocument-20.08.2
# kanagram-20.08.2
kbackup-20.08.2
# kblog-20.08.2
# kbruch-20.08.2
kcachegrind-20.08.2
okteta-0.26.4
# kdevelop-5.4.2
# kde-dev-utils-20.08.2
# kde-dev-scripts-20.08.2
#kdevelop-pg-qt-2.2.0
# kdev-python-5.4.2
#kdev-php-5.4.2
kirigami-gallery-20.08.2
kdegraphics-mobipocket-20.08.2
kdenetwork-filesharing-20.08.2
kfloppy-20.08.2
# kgeography-20.08.2
# khangman-20.08.2
# kiten-20.08.2
kmag-20.08.2
kmousetool-20.08.2
kmouth-20.08.2
#kmplot-20.08.2
kolourpaint-20.08.2
kopete-20.08.2
kqtquickcharts-20.08.2
#krfb-20.08.2
kross-interpreters-20.08.2
kruler-20.08.2
# kspaceduel-20.08.2
#kteatime-20.08.2
ktimer-20.08.2
#kturtle-20.08.2
kwave-20.08.2
# kwordquiz-20.08.2
#marble-20.08.2
#libkgeomap-20.08.2
lokalize-20.08.2
mbox-importer-20.08.2
fluidsynth-2.1.5
#minuet-20.08.2
# parley-20.08.2
print-manager-20.08.2
#rocs-20.08.2
signon-kwallet-extension-20.08.2
# eigen-3.3.7
# step-20.08.2
svgpart-20.08.2
sweeper-20.08.2
umbrello-20.08.2
zeroconf-ioslave-20.08.2
#libkvkontakte-5.0.0
#lensfun-0.3.95
kuser-16.04.1
# digikam-6.4.0
# 

