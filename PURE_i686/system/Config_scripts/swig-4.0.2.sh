#!/bin/bash
###############################################################################
set -e
# SWIG (Simplified Wrapper and Interface Generator) is a compiler that integrates C and C++ with languages including Perl, Python, Tcl, Ruby, PHP, Java, C#, D, Go, Lua, Octave, R, Scheme, and Ocaml. SWIG can also export its parse tree into Lisp s-expressions and XML.
# SWIG reads annotated C/C++ header files and creates wrapper code (glue code) in order to make the corresponding C/C++ libraries available to the listed languages, or to extend C/C++ programs with a scripting language.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/swig/swig-4.0.1.tar.gz
# Download MD5 sum: 54cc40b3804816f7d38ab510b6f13b04
# Download size: 7.7 MB
# Estimated disk space required: 181 MB (1.2 GB with tests)
# Estimated build time: 0.2 SBU (add 9.4 SBU for tests; both using parallelism=4)
# SWIG Dependencies
# Required
# PCRE-8.43
# Optional
# Boost-1.72.0 for tests, and any of the languages mentioned in the introduction, as run-time dependencies
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/swig_rubytracking.diff
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir} \
--without-maximum-compile-warnings
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
