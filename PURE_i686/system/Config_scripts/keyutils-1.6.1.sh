#!/bin/bash
###############################################################################
set -e
# Keyutils is a set of utilities for managing the key retention facility in the kernel, which can be used by filesystems, block devices and more to gain and retain the authorization and encryption keys required to perform secure operations.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://people.redhat.com/~dhowells/keyutils/keyutils-1.6.1.tar.bz2
# Download MD5 sum: 919af7f33576816b423d537f8a8692e8
# Download size: 96 KB
# Estimated disk space required: 1.9 MB (with tests)
# Estimated build time: less than 0.1 SBU (add 0.6 SBU for tests)
# keyutils Dependencies
# Required
# MIT Kerberos V5-1.17.1
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make NO_ARLIB=1 LIBDIR=/lib USRLIBDIR=/usr/lib MANDIR=/usr/share/man DESTDIR=${pkg} install
mv ${pkg}/lib/pkgconfig ${pkg}/usr/lib
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
echo "###########################**** COMPLITE!!! ****##################################"
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4 LIBDIR=/lib USRLIBDIR=/usr/lib MANDIR=/usr/share/man
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
