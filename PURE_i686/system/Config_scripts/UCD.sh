# Unicode Character Database
# Date: 2017-06-18, 23:32:00 GMT [KW]
# © 2017 Unicode®, Inc.
# For terms of use, see http://www.unicode.org/terms_of_use.html
#
# For documentation, see the following:
# NamesList.html
# UAX #38, "Unicode Han Database (Unihan)"
# UAX #44, "Unicode Character Database."
#https://www.unicode.org/Public/zipped/13.0.0/UCD.zip
# https://www.unicode.org/Public/zipped/13.0.0/Unihan.zip
# The UAXes can be accessed at http://www.unicode.org/versions/Unicode10.0.0/
#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/share/${packagedir}/usd
cp -ar * ${pkg}/usr/share/${packagedir}/usd
ln -sv ${packagedir} ${pkg}/usr/share/unicode
echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
mkdir ${packagedir}
cd ${packagedir}
unpack ${SOURCE}/${package}
unpack ${SOURCE}/Unihan.zip
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
