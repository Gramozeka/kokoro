#!/bin/bash
###############################################################################
set -e
# liba52 is a free library for decoding ATSC A/52 (also known as AC-3) streams. The A/52 standard is used in a variety of applications, including digital television and DVD.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://liba52.sourceforge.net/files/a52dec-0.7.4.tar.gz
# Download MD5 sum: caa9f5bc44232dc8aeea773fea56be80
# Download size: 236 KB
# Estimated disk space required: 2.5 MB
# Estimated build time: less than 0.1 SBU
# Optional
# djbfft
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
cp liba52/a52_internal.h ${pkg}/usr/include/a52dec &&
mkdir -pv  ${pkg}/usr/share/doc/liba52-0.7.4
install -v -m644 -D doc/liba52.txt \
    ${pkg}/usr/share/doc/liba52-0.7.4/liba52.txt
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
            --mandir=/usr/share/man \
            --enable-shared \
            --disable-static \
            CFLAGS="-g -O2" \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
