#!/bin/bash
###############################################################################
set -e
###############################################################################
# Tracker is the file indexing and search provider used in the GNOME desktop environment.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/tracker/2.3/tracker-2.3.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/tracker/2.3/tracker-2.3.1.tar.xz
# Download MD5 sum: 5e75b611b190c4cebf53e3de15266429
# Download size: 1.5 MB
# Estimated disk space required: 194 MB (with tests)
# Estimated build time: 1.0 SBU (with tests)
# Tracker Dependencies
# Required
# JSON-GLib-1.4.4, libseccomp-2.4.2, libsoup-2.68.3, and Vala-0.46.5
# Recommended
# gobject-introspection-1.62.0, ICU-65.1, NetworkManager-1.22.4, SQLite-3.31.1, and UPower-0.99.11
# Optional
# GTK-Doc-1.32, bash-completion, and libstemmer
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
glib-compile-schemas /usr/share/glib-2.0/schemas
