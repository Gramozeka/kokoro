#!/bin/bash
###############################################################################
set -e
###############################################################################
# xterm is a terminal emulator for the X Window System.
# This package is not a part of the Xorg katamari and is provided only as a dependency to other packages or for testing the completed Xorg installation.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://invisible-mirror.net/archives/xterm/xterm-352.tgz
# Download MD5 sum: 9d41798d035976715d532d38327f5e41
# Download size: 1.3 MB
# Estimated disk space required: 13 MB
# Estimated build time: 0.2 SBU
# xterm Dependencies
# Required
# Xorg Applications
# Required (at runtime)
# A monospace TTF or OTF font such as Dejavu fonts
# Optional
# PCRE-8.43 or pcre2-10.34, Valgrind-3.15.0 and man2html
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make DESTDIR=${pkg} install-ti &&
mkdir -pv ${pkg}/usr/share/applications &&
cp -v *.desktop ${pkg}/usr/share/applications/
mkdir -pv ${pkg}/etc/X11/app-defaults
cat >> ${pkg}/etc/X11/app-defaults/XTerm << "EOF"
*VT100*locale: true
*VT100*faceName: Terminus
*VT100*faceSize: 12
*backarrowKeyIsErase: true
*ptyInitialErase: true
EOF

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/v0/{n;s/new:/new:kb=^?:/}' termcap &&
printf '\tkbs=\\177,\n' >> terminfo &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
TERMINFO=/usr/share/terminfo \
./configure --prefix=/usr \
 --with-app-defaults=/etc/X11/app-defaults \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
