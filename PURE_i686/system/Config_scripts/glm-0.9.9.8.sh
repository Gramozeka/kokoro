#!/bin/bash
###############################################################################
set -e
###############################################################################
# OpenGL Mathematics (GLM) is a header-only C++ mathematics library for graphics software based on the OpenGL Shading Language (GLSL) specifications. An extension system provides extended capabilities such as matrix transformations and quaternions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/g-truc/glm/archive/0.9.9.7/glm-0.9.9.7.tar.gz
# Download MD5 sum: 1464f8f9fdea069f7c32c5f9a204941c
# Download size: 4.2 MB
# Estimated disk space required: 22 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/{i686,usr}/include
cp -r glm ${pkg}/usr/include
echo "$1 ----- $(date)" >> ${destdir}/loginstall

cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
ARCH="noarch"
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
