#!/bin/bash
###############################################################################
set -e
###############################################################################
# Polkit is a toolkit for defining and handling authorizations. It is used for allowing unprivileged processes to communicate with privileged processes.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/polkit/releases/polkit-0.116.tar.gz
# Download MD5 sum: 4b37258583393e83069a0e2e89c0162a
# Download size: 1.5 MB
# Estimated disk space required: 15 MB (with tests)
# Estimated build time: 0.4 SBU (with tests)
# Additional Downloads
# Recommended patch: http://www.linuxfromscratch.org/patches/blfs/svn/polkit-0.116-fix_elogind_detection-1.patch
# Polkit Dependencies
# Required
# GLib-2.62.4 and js60-60.8.0
# Recommended
# Linux-PAM-1.3.1 and elogind-241.4
# [Note] Note
# Since elogind uses PAM to register user sessions, it is a good idea to build Polkit with PAM support so elogind can track Polkit sessions.
# Optional (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# docbook-xml-4.5, docbook-xsl-1.79.2, GTK-Doc-1.32, and libxslt-1.1.34
# [Note] Note
# If libxslt-1.1.34 is installed, then docbook-xml-4.5 and docbook-xsl-1.79.2 are required. If you have installed libxslt-1.1.34, but you do not want to install any of the DocBook packages mentioned, you will need to use --disable-man-pages in the instructions below.
###############################################################################
groupadd -fg 28 polkitd &&
useradd -c "PolicyKit Daemon Owner" -d /etc/polkit-1 -u 28 \
        -g polkitd -s /bin/false polkitd || true
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/etc/pam.d
cat > ${pkg}/etc/pam.d/polkit-1 << "EOF"
# Begin /etc/pam.d/polkit-1

auth     include        system-auth
account  include        system-account
password include        system-password
session  include        system-session

# End /etc/pam.d/polkit-1
EOF
#  chown root:root ${pkg}/usr/lib/polkit-1/polkit-agent-helper-1
# chmod 4755  ${pkg}/usr/lib/polkit-1/polkit-agent-helper-1
#  chown root:root ${pkg}/usr/bin/pkexec
# chmod 4755   ${pkg}/usr/bin/pkexec

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/polkit-0.118-fix_elogind_detection-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoreconf -fiv
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--libexecdir=/usr/libexec \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--with-pam-module-dir=/lib/security \
            --with-os-type=Phoniex   \
--disable-libsystemd-login \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
chown -R polkitd:polkitd /etc/polkit-1/rules.d
chown -R polkitd:polkitd /usr/share/polkit-1/rules.d
