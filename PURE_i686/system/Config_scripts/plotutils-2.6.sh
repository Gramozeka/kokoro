#!/bin/bash
###############################################################################
set -e
###############################################################################
# https://ftp.gnu.org/gnu/plotutils/
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np0 -i ${PATCHSOURCE}/plotutils-2.6-libpng-1.5.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
  --enable-static=no \
  --enable-shared=yes \
  --enable-libplotter \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
