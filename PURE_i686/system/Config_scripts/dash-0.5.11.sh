#!/bin/bash
###############################################################################
set -e
###############################################################################
# Dash is a POSIX compliant shell. It can be installed as /bin/sh or as the default shell for either root or a second user with a userid of 0. It depends on fewer libraries than the Bash shell and is therefore less likely to be affected by an upgrade problem or disk failure. Dash is also useful for checking that a script is completely compatible with POSIX syntax.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://gondor.apana.org.au/~herbert/dash/files/dash-0.5.10.2.tar.gz
# Download MD5 sum: 8f485f126c05d0ab800e85abfe1987aa
# Download size: 220 KB
# Estimated disk space required: 2.9 MB
# Estimated build time: less than 0.1 SBU
# Dash Dependencies
# Optional
# libedit (command line editor library)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--bindir=/bin --mandir=/usr/share/man \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cat >> /etc/shells << "EOF"
/bin/dash
EOF
