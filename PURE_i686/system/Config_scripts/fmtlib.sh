#!/bin/bash
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
# 	git clone https://github.com/fmtlib/fmt.git fmtlib
	unpack ${SOURCE}/${package}
	cd ${packagedir}
	mkdir build
	cd    build
	CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
	cmake    \
	-DCMAKE_INSTALL_PREFIX=/usr                \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib   \
	-DBUILD_SHARED_LIBS=ON                         \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX=""                                           \
	-DX11_RENDER_SYSTEM=gl \
	-DBUILD_TESTING=OFF                                  \
	-Wno-dev ..
	make -j4 || make || exit 1

	pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


















