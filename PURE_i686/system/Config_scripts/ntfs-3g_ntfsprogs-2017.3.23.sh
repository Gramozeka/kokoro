#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Ntfs-3g package contains a stable, read-write open source driver for NTFS partitions. NTFS partitions are used by most Microsoft operating systems. Ntfs-3g allows you to mount NTFS partitions in read-write mode from your Linux system. It uses the FUSE kernel module to be able to implement NTFS support in user space. The package also contains various utilities useful for manipulating NTFS partitions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://tuxera.com/opensource/ntfs-3g_ntfsprogs-2017.3.23.tgz
# Download MD5 sum: d97474ae1954f772c6d2fa386a6f462c
# Download size: 1.2 MB
# Estimated disk space required: 20 MB
# Estimated build time: 0.2 SBU
# Ntfs-3g Dependencies
# Optional
# fuse 2.x (this disables user mounts)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/{bin,sbin,lib,usr}
mkdir -pv ${pkg}/usr/share/man/man8
mkdir -pv ${pkg}/usr/{bin,lib}
make DESTDIR=${pkg} install
ln -sv ../bin/ntfs-3g ${pkg}/sbin/mount.ntfs &&
ln -sv ntfs-3g.8 ${pkg}/usr/share/man/man8/mount.ntfs.8
chmod -v 4755 ${pkg}/bin/ntfs-3g
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--with-fuse=internal  \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
