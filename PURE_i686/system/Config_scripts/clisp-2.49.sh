#!/bin/bash
###############################################################################
set -e
###############################################################################
# GNU Clisp is a Common Lisp implementation which includes an interpreter, compiler, debugger, and many extensions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/clisp/latest/clisp-2.49.tar.bz2
# Download (FTP): ftp://ftp.gnu.org/gnu/clisp/latest/clisp-2.49.tar.bz2
# Download MD5 sum: 1962b99d5e530390ec3829236d168649
# Download size: 7.8 MB
# Estimated disk space required: 163 MB (add 8 MB for tests)
# Estimated build time: 0.9 SBU (1.2 SBU with tests)
# Additional Downloads
# Optional patch: http://www.linuxfromscratch.org/patches/blfs/svn/clisp-2.49-readline7_fixes-1.patch (required if building against libffcall)
# Clisp Dependencies
# Recommended
# libsigsegv-2.12
# Optional
# libffcall
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install DESTDIR=${pkg}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# zcat ${PATCHSOURCE}/clisp.c_data.compact_empty_buckets.diff.gz | patch -p1 --verbose || exit
sed -i -e '/socket/d' -e '/"streams"/d' tests/tests.lisp
patch -Np1 -i ${PATCHSOURCE}/clisp-2.49-readline7_fixes-1.patch
# export 
# export CXXFLAGS=" -O2 -march=i686 -falign-functions=4"
export FORCE_UNSAFE_CONFIGURE=1
mkdir build &&
cd    build &&
CFLAGS=" -O2 -falign-functions=4" \
../configure  --srcdir=../ \
  --prefix=/usr \
  --libdir=/usr/lib \
--with-libffcall-prefix=/usr \
--with-libsigsegv-prefix=/usr \
--docdir=/usr/share/doc/${packagedir}
# ulimit -s 16384 &&
make -j1
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
