#!/bin/bash
###############################################################################
set -e
###############################################################################
# Valgrind is an instrumentation framework for building dynamic analysis tools. There are Valgrind tools that can automatically detect many memory management and threading bugs, and profile programs in detail. Valgrind can also be used to build new tools.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://sourceware.org/ftp/valgrind/valgrind-3.15.0.tar.bz2
# Download (FTP): ftp://sourceware.org/pub/valgrind/valgrind-3.15.0.tar.bz2
# Download MD5 sum: 46e5fbdcbc3502a5976a317a0860a975
# Download size: 19 MB
# Estimated disk space required: 425 MB (add 70 MB for tests)
# Estimated build time: 0.6 SBU (Using parallelism=4; add 11 SBU for tests)
# Valgrind Dependencies
# Optional
# BIND-9.14.9 or BIND Utilities-9.14.9 (for tests), Boost-1.72.0, GDB-8.3.1 (for tests), LLVM-9.0.1 (with Clang), and Which-2.21 (for tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/libmpiwrap.diff
sed -i 's|/doc/valgrind||' docs/Makefile.in &&
CFLAGS="-O2" \
CXXFLAGS="-O2" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--enable-lto=yes \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
