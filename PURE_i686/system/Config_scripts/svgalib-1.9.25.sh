#!/bin/bash
###############################################################################


###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}
unpack ${SOURCE}/${package}
cd ${packagedir}
# export ARCH=i586
CFLAGS="-O2 -march=i686 -mtune=i686"
chown -R root:root .
find . -perm 666 -exec chmod 644 {} \;
find . -perm 664 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
find . -name "*.h" -exec chmod 644 {} \;
zcat ${PATCHSOURCE}/svgalib.prefix.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-kernel-2.6.26.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib.nohelper.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-round_gtf_gtfcalc_c.patch.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/svgalib-1.9.25-vga_getmodenumber.patch.gz | patch -p1 --verbose || exit 1
patch -Np1 -i ${PATCHSOURCE}/quickmath.diff
echo 'NO_ASM = y' >> Makefile.cfg
make install NO_HELPER=y ARCH=i386 || exit 1
make install \
 TOPDIR=${pkg} \
  prefix=${pkg}/usr \
  mandir=${pkg}/usr/share/man \
  sharedlibdir=${pkg}/usr/lib \
  MANFORMAT=compressed \
  NO_HELPER=y \
  ARCH=i386 \
  || exit 1
ARCH=i386 \
make demoprogs || exit 1

# Install demos:
mkdir -p ${pkg}/usr/share/svgalib-demos
( cd demos
  # this will produce a harmless error... hey, some of these demos might come back, right?rwpage bad
  cp fun testgl speedtest mousetest vgatest scrolltest testlinear keytest testaccel accel forktest eventtest spin bg_test printftest joytest mjoytest bankspeed lineart linearspeed addmodetest svidtune linearfork cursor vgatweak buildcsr  \
  linuxlogo.bitmap \
  ${pkg}/usr/share/svgalib-demos
  chmod 755 ${pkg}/usr/share/svgalib-demos/*
)

# Add a documentation directory:
mkdir -p ${pkg}/usr/share/doc/svgalib-1.9.25
cp -a \
  0-README LICENSE README svgalib.lsm \
  ${pkg}/usr/share/doc/svgalib-1.9.25
( cd doc
  cp -a \
  0-INSTALL CHANGES DESIGN Driver-programming-HOWTO README.joystick \
  README.keymap README.multi-monitor README.patching README.vesa TODO dual-head-howto \
  ${pkg}/usr/share/doc/svgalib-1.9.25
)

# Make sure the package contains all library symlinks:
( cd ${pkg}/usr/lib
  ldconfig -l *
)
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home

echo "###########################**** COMPLITE!!! ****##################################"
