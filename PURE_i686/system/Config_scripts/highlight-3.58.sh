#!/bin/bash
###############################################################################
set -e
###############################################################################
# Highlight is an utility that converts source code to formatted text with syntax highlighting.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.andre-simon.de/zip/highlight-3.54.tar.bz2
# Download MD5 sum: 927004f5da4c3585a76086a0ab107a82
# Download size: 1.3 MB
# Estimated disk space required: 22 MB (with gui)
# Estimated build time: 0.3 SBU (Using paralllelism=4; with gui)
# Highlight Dependencies
# Required
# Boost-1.72.0 and Lua-5.3.5
# Optional
# Qt-5.14.0 (to build the GUI front-end)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make DESTDIR=${pkg} install-gui
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
if (pkg-config --exists Qt5Core) 
then 
make doc_dir=/usr/share/doc/highlight-3.58 gui
pack_local64 ${packagedir}
else
pack ${packagedir}
fi
echo "###########################**** COMPLITE!!! ****##################################"
