#!/bin/bash
###############################################################################
set -e
# The GStreamer Good Plug-ins is a set of plug-ins considered by the GStreamer developers to have good quality code, correct functionality, and the preferred license (LGPL for the plug-in code, LGPL or LGPL-compatible for the supporting library). A wide range of video and audio decoders, encoders, and filters are included.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-1.18.0.tar.xz
# Download MD5 sum: bd025f8f14974f94b75ac69a9d1b9c93
# Download size: 3.7 MB
# Estimated disk space required: 97 MB (with tests)
# Estimated build time: 0.5 SBU (Using parallelism=4; with tests)
# GStreamer Good Plug-ins Dependencies
# Required
# gst-plugins-base-1.18.0
# Recommended
# Cairo-1.17.2+f93fc72c03e, FLAC-1.3.3, LAME-3.100, mpg123-1.25.13, Mesa-19.3.3, gdk-pixbuf-2.40.0, libgudev-233, libjpeg-turbo-2.0.4, libpng-1.6.37, libsoup-2.68.3, libvpx-1.8.2, and Xorg Libraries
# Optional
# AAlib-1.4rc5, ALSA OSS-1.1.8, GTK+-3.24.13 (for examples), GTK-Doc-1.32, libdv-1.0.0, PulseAudio-13.0, Qt-5.14.1, Speex-1.2.0, taglib-1.11.1, Valgrind-3.15.0, v4l-utils-1.18.0, Wayland-1.17.0, JACK, libcaca, libiec61883, libraw1394, libshout, Orc, TwoLame, and WavPack
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-Qt*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
