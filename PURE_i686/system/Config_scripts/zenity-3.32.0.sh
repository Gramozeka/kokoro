#!/bin/bash
###############################################################################
set -e
###############################################################################
# Zenity is a rewrite of gdialog, the GNOME port of dialog which allows you to display GTK+ dialog boxes from the command line and shell scripts.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/zenity/3.32/zenity-3.32.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/zenity/3.32/zenity-3.32.0.tar.xz
# Download MD5 sum: ba2b2a13248773b4ec0fd323d95e6d5a
# Download size: 4.5 MB
# Estimated disk space required: 27 MB
# Estimated build time: 0.1 SBU
# Zenity Dependencies
# Required
# GTK+-3.24.13 and itstool-2.0.6
# Recommended
# libnotify-0.7.8 and libxslt-1.1.34
# Optional
# WebKitGTK+-2.26.3
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
