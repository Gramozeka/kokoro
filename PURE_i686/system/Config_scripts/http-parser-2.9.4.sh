#!/bin/bash
###############################################################################
set -e
###############################################################################
##https://github.com/nodejs/http-parser/releases
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install-strip
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e 's|PREFIX ?= /usr/local|PREFIX ?= /usr|g' -i Makefile
# sed -e 's|LIBDIR = $(PREFIX)/lib|LIBDIR = $(PREFIX)/lib|g' -i Makefile
sed -e 's|INCLUDEDIR = $(PREFIX)/include|INCLUDEDIR = /usr/include|g' -i Makefile
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4 
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
