#!/bin/bash
###############################################################################
###############################################################################
set -e
###############################################################################
# D-Bus Python provides Python bindings to the D-Bus API interface.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://dbus.freedesktop.org/releases/dbus-python/dbus-python-1.2.16.tar.gz
# Download MD5 sum: 51a45c973d82bedff033a4b57d69d5d8
# Download size: 564 KB
# Estimated disk space required: 11 MB (both versions, with tests)
# Estimated build time: 0.4 SBU (both versions, with tests)
# D-Bus Python Dependencies
# Required
# dbus-1.12.16 and GLib-2.62.4
# Recommended
# Python-2.7.17 (some applications in the book require the Python 2 module)
# Optional
# PyGObject-3.34.0 and tap.py (required for some tests)
# Optional (Required to build the API and HTML Documentation)
# docutils-0.16 and Sphinx with sphinx_rtd_theme
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make -C python2 DESTDIR=${pkg} install
make -C python3 DESTDIR=${pkg} install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}

mkdir python2 &&
pushd python2 &&
PYTHON=/usr/bin/python2     \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../configure --prefix=/usr \
--libdir=/usr/lib \
--docdir=/usr/share/doc/dbus-python-1.2.16 &&
make &&
popd
mkdir python3 &&
pushd python3 &&
PYTHON=/usr/bin/python3    \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
../configure --prefix=/usr \
--libdir=/usr/lib \
--docdir=/usr/share/doc/dbus-python-1.2.16 &&
make &&
popd
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

