#!/bin/bash
###############################################################################
set -e
# The Gtkmm package provides a C++ interface to GTK+ 3.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtkmm/3.24/gtkmm-3.24.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gtkmm/3.24/gtkmm-3.24.2.tar.xz
# Download MD5 sum: e311db484ca9c53f1689d35f5f58a06b
# Download size: 13 MB
# Estimated disk space required: 411 MB (with tests)
# Estimated build time: 2.1 SBU (Using parallelism=4; with tests)
# Gtkmm Dependencies
# Required
# Atkmm-2.28.0, GTK+-3.24.13, and Pangomm-2.42.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e '/^libdocdir =/ s/$(book_name)/gtkmm-3.24.2/' \
    -i docs/Makefile.in
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
