#!/bin/bash
###############################################################################
set -e
# FUSE (Filesystem in Userspace) is a simple interface for userspace programs to export a virtual filesystem to the Linux kernel. Fuse also aims to provide a secure method for non privileged users to create and mount their own filesystem implementations.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/libfuse/libfuse/releases/download/fuse-3.9.3/fuse-3.9.3.tar.xz
# Download MD5 sum: 5dc2be21c0d06c7eace340996e8a37e2
# Download size: 1.5 MB
# Estimated disk space required: 52 MB
# Estimated build time: 0.1 SBU
# Fuse Dependencies
# Optional
# Doxygen-1.8.17 (to rebuild the API documentation)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} ninja install
cd ..

mkdir -pv ${pkg}/{lib,sbin,etc,bin}
mv -vf   ${pkg}/usr/lib/libfuse3.so.3*     ${pkg}/lib
ln -sfvn ../../lib/libfuse3.so.3.9.3 ${pkg}/usr/lib/libfuse3.so

mv -vf ${pkg}/usr/bin/fusermount3  ${pkg}/bin/
mv -vf ${pkg}/usr/sbin/mount.fuse3 ${pkg}/sbin/
chmod u+s ${pkg}/bin/fusermount3
mkdir -pv ${pkg}/usr/share/doc/fuse-3.9.3
  doxygen -u doc/Doxyfile
  doxygen doc/Doxyfile
cp -Rv doc/html  ${pkg}/usr/share/doc/fuse-3.9.3 

cat > ${pkg}/etc/fuse.conf << "EOF"
# Set the maximum number of FUSE mounts allowed to non-root users.
# The default is 1000.
#
#mount_max = 1000

# Allow non-root users to specify the 'allow_other' or 'allow_root'
# mount options.
#
#user_allow_other
EOF
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/^udev/,$ s/^/#/' util/meson.build &&
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
