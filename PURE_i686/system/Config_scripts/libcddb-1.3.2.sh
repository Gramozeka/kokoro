#!/bin/bash
###############################################################################
set -e
# The libcddb is a library that implements the different protocols (CDDBP, HTTP, SMTP) to access data on a CDDB server.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/libcddb/libcddb-1.3.2.tar.bz2
# Download MD5 sum: 8bb4a6f542197e8e9648ae597cd6bc8a
# Download size: 384 KB
# Estimated disk space required: 3.9 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
