#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GPGME package is a C library that allows cryptography support to be added to a program. It is designed to make access to public key crypto engines like GnuPG or GpgSM easier for applications. GPGME provides a high-level crypto API for encryption, decryption, signing, signature verification and key management.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/gpgme/gpgme-1.13.1.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/gpgme/gpgme-1.13.1.tar.bz2
# Download MD5 sum: 198f0a908ec3cd8f0ce9a4f3a4489645
# Download size: 1.7 MB
# Estimated disk space required: 232 MB (with all bindings)
# Estimated build time: 0.7 SBU (with parallelism=4; with all bindings, add 0.5 SBU for tests)
# GPGME Dependencies
# Required
# Libassuan-2.5.3
# Optional
# Doxygen-1.8.17 and Graphviz-2.42.3 (for API documentation), GnuPG-2.2.19 (required if Qt or SWIG are installed; used during the testsuite), Clisp-2.49, Python-2.7.17, Qt-5.14.1, and/or SWIG-4.0.1 (for language bindings)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
 --disable-gpg-test \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
