#!/bin/bash
###############################################################################
set -e
###############################################################################
# NetworkManager is a set of co-operative tools that make networking simple and straightforward. Whether you use WiFi, wired, 3G, or Bluetooth, NetworkManager allows you to quickly move from one network to another: Once a network has been configured and joined once, it can be detected and re-joined automatically the next time it's available.'
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/NetworkManager/1.22/NetworkManager-1.22.4.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/NetworkManager/1.22/NetworkManager-1.22.4.tar.xz
# Download MD5 sum: 14e4933253b2135ccfb6adc97d322fab
# Download size: 4.6 MB
# Estimated disk space required: 861 MB (with tests)
# Estimated build time: 0.9 SBU (with tests; both using parallelism=4)
# NetworkManager Dependencies
# Required
# dbus-glib-0.110 and libndp-1.7
# Recommended
# cURL-7.68.0, dhcpcd-8.1.6 or DHCP-4.4.2 (client only), gobject-introspection-1.62.0, iptables-1.8.4, jansson-2.12, newt-0.52.21 (for nmtui), NSS-3.49.2, Polkit-0.116, PyGObject-3.34.0, elogind-241.4, UPower-0.99.11, Vala-0.46.5, and wpa_supplicant-2.9 (built with D-Bus support),
# Optional
# BlueZ-5.52, D-Bus Python-1.2.16 (for the test suite), GnuTLS-3.6.11.1 (used if NSS-3.49.2 is not found), GTK-Doc-1.32, libpsl-0.21.0, Qt-5.14.1 (for examples), ModemManager-1.12.4, Valgrind-3.15.0, dnsmasq, libaudit, libteam, mobile-broadband-provider-info, PPP, and RP-PPPoE
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
DESTDIR=${pkg} ninja install
mkdir -pv  ${pkg}/etc/NetworkManager
mkdir -pv  ${pkg}/etc/NetworkManager/conf.d
cat >> ${pkg}/etc/NetworkManager/NetworkManager.conf << "EOF"
[main]
plugins=keyfile
EOF
cat > ${pkg}/etc/NetworkManager/conf.d/polkit.conf << "EOF"
[main]
auth-polkit=true
EOF
cat > ${pkg}/etc/NetworkManager/conf.d/dhcp.conf << "EOF"
[main]
dhcp=dhclient
EOF
cat > ${pkg}/etc/NetworkManager/conf.d/no-dns-update.conf << "EOF"
[main]
dns=default
EOF
# # # # groupadd -fg 86 netdev &&
# # # # /usr/sbin/usermod -a -G netdev mike
mkdir -pv ${pkg}/usr/share/polkit-1/rules.d
cat > ${pkg}/usr/share/polkit-1/rules.d/org.freedesktop.NetworkManager.rules << "EOF"
polkit.addRule(function(action, subject) {
    if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("netdev")) {
        return polkit.Result.YES;
    }
});
EOF
chown -R polkitd:polkitd ${pkg}/usr/share/polkit-1/rules.d/org.freedesktop.NetworkManager.rules
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e 's/-qt4/-qt5/'              \
    -e 's/moc_location/host_bins/' \
    -i examples/C/qt/meson.build   &&

sed -e 's/Qt/&5/'                  \
    -i meson.build
sed '/initrd/d' -i src/meson.build
grep -rl '^#!.*python$' | xargs sed -i '1s/python/&3/'
sed -i -E '/(setting name)/{
  s/(description=).%s./\1%s/
  N
  s/(class_desc)/xml_quoteattr(\1)/
}' tools/generate-docs-nm-settings-docs-gir.py

if (pkg-config --exists ModemManager) 
then 
MODEMMANAGER="-Dmodem_manager=true"
else 
MODEMMANAGER="-Dmodem_manager=false"
fi
if (pkg-config --exists Qt5Core)
then 
QT="-Dqt=true"
CFLAGS="-O2 -fPIC -march=i686"
CXXFLAGS="-O2 -fPIC -march=i686"
else 
QT="-Dqt=false"
fi
if [ -e /usr/sbin/pppd ] 
then
DPPPD="-Dppp=true -Dpppd=/usr/sbin/pppd -Dpppd_plugin_dir=/usr/lib/pppd/2.4.8"
fi
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
        -Djson_validation=false    \
      -Dlibaudit=no              \
      -Dlibpsl=false             \
      -Dnmtui=true               \
      -Dovs=false                \
      -Dppp=false                \
      -Dselinux=false            \
      -Dudev_dir=/lib/udev       \
      -Dsession_tracking=elogind \
      ${MODEMMANAGER}      \
      $DPPPD                \
      -Dsystemdsystemunitdir=no  \
      -Dsystemd_journal=false    \
      $QT                 \
  ..
  ninja -j5
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
CFLAGS="-O2 -march=i686 -mtune=i686"
CXXFLAGS="-O2 -march=i686 -mtune=i686"
# /etc/NetworkManager/NetworkManager.conf
# 
# Configuration Information
# For NetworkManager to work, at least a minimal configuration file must be present. Such file is not installed with make install. Issue the following command as the root user to create minimal NetworkManager.conf file:
# 
# cat >> /etc/NetworkManager/NetworkManager.conf << "EOF"
# [main]
# plugins=keyfile
# EOF
# This file should not be modified directly by users of the system. Instead, system specific changes should be made using configuration files in the /etc/NetworkManager/conf.d direcotry.
# 
# To allow polkit to manage authorizations, add the following configuration file:
# 
# cat > /etc/NetworkManager/conf.d/polkit.conf << "EOF"
# [main]
# auth-polkit=true
# EOF
# To use something other than the built-in dhcp client (recommended if using only nmcli), use the following configuration (valid values are dhclient, dhcpcd, and internal):
# 
# cat > /etc/NetworkManager/conf.d/dhcp.conf << "EOF"
# [main]
# dhcp=dhclient
# EOF
# To prevent NetworkManager from updating the /etc/resolv.conf file, add the following configuration file:
# 
# cat > /etc/NetworkManager/conf.d/no-dns-update.conf << "EOF"
# [main]
# dns=none
# EOF
# For additional configuation options, see man 5 NetworkManager.conf.
# 
# To allow regular users permission to configure network connections, you should add them to the netdev group, and create a polkit rule that grants access. Run the following commands as the root user:
# 
# groupadd -fg 86 netdev &&
# /usr/sbin/usermod -a -G netdev <username>
# 
# cat > /usr/share/polkit-1/rules.d/org.freedesktop.NetworkManager.rules << "EOF"
# polkit.addRule(function(action, subject) {
#     if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("netdev")) {
#         return polkit.Result.YES;
#     }
# });
# EOF
# Boot Script
# To automatically start the NetworkManager daemon when the system is rebooted, install the /etc/rc.d/init.d/networkmanagerbootscript from the blfs-bootscripts-20191204 package.
# 
# [Note] Note
# If using Network Manager to manage an interface, any previous configuration for that interface should be removed, and the interface brought down prior to starting Network Manager.
# 
# 
# cd boot-script
# make install-networkmanager

