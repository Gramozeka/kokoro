#!/bin/bash
###############################################################################
set -e
# Freeglut is intended to be a 100% compatible, completely opensourced clone of the GLUT library. GLUT is a window system independent toolkit for writing OpenGL programs, implementing a simple windowing API, which makes learning about and exploring OpenGL programming very easy.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/freeglut/freeglut-3.2.1.tar.gz
# Download MD5 sum: cd5c670c1086358598a6d4a9d166949d
# Download size: 432 KB
# Estimated disk space required: 4.6 MB
# Estimated build time: less than 0.1 SBU
# Freeglut Dependencies
# Required
# CMake-3.16.3 and Mesa-19.3.2
# Recommended
# GLU-9.0.1
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/freeglut-3.2.1-gcc10_fix-1.patch
 mkdir -p build
cd build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake \
    -DCMAKE_C_FLAGS:STRING="$CFLAGS" \
    -DCMAKE_C_FLAGS_RELEASE:STRING="$CFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
    -DCMAKE_CXX_FLAGS_RELEASE:STRING="$CXXFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DFREEGLUT_BUILD_DEMOS=OFF \
    -DFREEGLUT_BUILD_SHARED_LIBS=ON \
    -DFREEGLUT_BUILD_STATIC_LIBS=OFF \
    -DLIB_SUFFIX="" \
    .. || exit 1
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
