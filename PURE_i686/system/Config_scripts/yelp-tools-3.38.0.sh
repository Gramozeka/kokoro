#!/bin/bash
###############################################################################
set -e
# yelp-tools is a collection of scripts and build utilities to help create,
# manage, and publish documentation for Yelp and the web. Most of the heavy
# lifting is done by packages like yelp-xsl and itstool. This package just
# wraps things up in a developer-friendly way.
# http://ftp.gnome.org/pub/gnome/sources/yelp-tools/3.32/yelp-tools-3.32.2.tar.xz
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
