#!/bin/bash
###############################################################################
set -e
###############################################################################
# This project aims to provide a full-featured exFAT file system implementation for Unix-like systems. It consists of a FUSE module (fuse-exfat) and a set of utilities (exfat-utils).
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/relan/exfat.git
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoreconf -vfi
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
