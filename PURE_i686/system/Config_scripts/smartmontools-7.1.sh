#!/bin/bash
###############################################################################
set -e
###############################################################################
# The smartmontools package contains utility programs (smartctl, smartd) to control/monitor storage systems using the Self-Monitoring, Analysis and Reporting Technology System (S.M.A.R.T.) built into most modern ATA and SCSI disks.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/smartmontools/smartmontools-7.1.tar.gz
# Download MD5 sum: 430cd5f64caa4524018b536e5ecd9c29
# Download size: 952 KB
# Estimated disk space required: 26 MB
# Estimated build time: 0.2 SBU
# smartmontools Dependencies
# Optional (runtime)
# cURL-7.68.0 or Lynx-2.8.9rel.1 or Wget-1.20.3 (download tools), and GnuPG-2.2.19 (encrypted hard disks)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
            --with-initscriptdir=no \
            --with-libsystemd=no    \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
