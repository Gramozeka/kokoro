#!/bin/bash
###############################################################################
set -e
###############################################################################
# The icon-naming-utils package contains a Perl script used for maintaining backwards compatibility with current desktop icon themes, while migrating to the names specified in the Icon Naming Specification.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://tango.freedesktop.org/releases/icon-naming-utils-0.8.90.tar.bz2
# Download MD5 sum: dd8108b56130b9eedc4042df634efa66
# Download size: 57 KB
# Estimated disk space required: 440 KB
# Estimated build time: less than 0.1 SBU
# icon-naming-utils Dependencies
# Required
# XML-Simple-2.25
###############################################################################
# cpan -i XML::Simple
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
