#!/bin/bash
encodings-1.0.5.tar.xz
font-alias-1.0.3.tar.xz
font-adobe-utopia-type1-1.0.4.tar.xz
font-bh-ttf-1.0.3.tar.xz
font-bh-type1-1.0.3.tar.xz
font-ibm-type1-1.0.3.tar.xz
font-xfree86-type1-1.0.4.tar.xz
font-adobe-100dpi-1.0.3.tar.xz
font-adobe-75dpi-1.0.3.tar.xz
font-adobe-utopia-75dpi-1.0.4.tar.xz
font-adobe-utopia-100dpi-1.0.4.tar.xz
font-bitstream-100dpi-1.0.3.tar.xz
font-bitstream-75dpi-1.0.3.tar.xz
font-bitstream-speedo-1.0.2.tar.xz
font-bitstream-type1-1.0.3.tar.xz
font-cronyx-cyrillic-1.0.3.tar.xz
font-cursor-misc-1.0.3.tar.xz
font-daewoo-misc-1.0.3.tar.xz
font-micro-misc-1.0.3.tar.xz
font-misc-cyrillic-1.0.3.tar.xz
font-misc-misc-1.1.2.tar.xz
font-mutt-misc-1.0.3.tar.xz
font-schumacher-misc-1.1.2.tar.xz
font-screen-cyrillic-1.0.4.tar.xz
font-sony-misc-1.0.3.tar.xz
font-sun-misc-1.0.3.tar.xz
font-winitzki-cyrillic-1.0.3.tar.xz
font-jis-misc-1.0.3.tar.xz
font-isas-misc-1.0.3.tar.xz
font-dec-misc-1.0.3.tar.xz
font-bh-type1-1.0.3.tar.xz
font-bh-ttf-1.0.3.tar.xz
font-bh-lucidatypewriter-100dpi-1.0.3.tar.xz
font-bh-lucidatypewriter-75dpi-1.0.3.tar.xz
font-bh-100dpi-1.0.3.tar.xz
font-bh-75dpi-1.0.3.tar.xz
font-arabic-misc-1.0.3.tar.xz
font-misc-ethiopic-1.0.3.tar.xz
