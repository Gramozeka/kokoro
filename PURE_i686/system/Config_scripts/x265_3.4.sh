#!/bin/bash
###############################################################################
set -e
# x265 package provides a library for encoding video streams into the H.265/HEVC format.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://bitbucket.org/multicoreware/x265/downloads/x265_3.2.1.tar.gz
# Download MD5 sum: 94808045a34d88a857e5eaf3f68f4bca
# Download size: 1.4 MB
# Estimated disk space required: 41 MB
# Estimated build time: 1.0 SBU (using parallelism=4)
# x265 Dependencies
# Required
# CMake-3.16.3
# Recommended
# NASM-2.14.02
# Optional
# numactl
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build-8

  cd build-8
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLIB_INSTALL_DIR=lib \
    -DENABLE_SHARED=ON \
    -DCMAKE_BUILD_TYPE=Release ../source
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
