#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GNOME Icon Theme Symbolic package contains symbolic icons for the default GNOME icon theme.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-symbolic/3.12/gnome-icon-theme-symbolic-3.12.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-symbolic/3.12/gnome-icon-theme-symbolic-3.12.0.tar.xz
# Download MD5 sum: 3c9c0e6b9fa04b3cbbb84da825a26fd9
# Download size: 228 KB
# Estimated disk space required: 6.8 MB
# Estimated build time: less than 0.1 SBU
# GNOME Icon Theme Symbolic Dependencies
# Required
# gnome-icon-theme-3.12.0
# Optional
# git-2.25.0 and Inkscape-0.92.4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
