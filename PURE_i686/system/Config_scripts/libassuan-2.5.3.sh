#!/bin/bash
###############################################################################
set -e
# The libassuan package contains an inter process communication library used by some of the other GnuPG related packages. libassuans primary use is to allow a client to interact with a non-persistent server. libassuan is not, however, limited to use with GnuPG servers and clients. It was designed to be flexible enough to meet the demands of many transaction based environments with non-persistent servers.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.gnupg.org/ftp/gcrypt/libassuan/libassuan-2.5.3.tar.bz2
# Download (FTP): ftp://ftp.gnupg.org/gcrypt/libassuan/libassuan-2.5.3.tar.bz2
# Download MD5 sum: 226c504ea78a232224bf3b6846b3adb9
# Download size: 560 KB
# Estimated disk space required: 6.7 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# libassuan Dependencies
# Required
# libgpg-error-1.36
# Optional
# texlive-20190410 (or install-tl-unx)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
