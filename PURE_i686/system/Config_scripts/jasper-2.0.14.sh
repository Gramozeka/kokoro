#!/bin/bash
###############################################################################
set -e
# The JasPer Project is an open-source initiative to provide a free software-based reference implementation of the JPEG-2000 codec.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.ece.uvic.ca/~frodo/jasper/software/jasper-2.0.14.tar.gz
# Download MD5 sum: 23561b51da8eb5d0dc85b91eff3d9a7f
# Download size: 1.6 MB
# Estimated disk space required: 11 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# JasPer Dependencies
# Required
# CMake-3.16.3
# Recommended
# libjpeg-turbo-2.0.4
# Optional
# Freeglut-3.2.1 (required for jiv), Doxygen-1.8.17 (needed for generating html documentation), and texlive-20190410 (needed to regnerate the pdf documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir BUILD &&
cd    BUILD &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_INSTALL_PREFIX=/usr    \
      -DCMAKE_BUILD_TYPE=Release     \
      -DCMAKE_SKIP_INSTALL_RPATH=YES \
      -DJAS_ENABLE_DOC=NO            \
      -DLIB_SUFFIX="" \
      -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${packagedir} \
      ..  &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
