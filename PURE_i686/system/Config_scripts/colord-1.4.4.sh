#!/bin/bash
###############################################################################
set -e
###############################################################################
groupadd -g 71 colord &&
useradd -c "Color Daemon Owner" -d /var/lib/colord -u 71 \
        -g colord -s /bin/false colord
###############################################################################
# Colord is a system service that makes it easy to manage, install, and generate color profiles. It is used mainly by GNOME Color Manager for system integration and use when no users are logged in.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/colord/releases/colord-1.4.4.tar.xz
# Download MD5 sum: 32c2709a6002d9ee750483aaed6379c8
# Download size: 1.8 MB
# Estimated disk space required: 23 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# Colord Dependencies
# Required
# dbus-1.12.16, GLib-2.62.4, Little CMS-2.9, Polkit-0.116, and SQLite-3.31.0
# Recommended
# gobject-introspection-1.62.0, libgudev-233, libgusb-0.3.2, and Vala-0.46.5
# Optional
# gnome-desktop-3.34.3 and colord-gtk-0.2.0 (to build the example tools), libxslt-1.1.34, SANE-1.0.27, ArgyllCMS, and Bash Completion
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mv po/fur.po po/ur.po &&
sed -i 's/fur/ur/' po/LINGUAS
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
          -Ddaemon_user=colord     \
      -Dvapi=true              \
      -Dsystemd=false          \
      -Dlibcolordcompat=true   \
      -Dargyllcms_sensor=false \
      -Dbash_completion=false  \
      -Ddocs=false             \
      -Dman=false  \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
