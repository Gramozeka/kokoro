#!/bin/bash
###############################################################################
set -e
# OpenJPEG is an open-source implementation of the JPEG-2000 standard. OpenJPEG fully respects the JPEG-2000 specifications and can compress/decompress lossless 16-bit images.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/uclouvain/openjpeg/archive/v2.3.1/openjpeg-2.3.1.tar.gz
# Download MD5 sum: 3b9941dc7a52f0376694adb15a72903f
# Download size: 2.1 MB
# Estimated disk space required: 15 MB
# Estimated build time: 0.3 SBU
# OpenJPEG Dependencies
# Required
# CMake-3.16.3
# Optional
# Little CMS-2.9, libpng-1.6.37, LibTIFF-4.1.0, and Doxygen-1.8.17 (to build the API documentation)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir -v build &&
cd       build &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/usr \
       -DOPENJPEG_INSTALL_LIB_DIR=lib \
      -DBUILD_STATIC_LIBS=OFF .. &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
