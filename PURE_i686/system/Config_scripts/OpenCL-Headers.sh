#!/bin/bash
###############################################################################
set -e
###############################################################################

###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/include/CL
cp -rv CL/* ${pkg}/usr/include/CL
echo "$1 ----- $(date)" >> ${destdir}/loginstall
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/KhronosGroup/OpenCL-Headers.git
cd ${packagedir}
ARCH="noarch"
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
