#!/bin/bash
###############################################################################
set -e
# gstreamer is a streaming media framework that enables applications to share a common set of plugins for things like video encoding and decoding, audio encoding and decoding, audio and video filters, audio visualisation, web streaming and anything else that streams in real-time or otherwise. This package only provides base functionality and libraries. You may need at least gst-plugins-base-1.18.0 and one of Good, Bad, Ugly or Libav plugins.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-1.18.0.tar.xz
# Download MD5 sum: 0e661ed5bdf1d8996e430228d022628e
# Download size: 3.2 MB
# Estimated disk space required: 60 MB (with tests; add 36 MB for docs)
# Estimated build time: 0.4 SBU (Using parallelism=4; with tests; add 1.5 SBU for docs)
# gstreamer Dependencies
# Required
# GLib-2.62.4
# Recommended
# gobject-introspection-1.62.0
# Optional
# GTK+-3.24.13 (for examples), Gsl-2.6, GTK-Doc-1.32, Valgrind-3.15.0, and libunwind
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "s/leaks.c'/&, not tracer_hooks or not gst_debug/" -i tests/check/meson.build
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
         -Dgst_debug=false   \
       -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
