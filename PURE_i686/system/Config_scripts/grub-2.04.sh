#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
EFI32_FLAGS="--target=i386 --program-prefix= " \
CFLAGS="-O2" \
./configure --prefix=/usr          \
            --sbindir=/sbin        \
            --sysconfdir=/etc      \
            --disable-efiemu       \
            --disable-werror       \
            --enable-grub-mkfont   \
            --enable-grub-themes   \
            --enable-grub-mount    \
            --target=i386 \
            --program-prefix=

make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
