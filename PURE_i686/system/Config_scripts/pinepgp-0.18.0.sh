#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/pinepgp-0.18.0-makefile-sed-fix.diff.gz | patch -p1 --verbose || exit 1
zcat ${PATCHSOURCE}/pinepgp.pinegpgp.in.diff.gz | patch -p1 --verbose || exit 1
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
