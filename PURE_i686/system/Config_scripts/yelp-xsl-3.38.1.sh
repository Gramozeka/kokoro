#!/bin/bash
###############################################################################
set -e
# The Yelp XSL package contains XSL stylesheets that are used by the Yelp help browser to format Docbook and Mallard documents.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/yelp-xsl/3.34/yelp-xsl-3.34.2.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/yelp-xsl/3.34/yelp-xsl-3.34.2.tar.xz
# Download MD5 sum: b9c1c53a9114b42054789f212ab37f59
# Download size: 628 KB
# Estimated disk space required: 9.9 MB
# Estimated build time: less than 0.1 SBU
# Yelp XSL Dependencies
# Required
# libxslt-1.1.34 and itstool-2.0.6
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
