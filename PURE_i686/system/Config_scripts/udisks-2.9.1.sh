#!/bin/bash
###############################################################################
set -e
###############################################################################
# The UDisks package provides a daemon, tools and libraries to access and manipulate disks and storage devices.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/storaged-project/udisks/releases/download/udisks-2.8.4/udisks-2.8.4.tar.bz2
# Download MD5 sum: ee74a32fe2a7ab3dd3aa9e2283b844ea
# Download size: 1.5 MB
# Estimated disk space required: 36 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# UDisks Dependencies
# Required
# libatasmart-0.19, libblockdev-2.23, libgudev-233, libxslt-1.1.34, and Polkit-0.116
# Required at runtime
# btrfs-progs-5.4.1, dbus-1.12.16, dosfstools-4.1, gptfdisk-1.0.4, mdadm-4.1, and xfsprogs-5.4.0
# Recommended
# elogind-241.4
# Optional (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# D-Bus Python-1.2.16 (for the integration tests), GTK-Doc-1.32, LVM2-2.03.07, ntfs-3g-2017.3.23, PyGObject-3.34.0 (for the integration tests), exFAT, and libiscsi
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--enable-introspection=auto \
 --enable-lvm2 \
 --enable-lvmcache \
 --enable-btrfs \
 --with-udevdir=/lib \
 --with-tmpfilesdir=/tmp \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
