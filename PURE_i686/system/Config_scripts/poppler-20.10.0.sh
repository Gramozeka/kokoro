#!/bin/bash
###############################################################################
set -e
# The Poppler package contains a PDF rendering library and command line tools used to manipulate PDF files. This is useful for providing PDF rendering functionality as a shared library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://poppler.freedesktop.org/poppler-0.84.0.tar.xz
# Download MD5 sum: e14a8aca8809908ad4364c32c17bcb92
# Download size: 1.5 MB
# Estimated disk space required: 64 MB (with Qt5 library and tests)
# Estimated build time: 0.9 SBU (with parallelism=4, Qt5 library, and tests)
# Additional Downloads
# Poppler Encoding Data
# Download (HTTP): https://poppler.freedesktop.org/poppler-data-0.4.9.tar.gz
# Download MD5 sum: 35cc7beba00aa174631466f06732be40
# Download size: 4.1 MB
# Estimated disk space required: 13 MB
# Estimated build time: less than 0.1 SBU
# The additional package consists of encoding files for use with Poppler. The encoding files are optional and Poppler will automatically read them if they are present. When installed, they enable Poppler to render CJK and Cyrillic properly.
# Poppler Dependencies
# Required
# CMake-3.16.3 and Fontconfig-2.13.1
# Recommended
# Cairo-1.17.2+f93fc72c03e, Little CMS-2.9, libjpeg-turbo-2.0.4, libpng-1.6.37, NSS-3.49.2, and OpenJPEG-2.3.1
# Optional
# Boost-1.72.0, cURL-7.68.0, gdk-pixbuf-2.40.0, git-2.25.0 (for downloading test files), gobject-introspection-1.62.0, GTK-Doc-1.32, GTK+-3.24.13, LibTIFF-4.1.0, and Qt-5.14.0 (required for PDF support in Okular-19.08.3)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir build
cd build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake \
  -DCMAKE_INSTALL_PREFIX=/usr \
  -DBUILD_SHARED_LIBS=ON \
  -DENABLE_XPDF_HEADERS=ON \
  -DENABLE_CMS=lcms2 \
  -DENABLE_DCTDECODER=libjpeg \
  -DENABLE_LIBOPENJPEG=openjpeg2 \
  -DENABLE_XPDF_HEADERS=ON \
  -DENABLE_UNSTABLE_API_ABI_HEADERS=ON \
  -DENABLE_ZLIB=ON \
  -DENABLE_ZLIB_UNCOMPRESS:BOOL=ON \
  ..
make  -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
