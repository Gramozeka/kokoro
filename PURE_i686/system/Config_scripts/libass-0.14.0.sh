#!/bin/bash
###############################################################################
set -e
# libass is a portable subtitle renderer for the ASS/SSA (Advanced Substation Alpha/Substation Alpha) subtitle format that allows for more advanced subtitles than the conventional SRT and similar formats.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/libass/libass/releases/download/0.14.0/libass-0.14.0.tar.xz
# Download MD5 sum: 5b8c23340654587b8a472cb74ee9366b
# Download size: 348 KB
# Estimated disk space required: 6.3 MB
# Estimated build time: 0.1 SBU
# libass Dependencies
# Required
# FreeType-2.10.1, FriBidi-1.0.8, and NASM-2.14.02
# Recommended
# Fontconfig-2.13.1
# Optional
# HarfBuzz-2.6.4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
