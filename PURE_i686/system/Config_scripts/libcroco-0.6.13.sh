#!/bin/bash
###############################################################################
set -e
# The libcroco package contains a standalone CSS2 parsing and manipulation library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libcroco/0.6/libcroco-0.6.13.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libcroco/0.6/libcroco-0.6.13.tar.xz
# Download MD5 sum: c80c5a8385011a0260dce6bd0da93dce
# Download size: 480 KB
# Estimated disk space required: 10 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# libcroco Dependencies
# Required
# GLib-2.62.4 and libxml2-2.9.10
# Optional
# GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
