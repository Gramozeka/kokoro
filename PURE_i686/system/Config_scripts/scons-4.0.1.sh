#!/bin/bash
###############################################################################
# SCons is a tool for building software (and other files) implemented in Python.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/scons/scons-3.1.2.tar.gz
# Download MD5 sum: 77b2f8ac2661b7a4fad51c17cb7f1b25
# Download size: 656 KB
# Estimated disk space required: 5.5 MB
# Estimated build time: less than 0.1 SBU
# SCons Dependencies
# Optional
# docbook-xsl-1.79.2 and libxslt-1.1.34
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
python3 setup.py install  --prefix=/usr  \
                        --optimize=1 --install-data=/usr/share --root=${pkg} &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd SCons-4.0.1
sed -i 's/env python/&3/' SCons/Utilities/*.py            &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
python3 setup.py build
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

