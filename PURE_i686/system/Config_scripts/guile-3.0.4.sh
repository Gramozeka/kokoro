#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make DESTDIR=${pkg} install-html

mkdir -p                       ${pkg}/usr/share/gdb/auto-load/usr/lib &&
mv ${pkg}/usr/lib/libguile-*-gdb.scm ${pkg}/usr/share/gdb/auto-load/usr/lib &&
mv ${pkg}/usr/share/doc/${packagedir}/{guile.html,ref} &&
mv ${pkg}/usr/share/doc/${packagedir}/r5rs{.html,}     &&

find examples -name "Makefile*" -delete         &&
cp -vR examples   ${pkg}/usr/share/doc/${packagedir}   &&

for DIRNAME in r5rs ref; do
  install -v -m644  doc/${DIRNAME}/*.txt \
                    ${pkg}/usr/share/doc/${packagedir}/${DIRNAME}
done &&
unset DIRNAME

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make html &&

makeinfo --plaintext -o doc/r5rs/r5rs.txt doc/r5rs/r5rs.texi &&
makeinfo --plaintext -o doc/ref/guile.txt doc/ref/guile.texi
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
