#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/autotrace.patch
# autoreconf -vif
./autogen.sh
# # includedir=/usr/include/ImageMagick-6
# # includearchdir=/usr/include/ImageMagick-6
MAGICK_CFLAGS="-I/usr/include/ImageMagick-6  -fopenmp -DMAGICKCORE_HDRI_ENABLE=1 -DMAGICKCORE_QUANTUM_DEPTH=16" \
MAGICK_LIBS=
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
  --with-pstoedit \
  --enable-static=no \
  --enable-shared=yes \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
