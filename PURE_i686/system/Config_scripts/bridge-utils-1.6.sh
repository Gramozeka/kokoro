#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
make install DESTDIR=${pkg} || exit 1
mkdir -p ${pkg}/usr/include
install -m 644 libbridge/libbridge.h ${pkg}/usr/include
mkdir -p ${pkg}/usr/lib
install -m 644 libbridge/libbridge.a ${pkg}/usr/lib

mkdir -p ${pkg}/sbin
mv ${pkg}/usr/sbin/brctl ${pkg}/sbin/brctl
( cd ${pkg}/usr/sbin ; ln -sf ../../sbin/brctl brctl )
strip --strip-unneeded ${pkg}/sbin/brctl

strip -g ${pkg}/usr/lib/libbridge.a
echo "$1 ----- $(date)" >> ${destdir}/loginstall
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoconf
# patch -Np1 -i ${PATCHSOURCE}/
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
  --includedir=/usr/include \
  --with-linux-headers=/usr/include
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
