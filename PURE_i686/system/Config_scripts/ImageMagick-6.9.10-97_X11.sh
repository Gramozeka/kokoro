#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install-libs-only

echo "$1 ----- $(date)" >> ${destdir}/loginstall
# rm -f $PKG/usr/lib/*.la
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
# rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\_X11*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/ImageMagick-6.9.10-93-libs_only-1.patch 
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
autoreconf -fiv
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
            --enable-hdri     \
            --with-modules    \
            --with-gslib \
            --with-rsvg \
            --with-gvc \
            --with-autotrace \
            --with-libtiff \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
