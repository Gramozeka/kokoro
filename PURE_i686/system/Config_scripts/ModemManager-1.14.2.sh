#!/bin/bash
###############################################################################
set -e
###############################################################################
# ModemManager provides a unified high level API for communicating with mobile broadband modems, regardless of the protocol used to communicate with the actual device.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/ModemManager/ModemManager-1.12.4.tar.xz
# Download MD5 sum: 22280110d75c87a89786a317aa9cee04
# Download size: 2.0 MB
# Estimated disk space required: 115 MB (with tests)
# Estimated build time: 0.5 SBU (Using parallelism=4; with tests)
# ModemManager Dependencies
# Required
# libgudev-233
# Recommended
# elogind-241.4, gobject-introspection-1.62.0, libmbim-1.22.0, libqmi-1.24.4, Polkit-0.116, and Vala-0.46.5
# Optional
# GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
            --enable-more-warnings=no     \
            --with-systemd-journal=no     \
            --with-systemd-suspend-resume=no \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
