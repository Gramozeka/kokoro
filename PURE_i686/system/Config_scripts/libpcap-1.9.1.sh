#!/bin/bash
###############################################################################
set -e
# libpcap provides functions for user-level packet capture, used in low-level network monitoring.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.tcpdump.org/release/libpcap-1.9.1.tar.gz
# Download MD5 sum: 21af603d9a591c7d96a6457021d84e6c
# Download size: 844 KB
# Estimated disk space required: 9.3 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Recommended patch: http://www.linuxfromscratch.org/patches/blfs/svn/libpcap-1.9.1-enable_bluetooth-1.patch (Needed for bluez-5.21)
# libpcap Dependencies
# Optional
# BlueZ-5.52, libnl-3.5.0, libusb-1.0.23, Software distribution for the DAG, and Septel range of passive network monitoring cards.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/libpcap-1.9.1-enable_bluetooth-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS 
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
sed -i '/INSTALL_DATA.*libpcap.a\|RANLIB.*libpcap.a/ s/^/#/' Makefile
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
