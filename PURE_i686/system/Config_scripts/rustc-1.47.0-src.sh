#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Rust programming language is designed to be a safe, concurrent, practical language.
# This package is updated on a six-weekly release cycle. Because it is such a large and slow package to build, and is at the moment only required by a few packages in this book, the BLFS editors take the view that it should only be updated when that is necessary (either to fix problems, or to allow a new version of firefox to build).
# As with many other programming languages, rustc (the rust compiler) needs a binary from which to bootstrap. It will download a stage0 binary and many cargo crates (these are actually .tar.gz source archives) at the start of the build, so you cannot compile it without an internet connection.
# These crates will then remain in various forms (cache, directories of extracted source), in ~/.cargo for ever more. It is common for large rust packages to use multiple versions of some crates. If you purge the files before updating this package, very few crates will need to be updated by the packages in this book which use it (and they will be downloaded as required). But if you retain an older version as a fallback option and then use it (when not building in /usr), it is likely that it will then have to re-download some crates. For a full download (i.e. starting with an empty or missing ~/.cargo) downloading the external cargo files for this version only takes a minute or so on a fast network.
# [Note] Note
# Although BLFS usually installs in /usr, when you later upgrade to a newer version of rust the old libraries in /usr/lib/rustlib will remain, with various hashes in their names, but will not be usable and will waste space. The editors recommend placing the files in the /opt directory. In particular, if you have reason to rebuild with a modified configuration (e.g. using the shipped LLVM after building with shared LLVM, but perhaps also the reverse situation) it it possible for the install to leave a broken cargo program. In such a situation, either remove the existing installation first, or use a different prefix such as /opt/rustc-1.47.0-build2.
# If you prefer, you can of course change the prefix to /usr and omit the ldconfig and the actions to add rustc to the PATH.
# The current rustbuild build-system will use all available processors, although it does not scale well and often falls back to just using one core while waiting for a library to compile.
# At the moment Rust does not provide any guarantees of a stable ABI.
# [Note] Note
# Rustc defaults to building for ALL supported architectures, using a shipped copy of LLVM. In BLFS the build is only for the X86 architecture. Rustc still claims to require Python 2, but that is only really necessary when building some other architectures with the shipped LLVM. If you intend to develop rust crates, this build may not be good enough for your purposes.
# The build times of this version when repeated on the same machine are often reasonably consistent, but as with all compilations using rustc there can be some very slow outliers.
# Unusually, a DESTDIR-style method is being used to install this package. This is because running the install as root not only downloads all of the cargo files again (to /root/.cargo), it then spends a very long time recompiling. Using this method saves a lot of time, at the cost of extra disk space.
# This package is known to build and work properly using an LFS-9.0 platform.
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
###############################################################################
pkg=${destdir}/${packagedir}

mkdir -pv ${pkg}/opt/rustc-1.47.0
mkdir -pv /opt/rustc-1.47.0
ln -svfin rustc-1.47.0 ${pkg}/opt/rustc
ln -svfin rustc-1.47.0 /opt/rustc
cat << EOF > config.toml
# see config.toml.example for more possible options
# See the 8.4 book for an example using shipped LLVM
# e.g. if not installing clang, or using a version before 8.0.
[llvm]
# by default, rust will build for a myriad of architectures
targets = "X86"

# When using system llvm prefer shared libraries
link-shared = true

[build]
# omit docs to save time and space (default is to build them)
docs = false

# install cargo as well as rust
extended = true

[install]
prefix = "/opt/rustc-1.47.0"
docdir = "share/doc/rustc-1.47.0"

[rust]
channel = "stable"
rpath = false

# BLFS does not install the FileCheck executable from llvm,
# so disable codegen tests
codegen-tests = false

[target.x86_64-unknown-linux-gnu]
# NB the output of llvm-config (i.e. help options) may be
# dumped to the screen when config.toml is parsed.
llvm-config = "/usr/bin/llvm-config"

[target.i686-unknown-linux-gnu]
# NB the output of llvm-config (i.e. help options) may be
# dumped to the screen when config.toml is parsed.
llvm-config = "/usr/bin/llvm-config"


EOF

LDFLAGS="-L/lib:/usr/lib"
LD_LIBRARY_PATH="/lib:/usr/lib"
export PKG_CONFIG_PATH=/usr/lib/pkgconfig
# CC="gcc $CFLAGS"
# CXX="g++ $CXXFLAGS"
# patch -Np1 -i ${PATCHSOURCE}/rustc-1.47.0-llvm9_fixes-1.patch
export RUSTFLAGS="$RUSTFLAGS -C link-args=-lffi" &&
python3 ./x.py build --exclude src/tools/miri

export LIBSSH2_SYS_USE_PKG_CONFIG=1 &&
DESTDIR=${pkg}  python3 ./x.py install &&
unset LIBSSH2_SYS_USE_PKG_CONFIG
mkdir -pv ${pkg}/etc/profile.d

cat >> /etc/ld.so.conf << EOF
# Begin rustc addition

/opt/rustc/lib

# End rustc addition
EOF

cat > ${pkg}/etc/profile.d/rustc.sh << "EOF"
# Begin /etc/profile.d/rustc.sh

pathprepend /opt/rustc/bin           PATH

# End /etc/profile.d/rustc.sh
EOF

# chown -R root:root install &&
# cp -avr install/* ${pkg}
chown -R root:root ${pkg}
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
# find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home

###############################################################################
###############################################################################
echo "###########################**** COMPLITE!!! ****##################################"
echo "y" | ln -svfin rustc-1.47.0 /opt/rustc
