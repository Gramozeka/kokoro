#!/bin/bash
###############################################################################
set -e
# Xdg-user-dirs is a tool to help manage “well known” user directories like the desktop folder and the music folder. It also handles localization (i.e. translation) of the filenames.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://user-dirs.freedesktop.org/releases/xdg-user-dirs-0.17.tar.gz
# Download MD5 sum: e0564ec6d838e6e41864d872a29b3575
# Download size: 251 KB
# Estimated disk space required: 3.0 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
