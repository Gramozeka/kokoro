#!/bin/bash
###############################################################################
set -e
###############################################################################
# The BIND package provides a DNS server and client utilities. If you are only interested in the utilities, refer to the BIND Utilities-9.14.10.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (FTP): ftp://ftp.isc.org/isc/bind9/9.14.10/bind-9.14.10.tar.gz
# Download MD5 sum: c4b4e48ae6dc87da4cae333665c0b4e3
# Download size: 6.0 MB
# Estimated disk space required: 101 MB (24 MB installed)
# Estimated build time: 1.0 SBU (with parallelism=4; add 34+ minutes, processor independent, to run the complete test suite)
# BIND Dependencies
# Recommended
# libcap-2.31 with PAM
# Optional
# libidn2-2.3.0, libxml2-2.9.10, MIT Kerberos V5-1.17.1, cmocka, and geoip
# Optional database backends
# Berkeley DB-5.3.28, MariaDB-10.4.11 or MySQL, OpenLDAP-2.4.48, PostgreSQL-12.1, and unixODBC-2.3.7
# Optional (to run the test suite)
# Net-DNS-1.21
# Optional (to rebuild the documentation)
# Doxygen-1.8.17, libxslt-1.1.34, and texlive-20190410 (or install-tl-unx)
###############################################################################
pip3 install ply
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
# # # # install -v -m755 -d ${pkg}/usr/share/doc/${packagedir}/arm &&
# # # # install -v -m644    doc/arm/*.html \
# # # #                     ${pkg}/usr/share/doc/${packagedir}/arm
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--with-libtool          \
--with-libidn2 \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
