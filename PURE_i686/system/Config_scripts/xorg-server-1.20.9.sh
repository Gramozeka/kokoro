#!/bin/bash
set -e
###############################################################################
# The Xorg Server is the core of the X Window system.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.x.org/pub/individual/xserver/xorg-server-1.20.7.tar.bz2
# Download (FTP): ftp://ftp.x.org/pub/individual/xserver/xorg-server-1.20.7.tar.bz2
# Download MD5 sum: d2e96355ad47244c675bce38db2b48a9
# Download size: 6.0 MB
# Estimated disk space required: 257 MB (with tests)
# Estimated build time: 0.8 SBU (using parallelism=4; with tests)
# Xorg Server Dependencies
# Required
# Pixman-0.38.4 and Xorg Fonts (only font-util), and at runtime: xkeyboard-config-2.28
# Recommended
# elogind-241.4, libepoxy-1.5.4 (needed for glamor and Xwayland), Polkit-0.116 (runtime), Wayland-1.17.0 (needed for Xwayland), and wayland-protocols-1.18
# Optional
# acpid-2.0.32 (runtime), Doxygen-1.8.17 (to build API documentation), fop-2.4 (to build documentation), Nettle-3.5.1, libgcrypt-1.8.5, xcb-util-keysyms-0.4.0, xcb-util-image-0.4.0, xcb-util-renderutil-0.3.9, xcb-util-wm-0.4.1 (all three to build Xephyr), xmlto-0.0.28 (to build documentation), libunwind, and xorg-sgml-doctools (to build documentation)
###############################################################################
DEF_FONTPATH="/usr/share/fonts/Speedo,/usr/share/fonts/TTF,/usr/share/fonts/OTF,/usr/share/fonts/Type1,/usr/share/fonts/terminus,/usr/share/fonts/misc,/usr/share/fonts/75dpi/:unscaled,/usr/share/fonts/100dpi/:unscaled,/usr/share/fonts/75dpi,/usr/share/fonts/100dpi,/usr/share/fonts/cyrillic,/usr/share/fonts/cantarell,/usr/share/fonts/ghostscript-9.53.3"
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}

BUILD_SERVERS="--enable-xorg \
  --enable-dmx \
  --enable-xvfb \
  --enable-xnest \
  --enable-glamor \
  --enable-kdrive \
  --enable-xephyr \
  --enable-xfbdev \
  --enable-config-udev \
  --enable-kdrive \
  --disable-config-hal \
   --enable-xf86bigfont"
#   --disable-systemd-logind"
###################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/etc/X11/xorg.conf.d &&
cat >> /etc/sysconfig/createfiles << "EOF"
/tmp/.ICE-unix dir 1777 root root
/tmp/.X11-unix dir 1777 root root
EOF
cat >> ${pkg}/etc/X11/xorg.conf.d/20-synaptics.conf << "EOF"
Section "InputClass"
    Identifier "touchpad"
    Driver "synaptics"
    MatchIsTouchpad "on"
        Option "TapButton1" "1"
        Option "TapButton2" "2"
        Option "TapButton3" "3"
        Option "VertEdgeScroll" "on"
        Option "VertTwoFingerScroll" "on"
        Option "HorizEdgeScroll" "on"
        Option "HorizTwoFingerScroll" "on"
        Option "CircularScrolling" "on"
        Option "CircScrollTrigger" "2"
        Option "EmulateTwoFingerMinZ" "40"
        Option "EmulateTwoFingerMinW" "8"
        Option "CoastingSpeed" "0"
        Option "FingerLow" "35"
        Option "FingerHigh" "40"
EndSection
EOF
cat >> ${pkg}/etc/X11/xorg.conf.d/10-evdev.conf << "EOF"
Section "InputClass"
      Identifier "evdev keyboard catchall"
      MatchIsKeyboard "on"
      MatchDevicePath "/dev/input/event*"
      Driver "evdev"
      # Keyboard layouts
      Option "XkbModel" "pc104"				
      Option "XkbLayout" "us, ru(winkeys)"		
      Option "XkbVariant" ","
      Option "XkbOptions" "grp:ctrl_shift_toggle, grp_led:scroll, terminate:ctrl_alt_bksp"
EndSection
EOF

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
unpack ${SOURCE}/Xorg/xorg-server/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib \
  --sysconfdir=/etc \
  --localstatedir=/var \
   --enable-suid-wrapper \
  --with-int10=x86emu \
  --infodir=/usr/info \
  --disable-static \
  --with-pic \
  --with-os-name="Phoniex" \
  --with-os-vendor="CLFS" \
  $BUILD_SERVERS --with-default-font-path="${DEF_FONTPATH}" \
  --with-xkb-output=/var/lib/xkb \
--build=${CLFS_TARGET} 
  sed -i -e 's#-ldl##' hw/xfree86/Makefile
  sed -i -e 's#-lm#-lm -ldl#' hw/xfree86/Makefile
make -j4
pack_local64 ${packagedir}

