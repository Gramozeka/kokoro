#!/bin/bash
###############################################################################
set -e
###############################################################################
# https://salsa.debian.org/iso-codes-team/iso-codes
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--localstatedir=/var
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
