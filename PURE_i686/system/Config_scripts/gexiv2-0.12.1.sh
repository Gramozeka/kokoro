#!/bin/bash
###############################################################################
set -e
###############################################################################
# gexiv2 is a GObject-based wrapper around the Exiv2 library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://download.gnome.org/sources/gexiv2/0.12/gexiv2-0.12.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/GNOME/sources/gexiv2/0.12/gexiv2-0.12.0.tar.xz
# Download MD5 sum: 0a618c5b053106d1801d89cc77385419
# Download size: 364 KB
# Estimated disk space required: 5.4 MB
# Estimated build time: 0.1 SBU
# gexiv2 Dependencies
# Required
# Exiv2-0.27.2
# Recommended
# Vala-0.46.5
# Optional
# GTK-Doc-1.32 (for documentation) and Python-2.7.17 (for the Python2 module)
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
