#!/bin/bash
###############################################################################
set -e
# The libnl suite is a collection of libraries providing APIs to netlink protocol based Linux kernel interfaces.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/thom311/libnl/releases/download/libnl3_5_0/libnl-3.5.0.tar.gz
# Download MD5 sum: 74ba57b1b1d6f9f92268aa8141d8e8e4
# Download size: 948 KB
# Estimated disk space required: 78 MB (with tests and API documentation)
# Estimated build time: 0.5 SBU (with tests and API documentation)
# Optional Download
# Download (HTTP): https://github.com/thom311/libnl/releases/download/libnl3_5_0/libnl-doc-3.5.0.tar.gz
# Download MD5 sum: 43a1a6f0c39f32bee05287c06c500bce
# Download size: 11 MB
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -vp ${pkg}/usr/share/doc/libnl-3.5.0 &&
tar -xf ${SOURCE}/libnl-doc-3.5.0.tar.gz --strip-components=1 --no-same-owner \
    -C  ${pkg}/usr/share/doc/libnl-3.5.0
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
