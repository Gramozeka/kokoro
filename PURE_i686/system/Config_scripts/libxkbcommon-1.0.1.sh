#!/bin/bash
###############################################################################
set -e
# libxkbcommon is a keymap compiler and support library which processes a reduced subset of keymaps as defined by the XKB specification.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://xkbcommon.org/download/libxkbcommon-0.10.0.tar.xz
# Download MD5 sum: 2d9ad3a46b317138b5e72a91cf105451
# Download size: 376 KB
# Estimated disk space required: 25 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# libxkbcommon Dependencies
# Required
# xkeyboard-config-2.28
# Recommended
# libxcb-1.13.1 and Wayland-1.17.0
# Optional
# Doxygen-1.8.17
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
