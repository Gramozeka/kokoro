#!/bin/bash
###############################################################################
set -e
# libidn2 is a package designed for internationalized string handling based on standards from the Internet Engineering Task Force (IETF)'s IDN working group, designed for internationalized domain names.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/libidn/libidn2-2.3.0.tar.gz
# Download (FTP): ftp://ftp.gnu.org/gnu/libidn/libidn2-2.3.0.tar.gz
# Download MD5 sum: 01c5084995295e519f95978ae9785ee0
# Download size: 2.1 MB
# Estimated disk space required: 21 MB (with tests)
# Estimated build time: 0.1 SBU (with tests)
# libidn2 Dependencies
# Required
# libunistring-0.9.10
# Optional
# git-2.25.0 and GTK-Doc-1.32'
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
