#!/bin/bash
###############################################################################
set -e
###############################################################################
# Openbox is a highly configurable desktop window manager with extensive standards support. It allows you to control almost every aspect of how you interact with your desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://openbox.org/dist/openbox/openbox-3.6.1.tar.gz
# Download MD5 sum: b72794996c6a3ad94634727b95f9d204
# Download size: 944 KB
# Estimated disk space required: 21 MB
# Estimated build time: 0.3 SBU
# Openbox Dependencies
# Required
# X Window System and Pango-1.44.7 (compiled with support for libXft)
# Optional
# dbus-1.12.16 (runtime), imlib2-1.6.1 (to enable icons in the right click menu), ImageMagick-7.0.8-60 (to show desktop backgrounds as seen in the Configuration Information section below), PyXDG-0.25, startup-notification-0.12, and librsvg-2.46.4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
2to3-3.8 -w data/autostart/openbox-xdg-autostart &&
sed 's/python/python3/' -i data/autostart/openbox-xdg-autostart
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cat > ~/.xinitrc << "EOF"
display -backdrop -window root /usr/share/wallpapers/background.jpg
exec openbox
EOF
