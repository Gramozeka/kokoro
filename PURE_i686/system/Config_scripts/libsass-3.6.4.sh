#!/bin/bash
###############################################################################
set -e
###############################################################################
# Download (HTTP): https://github.com/sass/libsass/archive/3.6.1/libsass-3.6.1.tar.gz
# Download MD5 sum: 8b0aee63fd535cf6f40b254a6f453d24
# Download size: 328 KB
# Estimated disk space required: 157 MB
# Estimated build time: 0.4 SBU (Using parallelism=4)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -fiv
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
