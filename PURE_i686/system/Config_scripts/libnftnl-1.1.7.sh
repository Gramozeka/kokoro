#!/bin/bash
###############################################################################
set -e
# The libnftnl library provides a low-level netlink programming interface (API) to the in-kernel nf_tables subsystem.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://netfilter.org/projects/libnftnl/files/libnftnl-1.1.5.tar.bz2
# Download MD5 sum: a72ae260f7da9c223ad5d4fa036a8f84
# Download size: 368 KB
# Estimated disk space required: 8.7 MB
# Estimated build time: less than 0.1 SBU
# libnftnl Dependencies
# Required
# libmnl-1.0.4
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/lib
mv ${pkg}/usr/lib/libnftnl.so.* ${pkg}/lib &&
ln -sfv ../../lib/$(readlink ${pkg}/usr/lib/libnftnl.so) ${pkg}/usr/lib/libnftnl.so
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
