cd $TEMPBUILD

unpack ${SOURCE}/aspell6-ru-0.99f7-1.tar.bz2
cd aspell6-ru-0.99f7-1
./configure &&
make -j4 || make
pack aspell6-ru-0.99f7-1
cd $TEMPBUILD
unpack ${SOURCE}/aspell6-en-2018.04.16-0.tar.bz2
cd aspell6-en-2018.04.16-0
./configure &&
make -j4 || make
pack aspell6-en-2018.04.16-0
