#!/bin/bash
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
# rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
cd ${packagedir}
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}

# patch -Np1 -i ${PATCHSOURCE}/
cat > mozconfig << "EOF"
# If you have a multicore machine, all cores will be used by default.

# If you have installed (or will install) wireless-tools, and you wish
# to use geolocation web services, comment out this line
ac_add_options --disable-necko-wifi
ac_add_options --enable-artifact-builds
# API Keys for geolocation APIs - necko-wifi (above) is required for MLS
# Uncomment the following line if you wish to use Mozilla Location Service
ac_add_options --with-mozilla-api-keyfile=$PWD/mozilla-key

# Uncomment the following line if you wish to use Google's geolocaton API
# (needed for use with saved maps with Google Maps)
ac_add_options --with-google-location-service-api-keyfile=$PWD/google-key

# startup-notification is required since firefox-78

# Uncomment the following option if you have not installed PulseAudio
#ac_add_options --disable-pulseaudio
# or uncomment this if you installed alsa-lib instead of PulseAudio
#ac_add_options --enable-alsa

# Comment out following options if you have not installed
# recommended dependencies:
ac_add_options --with-system-libevent
ac_add_options --with-system-webp
# ac_add_options --with-system-nspr
# ac_add_options --with-system-nss
ac_add_options --with-system-icu

# Do not specify the gold linker which is not the default. It will take
# longer and use more disk space when debug symbols are disabled.

# libdavid (av1 decoder) requires nasm. Uncomment this if nasm
# has not been installed.
#ac_add_options --disable-av1

# You cannot distribute the binary if you do this
ac_add_options --enable-official-branding

# Stripping is now enabled by default.
# Uncomment these lines if you need to run a debugger:
#ac_add_options --disable-strip
#ac_add_options --disable-install-strip

# Disabling debug symbols makes the build much smaller and a little
# faster. Comment this if you need to run a debugger. Note: This is
# required for compilation on i686.
#ac_add_options --disable-debug-symbols

# The elf-hack is reported to cause failed installs (after successful builds)
# on some machines. It is supposed to improve startup time and it shrinks
# libxul.so by a few MB - comment this if you know your machine is not affected.
# ac_add_options --disable-elf-hack

# The BLFS editors recommend not changing anything below this line:
ac_add_options --prefix=/usr
ac_add_options --libdir=/usr/lib
ac_add_options --enable-application=browser
ac_add_options --disable-crashreporter
ac_add_options --disable-updater
# enabling the tests will use a lot more space and significantly
# increase the build time, for no obvious benefit.
ac_add_options --disable-tests

# The default level of optimization again produces a working build with gcc.
#ac_add_options --enable-optimize

# ac_add_options --enable-system-ffi
ac_add_options --enable-system-pixman

# --with-system-bz2 was removed in firefox-78
# ac_add_options --with-system-jpeg
ac_add_options --with-system-png
ac_add_options --with-system-zlib

# The following option unsets Telemetry Reporting. With the Addons Fiasco,
# Mozilla was found to be collecting user's data, including saved passwords and
# web form data, without users consent. Mozilla was also found shipping updates
# to systems without the user's knowledge or permission.
# As a result of this, use the following command to permanently disable
# telemetry reporting in Firefox.
unset MOZ_TELEMETRY_REPORTING

mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/firefox-build-dir
EOF
echo "AIzaSyDxKL42zsPjbke5O8_rPVpVrLrJ8aeE9rQ" > $PWD/google-key
echo "613364a7-9418-4c86-bcee-57e32fd70c23" > $PWD/mozilla-key
# # # 
# # # sed -i.allow-warnings -e '/#!\[deny(warnings)\]/a #![allow(unused_imports)]' \
# # #   servo/components/style/lib.rs
# # # zcat $PATCHSOURCE/unbreakdocs.diff.gz | patch -p1 --verbose || exit 1
# # # export MOZILLA_OFFICIAL="1"
# # # export BUILD_OFFICIAL="1"
# # # export MOZ_PHOENIX="1"
# # # export MOZ_PACKAGE_JSSHELL="1"
# # # export CFLAGS="-Os"
# # # export CXXFLAGS="$CFLAGS -fno-delete-null-pointer-checks"
export MOZ_MAKE_FLAGS="-j4"
# # # 
# # # # Clear some variables that could break the build
# # # unset DBUS_SESSION_BUS_ADDRESS ORBIT_SOCKETDIR SESSION_MANAGER \
# # #   XDG_SESSION_COOKIE XAUTHORITY MAKEFLAGS
# # # export RUSTFLAGS="-Cdebuginfo=0"
# # # export LDFLAGS=" -Wl,--as-needed -Wl,--no-keep-memory -Wl,--stats"
# # # export MOZ_LINK_FLAGS=" -Wl,--as-needed -Wl,--no-keep-memory -Wl,--stats"
unset CFLAGS CXXFLAGS
export PKG_CONFIG=/usr/bin/pkg-config
export MACH_USE_SYSTEM_PYTHON=1
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
export MOZBUILD_STATE_PATH=${PWD}/mozbuild &&
export SHELL=/bin/sh &&
./mach build

DESTDIR=${pkg} ./mach install                                                  &&

mkdir -pv  ${pkg}/usr/lib/mozilla/plugins                             &&
ln    -sfv ../../mozilla/plugins ${pkg}/usr/lib/firefox/browser/

unset CC CXX MOZBUILD_STATE_PATH

mkdir -pv ${pkg}/usr/share/applications &&
mkdir -pv ${pkg}/usr/share/pixmaps &&

cat > ${pkg}/usr/share/applications/firefox.desktop << "EOF" &&
[Desktop Entry]
Encoding=UTF-8
Name=Firefox Web Browser
Comment=Browse the World Wide Web
GenericName=Web Browser
Exec=firefox %u
Terminal=false
Type=Application
Icon=firefox
Categories=GNOME;GTK;Network;WebBrowser;
MimeType=application/xhtml+xml;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
EOF

ln -sfv /usr/lib/firefox/browser/chrome/icons/default/default128.png \
        ${pkg}/usr/share/pixmaps/firefox.png
        
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
echo "###########################**** COMPLITE!!! ****##################################"
