#!/bin/bash
###############################################################################
set -e
###############################################################################
###https://swarm.workshop.perforce.com/files/guest/perforce_software/jam/jam-2.6.1.zip
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/bin
./jam0 -sBINDIR=${pkg}/usr/bin -sOPTIM="$CFLAGS" install
strip --strip-unneeded ${pkg}/usr/bin/jam
echo "$1 ----- $(date)" >> ${destdir}/loginstall
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
