#!/bin/bash
###############################################################################
# Libraw is a library for reading RAW files obtained from digital photo cameras (CRW/CR2, NEF, RAF, DNG, and others).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.libraw.org/data/LibRaw-0.19.5.tar.gz
# Download MD5 sum: 865ab9a40910709ff86988e8c0a7d146
# Download size: 1.3 MB
# Estimated disk space required: 23 MB
# Estimated build time: 0.2 SBU
# libraw Dependencies
# Recommended
# libjpeg-turbo-2.0.4, JasPer-2.0.14, and Little CMS-2.9
# Optional
# LibRaw-demosaic-pack-GPL2 and LibRaw-demosaic-pack-GPL3
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# # # # unpack ${SOURCE}/LibRaw-demosaic-pack-GPL2-0.18.8.tar.gz
# # # # unpack ${SOURCE}/LibRaw-demosaic-pack-GPL3-0.18.8.tar.gz
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
            --enable-jpeg    \
            --enable-jasper  \
            --enable-lcms    \
            --disable-static \
            --docdir=/usr/share/doc/${packagedir} \
            --build=${CLFS_TARGET} &&
# # # # # --enable-demosaic-pack-gpl2=./LibRaw-demosaic-pack-GPL2-0.18.8 \
# # # # # --enable-demosaic-pack-gpl3=./LibRaw-demosaic-pack-GPL3-0.18.8 \

make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
