#!/bin/bash
###############################################################################
set -e
# The Libcap package implements the user-space interfaces to the POSIX 1003.1e capabilities available in Linux kernels. These capabilities are a partitioning of the all powerful root privilege into a set of distinct privileges.
# The libcap package was installed in LFS, but if Linux-PAM support is desired, the PAM module must be built (after installation of Linux-PAM).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-2.31.tar.xz
# Download MD5 sum: 52120c05dc797b01f5a7ae70f4335e96
# Download size: 97 KB
# Estimated disk space required: 1 MB
# Estimated build time: less than 0.1 SBU
# libcap Dependencies
# Required
# Linux-PAM-1.3.1
###############################################################################

###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/lib
mkdir -pv  ${pkg}/lib
make DESTDIR=${pkg}  lib=lib install
chmod -v 755 ${pkg}/lib/libcap.so.2.43
ln -sv ../../lib/libcap.so.2.43 ${pkg}/usr/lib/libcap.so
mv -vf ${pkg}/lib/pkgconfig ${pkg}/usr/lib/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/install.*STALIBNAME/d' libcap/Makefile
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make lib=lib
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
