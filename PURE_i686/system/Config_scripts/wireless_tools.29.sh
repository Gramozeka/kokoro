#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Wireless Extension (WE) is a generic API in the Linux kernel allowing a driver to expose configuration and statistics specific to common Wireless LANs to user space. A single set of tools can support all the variations of Wireless LANs, regardless of their type as long as the driver supports Wireless Extensions. WE parameters may also be changed on the fly without restarting the driver (or Linux).
# The Wireless Tools (WT) package is a set of tools allowing manipulation of the Wireless Extensions. They use a textual interface to support the full Wireless Extension.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://hewlettpackard.github.io/wireless-tools/wireless_tools.29.tar.gz
# Download MD5 sum: e06c222e186f7cc013fd272d023710cb
# Download size: 288 KB
# Estimated disk space required: 2.0 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/wireless_tools-29-fix_iwlist_scanning-1.patch
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/WirelessTools
# Kernel Configuration
# To use Wireless Tools, the kernel must have the appropriate drivers and other support available. The appropriate bus must also be available. For many laptops, the PCMCIA bus (CONFIG_PCCARD) needs to be built. In some cases, this bus support will also need to be built for embedded wireless cards. The appropriate bridge support also needs to be built. For many modern laptops, the CardBus host bridge (CONFIG_YENTA) will be needed.
# In addition to the bus, the actual driver for the specific wireless card must also be available. There are many wireless cards and they don't all work with Linux. The first place to look for card support is the kernel. The drivers are located in Device Drivers → Network Device Support → Wireless LAN (non-hamradio). There are also external drivers available for some very common cards. For more information, look at the user notes.
# After the correct drivers are loaded, the interface will appear in /proc/net/wireless.'
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
install -m 755 -d ${pkg}/usr/lib/
install -m 755 libiw.so.29 /usr/lib/
ln -sfn libiw.so.29 /usr/lib/libiw.so
install -m 755 -d ${pkg}/usr/sbin/
install -m 755 iwconfig iwlist iwpriv iwspy iwgetid iwevent ifrename ${pkg}/usr/sbin/
install -m 755 -d ${pkg}/usr/include/
install -m 644 iwlib.h ${pkg}/usr/include/
install -m 644 wireless.h ${pkg}/usr/include/
install -m 755 -d ${pkg}/usr/share/man/man8/
install -m 644 iwconfig.8 iwlist.8 iwpriv.8 iwspy.8 iwgetid.8 iwevent.8 ifrename.8 ${pkg}/usr/share/man/man8/
install -m 755 -d ${pkg}/usr/share/man/man7/
install -m 644 wireless.7 ${pkg}/usr/share/man/man7/
install -m 755 -d ${pkg}/usr/share/man/man5/
install -m 644 iftab.5 ${pkg}/usr/share/man/man5/
install -m 755 -d ${pkg}/etc/udev/rules.d/
install -m 644 19-udev-ifrename.rules ${pkg}/etc/udev/rules.d/


echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/wireless_tools-29-fix_iwlist_scanning-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
