#!/bin/bash
###############################################################################
set -e
###############################################################################
# The CDParanoia package contains a CD audio extraction tool. This is useful for extracting .wav files from audio CDs. A CDDA capable CDROM drive is needed. Practically all drives supported by Linux can be used.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.xiph.org/releases/cdparanoia/cdparanoia-III-10.2.src.tgz
# Download MD5 sum: b304bbe8ab63373924a744eac9ebc652
# Download size: 179 KB
# Estimated disk space required: 2.9 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/cdparanoia-III-10.2-gcc_fixes-1.patch
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
install -d ${pkg}/usr/bin
install -d ${pkg}/usr/include/cdda
install -d ${pkg}/usr/lib
install -d ${pkg}/usr/share/man/man1
install -m 0755 cdparanoia ${pkg}/usr/bin/
install -m 0644 cdparanoia.1  ${pkg}/usr/share/man/man1/
install -m 0644 utils.h paranoia/cdda_paranoia.h interface/cdda_interface.h \
	${pkg}/usr/include/cdda/
install -m 0755 paranoia/libcdda_paranoia.so.0.10.? \
	interface/libcdda_interface.so.0.10.? \
	${pkg}/usr/lib/
install -m 0644 paranoia/libcdda_paranoia.a interface/libcdda_interface.a \
	${pkg}/usr/lib/

ldconfig -n ${pkg}/usr/lib

( cd ${pkg}/usr/lib
  ln -sf libcdda_paranoia.so.0.10.? libcdda_paranoia.so
  ln -sf libcdda_interface.so.0.10.? libcdda_interface.so
)
chmod -v 755 ${pkg}/usr/lib/libcdda_*.so.0.10.2
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/cdparanoia-III-10.2-gcc_fixes-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
OPT="-O2" \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--includedir=/usr/include/cdda \
--mandir=/usr/share/man \
--build=${CLFS_TARGET}
make -j1 OPT="-O2"
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
