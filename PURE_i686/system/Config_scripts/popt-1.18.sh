#!/bin/bash
###############################################################################
set -e
# The popt package contains the popt libraries which are used by some programs to parse command-line options.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://rpm5.org/files/popt/popt-1.16.tar.gz
# Download (FTP): ftp://anduin.linuxfromscratch.org/BLFS/popt/popt-1.16.tar.gz
# Download MD5 sum: 3743beefa3dd6247a73f8f7a32c14c33
# Download size: 702 kB
# Estimated disk space required: 8 MB (includes installing documentation)
# Estimated build time: 0.1 SBU
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -m755 -d ${pkg}/usr/share/doc/${packagedir}
install -v -m644 doxygen/html/* ${pkg}/usr/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
sed -i 's@\./@src/@' Doxyfile &&
doxygen
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
