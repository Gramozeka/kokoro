#!/bin/bash
###############################################################################
set -e
###############################################################################
# The libgsf package contains a library used for providing an extensible input/output abstraction layer for structured file formats.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libgsf/1.14/libgsf-1.14.46.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libgsf/1.14/libgsf-1.14.46.tar.xz
# Download MD5 sum: 5bc6d1d6394f0ed5a58e8f2e5e4ead7f
# Download size: 676 KB
# Estimated disk space required: 15 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# libgsf Dependencies
# Required
# GLib-2.62.4 and libxml2-2.9.10
# Recommended
# gdk-pixbuf-2.40.0 (To build gsf-office-thumbnailer)
# Optional
# gobject-introspection-1.62.0 and GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
