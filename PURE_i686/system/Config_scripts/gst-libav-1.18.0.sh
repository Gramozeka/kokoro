#!/bin/bash
###############################################################################
set -e
# The GStreamer Libav package contains GStreamer plugins for Libav (a fork of FFmpeg).
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gstreamer.freedesktop.org/src/gst-libav/gst-libav-1.18.0.tar.xz
# Download MD5 sum: eacebd0136ede3a9bd3672eeb338806b
# Download size: 9.0 MB
# Estimated disk space required: 87 MB
# Estimated build time: less than 0.1 SBU (Using parallelism=4)
# GStreamer Libav Dependencies
# Required
# FFmpeg-4.2.2 and gst-plugins-base-1.18.0
# Recommended
# yasm-1.3.0
# Optional
# Valgrind-3.15.0 and Orc
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
         -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.18.0 BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
