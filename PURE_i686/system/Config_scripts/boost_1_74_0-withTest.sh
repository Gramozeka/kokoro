#!/bin/bash
###############################################################################
# Boost provides a set of free peer-reviewed portable C++ source libraries. It includes libraries for linear algebra, pseudorandom number generation, multithreading, image processing, regular expressions and unit testing.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://dl.bintray.com/boostorg/release/1.72.0/source/boost_1_72_0.tar.bz2
# Download MD5 sum: cb40943d2a2cb8ce08d42bc48b0f84f0
# Download size: 102 MB
# Estimated disk space required: 1.0 GB (168 MB installed)
# Estimated build time: 1.5 SBU (Using parallelism=4; add 1.3 SBU for tests)
# Boost Dependencies
# Recommended
# Which-2.21
# Optional
# ICU-65.1 and Open MPI
###############################################################################
cd $TEMPBUILD
rm -rf *
packagedir=$(sed -e "s/\-withTest*$//" <<< $(basename $line))
package="${packagedir}.tar.*"
unpack ${SOURCE}/${package}
cd ${packagedir}
PYTHON_VERSION=$(python -c 'import sys; print(sys.version[:3])')
PYTHON3_VERSION=$(python3 -c 'import sys; print(sys.version[:3])')

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./bootstrap.sh \
  --with-toolset=gcc \
  --with-icu \
  --with-python=/usr/bin/python2.7

./b2 \
  --layout=system \
  --build-dir=build-python2 \
  --build-type=minimal \
  toolset=gcc \
  variant=release \
  debug-symbols=off \
  link=shared \
  threading=multi \
  runtime-link=shared \
  python=2.7 \
  cflags="-O2 -march=i686" \
  cxxflags="-O2 -march=i686 $EXTRA_CXXFLAGS" \
  stage

echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall

./b2 -j5 \
  --layout=system \
  --build-dir=build-python2 \
  --build-type=minimal \
--prefix=${destdir}/${packagedir}/usr \
--libdir=${destdir}/${packagedir}/usr/lib \
  toolset=gcc \
  variant=release \
  debug-symbols=off \
  link=shared \
  threading=multi \
  runtime-link=shared \
  python=2.7 \
  cflags="-O2 -march=i686" \
  cxxflags="-O2 -march=i686 $EXTRA_CXXFLAGS" \
  install

sed -e '/using python/ s@;@: /usr/include/python${PYTHON_VERSION/3*/${PYTHON_VERSION}} ;@' -i bootstrap.sh

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./bootstrap.sh \
  --with-toolset=gcc \
  --with-icu \
  --with-python=/usr/bin/python3.8

CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./b2  --layout=system \
  --build-dir=build-python3 \
  --prefix=/usr \
  --libdir=/usr/lib \
  --build-type=minimal \
  toolset=gcc \
  variant=release \
  debug-symbols=off \
  link=shared \
  threading=multi \
  runtime-link=shared \
  python=3.8 \
  cflags="-O2 -march=i686" \
  cxxflags="-O2 -march=i686 $EXTRA_CXXFLAGS" \
 stage

./b2 -j5 \
  --layout=system \
  --build-dir=build-python3 \
  --prefix=${destdir}/${packagedir}/usr \
  --libdir=${destdir}/${packagedir}/usr/lib \
  --build-type=minimal \
  toolset=gcc \
  variant=release \
  debug-symbols=off \
  link=shared \
  threading=multi \
  runtime-link=shared \
  python=3.8 \
  cflags="-O2 -march=i686" \
  cxxflags="-O2 -march=i686 $EXTRA_CXXFLAGS" \
  install
cd ${destdir}/${packagedir} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-i686-tree
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-i686-py3.txz

echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall

installpkg ${destdir}/1-repo/${packagedir}-i686-py3.txz
rm -rf ${destdir}/${packagedir}
