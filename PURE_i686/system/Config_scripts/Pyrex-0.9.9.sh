#!/bin/bash
###############################################################################
###############################################################################
set -e
# Pyrex is a Python-like language for rapidly and easily writing python extension modules. It can be described as python with C data types. With Pyrex, one can produce Python-like code that runs as fast as in C, with easy access to C libraries and functions.
# 
# The Pyrex homepage is at http://www.cosc.canterbury.ac.nz/~greg/python/Pyrex/
# http://www.cosc.canterbury.ac.nz/greg.ewing/python/Pyrex/Pyrex-0.9.9.tar.gz
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
# python2 setup.py install  --optimize=1  --root=${pkg} &&
python2 setup.py install  --optimize=1  --root=${pkg} &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
python2 setup.py build
# python2 setup.py build
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

