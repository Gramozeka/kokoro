#!/bin/sh
cd $TEMPBUILD
rm -rf *
ARCH=noarch
packagedir=composer
pkg=${destdir}/$packagedir
mkdir -pv ${pkg}/usr/bin

EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
RESULT=$?
rm composer-setup.php
install -m 0755 composer.phar ${pkg}/usr/bin
ln -s composer.phar ${pkg}/usr/bin/composer
chown -R root:root ${pkg}/usr/bin/composer
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/composer-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/composer-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/composer-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
