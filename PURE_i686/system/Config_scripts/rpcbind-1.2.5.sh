#!/bin/bash
###############################################################################
set -e
###############################################################################
# The rpcbind program is a replacement for portmap. It is required for import or export of Network File System (NFS) shared directories.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/rpcbind/rpcbind-1.2.5.tar.bz2
# Download MD5 sum: ed46f09b9c0fa2d49015f6431bc5ea7b
# Download size: 124 KB
# Estimated disk space required: 1.7 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Required patch: http://www.linuxfromscratch.org/patches/blfs/svn/rpcbind-1.2.5-vulnerability_fixes-1.patch
# rpcbind Dependencies
# Required
# libtirpc-1.2.5
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i "/servname/s:rpcbind:sunrpc:" src/rpcbind.c
patch -Np1 -i ${PATCHSOURCE}/rpcbind-1.2.5-vulnerability_fixes-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
            --bindir=/sbin                                 \
            --sbindir=/sbin                                \
            --with-rpcuser=root                            \
            --enable-warmstarts                            \
            --without-systemdsystemunitdir \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
