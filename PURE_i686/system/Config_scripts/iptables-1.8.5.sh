#!/bin/bash
###############################################################################
set -e
###############################################################################
# iptables is a userspace command line program used to configure Linux 2.4 and later kernel packet filtering ruleset.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://www.netfilter.org/projects/iptables/files/iptables-1.8.4.tar.bz2
# Download (FTP): ftp://ftp.netfilter.org/pub/iptables/iptables-1.8.4.tar.bz2
# Download MD5 sum: 9b201107957fbf62709c3d8226239b0d
# Download size: 688 KB
# Estimated disk space required: 17 MB
# Estimated build time: 0.2 SBU
# iptables Dependencies
# Optional
# nftables-0.9.3, libpcap-1.9.1 (required for nfsypproxy support), bpf-utils (required for Berkely Packet Filter support), libnfnetlink (required for connlabel support), and libnetfilter_conntrack" (required for connlabel support)
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/iptables
# Kernel Configuration
# A firewall in Linux is accomplished through the netfilter interface. To use iptables to configure netfilter, the following kernel configuration parameters are required:
# [*] Networking support  --->                                          [CONFIG_NET]
#       Networking Options  --->
#         [*] Network packet filtering framework (Netfilter) --->       [CONFIG_NETFILTER]
#           [*] Advanced netfilter configuration                        [CONFIG_NETFILTER_ADVANCED]
#           Core Netfilter Configuration --->
#             <*/M> Netfilter connection tracking support               [CONFIG_NF_CONNTRACK]
#             <*/M> Netfilter Xtables support (required for ip_tables)  [CONFIG_NETFILTER_XTABLES]
#             <*/M> LOG target support                                  [CONFIG_NETFILTER_XT_TARGET_LOG]
#           IP: Netfilter Configuration --->
#             <*/M> IP tables support (required for filtering/masq/NAT) [CONFIG_IP_NF_IPTABLES]
# Include any connection tracking protocols that will be used, as well as any protocols that you wish to use for match support under the "Core Netfilter Configuration" section. The above options are enough for running Creating a Personal Firewall With iptables below."
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/{sbin,lib}
ln -sfv ../../sbin/xtables-legacy-multi ${pkg}/usr/bin/iptables-xml &&
for file in ip4tc ip6tc ipq xtables
do
  mv -v ${pkg}/usr/lib/lib${file}.so.* ${pkg}/lib &&
  ln -sfv ../../lib/$(readlink ${pkg}/usr/lib/lib${file}.so) ${pkg}/usr/lib/lib${file}.so
done
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
            --sbindir=/sbin    \
            --enable-libipq    \
            --with-xtlibdir=/lib/xtables \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
