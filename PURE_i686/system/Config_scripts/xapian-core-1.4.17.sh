#!/bin/bash
###############################################################################
set -e
# Xapian is an open source search engine library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://oligarchy.co.uk/xapian/1.4.14/xapian-core-1.4.14.tar.xz
# Download MD5 sum: 714b02c306a5f11dea035da70462c288
# Download size: 2.8 MB
# Estimated disk space required: 185 MB (add estimated 100 MB for tests)
# Estimated build time: 0.6 SBU (using parallelism=4; add estimated 180 SBU for tests)
# Xapian Dependencies
# Optional
# Valgrind-3.15.0 (for tests)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
