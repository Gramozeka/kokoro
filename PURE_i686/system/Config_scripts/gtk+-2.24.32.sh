#!/bin/bash
###############################################################################
set -e
# The GTK+ 2 package contains libraries used for creating graphical user interfaces for applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gtk+/2.24/gtk+-2.24.32.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gtk+/2.24/gtk+-2.24.32.tar.xz
# Download MD5 sum: d5742aa42275203a499b59b4c382a784
# Download size: 12 MB
# Estimated disk space required: 275 MB
# Estimated build time: 1.0 SBU (using parallelism=4; add 0.1 SBU for tests)
# GTK+ 2 Dependencies
# Required
# ATK-2.34.1, gdk-pixbuf-2.40.0 and Pango-1.44.7
# Recommended
# hicolor-icon-theme-0.17
# Optional
# Cups-2.3.1, DocBook-utils-0.6.14, gobject-introspection-1.62.0 and GTK-Doc-1.32
# [Note] Note
# If gobject-introspection-1.62.0 was installed after ATK-2.34.1, gdk-pixbuf-2.40.0, and/or Pango-1.44.7, those packages will have to be rebuilt before this package can be built.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -e 's#l \(gtk-.*\).sgml#& -o \1#' \
    -i docs/{faq,tutorial}/Makefile.in 
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# /i686/bin/gtk-query-immodules-2.0 --update-cache
gtk-query-immodules-2.0 --update-cache
