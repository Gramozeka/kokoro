#!/bin/bash
###############################################################################
set -e
###############################################################################
# The hd2u package contains an any to any text format converter.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://hany.sk/~hany/_data/hd2u/hd2u-1.0.4.tgz
# Download MD5 sum: d516d794deb42ee95bd4e96af94088de
# Download size: 64 KB
# Estimated disk space required: 364 KB
# Estimated build time: less than 0.1 SBU
# Hd2u Dependencies
# Required
# popt-1.16
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
/usr/bin/install -c -d -m 755 ${pkg}/usr/bin
/usr/bin/install -c -m 755 -s dos2unix ${pkg}/usr/bin


echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
