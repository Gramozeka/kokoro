#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Qpdf package contains command-line programs and library that do structural, content-preserving transformations on PDF files.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/qpdf/qpdf/releases/download/release-qpdf-9.1.1/qpdf-9.1.1.tar.gz
# Download MD5 sum: 8a2ddc3bdf0671234a5651251a7e9da6
# Download size: 17 MB
# Estimated disk space required: 255 MB (add 50 MB for tests)
# Estimated build time: 0.5 SBU (using parallelism=4; add 1.6 SBU for tests)
# Qpdf Dependencies
# Required
# libjpeg-turbo-2.0.4
# Optional
# fop-2.4 and libxslt-1.1.34
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
