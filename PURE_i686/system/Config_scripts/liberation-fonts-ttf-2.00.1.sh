#!/bin/bash
###############################################################################
set -e
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
pkg=${destdir}/${packagedir}
mkdir -p ${pkg}/usr/share/fonts/TTF/
cp -a *.ttf ${pkg}/usr/share/fonts/TTF/
mkdir -p ${pkg}/etc/fonts/conf.avail
mkdir -p ${pkg}/etc/fonts/conf.d

cat > ${pkg}/etc/fonts/conf.avail/60-liberation.conf << "EOF"
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<!-- $Id: 60-liberation.conf,v 1.1 2008/03/27 11:14:42 root Exp root $ -->
<fontconfig>

	<!-- Symlinking this file to /etc/fonts/conf.d/ will allow 
         you to use liberation fonts instead of the microsoft truetype fonts.
         (from http://uwstopia.nl/blog/2007/05/free-your-fonts) -->

	<!-- Liberation fonts -->
	<match target="pattern">
		<test qual="any" name="family"><string>Times New Roman</string></test>
		<edit name="family" mode="assign"><string>Liberation Serif</string></edit>
	</match>
	<match target="pattern">
		<test qual="any" name="family"><string>Arial</string></test>
		<edit name="family" mode="assign"><string>Liberation Sans</string></edit>
	</match>
	<match target="pattern">
		<test qual="any" name="family"><string>Courier</string></test>
		<edit name="family" mode="assign"><string>Liberation Mono</string></edit>
	</match>
	<match target="pattern">
		<test qual="any" name="family"><string>Courier New</string></test>
		<edit name="family" mode="assign"><string>Liberation Mono</string></edit>
	</match>


</fontconfig>
EOF

( cd ${pkg}/etc/fonts/conf.d && \
  ln -sf ../conf.avail/60-liberation.conf
)
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home
