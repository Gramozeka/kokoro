#!/bin/bash
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vi
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib \
--disable-static \
  --enable-java  \
  --enable-net  \
  --enable-python  \
  --enable-ruby  \
  --enable-vala 
make -j1
mkdir -pv ${destdir}/${packagedir}
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
