#!/bin/bash
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
#  unpack ${SOURCE}/${package}
git clone https://gitlab.com/accounts-sso/accounts-qml-module ${packagedir}
cd ${packagedir}
#   sed -e 's|src \\|src|' -e '/tests/d' -i signon-ui.pro
#   # Fake user ID to bypass Google blacklist
# patch -Np1 -i ${PATCHSOURCE}/fake-user-agent.patch
CWD=$(pwd)

LDFLAGS="-L/lib:/usr/lib" 
LD_LIBRARY_PATH="/lib:/usr/lib" 
OPENSSL_LIBS="-L/usr/lib -lssl -lcrypto" \
qmake \
CMAKE_CONFIG_PATH=/usr/lib/cmake \
LIBDIR=/usr/lib \
PREFIX=/usr   &&
make -j4
mkdir -pv $pkg
make install INSTALL_ROOT=${pkg}

cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
