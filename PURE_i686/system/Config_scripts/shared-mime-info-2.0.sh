#!/bin/bash
###############################################################################
set -e
# The Shared Mime Info package contains a MIME database. This allows central updates of MIME information for all supporting applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://gitlab.freedesktop.org/xdg/shared-mime-info/uploads/b27eb88e4155d8fccb8bb3cd12025d5b/shared-mime-info-1.15.tar.xz
# Download MD5 sum: 5215f3d679a817de97eb8b0b911e9393
# Download size: 756 KB
# Estimated disk space required: 12 MB
# Estimated build time: 0.1 SBU
# Shared Mime Info Dependencies
# Required
# GLib-2.62.4, itstool-2.0.6, and libxml2-2.9.10
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
-Dupdate-mimedb=true \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
