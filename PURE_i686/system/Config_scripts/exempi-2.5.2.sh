#!/bin/bash
###############################################################################
set -e
###############################################################################
# Exempi is an implementation of XMP (Adobe's Extensible Metadata Platform).'
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://libopenraw.freedesktop.org/download/exempi-2.5.1.tar.bz2
# Download MD5 sum: c32bcd9feed5a0c1523d5652ef1804b0
# Download size: 3.5 MB
# Estimated disk space required: 389 MB (add 38 MB for tests)
# Estimated build time: 0.6 SBU (using parallelism=4; add 0.4 SBU for tests)
# Exempi Dependencies
# Required
# Boost-1.72.0
# Optional
# Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -fiv
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
