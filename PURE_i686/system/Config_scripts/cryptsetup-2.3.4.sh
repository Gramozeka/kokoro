#!/bin/bash
###############################################################################
set -e
###############################################################################
# cryptsetup is used to set up transparent encryption of block devices using the kernel crypto API.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.kernel.org/pub/linux/utils/cryptsetup/v2.0/cryptsetup-2.0.6.tar.xz
# Download MD5 sum: ec03e09cbe978a19fa6d6194ac642bae
# Download size: 10 MB
# Estimated disk space required: 25 MB (add 9 MB for tests)
# Estimated build time: 0.2 SBU (add 12 SBU for tests)
# cryptsetup Dependencies
# Required
# JSON-C-0.13.1, libgcrypt-1.8.5, LVM2-2.03.07, and popt-1.16
# Optional
# libpwquality-1.4.2, Python-2.7.17, and passwdqc
# User Notes: http://wiki.linuxfromscratch.org/blfs/wiki/cryptsetup
# Kernel Configuration
# Encrypted block devices require kernel support. To use it, the appropriate kernel configuration parameters need to be set:
# Device Drivers  --->          
#   [*] Multiple devices driver support (RAID and LVM) ---> [CONFIG_MD]
#        <*/M> Device mapper support                        [CONFIG_BLK_DEV_DM]
#        <*/M> Crypt target support                         [CONFIG_DM_CRYPT]
# Cryptographic API  --->                                    
#   <*/M> XTS support                                       [CONFIG_CRYPTO_XTS]
#   <*/M> SHA224 and SHA256 digest algorithm                [CONFIG_CRYPTO_SHA256]
#   <*/M> AES cipher algorithms                             [CONFIG_CRYPTO_AES]
#   <*/M> AES cipher algorithms (x86_64)                    [CONFIG_CRYPTO_AES_X86_64] 
#   <*/M> User-space interface for symmetric key cipher algorithms
#                                                           [CONFIG_CRYPTO_USER_API_SKCIPHER]
#   For tests:
#   <*/M> Twofish cipher algorithm                          [CONFIG_CRYPTO_TWOFISH]
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--with-crypto_backend=openssl  \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
