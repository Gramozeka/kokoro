#!/bin/bash
###############################################################################
set -e
# AAlib is a library to render any graphic into ASCII Art.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/aa-project/aalib-1.4rc5.tar.gz
# Download MD5 sum: 9801095c42bba12edebd1902bcf0a990
# Download size: 388 KB
# Estimated disk space required: 6.5 MB
# Estimated build time: 0.1 SBU
# AAlib Dependencies
# Optional
# X Window System, slang-2.3.2, and GPM-1.20.7
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -e '/AM_PATH_AALIB,/s/AM_PATH_AALIB/[&]/' aalib.m4
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
            --infodir=/usr/share/info \
            --mandir=/usr/share/man   \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
