#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
# MAGICK_CFLAGS="-I/usr/include/ImageMagick-7:/usr/include/ImageMagick-7/MagickWand" \
PKG_CONFIG_PATH=/usr/lib/pkgconfig \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--disable-video \
--without-qt \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
