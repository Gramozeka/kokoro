#!/bin/bash
###############################################################################
set -e
# x264 package provides a library for encoding video streams into the H.264/MPEG-4 AVC format.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://download.videolan.org/x264/snapshots/x264-20200218.tar.bz2
# Download size: 753 KB
# Estimated disk space required: 14 MB
# Estimated build time: 0.8 SBU
# x264 Dependencies
# Recommended
# NASM-2.14.02
# Optional
# ffms2, gpac or liblsmash
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--libdir=/usr/lib \
--enable-shared
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
