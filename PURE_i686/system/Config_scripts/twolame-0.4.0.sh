#!/bin/bash
###############################################################################
set -e
# TwoLAME is an optimised MPEG Audio Layer 2 (MP2) encoder based on tooLAME by Mike Cheng, which in turn is based upon the ISO dist10 code and portions of LAME.
# Features added to TwoLAME:
# Fully thread-safe
# Static and shared library (libtwolame)
# API very similar to LAME’s (for easy porting)
# Frontend supports wider range of input files (using libsndfile)
# automake/libtool/pkgconfig based build system
# Written in Standard C (ISO C99 compliant)
# http://downloads.sourceforge.net/twolame/twolame-0.4.0.tar.gz
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
