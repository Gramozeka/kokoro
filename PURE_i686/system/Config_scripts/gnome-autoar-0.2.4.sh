#!/bin/bash
###############################################################################
set -e
###############################################################################
# The gnome-autoar package provides a framework for automatic archive extraction, compression, and management.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-autoar/0.2/gnome-autoar-0.2.4.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-autoar/0.2/gnome-autoar-0.2.4.tar.xz
# Download MD5 sum: 36ab263f477eeee3c95c9381766eb3c2
# Download size: 292 KB
# Estimated disk space required: 5.1 MB
# Estimated build time: 0.1 SBU
# gnome-autoar Dependencies
# Required
# libarchive-3.4.1 and GTK+-3.24.13
# Recommended
# Vala-0.46.5
# Optional
# GTK-Doc-1.32 (for building documentation)
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
