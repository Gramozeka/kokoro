#!/bin/bash
###############################################################################
set -e
# libdvdnav is a library that allows easy use of sophisticated DVD navigation features such as DVD menus, multiangle playback and even interactive DVD games.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://get.videolan.org/libdvdnav/6.0.1/libdvdnav-6.0.1.tar.bz2
# Download MD5 sum: 3a28d2cc7e25c1cbcb06443f3114f0b1
# Download size: 372 KB
# Estimated disk space required: 3.7 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
