#!/bin/bash
###############################################################################
set -e
###############################################################################
# # pip install fontmake
# pip3 install fontmake
# # pip install psautohint
# pip3 install psautohint
# # pip install attr
# pip3 install attr
# # pip install ufoLib2
# pip3 install ufoLib2
# # pip install statmake
# pip3 install statmake
###############################################################################
cd $TEMPBUILD
rm -rf *
# package="$(basename $line).tar.*"
packagedir=$(basename $line)
# unpack ${SOURCE}/${package}
git clone https://github.com/GNOME/cantarell-fonts.git ${packagedir}
cd ${packagedir}
pip2 install -r requirements.txt
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
