#!/bin/bash
###############################################################################
set -e
# libcanberra is an implementation of the XDG Sound Theme and Name Specifications, for generating event sounds on free desktops, such as GNOME.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://0pointer.de/lennart/projects/libcanberra/libcanberra-0.30.tar.xz
# Download MD5 sum: 34cb7e4430afaf6f447c4ebdb9b42072
# Download size: 312 KB
# Estimated disk space required: 7.5 MB
# Estimated build time: 0.1 SBU
# libcanberra Dependencies
# Required
# libvorbis-1.3.6
# Recommended
# alsa-lib-1.2.1.2, gstreamer-1.16.2 and GTK+-3.24.13
# Optional
# GTK+-2.24.32, PulseAudio-13.0 and tdb
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--disable-oss \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
