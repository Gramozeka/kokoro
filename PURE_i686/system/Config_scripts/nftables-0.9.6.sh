#!/bin/bash
###############################################################################
set -e
###############################################################################
# The nftables package, intended to be the successor to iptables-1.8.4, provides a low-level netlink programming interface (API), and userspace uitlities for the in-kernel nf_tables subsystem.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://netfilter.org/projects/nftables/files/nftables-0.9.3.tar.bz2
# Download MD5 sum: 9913b2b46864394d41916b74638e0875
# Download size: 772 KB
# Estimated disk space required: 34 MB
# Estimated build time: 0.2 SBU
# nftables Dependencies
# Required
# libnftnl-1.1.5
# Recommended
# jansson-2.12 (for JSON rules table support)
# Optional
# iptables-1.8.4 and DocBook-utils-0.6.14
# Optional (runtime)
# contrack-tools nfacct ulogd
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/lib
mv ${pkg}/usr/lib/libnftables.so.* ${pkg}/lib &&
ln -sfv ../../lib/$(readlink ${pkg}/usr/lib/libnftables.so) ${pkg}/usr/lib/libnftables.so
mkdir -pv ${pkg}/etc/nftables
cat > ${pkg}/etc/nftables/nftables.conf << "EOF"
#!/sbin/nft -f

# You're using the example configuration for a setup of a firewall
# from Beyond Linux From Scratch.
#
# This example is far from being complete, it is only meant
# to be a reference.
#
# Firewall security is a complex issue, that exceeds the scope
# of the configuration rules below.
#
# You can find additional information
# about firewalls in Chapter 4 of the BLFS book.
# http://www.linuxfromscratch.org/blfs

# Drop all existing rules
flush ruleset

# Filter for both ip4 and ip6 (inet)
table inet filter {

        # filter incomming packets
        chain input {

                # Drop everything that doesn't match policy
                type filter hook input priority 0; policy drop;

                # accept packets for established connections
                ct state { established, related } accept

                # Drop packets that have a connection state of invalid
                ct state invalid drop

                # Allow connections to the loopback adapter
                iifname "lo" accept

                # Allow connections to the LAN1 interface
                iifname "LAN1" accept

                # Accept icmp requests
                ip protocol icmp accept

                # Allow ssh connections on LAN1
                iifname "LAN1" tcp dport ssh accept

                # Drop everything else
                drop
        }

        # Allow forwarding for external connections to WAN1
        chain forward {

                # Drop if it doesn't match policy
                type filter hook forward priority 0; policy drop;

                # Accept connections on WAN1
                oifname "WAN1" accept

                # Allow forwarding to another host via this interface
                # Uncomment the following line to allow connections
                # ip daddr 192.168.0.2 ct status dnat accept

                # Allow established and related connections
                iifname "WAN1" ct state { established, related } accept
        }

        # Filter output traffic
        chain output {

                # Allow everything outbound
                type filter hook output priority 0; policy accept;
        }
}

# Allow NAT for ip protocol (both ip4 and ip6)
table ip nat {

        chain prerouting {

                # Accept on inbound interface for policy match
                type nat hook prerouting priority 0; policy accept;

                # Accept http and https on 192.168.0.2
                # Uncomment the following line to allow http and https
                #iifname "WAN1" tcp dport { http, https } dnat to 192.168.0.2
        }

        chain postrouting {

                # accept outbound
                type nat hook postrouting priority 0; policy accept;

                # Masquerade on WAN1 outbound
                oifname "WAN1" masquerade
        }
}
EOF
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
            --sbindir=/sbin   \
            --sysconfdir=/etc \
            --with-json       \
            --with-python-bin=/usr/bin/python3 \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
