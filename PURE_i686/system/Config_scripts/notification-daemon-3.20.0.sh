#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Notification Daemon package contains a daemon that displays passive pop-up notifications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/notification-daemon/3.20/notification-daemon-3.20.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/notification-daemon/3.20/notification-daemon-3.20.0.tar.xz
# Download MD5 sum: 2de7f4075352831f1d98d8851b642124
# Download size: 336 KB
# Estimated disk space required: 4.1 MB
# Estimated build time: less than 0.1 SBU
# Notification Daemon Dependencies
# Required
# GTK+-3.24.13 and libcanberra-0.30 (Built with GTK+-3.24.13 support).
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
