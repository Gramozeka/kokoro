#!/bin/bash
###############################################################################
set -e
###############################################################################
# The libglade package contains libglade libraries. These are useful for loading Glade interface files in a program at runtime.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libglade/2.6/libglade-2.6.4.tar.bz2
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libglade/2.6/libglade-2.6.4.tar.bz2
# Download MD5 sum: d1776b40f4e166b5e9c107f1c8fe4139
# Download size: 348 KB
# Estimated disk space required: 5 MB
# Estimated build time: 0.1 SBU
# libglade Dependencies
# Required
# libxml2-2.9.10 and GTK+-2.24.32
# Optional
# Python-2.7.17 and GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/DG_DISABLE_DEPRECATED/d' glade/Makefile.in &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
