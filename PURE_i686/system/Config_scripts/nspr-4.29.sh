#!/bin/bash
###############################################################################
set -e
# Netscape Portable Runtime (NSPR) provides a platform-neutral API for system level and libc like functions.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.mozilla.org/pub/nspr/releases/v4.24/src/nspr-4.24.tar.gz
# Download MD5 sum: d630c2111e1db6d2ec2069aad22b4121
# Download size: 1.0 MB
# Estimated disk space required: 10 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
cd nspr                                                     &&
sed -ri 's#^(RELEASE_BINS =).*#\1#' pr/src/misc/Makefile.in &&
sed -i 's#$(LIBRARY) ##'            config/rules.mk         &&
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
            --with-mozilla \
            --with-pthreads \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
