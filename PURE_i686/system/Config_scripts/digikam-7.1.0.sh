#!/bin/bash
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
	unpack ${SOURCE}/${package}
	cd ${packagedir}

# Uncomment and adjust lines below to use Clang compiler instead GCC
# export CC=/usr/bin/clang
# export CXX=/usr/bin/clang++

SOURCEDIR=$PWD

command -v qtpaths >/dev/null 2>&1 || { echo >&2 "This script require qtpaths CLI tool from Qt5 project but it's not installed. Aborting."; exit 1; }

    MAKEFILES_TYPE='Unix Makefiles'
    BUILDDIR=$PWD"/build"
    MESSAGE="Now run make in $BUILDDIR."



    LIBPATH="lib"

QT_INSTALL_PREFIX=`qtpaths --install-prefix`
QT_PLUGIN_INSTALL_DIR=`qtpaths --plugin-dir`

CMAKE_BINARY="cmake"

 DIGIKAM_INSTALL_PREFIX="/usr"
echo "Qt5     Install Path : $QT_INSTALL_PREFIX"
echo "digiKam Install Path : $DIGIKAM_INSTALL_PREFIX"
echo "CMake binary         : $CMAKE_BINARY"
echo "Build Directory      : $BUILDDIR"
mkdir -p $BUILDDIR
cd $BUILDDIR
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
$CMAKE_BINARY -G "$MAKEFILES_TYPE" . \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=$DIGIKAM_INSTALL_PREFIX/ \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
      -DKDE_INSTALL_QTPLUGINDIR=$QT_PLUGIN_INSTALL_DIR/ \
      -DBUILD_TESTING=OFF \
      -DDIGIKAMSC_CHECKOUT_PO=ON \
      -DDIGIKAMSC_CHECKOUT_DOC=OFF \
      -DDIGIKAMSC_COMPILE_PO=ON \
      -DDIGIKAMSC_COMPILE_DOC=OFF \
      -DDIGIKAMSC_COMPILE_DIGIKAM=ON \
      -DENABLE_KFILEMETADATASUPPORT=ON \
      -DENABLE_AKONADICONTACTSUPPORT=ON \
      -DCMAKE_CXX_FLAGS:STRING="-L/usr/lib -lMagick++-7.Q16HDRI -lMagickWand-7.Q16HDRI -lMagickCore-7.Q16HDRI" \
      -DENABLE_MYSQLSUPPORT=ON \
      -DENABLE_INTERNALMYSQL=ON \
      -DENABLE_MEDIAPLAYER=ON \
      -DENABLE_DBUS=ON \
      -DENABLE_APPSTYLES=ON \
      -DENABLE_QWEBENGINE=ON \
      -Wno-dev \
      $SOURCEDIR 

	make -j4 || make || exit 1

	pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


















