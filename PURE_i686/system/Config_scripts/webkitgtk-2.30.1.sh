#!/bin/bash
###############################################################################
set -e
# The WebKitGTK+ package is a port of the portable web rendering engine WebKit to the GTK+ 3 and GTK+ 2 platforms.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://webkitgtk.org/releases/webkitgtk-2.26.3.tar.xz
# Download MD5 sum: 4c27d59a032710dae3cffa5990bb6aea
# Download size: 18 MB
# Estimated disk space required: 617 MB (107 MB installed)
# Estimated build time: 22 SBU (using parallelism=4, estimated 75 SBUs using one core)
# WebKitGTK+ Dependencies
# Required
# Cairo-1.17.2+f93fc72c03e, CMake-3.16.3, gst-plugins-base-1.16.2, gst-plugins-bad-1.16.2, GTK+-2.24.32, GTK+-3.24.13, ICU-65.1, libgudev-233, libsecret-0.20.0, libsoup-2.68.3, libwebp-1.1.0, Mesa-19.3.3, OpenJPEG-2.3.1, Ruby-2.7.0, SQLite-3.31.1, and Which-2.21
# Recommended
# enchant-2.2.7, GeoClue-2.5.5, gobject-introspection-1.62.0, hicolor-icon-theme-0.17, and libnotify-0.7.8
# Optional
# GTK-Doc-1.32, HarfBuzz-2.6.4, LLVM-9.0.1, Wayland-1.17.0, WOFF2-1.0.2, Hyphen, MathML, libwpe, WPEBackend-fdo, and xdg-dbus-proxy
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake     \
-DCMAKE_INSTALL_PREFIX=/usr \
-DCMAKE_INSTALL_LIBDIR=/usr/lib \
-DLIB_INSTALL_DIR=/usr/lib \
-DCMAKE_BUILD_TYPE=Release         \
      -DCMAKE_SKIP_RPATH=ON       \
      -DPORT=GTK                  \
      -DUSE_LIBHYPHEN=ON         \
      -DENABLE_MINIBROWSER=ON     \
      -DUSE_WOFF2=ON             \
      -DENABLE_GEOLOCATION=ON  \
      -DUSE_SYSTEMD=OFF        \
      -Wno-dev -G Ninja ..
      ninja -j5 || make || exit 1

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
