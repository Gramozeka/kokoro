#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
git clone https://github.com/brailcom/speechd.git ${packagedir}
#unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -vfi
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHON=/usr/bin/python3.8 ./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--with-pulse \
--with-alsa \
--with-espeak \
  --with-espeak-ng \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--with-systemdsystemunitdir=no \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
