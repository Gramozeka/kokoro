#!/bin/bash
###############################################################################
set -e
###############################################################################
# The libatasmart package is a disk reporting library. It only supports a subset of the ATA S.M.A.R.T. functionality.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://0pointer.de/public/libatasmart-0.19.tar.xz
# Download MD5 sum: 53afe2b155c36f658e121fe6def33e77
# Download size: 248 KB
# Estimated disk space required: 3 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
