#!/bin/bash
###############################################################################
set -e
# This package provides the Mesa OpenGL Utility library.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# GLU Download (FTP): ftp://ftp.freedesktop.org/pub/mesa/glu/glu-9.0.1.tar.xz
# GLU Download MD5 sum: 151aef599b8259efe9acd599c96ea2a3
# GLU Download size: 428 KB
# Estimated GLU disk space required: 13 MB
# Estimated GLU build time: 0.2 SBU
# GLU Dependencies
# Required
# Mesa-19.3.2
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
