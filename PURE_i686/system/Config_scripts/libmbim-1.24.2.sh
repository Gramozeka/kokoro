#!/bin/bash
###############################################################################
set -e
###############################################################################
# The libmbim package contains a GLib-based library for talking to WWAN modems and devices which speak the Mobile Interface Broadband Model (MBIM) protocol.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/libmbim/libmbim-1.22.0.tar.xz
# Download MD5 sum: 87060e8957013177e4140edc6f64f5bd
# Download size: 484 KB
# Estimated disk space required: 22 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
# libmbim Dependencies
# Required
# libgudev-233
# Optional
# GTK-Doc-1.32 and help2man
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
