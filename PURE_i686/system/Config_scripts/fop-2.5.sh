#!/bin/bash
###############################################################################
# The FOP (Formatting Objects Processor) package contains a print formatter driven by XSL formatting objects (XSL-FO). It is a Java application that reads a formatting object tree and renders the resulting pages to a specified output. Output formats currently supported include PDF, PCL, PostScript, SVG, XML (area tree representation), print, AWT, MIF and ASCII text. The primary output target is PDF.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://archive.apache.org/dist/xmlgraphics/fop/source/fop-2.4-src.tar.gz
# Download MD5 sum: e379d5916bc705b6fd3808a77f5d8478
# Download size: 26 MB
# Estimated disk space required: 252 MB
# Estimated build time: 0.4 SBU
# Additional Downloads
# Required Additional Downloads:
# PDFBox:
# http://mirror.reverse.net/pub/apache/pdfbox/2.0.17/pdfbox-2.0.17.jar
# c713a8e252d0add65e9282b151adf6b4
# 2.6 MB
# PDFBox Fonts:
# http://mirror.reverse.net/pub/apache/pdfbox/2.0.17/fontbox-2.0.17.jar
# 25d3b08b7105f03ab62a5360f874bdf1
# 1.5 MB
# Recommended packages
# Objects for Formatting Objects (OFFO) hyphenation patterns:
# https://downloads.sourceforge.net/offo/2.2/offo-hyphenation.zip
# bf9c09bf05108ef9661b8f08d91c2336
# 862 KB
# fop Dependencies
# Required
# apache-ant-1.10.7
# Optional
# X Window System (to run tests), JAI Image I/O Tools, and JEuclid
###############################################################################
pack_local () {
pkg=${destdir}/${packagedir}
mkdir -pv ${pkg}/opt/${packagedir}

install -v -d -m755 -o root -g root          ${pkg}/opt/${packagedir} &&
cp -vR build conf examples fop* javadocs lib       ${pkg}/opt/${packagedir} &&
chmod a+x ${pkg}/opt/${packagedir}/fop                                &&
cd ${pkg}/opt
ln -v -sfn ${packagedir} fop

echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
unzip ${SOURCE}/offo-hyphenation.zip &&
cp offo-hyphenation/hyph/* fop/hyph &&
rm -rf offo-hyphenation

tar -xf ${SOURCE}/apache-maven-3.6.3-bin.tar.gz -C /tmp

sed -i '\@</javad@i\
<arg value="-Xdoclint:none"/>\
<arg value="--allow-script-in-comments"/>\
<arg value="--ignore-source-errors"/>' \
    fop/build.xml

sed -e '/hyph\.stack/s/512k/1M/' \
    -i fop/build.xml
sed -e 's/1\.6/1.7/' \
    -i fop/build.xml
cp ${SOURCE}/{pdf,font}box-2.0.21.jar fop/lib
cd fop                    &&
LC_ALL=en_US.UTF-8                     \
PATH=$PATH:/tmp/apache-maven-3.6.3/bin \
ant all javadocs
mv build/javadocs .

pack_local ${packagedir}

cat > ~/.foprc << "EOF"
FOP_OPTS="-Xmx7000m"
FOP_HOME="/opt/fop"
EOF
cat > /etc/profile.d/fop.sh << "EOF"
pathappend /opt/fop             PATH
FOP_OPTS="-Xmx7000m"
FOP_HOME="/opt/fop"
EOF

export FOP_OPTS="-Xmx7000m"
export FOP_HOME="/opt/fop"
export PATH="$PATH:/opt/fop"

echo "###########################**** COMPLITE!!! ****##################################"
