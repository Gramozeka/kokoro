#!/bin/bash
###############################################################################
set -e
# PyGTK lets you to easily create programs with a graphical user interface using the Python programming language.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/pygtk/2.24/pygtk-2.24.0.tar.bz2
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/pygtk/2.24/pygtk-2.24.0.tar.bz2
# Download MD5 sum: a1051d5794fd7696d3c1af6422d17a49
# Download size: 2.2 MB
# Estimated disk space required: 83 MB
# Estimated build time: 0.7 SBU
# PyGTK Dependencies
# Required
# PyGObject-2.28.7 and Python-2.7.17
# Required (atk module)
# ATK-2.34.1
# Required (pango module)
# Pango-1.44.7
# Required (pangocairo module)
# PyCairo-1.19.0 (Python 2) and Pango-1.44.7
# Required (gtk and gtk.unixprint modules)
# PyCairo-1.19.0 (Python 2) and GTK+-2.24.32.
# Required (gtk.glade module)
# PyCairo-1.19.0 (Python 2) and libglade-2.6.4.
# Optional
# NumPy
# Optional (to Build Documentation)
# libxslt-1.1.34
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '1394,1402 d' pango.defs
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
