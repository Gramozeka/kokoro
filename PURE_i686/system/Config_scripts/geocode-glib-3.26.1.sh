#!/bin/bash
###############################################################################
set -e
###############################################################################
# geocode-glib is a convenience library for the geocoding (finding longitude,
# and latitude from an address) and reverse geocoding (finding an address from
# coordinates). It uses Nominatim service to achieve that. It also caches
# (reverse-)geocoding requests for faster results and to avoid unnecessary server
# load.
# Requirements
# ------------
# geocode-glib requires json-glib, gio and libsoup to compile and run.
# Applications using geocode-glib
# -------------------------------
# - Empathy
# - Evolution
# Links
# -----
# Nominatim API:
# http://wiki.openstreetmap.org/wiki/Nominatim
# Geocoding:
# http://en.wikipedia.org/wiki/Geocoding
# Reverse-Geocoding:
# http://en.wikipedia.org/wiki/Reverse_geocoding
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Denable-gtk-doc=false \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
