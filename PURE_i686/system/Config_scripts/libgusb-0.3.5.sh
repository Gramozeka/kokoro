#!/bin/bash
###############################################################################
set -e
# The libgusb package contains the GObject wrappers for libusb-1.0 that makes it easy to do asynchronous control, bulk and interrupt transfers with proper cancellation and integration into a mainloop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://people.freedesktop.org/~hughsient/releases/libgusb-0.3.2.tar.xz
# Download MD5 sum: ffffc94868dd88f984ca8034e6006d9c
# Download size: 40 KB
# Estimated disk space required: 1.8 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# libgusb Dependencies
# Required
# libusb-1.0.23
# Recommended
# GTK-Doc-1.32 gobject-introspection-1.62.0, usbutils-012 (for usb.ids data file, which is also required for the tests), and Vala-0.46.5
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
