#!/bin/bash
set -e
###################
TEMP=${TEMPBUILD}
cd $TEMP
 rm -rf *

packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
srcdir=${packagedir}
mkdir -v ${packagedir}
package="$(basename $line).tar.xz"
unpack ${SOURCE}/${package}
# git clone https://code.qt.io/qt/qt5.git ${packagedir}
cd ${srcdir}
#  git checkout 5.15.1
# git submodule update --init --recursive
# ./init-repository --module-subset=all

sed -i 's/python /python3 /' qtdeclarative/qtdeclarative.pro \
                              qtdeclarative/src/3rdparty/masm/masm.pri &&
if [ -d BUILDQT ]; then
rm -rfv BUILDQT
mkdir -v BUILDQT
cd BUILDQT
else
mkdir -v BUILDQT
cd BUILDQT
fi
CWD=$(pwd)
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
ICU_PREFIX=/usr \
ICU__INCDIR=/usr/include/unicode \
ICU_LIBS="-L/usr/lib -licui18n -licuuc -licudata"   \
OPENSSL_LIBS="-L/usr/lib -lssl -lcrypto"            \
../configure                                          \
-prefix                   /usr                        \
-bindir                   /usr/bin                    \
-libdir                   /usr/lib                  \
-archdatadir              /usr/lib/qt5              \
-plugindir                /usr/lib/qt5/plugins      \
-importdir                /usr/lib/qt5/imports      \
-headerdir                /usr/include/qt5            \
-datadir                  /usr/share/qt5              \
-docdir                   /usr/share/doc/qt5          \
-translationdir           /usr/share/qt5/translations \
-examplesdir              /usr/share/doc/qt5/examples \
-sysconfdir               /etc/xdg                    \
-force-pkg-config                                     \
-ccache                                               \
            -confirm-license                          \
            -opensource                               \
            -dbus-linked                              \
            -openssl-linked                           \
            -system-harfbuzz                          \
            -system-sqlite                            \
            -no-rpath                                 \
            -skip qtwebengine                         \
            -nomake examples                                            
make -j5 || make -j1 || exit 1
make INSTALL_ROOT=${pkg} install
echo "###########################**** COMPLITE!!! ****##################################"

install -v -dm755 ${pkg}/usr/share/pixmaps/                  &&

install -v -Dm644 ../qttools/src/assistant/assistant/images/assistant-128.png \
                  ${pkg}/usr/share/pixmaps/assistant-qt5.png &&

install -v -Dm644 ../qttools/src/designer/src/designer/images/designer.png \
                  ${pkg}/usr/share/pixmaps/designer-qt5.png  &&

install -v -Dm644 ../qttools/src/linguist/linguist/images/icons/linguist-128-32.png \
                  ${pkg}/usr/share/pixmaps/linguist-qt5.png  &&

install -v -Dm644 ../qttools/src/qdbus/qdbusviewer/images/qdbusviewer-128.png \
                  ${pkg}/usr/share/pixmaps/qdbusviewer-qt5.png &&

install -dm755 ${pkg}/usr/share/applications &&

cat > ${pkg}/usr/share/applications/assistant-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 Assistant
Comment=Shows Qt5 documentation and examples
Exec=/usr/bin/assistant
Icon=assistant-qt5.png
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Documentation;
EOF

cat > ${pkg}/usr/share/applications/designer-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 Designer
GenericName=Interface Designer
Comment=Design GUIs for Qt5 applications
Exec=/usr/bin/designer
Icon=designer-qt5.png
MimeType=application/x-designer;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF

cat > ${pkg}/usr/share/applications/linguist-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 Linguist
Comment=Add translations to Qt5 applications
Exec=/usr/bin/linguist
Icon=linguist-qt5.png
MimeType=text/vnd.trolltech.linguist;application/x-linguist;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF

cat > ${pkg}/usr/share/applications/qdbusviewer-qt5.desktop << EOF
[Desktop Entry]
Name=Qt5 QDbusViewer
GenericName=D-Bus Debugger
Comment=Debug D-Bus applications
Exec=/usr/bin/qdbusviewer
Icon=qdbusviewer-qt5.png
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Debugger;
EOF

install -v -dm755 ${pkg}/etc/profile.d/ &&

cat > ${pkg}/etc/profile.d/qt5.sh << EOF
QT5DIR=/usr
export QT5DIR

pathappend /usr/lib/qt5/plugins    QT_PLUGIN_PATH
pathappend /usr/lib/qt5/libexec
pathappend /usr/lib/qt5/qml        QML2_IMPORT_PATH
EOF

find ${pkg}/ -name \*.prl \
   -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

cd ${pkg}/usr/bin
for file in moc uic rcc qmake lconvert lrelease lupdate; do
  ln -sfrvn $file $file-qt5
done
rm -v ${pkg}/usr/lib/*.la
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
ldconfig &&
cd $TEMP
cd ${srcdir}
cd BUILDQT
make docs
rm -rf ${pkg}/*
make INSTALL_ROOT=${pkg} install_docs &&
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-doc-noarch-$BUILD-tree
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-doc-noarch-$BUILD.txz
rm -rf ${pkg}
cd $home

