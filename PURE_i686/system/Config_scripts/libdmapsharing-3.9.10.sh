#!/bin/bash
###############################################################################
set -e
###############################################################################
# # # https://github.com/GNOME/libdmapsharing/releases
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
./autogen.sh
# patch -Np1 -i ${PATCHSOURCE}/
 JDK_HOME=/opt/jdk \
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
 --disable-tests \
 --with-mdns=avahi \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
