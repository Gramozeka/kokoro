#!/bin/bash
###############################################################################
set -e
###############################################################################
# Check is a unit testing framework for C. It features a simple interface for defining unit tests, putting little in the way of the developer. Tests are run in a separate address space, so Check can catch both assertion failures and code errors that cause segmentation faults or other signals. The output from unit tests can be used within source code editors and IDEs.
# See https://libcheck.github.io/check for more information, including a tutorial. The tutorial is also available as info check.
# Installation
# Check has the following dependencies:
# automake-1.9.6 (1.11.3 on OS X if you are using /usr/bin/ar)
# autoconf-2.59
# libtool-1.5.22
# pkg-config-0.20
# texinfo-4.7 (for documentation)
# tetex-bin (or any texinfo-compatible TeX installation, for documentation)
# POSIX sed
# The versions specified may be higher than those actually needed.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
