#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install LIBDIR=$LIBDIR DESTDIR=${pkg}
  rm -f ${pkg}/$LIBDIR/*.a # guidelines say no static libs
cd ..

echo "$1 ----- $(date)" >> ${destdir}/loginstall

mkdir -p ${pkg}/usr/man/man1

gzip -9c ${SOURCE}/espeak.1 > ${pkg}/usr/man/man1/espeak.1.gz
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
# # # unpack ${SOURCE}/${package}

unzip ${SOURCE}/${packagedir}.zip -x '*/linux_32bit/*' '*/platforms/*'

cd ${packagedir}

chown -R root:root .
# Permissions are bletcherous, reset them all.
find -L . \( -type d -a -exec chmod 755 {} + \) -o \
          \( -type f -a -exec chmod 644 {} + \)

  AUDIO=runtime
  DRIVERS="portaudio and pulseaudio"
# patch -Np1 -i ${PATCHSOURCE}/

cd src
  rm -f portaudio.h # use system-wide header!
  LIBDIR=/usr/lib
  CFLAGS="$CFLAGS -Wno-narrowing"
  make LDFLAGS="-Wl,-s" LIBDIR=$LIBDIR CXXFLAGS="$CFLAGS -Wno-narrowing" AUDIO="$AUDIO"

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
