#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/usr/share/xpdf/cyrillic
unpack ${SOURCE}/xpdf-cyrillic.tar.gz -C ${pkg}/usr/share/xpdf/cyrillic
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
	unpack ${SOURCE}/${package}
	cd ${packagedir}
	mkdir build
	cd    build
	CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
	cmake    \
	-DCMAKE_INSTALL_PREFIX=/usr                \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib    \
	-DLIB_INSTALL_DIR=/usr/lib                   \
	-DBUILD_SHARED_LIBS=ON                         \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX=""                                           \
	-DBUILD_TESTING=OFF                                  \
	-Wno-dev ..
	make -j4 || make || exit 1

pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"


















