#!/bin/bash
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
pkg=${destdir}/${packagedir}
ARCH="i686"
install -vdm755 ${pkg}/opt/OpenJDK-11.0.2+9-i686-bin &&
mv -v * ${pkg}/opt/OpenJDK-11.0.2+9-i686-bin        &&
chown -R root:root ${pkg}/opt/OpenJDK-11.0.2+9-i686-bin
ln -sfn OpenJDK-11.0.2+9-i686-bin ${pkg}/opt/jdk
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home

echo "###########################**** COMPLITE!!! ****##################################"
