#!/bin/bash
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg}  install || exit 1
install -v -m755 tab2space ${pkg}/usr/bin
echo "$1 ----- $(date)" >> ${destdir}/loginstall
# mv -v ${pkg}/usr/lib{,64}

# sed -i 's/\/lib/\/lib64/g' ${pkg}/usr/lib64/pkgconfig/tidy.pc
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}

cd build/cmake &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake   -DCMAKE_C_FLAGS="$CFLAGS" \
  -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
  -DCMAKE_INSTALL_PREFIX=/usr    \
  -DCMAKE_INSTALL_LIB_SUFFIX="" \
      -DBUILD_TAB2SPACE=ON        \
      ../..    &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
