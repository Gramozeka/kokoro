#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Sharutils package contains utilities that can create 'shell' archives.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://ftp.gnu.org/gnu/sharutils/sharutils-4.15.2.tar.xz
# Download (FTP): ftp://ftp.gnu.org/gnu/sharutils/sharutils-4.15.2.tar.xz
# Download MD5 sum: 5975ce21da36491d7aa6dc2b0d9788e0
# Download size: 1.1 MB
# Estimated disk space required: 22 MB (with the test suite)
# Estimated build time: 0.4 SBU (with the test suite)
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/BUFSIZ/rw_base_size/' src/unshar.c &&
sed -i '/program_name/s/^/extern /' src/*opts.h
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c        &&
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
