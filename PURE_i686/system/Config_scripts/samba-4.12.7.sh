#!/bin/bash
###############################################################################
set -e
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
mkdir -pv ${pkg}/usr/lib/cups/backend
ln -v -sf /usr/bin/smbspool ${pkg}/usr/lib/cups/backend/smb
mv -v ${pkg}/usr/lib/libnss_win{s,bind}.so*   ${pkg}/lib                       &&
ln -v -sf ../../lib/libnss_winbind.so.2 ${pkg}/usr/lib/libnss_winbind.so &&
ln -v -sf ../../lib/libnss_wins.so.2    ${pkg}/usr/lib/libnss_wins.so    &&
mkdir -pv ${pkg}/etc
install -v -m644    examples/smb.conf.default ${pkg}/etc/samba &&

mkdir -pv ${pkg}/etc/openldap/schema                        &&

install -v -m644    examples/LDAP/README              \
                    ${pkg}/etc/openldap/schema/README.LDAP  &&

install -v -m644    examples/LDAP/samba*              \
                    ${pkg}/etc/openldap/schema              &&

install -v -m755    examples/LDAP/{get*,ol*} \
                    ${pkg}/etc/openldap/schema
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -r 's/nss_(setpw|endpw|setgr|endgr)ent/my_&/' \
    -i nsswitch/nsstest.c
echo "^samba4.rpc.echo.*on.*ncacn_np.*with.*object.*nt4_dc" >> selftest/knownfail
CXXFLAGS=$CXXFLAGS \
CFLAGS="$CFLAGS -I/usr/include/tirpc"          \
LDFLAGS="$LDFLAGS -ltirpc"                      \
  ./configure                          \
    --prefix=/usr                      \
    --sysconfdir=/etc                  \
    --libdir=/usr/lib \
    --localstatedir=/var               \
    --with-piddir=/run/samba           \
    --with-pammodulesdir=/lib/security \
    --enable-fhs                       \
    --without-ad-dc                    \
    --without-systemd                  \
    --enable-selftest                  &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
# cd ${home}/blfs-bootscripts-20170731
# make install-samba
# make install-winbindd
cd ${home}
