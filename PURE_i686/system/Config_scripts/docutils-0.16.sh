#!/bin/bash
###############################################################################
###############################################################################
set -e
###############################################################################
# docutils is a set of Python modules and programs for processing plaintext docs into formats such as HTML, XML, or LaTeX.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://downloads.sourceforge.net/docutils/docutils-0.16.tar.gz
# Download MD5 sum: 9ccb6f332e23360f964de72c8ea5f0ed
# Download size: 1.8 MB
# Estimated disk space required: 12 MB
# Estimated build time: less than 0.1 SBU
# Docutils Dependencies
# Optional
# Python-2.7.17
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
python3 setup.py install  --optimize=1  --root=${pkg} &&

for f in ${pkg}/usr/bin/rst*.py; do
  ln -svf $(basename $f) ${pkg}/usr/bin/$(basename $f .py)
done

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
python3 setup.py build
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

