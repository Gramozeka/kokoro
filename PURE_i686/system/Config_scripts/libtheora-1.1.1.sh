#!/bin/bash
###############################################################################
set -e
# libtheora is a reference implementation of the Theora video compression format being developed by the Xiph.Org Foundation.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.xiph.org/releases/theora/libtheora-1.1.1.tar.xz
# Download MD5 sum: 9eeabf1ad65b7f41533854a59f7a716d
# Download size: 1.4 MB
# Estimated disk space required: 13.4 MB (without static libs or API docs and without installing the examples)
# Estimated build time: 0.2 SBU
# libtheora Dependencies
# Required
# libogg-1.3.4
# Recommended
# libvorbis-1.3.6
# Optional
# SDL-1.2.15 and libpng-1.6.37 (both to build the example players), Doxygen-1.8.17, texlive-20190410 (or install-tl-unx), BibTex, and Transfig (all four to build the API documentation), and Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's/png_\(sizeof\)/\1/g' examples/png2theora.c &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
