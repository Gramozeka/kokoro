#!/bin/bash
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} \
PREFIX=/usr \
LIBDIR=/usr/lib \
HAS_SOLIB_VERSION=1 \
install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make \
PREFIX=/usr \
LIBDIR=/usr/lib \
HAS_SOLIB_VERSION=1 \
 static dynamic cryptest.exe \
-j5
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
