#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install SSLTYPE=unix DESTDIR=${pkg}
mkdir -pv ${pkg}/etc
./alpine/alpine -conf > ${pkg}/etc/pine.conf
cd imap
make
mkdir -p ${pkg}/usr/sbin
cat imapd/imapd > ${pkg}/usr/sbin/imapd
cat ipopd/ipop3d > ${pkg}/usr/sbin/ipop3d
chmod 755 ${pkg}/usr/sbin/imapd ${pkg}/usr/sbin/ipop3d
mkdir -pv ${pkg}/usr/share/man/man8
for file in src/imapd/imapd.8 src/ipopd/ipopd.8 ; do
  cat $file | gzip -9c > ${pkg}/usr/share/man/man8/$(basename $file).gz
done

cd c-client
   strip -g c-client.a
   mkdir -p ${pkg}/usr/local/lib
   cp -v c-client.a ${pkg}/usr/local/lib/
       mkdir -p ${pkg}/usr/local/include
    cp *.h ${pkg}/usr/local/include

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
zcat ${PATCHSOURCE}/alpine.manpage.diff.gz | patch -p1 --verbose || exit 1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
  --with-ssl-dir=/usr \
  --with-ssl-certs-dir=/etc/ssl/certs \
  --with-c-client-target=slx \
  --with-system-pinerc=/etc/pine.conf \
  --with-system-fixed-pinerc=/etc/pine.conf.fixed \
  --with-passfile=.alpine.passfile \
  --disable-debug \
  --with-debug-level=0 \
  --without-tcl \
  --program-prefix= \
  --program-suffix= \
--build=${CLFS_TARGET}
( cd doc/tech-notes
  sed -i "s,/usr/local/lib/pine.info,/usr/lib/pine.info,g" tech-notes.txt
  sed -i "s,/usr/local/lib,/etc,g" tech-notes.txt
  sed -i "s,/usr/local,/etc,g" tech-notes.txt
)
echo y | make EXTRACFLAGS="-march=i686" SSLTYPE=unix
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
