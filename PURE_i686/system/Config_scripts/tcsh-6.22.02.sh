#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Tcsh package contains “an enhanced but completely compatible version of the Berkeley Unix C shell (csh)”. This is useful as an alternative shell for those who prefer C syntax to that of the bash shell, and also because some programs require the C shell in order to perform installation tasks.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (FTP): ftp://ftp.astron.com/pub/tcsh/tcsh-6.22.02.tar.gz
# Download MD5 sum: f34909eab33733aecc05d27adc82277b
# Download size: 984 KB
# Estimated disk space required: 13 MB (with tests)
# Estimated build time: 0.2 SBU (with tests)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install install.man &&
ln -v -sf tcsh   ${pkg}/bin/csh &&
ln -v -sf tcsh.1 ${pkg}/usr/share/man/man1/csh.1 &&

install -v -m755 -d          ${pkg}/usr/share/doc/tcsh-6.22.02/html &&
install -v -m644 tcsh.html/* ${pkg}/usr/share/doc/tcsh-6.22.02/html &&
install -v -m644 FAQ         ${pkg}/usr/share/doc/tcsh-6.22.02
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS="$CFLAGS -fcommon" \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--bindir=/bin \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  --docdir=/usr/share/doc/${packagedir}
make -j4
sh ./tcsh.man2html
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cat >> /etc/shells << "EOF"
/bin/tcsh
/bin/csh
EOF
cat > ~/.cshrc << "EOF"
# Original at:
# https://www.cs.umd.edu/~srhuang/teaching/code_snippets/prompt_color.tcsh.html

# Modified by the BLFS Development Team.

# Add these lines to your ~/.cshrc (or to /etc/csh.cshrc).

# Colors!
set     red="%{\033[1;31m%}"
set   green="%{\033[0;32m%}"
set  yellow="%{\033[1;33m%}"
set    blue="%{\033[1;34m%}"
set magenta="%{\033[1;35m%}"
set    cyan="%{\033[1;36m%}"
set   white="%{\033[0;37m%}"
set     end="%{\033[0m%}" # This is needed at the end...

# Setting the actual prompt.  Two separate versions for you to try, pick
# whichever one you like better, and change the colors as you want.
# Just don't mess with the ${end} guy in either line...  Comment out or
# delete the prompt you don't use.

set prompt="${green}%n${blue}@%m ${white}%~ ${green}%%${end} "
set prompt="[${green}%n${blue}@%m ${white}%~ ]${end} "

# This was not in the original URL above
# Provides coloured ls
alias ls ls --color=always

# Clean up after ourselves...
unset red green yellow blue magenta cyan yellow white end
EOF

