#!/bin/bash
###############################################################################
set -e
###############################################################################

###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
#unpack ${SOURCE}/${package}
git clone https://gitlab.gnome.org/GNOME/pyatspi2.git ${packagedir}
cd ${packagedir}
PYTHON=/usr/bin/python3.8 ./autogen.sh
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
PYTHON=/usr/bin/python3.8 ./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
