#!/bin/bash
###############################################################################
set -e
###############################################################################
# https://github.com/libproxy/libproxy
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
	mkdir build
	cd    build
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLIB_INSTALL_DIR=/usr/lib \
    -DPYTHON3_EXECUTABLE:FILEPATH=/usr/bin/python3 \
    -DPYTHON3_SITEPKG_DIR=/usr/lib/python3.8/site-packages \
    -DGAC_DIR=/usr/lib/mono \
    -DLIB_SUFIX=64 \
    -DWITH_VALA=yes \
    -DWITH_DOTNET=yes \
    -DPERL_VENDORINSTALL=yes \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_FLAGS="$BUILD64" \
    -DCMAKE_C_FLAGS="$BUILD64" \
    ..
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
