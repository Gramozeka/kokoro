#!/bin/bash
###############################################################################
set -e
###############################################################################
# The OpenSSH package contains ssh clients and the sshd daemon. This is useful for encrypting authentication and subsequent traffic over a network. The ssh and scp commands are secure implementations of telnet and rcp respectively.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-8.1p1.tar.gz
# Download MD5 sum: 513694343631a99841e815306806edf0
# Download size: 1.5 MB
# Estimated disk space required: 45 MB (add 12 MB for tests)
# Estimated build time: 0.4 SBU (running the tests takes 17+ minutes, irrespective of processor speed)
# OpenSSH Dependencies
# Optional
# GDB-8.3.1 (for tests), Linux-PAM-1.3.1, X Window System, MIT Kerberos V5-1.17.1, libedit, LibreSSL Portable, OpenSC, and libsectok
# Optional Runtime (Used only to gather entropy)
# OpenJDK-12.0.2, Net-tools-CVS_20101030, and Sysstat-12.3.1
###############################################################################
install  -v -m700 -d /var/lib/sshd &&
chown    -v root:sys /var/lib/sshd &&

groupadd -g 50 sshd        &&
useradd  -c 'sshd PrivSep' \
         -d /var/lib/sshd  \
         -g sshd           \
         -s /bin/false     \
         -u 50 sshd
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

echo "PermitRootLogin no" >> ${pkg}/etc/ssh/sshd_config

echo "PasswordAuthentication no" >> ${pkg}/etc/ssh/sshd_config &&
echo "ChallengeResponseAuthentication no" >> ${pkg}/etc/ssh/sshd_config
sed 's@d/login@d/sshd@g' /etc/pam.d/login > ${pkg}/etc/pam.d/sshd &&
chmod 644 ${pkg}/etc/pam.d/sshd &&
echo "UsePAM yes" >> ${pkg}/etc/ssh/sshd_config

install  -v -m700 -d ${pkg}/var/lib/sshd &&
chown    -v root:sys ${pkg}/var/lib/sshd
mkdir -pv ${pkg}/usr/share/man/man1
mkdir -pv ${pkg}/usr/share/doc
install -v -m755    contrib/ssh-copy-id ${pkg}/usr/bin     &&

install -v -m644    contrib/ssh-copy-id.1 \
                    ${pkg}/usr/share/man/man1              &&
install -v -m755 -d ${pkg}/usr/share/doc/${packagedir}     &&
install -v -m644    INSTALL LICENCE OVERVIEW README* \
                    ${pkg}/usr/share/doc/${packagedir}

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
if [[ -e "/usr/lib/libpam.so" ]];then 
PAM="--with-pam"
fi
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
            --sysconfdir=/etc/ssh             \
            --with-md5-passwords              \
            --with-privsep-path=/var/lib/sshd \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir} $PAM
make -j4
pack_local64 ${packagedir}
  if [ ! -f /etc/ssh/ssh_host_dsa_key ]; then
    /usr/bin/ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key -N ''
  fi
  if [ ! -f /etc/ssh/ssh_host_rsa_key ]; then
    /usr/bin/ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
  fi
  if [ ! -f /etc/ssh/ssh_host_ecdsa_key ]; then
    /usr/bin/ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
  fi
  if [ ! -f /etc/ssh/ssh_host_ed25519_key ]; then
    /usr/bin/ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
  fi
  # Catch any new host key types not yet created above:
  /usr/bin/ssh-keygen -A

echo "###########################**** COMPLITE!!! ****##################################"
cd boot-script
make install-sshd
