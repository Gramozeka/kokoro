#!/bin/bash
###############################################################################
# The lsof package is useful to LiSt Open Files for a given running application or process.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (FTP): https://www.mirrorservice.org/sites/lsof.itap.purdue.edu/pub/tools/unix/lsof/lsof_4.91.tar.gz
# Download MD5 sum: 10e1353aa4bf2fd5bbed65db9ef6fd47
# Download size: 1.1 MB
# Estimated disk space required: 9.6 MB
# Estimated build time: less than 0.1 SBU
# lsof Dependencies
# Required
# libtirpc-1.2.5
###############################################################################
pack_local () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/bin
mkdir -pv ${pkg}/usr/share/man/man8
install -v -m0755 -o root -g root lsof ${pkg}/usr/bin &&
install -v lsof.8 ${pkg}/usr/share/man/man8
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
ARCH="x86_64"
unpack ${SOURCE}/${package}
cd ${packagedir}
tar -xf lsof_4.91_src.tar  &&
cd lsof_4.91_src           &&
./Configure -n linux       &&
make CFGL="-L./lib -ltirpc"
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
