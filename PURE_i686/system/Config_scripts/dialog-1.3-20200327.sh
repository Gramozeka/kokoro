#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/dialog.smaller.min.height.diff.gz | patch -p1 --verbose || exit 1
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
  --disable-static \
  --enable-nls \
  --with-ncursesw \
  --enable-widec \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
