#!/bin/bash
###############################################################################
set -e
###############################################################################
	cd $TEMPBUILD
	rm -rf *
	package="$(basename $line).*"
	packagedir=$(basename $line)
###############################################################################
###############################################################################
# 	unpack ${SOURCE}/${package}
git clone https://github.com/FNA-XNA/FAudio.git ${packagedir}
	cd ${packagedir}
	mkdir build
	cd    build
	CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
	cmake    \
	-DCMAKE_INSTALL_PREFIX=/usr                \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib    \
	-DBUILD_SHARED_LIBS=ON                         \
	-DCMAKE_BUILD_TYPE=Release                 \
	-DLIB_SUFFIX=""                                           \
	-DBUILD_TESTING=OFF                                  \
	-Wno-dev ..
	make -j4 || make || exit 1

	pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

















