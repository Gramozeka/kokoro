#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GNOME Icon Theme Extras package contains extra icons for the GNOME Desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-extras/3.12/gnome-icon-theme-extras-3.12.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-extras/3.12/gnome-icon-theme-extras-3.12.0.tar.xz
# Download MD5 sum: 91f8f7e35a3d8d926716d88b8b1e9a29
# Download size: 1.7 MB
# Estimated disk space required: 12 MB
# Estimated build time: less than 0.1 SBU
# GNOME Icon Theme Extras Dependencies
# Required
# gnome-icon-theme-3.12.0
# Optional
# git-2.25.0 and Inkscape-0.92.4
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
