#!/bin/bash
###############################################################################
set -e
# The CrackLib package contains a library used to enforce strong passwords by comparing user selected passwords to words in chosen word lists.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/cracklib/cracklib/releases/download/v2.9.7/cracklib-2.9.7.tar.bz2
# Download MD5 sum: 0d68de25332cee5660850528a385427f
# Download size: 592 KB
# Estimated disk space required: 4.2 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Recommended word list for English-speaking countries (size: 6.7 MB; md5sum: 94e9963e4786294f7fb0f2efd7618551): https://github.com/cracklib/cracklib/releases/download/v2.9.7/cracklib-words-2.9.7.bz2
# There are additional word lists available for download, e.g., from http://www.cotse.com/tools/wordlists.htm. CrackLib can utilize as many, or as few word lists you choose to install.
# [Important] Important
# Users tend to base their passwords on regular words of the spoken language, and crackers know that. CrackLib is intended to filter out such bad passwords at the source using a dictionary created from word lists. To accomplish this, the word list(s) for use with CrackLib must be an exhaustive list of words and word-based keystroke combinations likely to be chosen by users of the system as (guessable) passwords.
# The default word list recommended above for downloading mostly satisfies this role in English-speaking countries. In other situations, it may be necessary to download (or even create) additional word lists.
# Note that word lists suitable for spell-checking are not usable as CrackLib word lists in countries with non-Latin based alphabets, because of “word-based keystroke combinations” that make bad passwords.
# CrackLib Dependencies
# Optional
# Python-2.7.17
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install

mkdir -pv ${pkg}/lib
mkdir -pv ${pkg}/usr/share/dict
mv -v ${pkg}/usr/lib/libcrack.so.* ${pkg}/lib
ln -sfv ../../lib/libcrack.so.2.9.0 ${pkg}/usr/lib/libcrack.so
install -v -m644 -D    ${SOURCE}/cracklib-words-2.9.7.bz2 \
                         ${pkg}/usr/share/dict/cracklib-words.bz2
bunzip2 -v            ${pkg}/usr/share/dict/cracklib-words.bz2
ln -v -sf cracklib-words ${pkg}/usr/share/dict/words
echo "Phoniex" >>      ${pkg}/usr/share/dict/cracklib-extra-words
install -v -m755 -d      ${pkg}/lib/cracklib 
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i '/skipping/d' util/packer.c &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS
./configure --prefix=/usr \
  --libdir=/usr/lib -with-default-dict=/lib/cracklib/pw_dict &&
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

create-cracklib-dict     /usr/share/dict/cracklib-words \
                         /usr/share/dict/cracklib-extra-words
