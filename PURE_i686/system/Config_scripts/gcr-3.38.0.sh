#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Gcr package contains libraries used for displaying certificates and accessing key stores. It also provides the viewer for crypto files on the GNOME Desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gcr/3.34/gcr-3.34.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gcr/3.34/gcr-3.34.0.tar.xz
# Download MD5 sum: 4af28919fb1dd36d93603e8230283b6f
# Download size: 1.4 MB
# Estimated disk space required: 88 MB (with tests)
# Estimated build time: 0.6 SBU (with tests)
# Gcr Dependencies
# Required
# GLib-2.62.4, libgcrypt-1.8.5, libtasn1-4.15.0, and p11-kit-0.23.19
# Recommended
# GnuPG-2.2.19, gobject-introspection-1.62.0, GTK+-3.24.13, libxslt-1.1.34, and Vala-0.46.5
# Optional
# GTK-Doc-1.32 and Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i -r 's:"(/desktop):"/org/gnome\1:' schema/*.xml &&
# sed -i '610 s/;/ = { 0 };/' gck/gck-slot.c &&
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
