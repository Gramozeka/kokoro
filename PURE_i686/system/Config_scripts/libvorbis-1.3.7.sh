#!/bin/bash
###############################################################################
set -e
# The libvorbis package contains a general purpose audio and music encoding format. This is useful for creating (encoding) and playing (decoding) sound in an open (patent free) format.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.xiph.org/releases/vorbis/libvorbis-1.3.6.tar.xz
# Download MD5 sum: b7d1692f275c73e7833ed1cc2697cd65
# Download size: 1.1 MB
# Estimated disk space required: 12 MB
# Estimated build time: 0.1 SBU
# libvorbis Dependencies
# Required
# libogg-1.3.4
# Optional
# Doxygen-1.8.17 and texlive-20190410 (or install-tl-unx) (specifically, pdflatex and htlatex) to build the PDF documentation
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
