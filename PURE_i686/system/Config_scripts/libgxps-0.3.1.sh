#!/bin/bash
###############################################################################
set -e
# The libgxps package provides an interface to manipulate XPS documents.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libgxps/0.3/libgxps-0.3.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libgxps/0.3/libgxps-0.3.1.tar.xz
# Download MD5 sum: ade83c264b3af2551a0dff9144478df8
# Download size: 92 KB
# Estimated disk space required: 6.0 MB
# Estimated build time: 0.1 SBU
# Libgxps Dependencies
# Required
# GTK+-3.24.13, Little CMS-2.9, libarchive-3.4.1, libjpeg-turbo-2.0.4, LibTIFF-4.1.0, and libxslt-1.1.34
# Optional
# git-2.25.0 and GTK-Doc-1.32
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
