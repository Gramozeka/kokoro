#!/bin/bash
###############################################################################
set -e
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
mkdir -pv ${pkg}/usr/share/man/man1
mkdir -pv ${pkg}/usr/bin
make prefix=${pkg}/usr MANDIR=${pkg}/usr/share/man/man1 \
 -f unix/Makefile install

echo "$1 ----- $(date)" >> ${destdir}/loginstall

find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/unzip-6.0-consolidated_fixes-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -f unix/Makefile generic
pack_local64 ${packagedir}-64
echo "###########################**** COMPLITE!!! ****##################################"
