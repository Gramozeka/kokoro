#!/bin/bash
###############################################################################
set -e
# libdvdread is a library which provides a simple foundation for reading DVDs.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://get.videolan.org/libdvdread/6.0.2/libdvdread-6.0.2.tar.bz2
# Download MD5 sum: 49990935174bf6b2fa501e789c578135
# Download size: 396 KB
# Estimated disk space required: 3.8 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
