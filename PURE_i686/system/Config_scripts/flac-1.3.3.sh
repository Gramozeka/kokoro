#!/bin/bash
###############################################################################
set -e
# FLAC is an audio CODEC similar to MP3, but lossless, meaning that audio is compressed without losing any information.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.xiph.org/releases/flac/flac-1.3.3.tar.xz
# Download MD5 sum: 26703ed2858c1fc9ffc05136d13daa69
# Download size: 1.0 MB
# Estimated disk space required: 21 MB (additional 95 MB to run the test suite)
# Estimated build time: 0.1 SBU (additional 0.7 SBU to run the test suite)
# FLAC Dependencies
# Optional
# libogg-1.3.4, NASM-2.14.02, DocBook-utils-0.6.14, Doxygen-1.8.17 and Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--disable-thorough-tests \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
