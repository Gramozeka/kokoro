#!/bin/bash
###############################################################################
set -e
###############################################################################
# The enchant package provide a generic interface into various existing spell checking libraries.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/AbiWord/enchant/releases/download/v2.2.7/enchant-2.2.7.tar.gz
# Download MD5 sum: 8a6ea1bb143c64e0edf5e49c7e7cb984
# Download size: 932 KB
# Estimated disk space required: 6.8 MB
# Estimated build time: 0.2 SBU
# enchant Dependencies
# Required
# GLib-2.62.4
# Recommended
# Aspell-0.60.8
# Optional
# dbus-glib-0.110, Hspell, Hunspell, Voikko, and unittest-cpp (required for tests)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
ln -sfv enchant-2       ${pkg}/usr/include/enchant   &&
ln -sfv enchant-2       ${pkg}/usr/bin/enchant       &&
ln -sfv libenchant-2.so ${pkg}/usr/lib/libenchant.so &&
ln -sfv enchant-2.pc    ${pkg}/usr/lib/pkgconfig/enchant.pc
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
