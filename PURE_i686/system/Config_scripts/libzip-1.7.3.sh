#!/bin/bash
###############################################################################
set -e
# libzip is a C library for reading, creating, and modifying zip archives. Files can be added from data buffers, files, or compressed data copied directly from other zip archives. Changes made without closing the archive can be reverted. The API is documented by man pages.
# Current version is 1.6.0, released on January 24, 2020.
# https://libzip.org/download/libzip-1.6.0.tar.xz
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
mkdir -p build
cd build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
 cmake \
    -DCMAKE_C_FLAGS:STRING="$CFLAGS" \
    -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_MANDIR=/usr/share/man \
    -DLIB_SUFFIX="" \
    -DCMAKE_BUILD_TYPE=Release .. || exit 1
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
