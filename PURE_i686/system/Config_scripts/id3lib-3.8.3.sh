#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
find ${PATCHSOURCE}/patches-id3lib/ -type f | sort -n |
    while read patch; do patch -p1 -i $patch; done

# iomanip.h is obsolete; use the standard C++ header:
sed -i "s%iomanip.h%iomanip%g" configure
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib  \
  --enable-static=no \
  --enable-debug=no \
--localstatedir=/var \
--build=${CLFS_TARGET}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
