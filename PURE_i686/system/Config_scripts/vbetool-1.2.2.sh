#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -v --install
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
  --mandir=/usr/share/man \
  --docdir=/usr/doc/${packagedir} \
--build=${CLFS_TARGET} &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
