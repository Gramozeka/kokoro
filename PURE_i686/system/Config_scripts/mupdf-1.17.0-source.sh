#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
USE_SYSTEM_LIBS=yes                     \
make DESTDIR=${pkg} prefix=/usr   libdir=/usr/lib               \
     build=release                      \
     docdir=/usr/share/doc/mupdf-1.16.1 \
     install                            &&

ln -sfv mupdf-x11 ${pkg}/usr/bin/mupdf        &&
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
patch -Np1 -i ${PATCHSOURCE}/mupdf-1.17.0-shared_libs-1.patch
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
USE_SYSTEM_LIBS=yes \
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
