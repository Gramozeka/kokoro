#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
perl Makefile.PL
make -j4
make install
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
