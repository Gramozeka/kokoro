#!/bin/bash
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
# git clone https://github.com/cryfs/cryfs.git ${packagedir}
unpack ${SOURCE}/${package}
      mkdir build
      cd    build
BOOST_LIBRARYDIR="/usr/lib -l" \
PKG_CONFIG_PATH=$PKG_CONFIG_PATH64 \
LDFLAGS="-L/usr/lib -lboost_thread  -lboost_program_options -lboost_filesystem -lcryptopp -lboost_chrono -lfuse" \
cmake      -DCMAKE_C_FLAGS:STRING="-O2" \
      -DCMAKE_CXX_FLAGS:STRING="-O2" \
-DCMAKE_INSTALL_PREFIX=/usr \
            -DCMAKE_PREFIX_PATH=/usr       \
            -DCMAKE_BUILD_TYPE=Release     \
            -DCMAKE_INSTALL_LIBDIR=/usr/lib   \
            -DCRYFS_UPDATE_CHECKS=off \
             -DBUILD_TESTING=OFF \
             -DBoost_INCLUDE_DIRS=/usr/include/boost \
             -DBoost_USE_STATIC_LIBS=off \
             -DCMAKE_INSTALL_COMPONENT=ON \
            -Wno-dev ..
make -j4 || make || exit 1
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
