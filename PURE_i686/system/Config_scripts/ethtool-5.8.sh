#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
  --mandir=/usr/share/man \
--build=${CLFS_TARGET} &&
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
