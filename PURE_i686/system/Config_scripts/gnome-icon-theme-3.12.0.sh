#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GNOME Icon Theme package contains an assortment of non-scalable icons of different sizes and themes.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme/3.12/gnome-icon-theme-3.12.0.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme/3.12/gnome-icon-theme-3.12.0.tar.xz
# Download MD5 sum: f14bed7f804e843189ffa7021141addd
# Download size: 17 MB
# Estimated disk space required: 85 MB
# Estimated build time: 0.5 SBU
# GNOME Icon Theme Dependencies
# Required
# GTK+-3.24.13 or GTK+-2.24.32, hicolor-icon-theme-0.17, and icon-naming-utils-0.8.90
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
