#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)
# https://github.com/nu-book/zxing-cpp/releases
unpack ${SOURCE}/${package}
cd ${packagedir}
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake      \
-DCMAKE_INSTALL_PREFIX=/usr \
 -DCMAKE_INSTALL_LIBDIR=/usr/lib \
 -DLIB_INSTALL_DIR=lib \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev ..
make -j4 || make || exit 1

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
