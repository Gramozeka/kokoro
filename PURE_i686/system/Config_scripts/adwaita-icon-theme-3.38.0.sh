#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Adwaita Icon Theme package contains an icon theme for Gtk+ 3 applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/adwaita-icon-theme/3.34/adwaita-icon-theme-3.34.3.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/adwaita-icon-theme/3.34/adwaita-icon-theme-3.34.3.tar.xz
# Download MD5 sum: 9aea4ad9bc002aacad155ee0748b357f
# Download size: 19 MB
# Estimated disk space required: 96 MB
# Estimated build time: 0.6 SBU
# Adwaita Icon Theme Dependencies
# Optional
# git-2.25.0, GTK+-2.24.32 or GTK+-3.24.13 (if present, librsvg-2.46.4 is also required, and gtk-update-icon-cache and gtk-encode-symbolic-svg are run after installing), Inkscape-0.92.4 and Icon Tools
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
