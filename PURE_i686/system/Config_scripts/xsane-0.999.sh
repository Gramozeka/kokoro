#!/bin/bash
###############################################################################
pack_local () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
  find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
mkdir -pv ${pkg}/usr/lib/gimp/2.0/plug-ins
ln -v -s /usr/bin/xsane ${pkg}/usr/lib/gimp/2.0/plug-ins/xsane
ln -v -s /usr/libexec/webkit2gtk-4.0/MiniBrowser ${pkg}/usr/bin/netscape
cd ${pkg} &&
mkdir -p usr/tree-info
tree > usr/tree-info/$1-$USE_ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$USE_ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

tar -xvf ${SOURCE}/${package}
cd ${packagedir}
sed -i -e 's/png_ptr->jmpbuf/png_jmpbuf(png_ptr)/' src/xsane-save.c &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
            --localstatedir=/var \
--disable-static \
--build=${CLFS_TARGET} &&
make -j4
pack_local ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"

gtk-update-icon-cache &&
update-desktop-database
