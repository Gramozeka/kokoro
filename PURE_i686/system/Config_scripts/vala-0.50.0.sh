#!/bin/bash
###############################################################################
set -e
# Vala is a new programming language that aims to bring modern programming language features to GNOME developers without imposing any additional runtime requirements and without using a different ABI compared to applications and libraries written in C.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/vala/0.46/vala-0.46.5.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/vala/0.46/vala-0.46.5.tar.xz
# Download MD5 sum: a0a2eb8babb72c1b4e81c4f1b98429df
# Download size: 3.2 MB
# Estimated disk space required: 172 MB (add 9 MB for tests)
# Estimated build time: 0.5 SBU (using parallelism=4; add 1.9 SBU for tests)
# Vala Dependencies
# Required
# GLib-2.62.4
# Recommended
# Graphviz-2.42.3 (Required for valadoc)
# Optional
# dbus-1.12.16 (Required for the tests), libxslt-1.1.34 (Required for generating the documentation), help2man, and weasyprint
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
