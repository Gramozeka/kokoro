#!/bin/bash
###############################################################################
set -e
# The Fontconfig package contains a library and support programs used for configuring and customizing font access.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.freedesktop.org/software/fontconfig/release/fontconfig-2.13.1.tar.bz2
# Download MD5 sum: 36cdea1058ef13cbbfdabe6cb019dc1c
# Download size: 1.6 MB
# Estimated disk space required: 16 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# Fontconfig Dependencies
# Required
# FreeType-2.10.1
# Optional
# DocBook-utils-0.6.14 and libxml2-2.9.10, texlive-20190410 (or install-tl-unx)
# [Note] Note
# If you have DocBook Utils installed and you remove the --disable-docs parameter from the configure command below, you must have SGMLSpm-1.1 and texlive-20190410 installed also, or the Fontconfig build will fail.
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -dm755 \
        ${pkg}/usr/share/{man/man{1,3,5},doc/${packagedir}/fontconfig-devel} &&
install -v -m644 fc-*/*.1         ${pkg}/usr/share/man/man1 &&
install -v -m644 doc/*.3          ${pkg}/usr/share/man/man3 &&
install -v -m644 doc/fonts-conf.5 /usr/share/man/man5 &&
install -v -m644 doc/fontconfig-devel/* \
                                  ${pkg}/usr/share/doc/${packagedir}/fontconfig-devel &&
install -v -m644 doc/*.{pdf,sgml,txt,html} \
                                  ${pkg}/usr/share/doc/${packagedir}
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
rm -f src/fcobjshash.h
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET}  \
--disable-docs \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
