#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Power Management Utilities provide simple shell command line tools to suspend and hibernate the computer. They can be used to run user supplied scripts on suspend and resume.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://pm-utils.freedesktop.org/releases/pm-utils-1.4.1.tar.gz
# Download MD5 sum: 1742a556089c36c3a89eb1b957da5a60
# Download size: 204 KB
# Estimated disk space required: 1.6 MB
# Estimated build time: 0.1 SBU
# Power Management Utilities Dependencies
# Optional
# xmlto-0.0.28 (to generate man pages)
# Optional (runtime)
# Hdparm-9.58, Wireless Tools-29, ethtool, and vbetool
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install DESTDIR=${pkg}
mkdir -pv ${pkg}/usr/lib/pm-utils
tar xf ${SOURCE}/pm-quirks-20100619.tar.gz -C ${pkg}/usr/lib/pm-utils
chown -R root:root ${pkg}/usr/lib/pm-utils/video-quirks
mv -v ${pkg}/usr/lib/pm-utils/sleep.d/49bluetooth{,-ibm}

cat > ${pkg}/usr/lib/pm-utils/sleep.d/49bluetooth-generic << "EOF"
#!/bin/sh

. "${PM_FUNCTIONS}"

case "$1" in
  hibernate|suspend)
    if [ -d /sys/devices/virtual/misc/rfkill -a -x /usr/sbin/rfkill -a -x /etc/rc.d/rc.bluetooth ]; then
      rfkill block bluetooth
    fi
   ;;
  thaw|resume)
    if [ -d /sys/devices/virtual/misc/rfkill -a -x /usr/sbin/rfkill -a -x /etc/rc.d/rc.bluetooth ]; then
      rfkill unblock bluetooth
    fi
    ;;
  *)
    ;;
esac
EOF

chmod 0755 ${pkg}/usr/lib/pm-utils/sleep.d/49bluetooth-generic
rm -f ${pkg}/usr/lib/pm-utils/sleep.d/55NetworkManager
rm -f ${pkg}/usr/lib/pm-utils/power.d/journal-commit
rm -f ${pkg}/usr/lib/pm-utils/power.d/readahead
rm -f ${pkg}/usr/lib/pm-utils/power.d/hal-cd-polling
chmod 644 ${pkg}/usr/lib/pm-utils/power.d/*

echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
zcat ${PATCHSOURCE}/use_more_sane_harddrive_defaults.patch.gz | patch -p1 || exit 1
zcat ${PATCHSOURCE}/fix-wrong-path-in-intel-audio-powersave.patch.gz | patch -p1 || exit 1
zcat ${PATCHSOURCE}/init-logfile-append.patch.gz | patch -p1 || exit 1
zcat ${PATCHSOURCE}/hook-exit-code-log.patch.gz | patch -p1 || exit 1
zcat ${PATCHSOURCE}/log-line-spacing-fix.patch.gz | patch -p1 || exit 1
zcat ${PATCHSOURCE}/add-in-kernel-suspend-to-both.patch.gz | patch -p1 || exit 1
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS 
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
