#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
sed -i 's@cert.pem@certs/ca-bundle.crt@' CMakeLists.txt
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake     \
-DCMAKE_INSTALL_PREFIX=/usr \
-DMAN_INSTALL_DIR=/usr/share/man \
-DQCA_INSTALL_IN_QT_PREFIX:BOOL=ON \
-DCMAKE_BUILD_TYPE=Release     \
-DQCA_MAN_INSTALL_DIR:PATH=/usr/share/man \
-Wno-dev ..
make -j4 || make || exit 1

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
