#!/bin/bash
###############################################################################
set -e
###############################################################################
# The GNOME Desktop package contains a library that provides an API shared by several applications on the GNOME Desktop.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/gnome-desktop/3.34/gnome-desktop-3.34.3.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/gnome-desktop/3.34/gnome-desktop-3.34.3.tar.xz
# Download MD5 sum: cdcd3006f55d8c8813315bef6f4f6b71
# Download size: 700 KB
# Estimated disk space required: 14 MB (with tests)
# Estimated build time: 0.1 SBU (Using parallelism=4; with tests)
# GNOME Desktop Dependencies
# Required
# gsettings-desktop-schemas-3.34.0, GTK+-3.24.13, ISO Codes-4.4, itstool-2.0.6, libseccomp-2.4.2, libxml2-2.9.10, and xkeyboard-config-2.28
# Recommended
# bubblewrap-0.4.0 (needed for thumbnailers in Nautilus) and gobject-introspection-1.62.0
# Optional
# GTK-Doc-1.32
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dgnome_distributor="BLFS" \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
