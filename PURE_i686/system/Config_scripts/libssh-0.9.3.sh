#!/bin/bash
###############################################################################
set -e
# http://www.libssh.org/
# https://www.libssh.org/files/0.9/libssh-0.9.3.tar.xz
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
      mkdir build
      cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
cmake   \
-DCMAKE_INSTALL_PREFIX=/usr \
 -DCMAKE_INSTALL_LIBDIR=/usr/lib \
 -DLIB_INSTALL_DIR=lib \
-DCMAKE_BUILD_TYPE=Release         \
    -DLIB_SUFFIX="" \
    -DBUILD_TESTING=OFF \
            -Wno-dev ..
make -j4 || make || exit 1

pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
