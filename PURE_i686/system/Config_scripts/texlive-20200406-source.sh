#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
pkg=${destdir}/${packagedir}
unpack ${SOURCE}/${package}
cd ${packagedir}
# autoreconf -vfi
# automake --add-missing
export LANG=C
export PATH="${pkg}/usr/bin:$PATH"
export LD_LIBRARY_PATH="/usr/lib:$LD_LIBRARY_PATH"
# export LT_SYS_LIBRARY_PATH=/usr/lib
export TEXARCH=$(uname -m | sed -e 's/i.86/i386/' -e 's/$/-linux/')
# # # set TEXMFROOT and TEXMFLOCAL
sed -i \
  -e 's|^TEXMFROOT.*|TEXMFROOT = $SELFAUTODIR/share|' \
  -e 's|^TEXMFLOCAL.*|TEXMFLOCAL = $TEXMFROOT/texmf-local|' \
  texk/kpathsea/texmf.cnf
# patch -Np1 -i $PATCHSOURCE/texlive-20190410-source-upstream_fixes-1.patch
# sed -i 's|-lXp ||' texk/xdvik/configure
export USE_ARCH="i686"
export LDFLAGS="-L/usr/lib"
export CFLAGS="-O2 -march=i686 -mtune=i686"
export CXXFLAGS="-O2 -march=i686 -mtune=i686 -fpermissive"
mkdir texlive-build &&
cd texlive-build    &&
../configure                                                    \
    --prefix=/usr                                  \
    --bindir=/usr/bin                    \
    --datarootdir=/usr/share                             \
    --includedir=/usr/include                      \
    --infodir=/usr/share/info             \
    --libdir=/usr/lib                              \
    --mandir=/usr/share/man               \
    --with-x \
    --disable-native-texlive-build                              \
    --disable-static --enable-shared                            \
    --disable-dvisvgm                                           \
    --with-system-cairo                                         \
    --with-system-fontconfig                                    \
    --with-system-freetype2                                     \
    --with-system-gmp                                           \
    --with-system-graphite2                                     \
    --with-system-harfbuzz                                      \
    --with-system-icu                                           \
    --with-system-libgs                                         \
    --with-system-libpaper                                      \
    --with-system-libpng                                        \
    --with-system-mpfr                                          \
    --with-system-pixman                                        \
    --with-system-zlib                                          \
    --with-system-teckit \
    --with-system-potrace \
    --with-banner-add=" - Phoniex" &&
make -j4
echo "###########################**** COMPLITE!!! ****##################################"
#

make DESTDIR=${pkg} install-strip
cd ../

rm -f ${pkg}/usr/lib/*.la
rm -rf ${pkg}/usr/share/*
tar xvf ${SOURCE}/texlive-base-2020.200608.tar.xz -C ${pkg}/usr/share || exit 1
tar xvf ${SOURCE}/texlive-20200406-texmf.tar.xz -C ${pkg}/usr/share --strip-components=1
chown -R root:root ${pkg}
chmod -R u+w,go-w,a+rX-st ${pkg}
mv -vf ${pkg}/usr/share/texmf-dist/linked_scripts/* ${pkg}/usr/bin
rmdir ${pkg}/usr/share/texmf-dist/linked_scripts

# set some paths
sed -i \
  -e 's|^TEXMFROOT.*|TEXMFROOT = $SELFAUTODIR/share|' \
  -e 's|^TEXMFLOCAL.*|TEXMFLOCAL = $TEXMFROOT/texmf-local|' \
  -e 's|^OSFONTDIR.*|OSFONTDIR = ~/.fonts:/usr/share/fonts|' \
  ${pkg}/usr/share/texmf-dist/web2c/texmf.cnf
# disable obsolete aleph/lamed/cslatex/pdfcslatex
sed -i \
  -e 's|^aleph|#! aleph|' \
  -e 's|^lamed|#! lamed|' \
  -e 's|^cslatex|#! cslatex|' \
  -e 's|^pdfcslatex|#! pdfcslatex|' \
  ${pkg}/usr/share/texmf-dist/web2c/fmtutil.cnf

sed -i \
  -e 's|selfautoparent:|/usr/share/|g' \
  -e 's|\(TEXMFLOCAL[ ]*=[ ]*\)[^,]*|\1"/usr/share/texmf-local"|' \
  -e '/selfautodir/d' \
  -e '/texmflocal/d' \
  ${pkg}/usr/share/texmf-dist/web2c/texmfcnf.lua

# mkdir -p ${pkg}/etc/fonts/{conf.avail,conf.d}
# cat > ${pkg}/etc/fonts/conf.avail/09-texlive-fonts.conf << EOF
# <?xml version='1.0'?>
# <!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
# <fontconfig>
#    <dir>/usr/share/texmf-dist/fonts</dir>
#    <dir>/usr/share/texmf-dist/fonts</dir>
# </fontconfig>
# EOF
# ln -svf ../conf.avail/09-texlive-fonts.conf ${pkg}/etc/fonts/conf.d/09-texlive-fonts.conf

mkdir -p ${pkg}/usr/share/doc/${packagedir}
mv ${pkg}/usr/share/texmf-dist/packages.base.gz ${pkg}/usr/share/doc/${packagedir}
ln -s ../../texmf-dist/doc ${pkg}/usr/share/doc/${packagedir}/doc

mkdir -p ${pkg}/usr/share/{texmf-config,texmf-var,texmf-local}
( cd ${pkg}/usr/share/texmf-dist/tex/latex/tabu
  cat $PATCHSOURCE/tabu.sty.diff | patch -p1 --verbose || exit 1
  ) || exit 1
mkdir -pv ${pkg}/usr/lib/perl5/site_perl/5.32.0
mv -v texk/tests/TeXLive ${pkg}/usr/lib/perl5/site_perl/5.32.0/

# Move manual pages to the correct place
mkdir ${pkg}/usr/share/man
mv ${pkg}/usr/share/texmf-dist/doc/man/man1 ${pkg}/usr/share/man
mv ${pkg}/usr/share/texmf-dist/doc/man/man5 ${pkg}/usr/share/man
rm -rf ${pkg}/usr/share/texmf-dist/doc/man
mv -v ${pkg}/usr/share/texmf-dist/doc/info ${pkg}/usr/share

if [ -d ${pkg}/usr/share/man ]; then
n|find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
n|find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
# # # # # ln -sv aleph ${pkg}/usr/bin/lamed
# # # # # ln -sv pdftex ${pkg}/usr/bin/amstex
# # # # # # skipped ConTeXtish symlink /usr/bin/cont-en -> pdftex (special case)
# # # # # # skipped ConTeXtish symlink /usr/bin/cont-en -> xetex (special case)
# # # # # ln -svf pdftex ${pkg}/usr/bin/cslatex
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdfcslatex
# # # # # ln -svf pdftex ${pkg}/usr/bin/csplain
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdfcsplain
# # # # # ln -svf pdftex ${pkg}/usr/bin/eplain
# # # # # ln -svf pdftex ${pkg}/usr/bin/jadetex
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdfjadetex
# # # # # ln -svf pdftex ${pkg}/usr/bin/latex
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdflatex
# # # # # ln -svf luatex ${pkg}/usr/bin/dvilualatex
# # # # # ln -svf luatex ${pkg}/usr/bin/lualatex
# # # # # ln -svf tex ${pkg}/usr/bin/lollipop
# # # # # ln -svf luatex ${pkg}/usr/bin/dviluatex
# # # # # # skipped metafont symlink /usr/bin/mf -> mf-nowin (special case)
# # # # # ln -svf pdftex ${pkg}/usr/bin/mex
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdfmex
# # # # # ln -svf pdftex ${pkg}/usr/bin/utf8mex
# # # # # ln -svf pdftex ${pkg}/usr/bin/mllatex
# # # # # ln -svf pdftex ${pkg}/usr/bin/mltex
# # # # # # skipped ConTeXtish symlink /usr/bin/mptopdf -> pdftex (special case)
# # # # # ln -svf pdftex ${pkg}/usr/bin/etex
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdfetex
# # # # # ln -svf eptex ${pkg}/usr/bin/platex
# # # # # ln -svf pdftex ${pkg}/usr/bin/texsis
# # # # # ln -svf euptex ${pkg}/usr/bin/uplatex
# # # # # ln -svf xetex ${pkg}/usr/bin/xelatex
# # # # # ln -svf pdftex ${pkg}/usr/bin/xmltex
# # # # # ln -svf pdftex ${pkg}/usr/bin/pdfxmltex
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree
cat > ${pkg}/doinst.sh << EOF
chroot . /usr/bin/mktexlsr 1>/dev/null 2>/dev/null
printf "y\n" | chroot . /usr/bin/updmap-sys --syncwithtrees 1>/dev/null 2>/dev/null
chroot . /usr/bin/updmap-sys 1>/dev/null 2>/dev/null
chroot . /usr/bin/fmtutil-sys --missing 1>/dev/null 2>/dev/null
chroot . /usr/bin/mtxrun --generate 1>/dev/null 2>/dev/null
EOF
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}.txz
installpkg ${destdir}/1-repo/${packagedir}.txz

# # cd $TEMPBUILD/${packagedir}/texlive-build

rm -rf ${pkg}
cd $home
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
###############################################################################

/sbin/ldconfig &&
mktexlsr &&
fmtutil-sys --all &&
mtxrun --generate
