#!/bin/bash
###############################################################################
set -e
###############################################################################
# The volume_key package provides a library for manipulating storage volume encryption keys and storing them separately from volumes to handle forgotten passphrases.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://github.com/felixonmars/volume_key/archive/volume_key-0.3.12.tar.gz
# Download MD5 sum: d1c76f24e08ddd8c1787687d0af5a814
# Download size: 196 KB
# Estimated disk space required: 11 MB
# Estimated build time: 0.2 SBU
# volume_key Dependencies
# Required
# cryptsetup-2.0.6, GLib-2.62.4, GPGME-1.13.1, and NSS-3.49.2
# Recommended
# SWIG-4.0.1
# Optional
# Python-2.7.17
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
autoreconf -fiv              &&
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
