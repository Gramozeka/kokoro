#!/bin/bash
###############################################################################
set -e
###############################################################################
# The Sysstat package contains utilities to monitor system performance and usage activity. Sysstat contains the sar utility, common to many commercial Unixes, and tools you can schedule via cron to collect and historize performance and activity data.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://sebastien.godard.pagesperso-orange.fr/sysstat-12.3.1.tar.xz
# Download MD5 sum: 66cb8d297bd6c14f14650dcd5fd4ac10
# Download size: 696 KB
# Estimated disk space required: 20 MB
# Estimated build time: 0.1 SBU
# Sysstat Dependencies
# There are no build-time requirements for this package; however, it is designed to be controlled by a cron daemon such as Fcron-3.2.1.
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
sa_lib_dir=/usr/lib/sa    \
sa_dir=/var/log/sa        \
conf_dir=/etc/sysconfig   \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--disable-file-attr \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
cd boot-script
make install-sysstat
