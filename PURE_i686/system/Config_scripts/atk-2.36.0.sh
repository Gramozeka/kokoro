#!/bin/bash
###############################################################################
set -e
# ATK provides the set of accessibility interfaces that are implemented by other toolkits and applications. Using the ATK interfaces, accessibility tools have full access to view and control running applications.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/atk/2.34/atk-2.34.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/atk/2.34/atk-2.34.1.tar.xz
# Download MD5 sum: f60bbaf8bdd08b93d98736b54b2fc8e9
# Download size: 288 KB
# Estimated disk space required: 7.8 MB
# Estimated build time: less than 0.1 SBU
# ATK Dependencies
# Required
# GLib-2.62.4
# Recommended (Required if building GNOME)
# gobject-introspection-1.62.0
# Optional
# GTK-Doc-1.32
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
