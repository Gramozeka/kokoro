#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).*"
packagedir=$(basename $line)

unpack ${SOURCE}/${package}
cd ${packagedir}
    mkdir build
    cd    build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
             cmake  \
             -DCMAKE_C_FLAGS=$CFLAGS \
             -DCMAKE_CXX_FLAGS=$CFLAGS \
             -DCMAKE_INSTALL_PREFIX=/usr \
             -DLIB_INSTALL_DIR=lib \
             -DCMAKE_BUILD_TYPE=Release         \
             -DLIB_SUFFIX="" \
             -DBUILD_TESTING=OFF \
             -Wno-dev ..
make -j4 || make || exit 1
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
