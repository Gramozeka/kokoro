#!/bin/bash
###############################################################################
set -e
###############################################################################
# Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://nodejs.org/dist/v12.14.1/node-v12.14.1.tar.xz
# Download MD5 sum: 1c78a75f5c95321f533ecccca695e814
# Download size: 22 MB
# Estimated disk space required: 587 MB (add 37 MB for tests)
# Estimated build time: 7.8 SBU (using parallelism=4; add 2.4 SBU for tests)
# [Note] Note
# This tarball was created using a BSD version of tar and extracting it with a linux™ version will produce harmless warnings about unknown extended header keywords.
# Node.js Dependencies
# Required
# Python-2.7.17 and Which-2.21
# Recommended
# c-ares-1.15.0, ICU-65.1, libuv-1.34.2, and nghttp2-1.40.0
# Optional
# http-parser, npm (an internal copy of npm will be installed if not present)'
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# # #   sed -i "s|\"lib\"|\"lib\"|" deps/npm/lib/npm.js
# # #   sed -i "s/'lib'/'lib'/" lib/module.js
# # #   sed -i "s|lib/|lib/|g" tools/install.py
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
            --shared-cares                 \
            --shared-libuv                 \
            --shared-nghttp2               \
            --shared-openssl               \
            --shared-zlib                  \
            --with-intl=system-icu
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
