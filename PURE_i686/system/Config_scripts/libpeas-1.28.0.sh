#!/bin/bash
###############################################################################
set -e
###############################################################################
# libpeas is a GObject based plugins engine, and is targeted at giving every application the chance to assume its own extensibility.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): http://ftp.gnome.org/pub/gnome/sources/libpeas/1.24/libpeas-1.24.1.tar.xz
# Download (FTP): ftp://ftp.gnome.org/pub/gnome/sources/libpeas/1.24/libpeas-1.24.1.tar.xz
# Download MD5 sum: bbecf334a7333d0a5d4d655ba38be9b4
# Download size: 184 KB
# Estimated disk space required: 9.8 MB (with tests)
# Estimated build time: less than 0.1 SBU (with tests)
# libpeas Dependencies
# Required
# gobject-introspection-1.62.0 and GTK+-3.24.13
# Recommended
# libxml2-2.9.10 and PyGObject-3.34.0
# Optional
# GTK-Doc-1.32, Glade, embed, LGI (for LUA bindings, built with LUA-5.1), with either luajit or LUA-5.1
###############################################################################
###############################################################################

cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
mkdir meson-build
cd meson-build
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
meson setup \
  --prefix=/usr \
  --libdir=lib \
  --libexecdir=/usr/libexec \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/share/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  ..
  ninja -j5
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
