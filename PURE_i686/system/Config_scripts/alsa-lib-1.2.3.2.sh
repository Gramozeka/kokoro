#!/bin/bash
###############################################################################
set -e
# The ALSA Library package contains the ALSA library used by programs (including ALSA Utilities) requiring access to the ALSA sound interface.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://www.alsa-project.org/files/pub/lib/alsa-lib-1.2.1.2.tar.bz2
# Download (FTP): ftp://ftp.alsa-project.org/pub/lib/alsa-lib-1.2.1.2.tar.bz2
# Download MD5 sum: 82ddd3698469beec147e4f4a67134ea0
# Download size: 981 KB
# Estimated disk space required: 35 MB (with tests)
# Estimated build time: 0.3 SBU (with tests)
# ALSA Library Dependencies
# Optional
# Doxygen-1.8.17 and Python-2.7.17
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install
install -v -d -m755 ${pkg}/usr/share/doc/${packagedir}/html/search &&
install -v -m644 doc/doxygen/html/*.* \
                ${pkg}/usr/share/doc/${packagedir}/html &&
install -v -m644 doc/doxygen/html/search/* \
                ${pkg}/usr/share/doc/${packagedir}/html/search
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
make doc
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
