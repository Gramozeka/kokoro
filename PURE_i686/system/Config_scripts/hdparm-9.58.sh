#!/bin/bash
###############################################################################
# The Hdparm package contains a utility that is useful for obtaining information about, and controlling ATA/IDE controllers and hard drives. It allows to increase performance and sometimes to increase stability.
# This package is known to build and work properly using an LFS-9.0 platform.
# [Warning] Warning
# As well as being useful, incorrect usage of Hdparm can destroy your information and in rare cases, drives. Use with caution and make sure you know what you are doing. If in doubt, it is recommended that you leave the default kernel parameters alone.
# Package Information
# Download (HTTP): https://downloads.sourceforge.net/hdparm/hdparm-9.58.tar.gz
# Download MD5 sum: 4652c49cf096a64683c05f54b4fa4679
# Download size: 136 KB
# Estimated disk space required: 1.0 MB
# Estimated build time: less than 0.1 SBU
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
