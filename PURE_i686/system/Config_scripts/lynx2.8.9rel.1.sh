#!/bin/bash
###############################################################################
set -e
###############################################################################
# Lynx is a text based web browser.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://invisible-mirror.net/archives/lynx/tarballs/lynx2.8.9rel.1.tar.bz2
# Download MD5 sum: 44316f1b8a857b59099927edc26bef79
# Download size: 2.5 MB
# Estimated disk space required: 31 MB
# Estimated build time: 0.3 SBU
# Lynx Dependencies
# Optional
# GnuTLS-3.6.11.1 (experimental, to replace openssl), Zip-3.0, UnZip-6.0, an MTA (that provides a sendmail command), and Sharutils-4.15.2 (for a uudecode program)
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install-full
chgrp -v -R root ${pkg}/usr/share/lynx_doc
sed -e '/#LOCALE/     a LOCALE_CHARSET:TRUE'     \
    -i ${pkg}/etc/lynx/lynx.cfg
    sed -e '/#DEFAULT_ED/ a DEFAULT_EDITOR:vi'       \
    -i ${pkg}/etc/lynx/lynx.cfg
    sed -e '/#PERSIST/    a PERSISTENT_COOKIES:TRUE' \
    -i ${pkg}/etc/lynx/lynx.cfg
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc/lynx \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
            --with-zlib            \
            --with-bzlib           \
            --with-ssl             \
            --with-screen=ncursesw \
            --enable-locale-charset
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
