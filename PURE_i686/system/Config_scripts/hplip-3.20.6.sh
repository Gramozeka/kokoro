#!/bin/bash
###############################################################################
set -e
###############################################################################
###############################################################################
###############################################################################
pack_local64 () {
pkg=${destdir}/$1
make install DESTDIR=${pkg} rulesdir=/lib/udev/rules.d
rm -rf $pkg/etc/sane.d
mkdir -p $pkg/var/lib/hp/
echo "$1 ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# Upgrades should not be done outside the package system:
zcat ${PATCHSOURCE}/hplip.no.upgrade.diff.gz | patch -p1 --verbose || exit 1

# Set LC_ALL=C to fix issues with CUPS output parsing in hp-setup:
zcat ${PATCHSOURCE}/setup.py.lc_all.c.diff.gz | patch -p1 --verbose || exit 1

# Fix a few .py files lacking #!/usr/bin/python3:
zcat ${PATCHSOURCE}/hplip.python3.shebang.diff.gz | patch -p1 --verbose || exit 1
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \+ -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \+

find -name '*.py' -print0 | xargs -0 \
    sed -i.env-python -e 's,^#!/usr/bin/env python,#!/usr/bin/python3,'
sed -i.env-python -e 's,^#!/usr/bin/env python,#!/usr/bin/python3,' \
    prnt/filters/hpps \
    fax/filters/pstotiff

# Forget it folks - this ImageProcessor closed-source blob is broken, and there
# is no way to fix it. We will use this patch from Debian to remove it from
# hplip, reverting to the way things worked in the 3.18.6 release. If HP ever
# sees fit to make ImageProcessor mandatory, we'll likely stick with the last
# hplip release that can be made to work without it, and any HP printers that
# require a newer version of hplip will not be supported.
zcat ${PATCHSOURCE}/0025-Remove-all-ImageProcessor-functionality-which-is-clo.patch.gz | patch -p1 -l --verbose || exit 1

# Add a cups-2.2.x header since some definitions were moved:
zcat ${PATCHSOURCE}/0021-Add-include-cups-ppd.h-in-various-places-as-CUPS-2.2.patch.gz | patch -p1 -l --verbose || exit 1

# autoreconf will fail if these files do not exist:
for file in NEWS README AUTHORS ChangeLog ; do
  if [ ! -r $file ]; then
    touch $file
  fi
done
autoreconf -vif || exit 1
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS="$CFLAGS -I/usr/include/python3.8" \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
  --with-cupsbackenddir=/usr/lib/cups/backend \
  --with-cupsfilterdir=/usr/lib/cups/filter \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --with-hpppddir=/usr/share/ppd/HP \
  --with-drvdir=/usr/share/cups/drv/HP \
  --enable-hpijs-install \
  --enable-scan-build \
  --enable-gui-build \
  --enable-fax-build \
  --enable-doc-build \
  --enable-foomatic-rip-hplip-install \
  --enable-pp-build \
  --disable-foomatic-ppd-install \
  --enable-foomatic-drv-install \
  --enable-network-build=no \
  --enable-policykit \
  --enable-cups-ppd-install \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack_local64 ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
