#!/bin/bash
###############################################################################
set -e
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
pkg=${destdir}/${packagedir}
cd ${packagedir}
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \+ -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \+

sh extract-and-patch.sh
cd tcp_wrappers_7.6 || exit 1
# add -DHAVE_STRERROR to linux Makefile to solve glibc-2.32 deprecated sys_errlist and sys_nerr (from fedora 33 mas rebuild spec)
sed -i -e 's/-DHAVE_WEAKSYMS/-DHAVE_WEAKSYMS -DHAVE_STRERROR/' Makefile
make REAL_DAEMON_DIR=/usr/sbin linux || exit 1
strip tcpd safe_finger tcpdchk tcpdmatch try-from
mkdir -p ${pkg}/usr/lib
cat libwrap.a > ${pkg}/usr/lib/libwrap.a
mkdir -p ${pkg}/usr/include
cat tcpd.h > ${pkg}/usr/include/tcpd.h
mkdir -p ${pkg}/usr/sbin
cat safe_finger > ${pkg}/usr/sbin/safe_finger
cat tcpd > ${pkg}/usr/sbin/tcpd
cat tcpdchk > ${pkg}/usr/sbin/tcpdchk
cat tcpdmatch > ${pkg}/usr/sbin/tcpdmatch
cat try-from > ${pkg}/usr/sbin/try-from
chmod 755 ${pkg}/usr/sbin/*
mkdir -p ${pkg}/usr/share/man/man{3,5,8}
cat hosts_access.3 | gzip -9c > ${pkg}/usr/share/man/man3/hosts_access.3.gz
cat hosts_access.5 | gzip -9c > ${pkg}/usr/share/man/man5/hosts_access.5.gz
cat hosts_options.5 | gzip -9c > ${pkg}/usr/share/man/man5/hosts_options.5.gz
cat tcpd.8 | gzip -9c > ${pkg}/usr/share/man/man8/tcpd.8.gz
cat tcpdchk.8 | gzip -9c > ${pkg}/usr/share/man/man8/tcpdchk.8.gz
cat tcpdmatch.8 | gzip -9c > ${pkg}/usr/share/man/man8/tcpdmatch.8.gz
echo "${packagedir} ----- $(date)" >> ${destdir}/loginstall
find ${pkg} -depth -print0  -type f -name "*.la" -exec rm -fv {} \;
find ${pkg} -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
if [ -d ${pkg}/usr/share/man ]; then
  find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/${packagedir}-$ARCH-$BUILD-tree

/sbin/makepkg -p -l y -c n ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/${packagedir}-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
echo "###########################**** COMPLITE!!!****##################################"
