#!/bin/bash
###############################################################################
set -e
# Speex is an audio compression format designed especially for speech. It is well-adapted to internet applications and provides useful features that are not present in most other CODECs.
# This package is known to build and work properly using an LFS-9.0 platform.
# Package Information
# Download (HTTP): https://downloads.xiph.org/releases/speex/speex-1.2.0.tar.gz
# Download MD5 sum: 8ab7bb2589110dfaf0ed7fa7757dc49c
# Download size: 884 KB
# Estimated disk space required: 4.5 MB
# Estimated build time: less than 0.1 SBU
# Additional Downloads
# Download (HTTP): https://downloads.xiph.org/releases/speex/speexdsp-1.2rc3.tar.gz
# Download MD5 sum: 70d9d31184f7eb761192fd1ef0b73333
# Download size: 884 KB
# Estimated disk space required: 5.1 MB
# Estimated build time: less than 0.1 SBU
# Speex Dependencies
# Required
# libogg-1.3.4
# Optional
# Valgrind-3.15.0
###############################################################################
###############################################################################
cd $TEMPBUILD
rm -rf *
package="$(basename $line).tar.*"
packagedir=$(basename $line)
unpack ${SOURCE}/${package}
cd ${packagedir}
# patch -Np1 -i ${PATCHSOURCE}/
CFLAGS=$CFLAGS \
CXXFLAGS=$CXXFLAGS \
./configure --prefix=/usr \
--sysconfdir=/etc \
--libdir=/usr/lib \
--disable-static \
--localstatedir=/var \
--build=${CLFS_TARGET} \
--docdir=/usr/share/doc/${packagedir}
make -j4
pack ${packagedir}
echo "###########################**** COMPLITE!!! ****##################################"
