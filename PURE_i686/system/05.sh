#!/bin/bash
###w3m-0.5.3
# # # assimp-5.0.1
stop
qt-everywhere-src-5.15.1
# qt-5.15.1
# highlight-3.58 создать гуи!
# # libsrtp-2.3.0
# # jsoncpp-1.9.2
qtwebengine-everywhere-src-5.15.1
highlight-3.58
qtwebkit-master
gpgme-1.14.0
extra-cmake-modules-5.75.0
xpdf-4.02
pinentry-1.1.0
polkit-qt-1-0.113.0
poppler-20.10.0
doxygen-1.8.20-Qt
libdbusmenu-qt_0.9.3+16.04.20160218
gstreamer-1.18.0
gst-plugins-base-1.18.0
gst-plugins-good-1.18.0
gst-plugins-bad-1.18.0
gst-plugins-ugly-1.18.0
gst-libav-1.18.0
gstreamer-vaapi-1.18.0
phonon-4.11.1
phonon-backend-gstreamer-4.10.0
botan-2.15.0
qca-2.3.1
logrotate-3.17.0
plasma-wayland-protocols-1.1.1
plasma-pam
# vlc-3.0.8
# phonon-backend-vlc-0.11.1
NetworkManager-1.26.2
MarkupSafe-1.1.1
lxml-4.5.2
Jinja2-2.11.2
fontforge-2.0.20170731
oxygen-fonts-5.4.3
# noto-fonts
openexr-2.5.2
media-player-info-24
hspell-1.4
hunspell-1.7.0
hunspell-dictionaries-ru
libdmtx-0.7.5
dmtx-wrappers-master
dmtx-utils-0.7.6
qrencode-4.1.1
sddm.git
catdoc-0.95
ebook-tools-0.2.2
taglib-1.11.1
grub-2.04
dracut
cryptopp820
conan.git
cryfs-0.10.2
zxing-cpp-1.1.0
signond-master
libaccounts-glib-master
libaccounts-qt-master
frei0r-plugins-1.7.0
mlt-6.22.1
faac-1_30
faad2-2_9_2
libquicktime-1.2.4
ffmpeg-4.3.1_doc
xine-lib-1.2.10
xine-ui-0.99.12
pulseaudio-qt-master
telepathy-qt-0.9.8
MPlayer-1.4
telepathy-logger-0.8.2
telepathy-logger-qt-17.09.0
telegram-qt.git
grantlee-5.2.0
bolt-0.9
id3lib-3.8.3
QtAV-master
accounts-qml-module
opencv-4.4.0
# LibRaw-0.19.5
RHVoice-1.2.2
Path-Tiny-0.114
Template2-3.009
appstream-0.12.11_Qt
LibVNCServer-0.9.13
Moo-2.004000
Module-Runtime-0.016
Sub-Quote-2.006006
djvulibre-3.5.27
compton-master
lxqt-git





