#!/bin/bash
set -e
unpack () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar  xvjf $1  2> /dev/null   ;;
      *.tar.gz)    tar xvzf $1   2> /dev/null  ;;
      *.tar.xz)    tar xvJf $1   2> /dev/null  ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1   2> /dev/null   ;;
      *.tbz2)      tar xvjf $1  2> /dev/null   ;;
      *.tgz)       tar xvzf $1  2> /dev/null   ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *.lz)        tar xvf $1   2> /dev/null   ;;
      *)           echo  $1 ": Unknown method of file compression" ;;
    esac
  else
    echo $1 "no found"
  fi
}

BUILD=$(sed -e "s/\.sh*$//" <<< $1)
read_list=$1
arg1=$2
arg2=$3
home=$(pwd)
if [ -n "$1" ]
then
scriptsdir="Config_scripts"
else
echo "Не указан входящий файл!"
exit 1
fi
TEMPBUILD=/$home/Building
SOURCE=/phoniex/sources/kf5/$read_list
PATCHSOURCE=/phoniex/Patches-kde
destdir=/phoniex/Base-Pack
######################################################################################
ARCH="i686"
export PKG_CONFIG_PATH="/usr/lib/pkgconfig"
export CPPFLAGS="-I/usr/include"
export CFLAGS="-O2 -march=i686 -mtune=i686"
export CXXFLAGS="-O2 -march=i686 -mtune=i686"
export LLVM_INSTALL_DIR=/usr
export CLANG_INSTALL_DIR=/usr
export LC_COLLATE=C
export LANG=C
export LC_ALL=POSIX
export CC=gcc
export CXX=g++
######################################################################################
pack () {
pkg=${destdir}/$1
make DESTDIR=${pkg} install && echo "$1 ----- $(date)" >> ${destdir}/loginstall
case "$read_list" in
khelpcenter-[0-9]* )
mv -v ${pkg}share/kde4/services/khelpcenter.desktop ${pkg}/usr/share/applications/ &&
rm -rv ${pkg}/share/kde4
;;
esac

if [ -d ${pkg}/usr/share/man ]; then
echo n|find ${pkg}/usr/share/man -type f -exec gzip -9 {} \;
fi
if [ -d ${pkg}/usr/share/info ]; then
echo n|find ${pkg}/usr/share/info -type f -exec gzip -9 {} \;
fi
find ${pkg} -depth -print0  -type f -name "*.la"  -exec rm -fv {} \;
find ${pkg} -depth -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
cd ${pkg} &&
mkdir -p usr/tree-info
du --all | cut -f 2 | cut -c 2- > usr/tree-info/$1-$ARCH-$BUILD-tree
/sbin/makepkg -p -l y -c n ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$1-$ARCH-$BUILD.txz
rm -rf ${pkg}
cd $home
}
######################################################################################

processing ()
{
cd $TEMPBUILD
rm -rf *
unpack $SOURCE/$sourcefile
cd $packagedir
	case "$packagedir" in
#         baloo-[0-9]* )
# patch -Np1 -i $PATCHSOURCE/baloo_frameworks_indexerconfig.diff
#         ;;
        kuser-[0-9]* )
patch -Np1 -i $PATCHSOURCE/kuser.patch
        ;;
        kdb-[0-9]* )
patch -Np1 -i $PATCHSOURCE/kdb-3.2.0-postgresql-12.patch || exit 1
		;;
		kate-[0-9]* )
patch -Np1 -i $PATCHSOURCE/kate-18.12.3-allow-root.patch
patch -Np1 -i $PATCHSOURCE/kwrite-19.08.2-allow-root.patch
		;;
		dolphin-[0-9]* )
patch -Np1 -i $PATCHSOURCE/dolphin-18.12.3-allow-root.patch
		;;
        ktorrent-[0-9]* )
# patch -Np1 -i $PATCHSOURCE/libktorrent.diff || exit 1
spec_conf="-DENABLE_MEDIAPLAYER_PLUGIN=OFF"
		;;
# 		konsole-[0-9]* )
# patch -Np1 -i $PATCHSOURCE/konsole-19.08.3-scrollbar-1.patch || exit 1
# 		;;
		telepathy-logger-qt-*)
patch -Np1 -i $PATCHSOURCE/TelepathyLoggerQt-log-manager.diff || exit 1
		;;
		freecell-solver-[0-9]* )
 pip3 install random2
 pip3 install pysol_cards
 spec_conf="-DFCS_WITH_TEST_SUITE=OFF"
		 ;;
        kitemviews*) sed -i '/<QList>/a #include <QPersistentModelIndex>' \
          src/kwidgetitemdelegatepool_p.h
          ;;
        kplotting*) sed -i '/<QHash>/a #include <QHelpEvent>' \
          src/kplotwidget.cpp
          ;;
        knotifica*) sed -i '/<QUrl>/a #include <QVariant>' \
          src/knotification.h
          ;;
        kcompleti*) sed -i '/<QClipboard>/a #include <QKeyEvent>' \
          src/klineedit.cpp
          ;;
        kwayland-5.7*) sed -i '/<wayland-xdg-output-server-proto/a #include <QHash>' \
          src/server/xdgoutput_interface.cpp
          ;;
        purpose*) sed -i 's/15,/16,/' \
          src/externalprocess/purposeprocess_main.cpp
          ;;
#        digikam-*)
#  spec_conf="-DDIGIKAMSC_CHECKOUT_PO=ON -DDIGIKAMSC_CHECKOUT_DOC=ON -DDIGIKAMSC_COMPILE_DOC=ON -DENABLE_MEDIAPLAYER:BOOL=ON "
#         ;;
                 plasma-workspace)
           sed -i '/set.HAVE_X11/a set(X11_FOUND 1)' CMakeLists.txt
         ;;
      
         khotkeys)
           sed -i '/X11Extras/a set(X11_FOUND 1)' CMakeLists.txt
         ;;
      
         plasma-desktop)
           sed -i '/X11.h)/i set(X11_FOUND 1)' CMakeLists.txt
         ;;
	esac
       
       if [[ ${packagedir} == breeze-grub* ]]
then
pkg=${destdir}/${packagedir}
         mkdir -pv ${pkg}/usr/share/grub/themes
         cd breeze
mk="grub-mkfont -v"
$mk  -o unifont-regular-14.pf2 /usr/share/fonts/terminus/ter-x14n.pcf
$mk  -o unifont-regular-16.pf2 /usr/share/fonts/terminus/ter-x16n.pcf
$mk  -o unifont-bold-16.pf2 -b /usr/share/fonts/terminus/ter-x16b.pcf
$mk  -o unifont-regular-32.pf2 /usr/share/fonts/terminus/ter-x32n.pcf
         cd ../
         cp -rv breeze ${pkg}/usr/share/grub/themes/
         cd ${pkg}
         /sbin/makepkg -p -l y -c n ${destdir}/1-repo/$packagedir-$ARCH-$BUILD.txz
installpkg ${destdir}/1-repo/$packagedir-$ARCH-$BUILD.txz
unset pkg
else
if [[ ${packagedir} == systemsettings-* ]]
then
mkdir sys_build
cd sys_build
else
      mkdir build
      cd    build
fi
      cmake -DCMAKE_INSTALL_PREFIX=$KF5_PREFIX \
            -DCMAKE_PREFIX_PATH=$QT5DIR        \
            -DCMAKE_BUILD_TYPE=Release         \
            -DBUILD_TESTING=OFF                \
	-DBUILD_QCH=ON                                            \
	$spec_conf                                                            \
	-Wno-dev ..
make -j4 || make || exit 1
pack $packagedir
fi
cd $home
ldconfig
spec_conf=""

}

######################################################################################

while read -r line; do
unset -f pack_local
source /etc/profile &&
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
if [ -n "$2" ]
then
if [ -n "$3" ]
then
case $arg2 in
"one" )
if [ $line != $arg1 ]
then
continue
else

    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
packagedir=${line}
sourcefile=${packagedir}.*
processing
break

fi
;;
"more" )
if [ $line != $arg1 ]; then continue; else
arg2="0"
fi
;;
esac
else

if [ $line != $arg1 ]
then
continue
else
    file=$(echo $line | cut -d" " -f2)
echo "второй аргумент отсутствует, будет собран только $file"
packagedir=${line}
sourcefile=${packagedir}.*
processing
break
fi
fi
fi
    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
packagedir=${line}
sourcefile=${packagedir}.*
processing
    unset -f pack_local
done < ${read_list}.sh
exit
