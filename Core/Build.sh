#!/bin/bash
set -e
# set +h
unpack () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xvjf $1    ;;
      *.tar.gz)    tar xvzf $1    ;;
      *.tar.xz)    tar xvJf $1    ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1     ;;
      *.tbz2)      tar xvjf $1    ;;
      *.tgz)       tar xvzf $1    ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *)           echo "\`$1': Unknown method of file compression" ;;
    esac
  else
    echo "\`$1' no found"
  fi
}
# arch_build=i686
arch_build=x86_64
arg1=$2
arg2=$3
home=$(pwd)
if [ -n "$1" ]
then
case $1 in
"01" )
scriptsdir="$arch_build/cross-tools"
;;
"02" )
scriptsdir="$arch_build/Temporary-System"
;;
"03" )
scriptsdir="$arch_build/Basic-System"
;;
"04" )
scriptsdir="$arch_build/Basic-System"
;;
esac
else 
echo "Не указан входящий файл!"
exit 1
fi
SOURCE=${home}/sources
# PATCHSOURCE=${home}/Patches
destdir=${home}/Base-Pack
TEMPBUILD=${home}/Building
if ! [ -d $TEMPBUILD ]
then
mkdir -pv $destdir/1-repo
mkdir -pv $TEMPBUILD
fi

######################################################################################
BUILD=${scriptsdir}
######################################################################################
while read -r line; do
#source /etc/profile &&
    if $(echo $line | grep -E -q '^ *$|^#' ); then continue; fi
if [ -n "$2" ]
then
if [ -n "$3" ]
then
case $arg2 in
"one" )
if [ $line != $arg1 ]
then
continue
else

    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
    . ${home}/${scriptsdir}/${line}
    wait

break

fi
;;
"more" )
if [ $line != $arg1 ]; then continue; else
arg2="0"
fi
;;
esac
else

if [ $line != $arg1 ]
then
continue
else
    file=$(echo $line | cut -d" " -f2)
echo "второй аргумент отсутствует, будет собран только $file"
    . ${home}/${scriptsdir}/${line}
    wait

break
fi
fi
fi
    file=$(echo $line | cut -d" " -f2)
echo "$file будет собран"
    . ${home}/${scriptsdir}/${line}
    unset -f pack_local
    unset -f pack_local32
    unset -f pack_local64

done < $1.sh
exit
