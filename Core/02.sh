
gmp-6.2.0
mpfr-4.1.0
mpc-1.2.0
isl-0.22.1
zlib-1.2.11
binutils-2.35.1
gcc-10.2.0
ncurses-6.2
bash-5.0
bzip2-1.0.8
check-0.15.2
coreutils-8.32
diffutils-3.7
file-5.39
findutils-4.7.0
gawk-5.1.0
grep-3.4
gzip-1.10
m4-1.4.18
make-4.3
patch-2.7.6
sed-4.8
tar-1.32
xz-5.2.5
stop_chroot
gettext-0.21
bison-3.7.3
perl-5.32.0
Python-3.9.0
multiarch_wrapper
texinfo-6.7
util-linux-2.36
vim-8.2.1845



